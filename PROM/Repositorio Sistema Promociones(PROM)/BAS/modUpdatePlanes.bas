Attribute VB_Name = "modUpdatePlanes"
Option Explicit

Function ModPlan(ByVal codi_plan As String, _
                 ByVal desc_plan As String, _
                 ByVal fecha_fin As String, _
                 ByVal codi_facilidad As String, _
                 ByVal flag_control As String, _
                 ByVal codi_renta As String, _
                 ByVal central As String, _
                 ByVal prop_cargaini As String, _
                 ByVal valor_plan As Long, _
                 ByVal tipo_saldo As String, _
                 ByVal valor_cargaini As Long, _
                 ByVal uso As String, _
                 ByVal flag_acumula As String, _
                 ByVal flag_portal As String, _
                 ByVal rut_empresa As String, _
                 ByVal codi_rentasms As String, _
                 ByVal codi_rentadatos As String, _
                 ByVal rut_tercero As String, _
                 ByVal Codi_Agrupa As String, _
                 ByVal paq_portal As String, _
                 ByVal cant_lineas As Long, _
                 ByVal flag_4g As String, _
                 ByVal flag_cargoboleta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrModPlan As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanModifica(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;"

    Set vrModPlan = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_fin)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, codi_facilidad)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, flag_control)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_renta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, central)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, prop_cargaini)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 13, valor_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, tipo_saldo)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 13, valor_cargaini)
        ' incorpora campo uso. JPABLOS. Proyecto OMV. 17-jun-2011.
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 15, uso)
        ' incorpora campo uso. IALCAINO. Proyecto OMV. 29-ABR-2013.
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, flag_acumula)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, flag_portal)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, rut_empresa)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_rentasms)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_rentadatos)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, rut_tercero)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 5, Codi_Agrupa)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, paq_portal)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 2, cant_lineas)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, flag_4g)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, flag_cargoboleta)
    End With
    On Error GoTo Error
    vrModPlan.Open adoCmd

    Set vrModPlan = Nothing

    ModPlan = True
    Exit Function

Error:
    Set vrModPlan = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanModificar(" + codi_plan + "," + desc_plan + "," + fecha_fin + "," + codi_facilidad + "," + flag_control + "," + codi_renta + "," + central + "," + prop_cargaini + "," + Trim(Str(valor_plan)) + "," + tipo_saldo + "," + Trim(Str(valor_cargaini)) + "," + Codi_Agrupa + "," + paq_portal + "," + Trim(Str(cant_lineas)) + "); end;"
    ModPlan = fn_error(Err.Description, sLlamadaProc)
    ModPlan = False
End Function

Function ModPlanProd(ByVal codi_plan As String, _
                     ByVal codi_producto As String, _
                     ByVal codi_obligatorio As String, _
                     ByVal codi_exclusion As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrModPlanProd As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanProdActualiza(?,?,?,?); end;"

    Set vrModPlanProd = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, codi_obligatorio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_exclusion)
    End With
    On Error GoTo Error
    vrModPlanProd.Open adoCmd

    Set vrModPlanProd = Nothing

    ModPlanProd = True
    Exit Function

Error:
    Set vrModPlanProd = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanProdActualizar(" + codi_plan + "," + codi_producto + "," + codi_obligatorio + "," + codi_exclusion + "); end;"
    ModPlanProd = fn_error(Err.Description, sLlamadaProc)
    ModPlanProd = False
End Function

Function ModPlanFaci(ByVal codi_plan As String, _
                     ByVal codi_facilidad As String, _
                     ByVal cant_facilidad As Long, _
                     ByVal codi_exclusion As String, _
                     ByVal prog_central As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrModPlanFaci As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanFaciModifica(?,?,?,?,?); end;"

    Set vrModPlanFaci = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, codi_facilidad)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, cant_facilidad)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_exclusion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, prog_central)
    End With
    On Error GoTo Error
    vrModPlanFaci.Open adoCmd

    Set vrModPlanFaci = Nothing

    ModPlanFaci = True
    Exit Function

Error:
    Set vrModPlanFaci = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanFaciModificar(" + codi_plan + "," + codi_facilidad + "," + Trim(Str(cant_facilidad)) + "," + codi_exclusion + "," + prog_central + "); end;"
    ModPlanFaci = fn_error(Err.Description, sLlamadaProc)
    ModPlanFaci = False
End Function



