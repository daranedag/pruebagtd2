Attribute VB_Name = "modUpdateProductos"
Option Explicit

Function ModProducto(ByVal codi_producto As String, _
                     ByVal desc_producto As String, _
                     ByVal fecha_fin As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrModProducto As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProductoModifica(?,?,?); end;"

    Set vrModProducto = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_fin)
    End With
    On Error GoTo Error
    vrModProducto.Open adoCmd

    Set vrModProducto = Nothing

    ModProducto = True
    Exit Function

Error:
    Set vrModProducto = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProductoModifica(" + codi_producto + "," + desc_producto + "," + fecha_fin + "); end;"
    ModProducto = fn_error(Err.Description, sLlamadaProc)
    ModProducto = False
End Function

Function ModProdEqui(ByVal codi_producto As String, _
                     ByVal codi_equipo As String, _
                     ByVal cant_equipo As String, _
                     ByVal codi_exclusion As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrModProdEqui As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdEquipoModifica(?,?,?,?); end;"

    Set vrModProdEqui = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_equipo)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_equipo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_exclusion)
    End With
    On Error GoTo Error
    vrModProdEqui.Open adoCmd

    Set vrModProdEqui = Nothing

    ModProdEqui = True
    Exit Function

Error:
    Set vrModProdEqui = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdEquipoModifica(" + codi_producto + "," + codi_equipo + "," + cant_equipo + "," + codi_exclusion + "); end;"
    ModProdEqui = fn_error(Err.Description, sLlamadaProc)
    ModProdEqui = False
End Function




