Attribute VB_Name = "modFunciones"
Option Explicit

Public Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" _
    (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, _
    ByVal lpDefault As String, _
    ByVal lpReturnedString As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) _
    As Long

Public Declare Function GetPrivateProfileInt Lib "kernel32" Alias _
    "GetPrivateProfileIntA" _
    (ByVal lpApplicationName As String, _
    ByVal lpKeyName As String, _
    ByVal nDefault As Integer, _
    ByVal lpFileName As String) As Long
    
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias _
      "WritePrivateProfileStringA" _
      (ByVal lpApplicationName As String, _
      ByVal lpKeyName As String, _
      ByVal lpString As String, _
      ByVal lplFileName As String) As Integer

Function fnDBSettingsGet(Sistname$, PathIni$, DBSrvType$, DBCliType$, DBName$, ConnStr$, Uid$, Pwd$, msg$) As Integer
Dim iSt As Integer, DBAddress$

    iSt = fnDBSettingsFullGet(Sistname$, PathIni$, DBSrvType$, DBCliType$, DBName$, DBAddress$, ConnStr$, Uid$, Pwd$, msg$)
    fnDBSettingsGet = iSt

End Function

Function fnDBSettingsFullGet(Sistname$, PathIni$, DBSrvType$, DBCliType$, DBName$, DBAddress$, ConnStr$, Uid$, Pwd$, msg$) As Integer
Dim iSt As Integer, iIdx As Integer
Dim DefaultStr As String, slash$
Dim FileIni As String

    'Inicializacion
    PathIni$ = ""
    DBSrvType$ = ""
    DBCliType$ = ""
    DBName$ = ""
    DBAddress$ = ""
    ConnStr$ = ""
    Uid$ = ""
    Pwd$ = ""

    fnDBSettingsFullGet = False

    'archivo win.ini, seccion cnt_orden, entry pathini
    PathIni$ = IniStringGet("win.ini", "cnt_orden", "PathIni", msg$)
    
    If PathIni$ = "" Then Exit Function

    'podria hacer un dir para ver si existe el path,
    'y luego ver si existe el archivo PathIni\SistName.ini

    'leo seccion orasrv, entries(dbsrvType, dbclitype, dbname, uid, pwd)
    FileIni$ = fnFileCast$(PathIni$, Sistname$, "ini")
    
    DBSrvType$ = IniStringGet(FileIni$, "DBSettings", "DBSrvType", msg$)
    If DBSrvType$ = "" Then Exit Function
    DBCliType$ = IniStringGet(FileIni$, "DBSettings", "DBCliType", msg$)
    If DBCliType$ = "" Then Exit Function
    DBName$ = IniStringGet(FileIni$, "DBSettings", "DBName", msg$)
    If DBName$ <> "" Then
       If UCase$(Trim$(DBCliType$)) = "ODBC" Then
            If UCase$(Trim$(DBSrvType$)) = "ORACLE" Then
                iSt = WritePrivateProfileString("ODBC Data Sources", DBName$, "Oracle", "odbc.ini")
                If iSt = 0 Then msg$ = "No pude escribir DBName en ODBC Data Sources en odbc.ini": Exit Function
                iSt = WritePrivateProfileString(DBName$, "Driver", "c:\windows\system\sqora.dll", "odbc.ini")
                If iSt = 0 Then msg$ = "No pude escribir Driver en odbc.ini": Exit Function
                iSt = WritePrivateProfileString(DBName$, "Description", "Oracle on Server", "odbc.ini")
                If iSt = 0 Then msg$ = "No pude escribir descripcion en odbc.ini": Exit Function
                 
            End If
       End If
    Else
       Exit Function
    End If
    DBAddress$ = IniStringGet(FileIni$, "DBSettings", "DBAddress", msg$)
    DBName = DBAddress
    If DBAddress$ <> "" Then
       If UCase$(Trim$(DBCliType$)) = "ODBC" Then
            'verifico eso si que sea de la forma t:...:...
            'Escribo el odbc.ini del PC
            iSt = WritePrivateProfileString(DBName$, "Server", DBAddress$, "odbc.ini")
            If iSt = 0 Then msg$ = "No pude escribir Address del Server en odbc.ini": Exit Function
       End If
    End If
    Uid$ = IniStringGet(FileIni$, "DBSettings", "UID", msg$)
    If Uid$ = "" Then Exit Function
    Pwd$ = IniStringGet(FileIni$, "DBSettings", "PID", msg$)
    If Pwd$ = "" Then Exit Function

    'Chequeo existencia de DBName (si DBCliType es ODBC)


    'Decodifico la PWD
    Pwd$ = DeCrypt(Pwd$)

    ConnStr$ = "UID=" & Uid$ & "; PWD=" & Pwd$
      
    fnDBSettingsFullGet = True

End Function

Function DeCrypt(EncryptedPwd$) As String
'Len, Mid$
Dim PwdLen As Integer, i As Integer
Dim PwdRes As String

    PwdRes = ""
    PwdLen = Len(EncryptedPwd$)
    For i = 1 To PwdLen
       PwdRes = PwdRes & Chr$(Asc(Mid$(EncryptedPwd$, i, 1)) + 10)
    Next i

    DeCrypt = PwdRes$
End Function

Function fnFileCast(Path$, File$, Ext$) As String
Dim slash$
'Moldea del archivo de acuerdo a Path\File.Ext

    slash = "\"
    If Right$(Path$, 1) = "\" Then slash$ = ""

    If InStr(File$, ".") Then
      Ext$ = ""
    Else
      Ext$ = "." & Ext$
    End If
    
    fnFileCast$ = Trim$(Path$) & slash$ & Trim$(File$) & Ext$

End Function

Function IniStringGet(File$, sect$, entry$, msg$) As String
Dim iSt As Integer, iIdx  As Integer
Dim Value As String

    Value$ = String$(255, 0)
    iSt = GetPrivateProfileString(sect$, entry$, "", Value$, Len(Value$), File$)
    If iSt = 0 Then
        IniStringGet$ = ""
        msg$ = "entry " & entry$ & " from section " & sect$ & " in " & File$ & " doesn't exists"
        'Msg$ = "No existe entry " & entry$ & " en seccion " & sect$ & " de " & File$
        Exit Function
    End If
    'como llame lleno de nulls, debo limpiarlos
    IniStringGet$ = fnNullClean(Value$)

End Function

Function fnNullClean(Value$) As String
Dim iIdx As Integer
    
    iIdx = InStr(Value$, String$(1, 0))
    Value$ = Left$(Value$, (iIdx - 1))
    fnNullClean$ = Value$

End Function


Function gfnFN(ByVal plValor)
    gfnFN = Format(plValor, "00")
End Function

Public Function FnVerificaRut(rut As String, DigitoVerificador As String) As Boolean
    Dim StrRutInverso, StrDigitoVerificador As String
    Dim mult, suma, Valor, i As Integer
    
    FnVerificaRut = False
    rut = Trim(Str(Val(rut)))

    StrRutInverso = StrReverse(rut)
    mult = 2
    suma = 0
    For i = 1 To Len(StrRutInverso)
        If mult > 7 Then
            mult = 2
        End If
        suma = mult * (Mid(StrRutInverso, i, 1)) + suma
        mult = mult + 1
    Next
    Valor = 11 - (suma Mod 11)
    If Valor = 11 Then
        StrDigitoVerificador = "0"
    Else
        If Valor = 10 Then
            StrDigitoVerificador = "K"
        Else
            StrDigitoVerificador = Valor
        End If
    End If
    If StrDigitoVerificador = UCase(DigitoVerificador) Then
        FnVerificaRut = True
    End If
End Function

Function gfnDevuelveValorCampo(ByVal pTipoFinal As String, _
                               ByVal pTipoInicial As String, _
                               ByVal pVariable As Variant)

    Select Case pTipoFinal
        Case "VARIANT" ' REVISANDO SI EL TIPO FINAL ES VARIANT
            If IsNull(pVariable) Then ' SI ES NULO RETORNO ""
                gfnDevuelveValorCampo = ""
                Exit Function
            Else ' SI NO ES NULO RETORNO LA VARIABLE
                gfnDevuelveValorCampo = pVariable
                Exit Function
            End If
        Case "INTEGER", "LONG", "DOUBLE", "SINGLE", "CURRENCY"
        ' VERIFICANDO SI EL TIPO FINAL ES DEL TIPO NUMERO
            On Error GoTo ErrorNmro
            If IsNull(pVariable) Then ' SI ES NULO RETORNO 0
                gfnDevuelveValorCampo = 0
                Exit Function
            Else ' SI NO ES NULO RETORNO EL VALOR NUMERICO DE LA VARIABLE
                gfnDevuelveValorCampo = Val(pVariable)
                Exit Function
            End If
        Case "STRING" ' VERIFICANDO SI EL TIPO FINAL DE STRING
            On Error GoTo ErrorStr
            If IsNull(pVariable) Then ' SI ES NULO RETORNO ""
                gfnDevuelveValorCampo = ""
                Exit Function
            Else ' SI NO ES NULO RETORNO EL TRIM DE LA VARIABLE
                gfnDevuelveValorCampo = Trim(pVariable)
                Exit Function
            End If
        Case "DATE" ' VERIFICANDO SI EL TIPO FINAL DE DATE
            gfnDevuelveValorCampo = pVariable
        Case Else
            gfnDevuelveValorCampo = ""
    End Select
    Exit Function
    
ErrorStr:
    If IsNull(pVariable) Then
        gfnDevuelveValorCampo = ""
    Else
        gfnDevuelveValorCampo = pVariable
    End If
    Exit Function
ErrorNmro:
    If IsNull(pVariable) Then
    gfnDevuelveValorCampo = 0
    Else
    gfnDevuelveValorCampo = pVariable
    End If
    Exit Function
End Function

Public Sub gsuCargaGrupos()
    Dim sCodiGrupo As String, sDescGrupo As String
    Dim iCant As Long, i As Long
    
    iCant = GetTitParam()
    
    If iCant > 0 Then
        
        ReDim arrGrupos(iCant - 1) As String
        ReDim arrCodiGrupos(iCant - 1) As String
        i = 0
        Do While GetTitParamRead(sCodiGrupo, sDescGrupo)
            arrGrupos(i) = sCodiGrupo & "  -  " & sDescGrupo
            arrCodiGrupos(i) = sCodiGrupo
            i = i + 1
        Loop
    End If
End Sub

Public Sub gsuCargaLocalidades()
    Dim sCodiLocalidad As String, sDescLocalidad As String
    Dim iCant As Long, i As Long

    iCant = GetLocalidades()

    If iCant > 0 Then
        ReDim arrLocalidades(iCant - 1) As String
        ReDim arrCodiLocalidades(iCant - 1) As String
        i = 0
        Do While GetLocalidadesRead(sCodiLocalidad, sDescLocalidad)
            arrLocalidades(i) = sCodiLocalidad & "  -  " & sDescLocalidad
            arrCodiLocalidades(i) = sCodiLocalidad
            i = i + 1
        Loop
    End If
End Sub

Public Sub gsuCargaEquipos()
    Dim sDescEquipo As String, sCodiEquipo As String
    Dim iCant As Long, i As Long
    
    iCant = GetEquipos()
    
    If iCant > 0 Then
        ReDim arrEquipos(iCant - 1) As String
        ReDim arrCodiEquipos(iCant - 1) As String
        i = 0
        Do While GetEquiposRead(sDescEquipo, sCodiEquipo)
            arrEquipos(i) = sDescEquipo
            arrCodiEquipos(i) = sCodiEquipo
            i = i + 1
        Loop
    End If
End Sub

Public Sub gsuCargaFacilidades()
    Dim iCant As Long, i As Long
    Dim sCodi As String, sDesc As String
    
    iCant = GetFacilidad()
    
    If iCant > 0 Then
        ReDim arrFacilidades(iCant - 1) As String
        ReDim arrCodiFacilidades(iCant - 1) As String
        
        i = 0
        Do While GetFacilidadRead(sCodi, sDesc)
            arrFacilidades(i) = sCodi & " - " & sDesc
            arrCodiFacilidades(i) = sCodi
            i = i + 1
        Loop
    End If
    
End Sub

Public Sub gsuCargaRentas()
    Dim iCant As Long, i As Long
    Dim sCodi As String, sDesc As String, lCodi1 As Long, sUnid As String
    
    iCant = GetRentas()
    
    If iCant > 0 Then
        ReDim arrRentas(iCant - 1) As String
        ReDim arrCodiRentas(iCant - 1) As String
        ReDim arrCodiConcepto(iCant - 1) As String
        ReDim arrDescConcepto(iCant - 1) As String
        ReDim arrUnidMonetar(iCant - 1) As String
        
        i = 0
        Do While GetRentasRead(sCodi, lCodi1, sDesc, sUnid)
            arrRentas(i) = Trim(Str(lCodi1)) & " - " & sDesc
            arrCodiRentas(i) = sCodi
            arrCodiConcepto(i) = Trim(Str(lCodi1))
            arrDescConcepto(i) = sDesc
            arrUnidMonetar(i) = sUnid
            i = i + 1
        Loop
    End If
    
End Sub

Public Sub gsuCargaProductos()
    Dim iCant As Long, i As Long
    Dim sCodi As String, sDesc As String, sFechaFin As String
    Dim iProductoOk As Integer
    
    iCant = GetProductos(0)
    
    If iCant > 0 Then
        
        i = 0
        Do While GetProductosRead(sCodi, sDesc, vbNull, sFechaFin, iProductoOk)
            ' s�lo se mostrar�n los productos
            ' que esten activos, osea, sin fecha de fin
            If sFechaFin = "" And iProductoOk = 1 Then
                ReDim Preserve arrProductos(i) As String
                ReDim Preserve arrCodiProductos(i) As String
                arrProductos(i) = sCodi & "  --  " & sDesc
                arrCodiProductos(i) = sCodi
                i = i + 1
            End If
        Loop
    End If

End Sub

Public Sub gsuCargaPlanes()
    Dim iCant As Long, i As Long
    Dim sCodi As String, sDesc As String, sFechaFin As String
    Dim iPlanOK As Integer
    
    iCant = GetPlanes(0)
    
    If iCant > 0 Then
        i = 0
          ' se incorpora campo Uso. JPABLOS. Proyecto OMV. 17-jun-2011.
        Do While GetPlanesRead(sCodi, sDesc, vbNull, sFechaFin, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, iPlanOK, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull, vbNull)
            ' s�lo se mostrar�n los productos
            ' que esten activos, osea, sin fecha de fin
            If sFechaFin = "" And iPlanOK = 1 Then
                ReDim Preserve arrPlanes(i) As String
                ReDim Preserve arrCodiPlanes(i) As String
                arrPlanes(i) = sCodi & " - " & sDesc
                arrCodiPlanes(i) = sCodi
                i = i + 1
            End If
        Loop
    End If

End Sub

Public Sub gsuLlenaComboSiNo(ByRef poCombo As ComboBox)
    
    poCombo.Clear
    poCombo.AddItem "SELECCIONE"
    poCombo.AddItem "SI"
    poCombo.AddItem "NO"
    poCombo.ListIndex = 0
End Sub

Public Sub gsuAsignaComboSiNo(ByRef poCombo As ComboBox, ByVal psValor As String)

    poCombo.ListIndex = 0
    If (psValor = "SI") Then
        poCombo.ListIndex = 1
    End If
    If (psValor = "NO") Then
        poCombo.ListIndex = 2
    End If
    
    If (poCombo.Name = "pbx4g" And psValor = "") Then
        poCombo.ListIndex = 2
    End If
    
    If (poCombo.Name = "pbxCargoBoleta" And psValor = "") Then
        poCombo.ListIndex = 2
    End If
    
    If (poCombo.Name = "pbxCargaBoleta" And psValor = "") Then
        poCombo.ListIndex = 2
    End If
    
End Sub

Public Sub gsuMarcaFilaGrilla(ByRef poGrilla As MSFlexGrid, ByVal piCualColumna)
    Dim i As Integer
    
    If poGrilla.Row > 0 Then
        For i = 0 To poGrilla.Cols - 1
            poGrilla.Col = i
            If poGrilla.CellBackColor = vbYellow Then
                poGrilla.CellBackColor = vbWhite
                poGrilla.TextMatrix(poGrilla.Row, piCualColumna) = 0
            Else
                poGrilla.CellBackColor = vbYellow
                poGrilla.TextMatrix(poGrilla.Row, piCualColumna) = 1
            End If
        Next i
    End If
End Sub

Public Function SoloNumeros(KEYASCII As Integer)
    If Not ((Chr(KEYASCII) >= 0 And Chr(KEYASCII) <= 9) Or KEYASCII = vbKeyBack) Then
        SoloNumeros = 0
        Exit Function
    End If
    SoloNumeros = KEYASCII
End Function

Public Function SoloNumerosDecimales(ByVal KEYASCII As Integer, ByRef Texto As TextBox)
Dim iCuantosPuntos As Integer, iCuantosDecimales As Integer
Dim iLargo As Integer
Dim i As Integer
iCuantosPuntos = 0
    If Not ((Chr(KEYASCII) >= 0 And Chr(KEYASCII) <= 9) Or KEYASCII = vbKeyBack Or KEYASCII = 46) Then
        SoloNumerosDecimales = 0
        Exit Function
    End If
    If KEYASCII = 46 Then
        
        iLargo = Len(Texto.Text)
        For i = 1 To iLargo
            If Mid(Texto.Text, i, 1) = "." Then
                iCuantosPuntos = iCuantosPuntos + 1
            End If
            
        Next i
        If iLargo = 0 And KEYASCII = 46 Then
            Texto.Text = "0."
            Texto.SelStart = 2
            Exit Function
            
        End If
        If iCuantosPuntos >= 1 Then
            SoloNumerosDecimales = 0
        Else
            SoloNumerosDecimales = KEYASCII
        End If
    Else
        SoloNumerosDecimales = KEYASCII
    End If
End Function

Public Sub gsuCargaAreas()
    Dim sCodiArea As String, sDescArea As String
    Dim iCant As Long, i As Long

    iCant = GetParam(8007)

    If iCant > 0 Then
        ReDim arrAreas(iCant - 1) As String
        ReDim arrCodiAreas(iCant - 1) As String
        i = 0
        Do While GetParamRead(sCodiArea, sDescArea)
            arrAreas(i) = sCodiArea & "  -  " & sDescArea
            arrCodiAreas(i) = sCodiArea
            i = i + 1
        Loop
    End If
End Sub

Public Sub gsuCargaClasePaque()
    Dim sCodiClasePaque As String, sDescClasePaque As String
    Dim iCant As Long, i As Long

    iCant = GetParam(8008)

    If iCant > 0 Then
        ReDim arrClasePaque(iCant - 1) As String
        ReDim arrCodiClasepaque(iCant - 1) As String
        i = 0
        Do While GetParamRead(sCodiClasePaque, sDescClasePaque)
            arrClasePaque(i) = sCodiClasePaque & "  -  " & sDescClasePaque
            arrCodiClasepaque(i) = sCodiClasePaque
            i = i + 1
        Loop
    End If
End Sub

Public Sub gsuAsignaCombo(XCombo As ComboBox, X As String)
    Dim i As Integer
    If XCombo.ListCount > 0 Then
        XCombo.ListIndex = 0
        For i = 0 To XCombo.ListCount - 1
            If Trim$(Left$(XCombo.List(i), InStr(XCombo.List(i), " "))) = Trim$(X) Then
                XCombo.ListIndex = i
            End If
        Next i
    End If
End Sub

Public Sub gsuBuscaItemCombobox1(ByRef pComboBox As ComboBox, ByVal plValor As Long)
    Dim i As Integer
    
    For i = 1 To pComboBox.ListCount - 1
        If pComboBox.ItemData(i) = plValor Then
            pComboBox.ListIndex = i
            Exit Sub
        End If
    Next i
    pComboBox.ListIndex = 0
End Sub

Public Sub gsuBuscaItemCombobox(ByRef pComboBox As ComboBox, ByVal pstexto As String)
    Dim i As Integer
    
    For i = 1 To pComboBox.ListCount - 1
        pComboBox.ListIndex = i
        If pComboBox.Text = pstexto Then
            Exit Sub
        End If
    Next i
    pComboBox.ListIndex = 0
End Sub

Public Function Fn_Crear_Linea_Agrupador(ByVal Elem As String, ByVal desc As String) As String
    Dim Linea As String

    Linea = Elem + Chr(9)
    Linea = Linea + desc + Chr(9)

    Fn_Crear_Linea_Agrupador = Linea
End Function

Public Function Fn_Crear_Linea_Conceptos(ByVal codi_elem As Integer, ByVal Codi_Concepto As Long, ByVal desc_concepto As String, ByVal prioridad As String) As String
    Dim Linea As String

    Linea = Str(codi_elem) + Chr(9)
    Linea = Linea + Str(Codi_Concepto) + Chr(9)
    Linea = Linea + desc_concepto + Chr(9)
    Linea = Linea + prioridad

    Fn_Crear_Linea_Conceptos = Linea
End Function

Public Function Fn_Crear_Linea_Proyectos(ByVal cod_proyecto As String, ByVal desc_proyecto As String, ByVal concepto As String) As String
    Dim Linea As String

    Linea = cod_proyecto + Chr(9)
    Linea = Linea + desc_proyecto + Chr(9)
    Linea = Linea + concepto
 
    Fn_Crear_Linea_Proyectos = Linea
End Function

Public Sub suLlenaAgrupador(ByRef Combo As ComboBox, codi_grupo As Integer)
    Dim Ret As Integer, i As Integer, Elem As String, desc As String
    
    Combo.Clear
    
    Ret = Srv_Parame_Open(codi_grupo)
    If Ret <> 0 Then
        For i = 1 To Ret
            Ret = Srv_Parame_Read(Elem, desc)
            Combo.AddItem Elem + " - " + desc
        Next i
        Combo.ListIndex = -1
    End If
    Ret = Srv_Parame_Close()
    
End Sub

Public Sub suLlenaGrillaAgrupador(ByRef grilla As MSFlexGrid, codi_grupo As Integer)
    Dim Total As Integer, Ret As Integer, Linea As String, i As Integer, Elem As String, desc As String
    grilla.Visible = False
    Call Su_Limpiar_Grilla(grilla)
    Total = Srv_Parame_Open(codi_grupo)
    For i = 1 To Total
        Total = Srv_Parame_Read(Elem, desc)
        Linea = Fn_Crear_Linea_Agrupador(Elem, desc)
        grilla.AddItem Linea, i
    Next i
    Ret = Srv_Parame_Close()
    grilla.Visible = True
End Sub

Public Sub suLlenaGrillaConceptos(ByRef grilla As MSFlexGrid)
    Dim Total As Integer, Ret As Integer, Linea As String, i As Integer, codi_elem As Integer, Codi_Concepto As Long, desc_concepto As String, prioridad As String
    grilla.Visible = False
    Call Su_Limpiar_Grilla(grilla)
    Total = Srv_ConceptoExcluyente_Open()
    For i = 1 To Total
        Total = Srv_ConceptoExcluyente_Read(codi_elem, Codi_Concepto, desc_concepto, prioridad)
        Linea = Fn_Crear_Linea_Conceptos(codi_elem, Codi_Concepto, desc_concepto, prioridad)
        grilla.AddItem Linea, i
    Next i
    Ret = Srv_ConceptoExcluyente_Close()
    grilla.Visible = True
End Sub

Public Sub suAgregaToLista(ByRef Lista As ListView, ByRef xValores() As String)
    
    Dim li As ListItem, i As Long
        
    If Lista.ListItems.Count > 1 Then
        Lista.ListItems.Remove Lista.ListItems.Count
    End If

    Set li = Lista.ListItems.Add(, , Trim$(xValores(0)))
    
    For i = 1 To Lista.ColumnHeaders.Count - 1
        li.SubItems(i) = Trim$(xValores(i))
    Next

    Lista.ListItems(Lista.ListItems.Count).Selected = True
    Lista.ListItems(Lista.ListItems.Count).EnsureVisible
    
    Set li = Lista.ListItems.Add(, , "")
    
End Sub

Public Sub suBorraSeleccionLista(ByRef Lista As ListView)
    Dim i As Long
        
    For i = 1 To Lista.ListItems.Count
        If Lista.ListItems(i).Checked Then
            Lista.ListItems(i).Checked = False
        End If
    Next
End Sub

Public Sub suModificaToLista(ByRef Lista As ListView, ByRef xValores() As String, ByVal Indice As Long)
    Dim li As ListItem
    
    Dim i As Long
    Set li = Lista.ListItems(Indice)
    For i = 1 To Lista.ColumnHeaders.Count - 1
        li.SubItems(i) = Trim$(xValores(i))
    Next

    Lista.ListItems(Indice).Selected = True
    Lista.ListItems(Indice).EnsureVisible
    
End Sub

Public Sub suEliminaFromLista(ByRef Lista As ListView, ByVal Indice As Long)

    Lista.ListItems.Remove Indice
    Lista.ListItems(Indice).Selected = True
    Lista.ListItems(Indice).EnsureVisible
      
End Sub

Public Sub suFiltro(ByRef Lista As ListView, Index As Long, ByVal Operador As String, ByVal Filtro As String)
    Dim i As Long
    Dim ValorCampo As String

    Screen.MousePointer = vbHourglass
    

    For i = Lista.ListItems.Count To 1 Step -1
    
        If Index = 0 Then
            ValorCampo = Lista.ListItems(i)
        Else
            ValorCampo = Lista.ListItems(i).SubItems(Index)
        End If
    
        Select Case Operador
            Case "Distinto":
                
                If UCase(ValorCampo) = Filtro Then
                    Lista.ListItems.Remove i
                End If
            
            Case "Igual":
                If UCase(ValorCampo) <> Filtro Then
                    Lista.ListItems.Remove i
                End If
                
            Case "Mayor que":
                If IsDate(ValorCampo) Then
                   If DateDiff("d", ValorCampo, Filtro) >= 0 Then
                      Lista.ListItems.Remove i
                   End If
                ElseIf IsNumeric(ValorCampo) Then
                   If ValorCampo <= Filtro Then
                       Lista.ListItems.Remove i
                   End If
                End If
                
            Case "Menor que":
                If IsDate(ValorCampo) Then
                   If DateDiff("d", ValorCampo, Filtro) <= 0 Then
                      Lista.ListItems.Remove i
                   End If
                ElseIf IsNumeric(ValorCampo) Then
                   If ValorCampo >= Filtro Then
                       Lista.ListItems.Remove i
                   End If
                End If
                
            Case "Mayor o Igual que":

                If IsDate(ValorCampo) Then
                   If DateDiff("d", ValorCampo, Filtro) > 0 Then
                      Lista.ListItems.Remove i
                   End If
                ElseIf IsNumeric(ValorCampo) Then
                   If ValorCampo < Filtro Then
                       Lista.ListItems.Remove i
                   End If
                End If
                
            Case "Menor o Igual que":
                
                If IsDate(ValorCampo) Then
                   If DateDiff("d", ValorCampo, Filtro) < 0 Then
                      Lista.ListItems.Remove i
                   End If
                ElseIf IsNumeric(ValorCampo) Then
                   If ValorCampo > Filtro Then
                       Lista.ListItems.Remove i
                   End If
                End If
                
            Case "Contiene":
                If InStr(1, UCase(ValorCampo), Filtro) = 0 Then
                    Lista.ListItems.Remove i
                End If
                
            Case "No Contiene":
                If InStr(1, UCase(ValorCampo), Filtro) > 0 Then
                    Lista.ListItems.Remove i
                End If
        End Select
    Next i
    
    Lista.ListItems.Add
    
    Screen.MousePointer = vbDefault
End Sub

Public Function fnDevuelveCodiElem(ByVal CodiGrupo As Integer) As String
  Dim Ret As Integer
  Dim CodiElem As String
  Ret = Srv_CodiElem_Open(CodiGrupo)
  Ret = Srv_CodiElem_Read(CodiElem)
  Ret = Srv_CodiElem_Close
  fnDevuelveCodiElem = CodiElem
End Function

Public Sub suLlenaGrillaProyectos(ByRef grilla As MSFlexGrid)
    Dim Ret As Integer, Linea As String, i As Integer, i_codi_paquete As Integer, cod_proyecto As String, desc_proyecto As String, concepto As String
    grilla.Visible = False
    i_codi_paquete = Val(frmPaquetes.lb_codi_paquete.Caption)
     
    Call Su_Limpiar_Grilla(grilla)
   Ret = Srv_CargaProyectos_Open(i_codi_paquete)
    For i = 1 To Ret
      
        Ret = Srv_CargaProyectos_Read(cod_proyecto, desc_proyecto, concepto)
        Linea = Fn_Crear_Linea_Proyectos(cod_proyecto, desc_proyecto, concepto)
        grilla.AddItem Linea, i
        
        
    Next i
    Ret = Srv_CargaProyectos_Close()
    grilla.Visible = True
End Sub
