Attribute VB_Name = "modVariables"
Option Explicit

Global GPBase As ADODB.Connection

Global XDBName As String

Public arrCodiLocalidades() As String
Public arrLocalidades() As String
Public arrGrupos() As String
Public arrCodiGrupos() As String
Public arrCodiParame() As String
Public arrEquipos() As String
Public arrCodiEquipos() As String
Public arrFacilidades() As String
Public arrCodiFacilidades() As String
Public arrRentas() As String
Public arrCodiConcepto() As String
Public arrDescConcepto() As String
Public arrUnidMonetar() As String
Public arrCodiRentas() As String
Public arrProductos() As String
Public arrCodiProductos() As String
Public arrPlanes() As String
Public arrCodiPlanes() As String
Public arrAreas() As String
Public arrCodiAreas() As String
Public arrClasePaque() As String
Public arrCodiClasepaque() As String
Public arrAgrupador() As String

' Para la b�squeda del Plan
Public giCualPlanSeleccionado As Integer

' Para la b�squeda de la Promocion
Public giCualPromocionSeleccionada As Integer
