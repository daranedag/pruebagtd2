Attribute VB_Name = "modDeleteBolsas"
Option Explicit

Function DelBolsa(ByVal inuIdBolsa As Long, _
                  ByRef osbResult As String _
                 ) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelBolsa As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.BolsaElimina(?,?); end;"

    Set vrDelBolsa = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuIdBolsa)
        .Parameters.Append adoCmd.CreateParameter("osbResult", adVarChar, adParamOutput, 32767)
    End With
    On Error GoTo Error
    vrDelBolsa.Open adoCmd
    
    osbResult = adoCmd.Parameters("osbResult")

    Set vrDelBolsa = Nothing

    DelBolsa = True
    Exit Function

Error:
    Set vrDelBolsa = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.BolsaElimina(" & inuIdBolsa & ", :osbResult); end;"
    DelBolsa = fn_error(Err.Description, sLlamadaProc)
    DelBolsa = False
End Function






