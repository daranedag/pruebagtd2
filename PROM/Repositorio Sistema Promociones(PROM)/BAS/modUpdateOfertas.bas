Attribute VB_Name = "modUpdateOfertas"
Option Explicit

Function UpdOferLoca(ByVal codi_promocion As Long, ByVal codi_localidad As String, ByVal fech_finvigen As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrUpdOferLoca As ADODB.Recordset

    sLlamadaProc = "begin siaa_ofertas.Upd_Localidad_Oferta(?,?,?); end;"

    Set vrUpdOferLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_localidad)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fech_finvigen)
    End With
    On Error GoTo Error
    vrUpdOferLoca.Open adoCmd

    Set vrUpdOferLoca = Nothing

    UpdOferLoca = True
    Exit Function

Error:
    Set vrUpdOferLoca = Nothing
    sLlamadaProc = "begin siaa_ofertas.Upd_Localidad_Oferta(" + Str(codi_promocion) + "," + codi_localidad + "," + fech_finvigen + "); end;"
    UpdOferLoca = fn_error(Err.Description, sLlamadaProc)
    UpdOferLoca = False
End Function

Function UdpDetOferta(ByVal codi_oferta As Long, ByVal codi_paquete As Long, ByVal codi_paquete_ant As Long, ByVal codi_area As String, ByVal tipo_paquete As String, ByVal codi_cargo As String, ByVal codi_desc As String, ByVal valor_total As Double, ByVal porc_desc As Long, _
         ByVal valor_cargo As Double, ByVal valor_desc As Double, ByVal desc_promo As String, ByVal valor_descPromo As Double, ByVal meses_promo As Long, ByVal codi_exclusion As String, ByVal clas_paquete As String, ByVal paquete_adic As Long, _
         ByVal codi_DescPC As String, ByVal valor_descPC As Double, ByVal inicia_cobro As String, ByVal flag_bligatorio As String, ByRef psRespuesta As String) As Integer
         
    Dim sParametrosProc As String
    Dim sLlamadaProcx As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrUpdDetOferta As ADODB.Recordset

    sLlamadaProc = "begin siaa_ofertas.Upd_Detalle_Oferta(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;"
    

    Set vrUpdDetOferta = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, Trim(codi_area))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, Trim(tipo_paquete))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_cargo))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_desc))
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, valor_total)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 5, porc_desc)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, valor_cargo)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, valor_desc)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(desc_promo))
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, valor_descPromo)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 2, meses_promo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, Trim(codi_exclusion))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, Trim(clas_paquete))
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 10, codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 10, codi_paquete_ant)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 10, paquete_adic)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_DescPC))
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, valor_descPC)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, Trim(inicia_cobro))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, Trim(flag_bligatorio))
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrUpdDetOferta.Open adoCmd
        If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
            psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
        Else
            psRespuesta = ""
        End If

    Set vrUpdDetOferta = Nothing

    UdpDetOferta = True
    Exit Function

Error:
    Set vrUpdDetOferta = Nothing
    sLlamadaProc = "begin siaa_ofertas.Upd_Detalle_Oferta(" + codi_oferta + "," + codi_paquete + "," + codi_paquete_ant + "," + codi_area + "," + tipo_paquete + "," + codi_cargo + "," + codi_desc + "," + valor_total + "," + porc_desc + "," + valor_cargo + "," + valor_desc + "," + desc_promo + "," + valor_descPromo + "," + meses_promo + "," + codi_exclusion + "," + clas_paquete + "," + paquete_adic + "," + codi_DescPC + "," + valor_descPC + "," + inicia_cobro + "," + flag_bligatorio + "); end;"
    UdpDetOferta = fn_error(Err.Description, sLlamadaProc)
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    UdpDetOferta = False
         
End Function
Function UpdOferta(ByVal codi_oferta As Long, ByVal desc_oferta As String, ByVal codi_cargo As String, ByVal codi_dcto As String, ByVal fech_inivigen As String, ByVal fech_finvigen As String, ByVal observacion As String, ByVal vlor_oferta As Double, ByVal vlor_promocion As Double, ByRef pack_comp As String, ByVal agrupador As String, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrUpdOferta As ADODB.Recordset

    sLlamadaProc = "begin siaa_ofertas.Upd_Oferta(?,?,?,?,?,?,?,?,?,?,?,?); end;"

    Set vrUpdOferta = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_cargo))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_dcto))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Trim(fech_inivigen))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Trim(fech_finvigen))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 2000, Trim(observacion))
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, vlor_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, vlor_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(pack_comp))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, agrupador)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrUpdOferta.Open adoCmd
        If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
            psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
        Else
            psRespuesta = ""
        End If

    Set vrUpdOferta = Nothing

    UpdOferta = True
    Exit Function

Error:
    Set vrUpdOferta = Nothing
    sLlamadaProc = "begin siaa_ofertas.Upd_Oferta(" + codi_oferta + "," + desc_oferta + "," + codi_cargo + "," + codi_dcto + "," + fech_inivigen + "," + fech_finvigen + "," + observacion + "," + vlor_oferta + "," + vlor_promocion + "); end;"
    UpdOferta = fn_error(Err.Description, sLlamadaProc)
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    UpdOferta = False
End Function
