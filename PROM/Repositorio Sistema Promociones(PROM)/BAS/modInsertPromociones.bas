Attribute VB_Name = "modInsertPromociones"
Option Explicit

Function InsPromocion(ByVal desc_promocion As String, _
                      ByVal fecha_inicio As String, _
                      ByVal fecha_fin As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsPromocion As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromocionAgrega(?,?,?); end;"

    Set vrInsPromocion = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_inicio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_fin)
    End With
    On Error GoTo Error
    vrInsPromocion.Open adoCmd

    Set vrInsPromocion = Nothing

    InsPromocion = True
    Exit Function

Error:
    Set vrInsPromocion = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromocionAgrega(" + desc_promocion + "," + fecha_inicio + "," + fecha_fin + "); end;"
    InsPromocion = fn_error(Err.Description, sLlamadaProc)
    InsPromocion = False
End Function

Function InsPromLoca(ByVal codi_promocion As Long, _
                     ByVal codi_localidad As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsPromLoca As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoLocalidadAgrega(?,?); end;"

    Set vrInsPromLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_localidad)
    End With
    On Error GoTo Error
    vrInsPromLoca.Open adoCmd

    Set vrInsPromLoca = Nothing

    InsPromLoca = True
    Exit Function

Error:
    Set vrInsPromLoca = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoLocalidadAgrega(" + Trim(Str(codi_promocion)) + "," + codi_localidad + "); end;"
    InsPromLoca = fn_error(Err.Description, sLlamadaProc)
    InsPromLoca = False
End Function

Function InsPromCobro(ByVal codi_promocion As Long, _
                      ByVal codi_cobro As String, _
                      ByVal tipo_cobro As String, _
                      ByVal tipo_origen As String, _
                      ByVal codi_origen As String, _
                      ByVal cant_gracia As Integer, _
                      ByVal cant_duracion As Integer, _
                      ByVal cant_mincuotas As Integer, _
                      ByVal cant_maxcuotas As Integer, _
                      ByVal valor_cobro As Double) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsPromCobro As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoCobroAgrega(?,?,?,?,?,?,?,?,?,?); end;"

    Set vrInsPromCobro = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_cobro)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, tipo_cobro)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, tipo_origen)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_origen)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_gracia)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_duracion)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_mincuotas)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_maxcuotas)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 13, valor_cobro)
    End With
    On Error GoTo Error
    vrInsPromCobro.Open adoCmd

    Set vrInsPromCobro = Nothing

    InsPromCobro = True
    Exit Function

Error:
    Set vrInsPromCobro = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoCobroAgrega(" + Trim(Str(codi_promocion)) + "," + codi_cobro + "," + tipo_cobro + "," + tipo_origen + "," + codi_origen + "," + Trim(Str(cant_gracia)) + "," + Trim(Str(cant_duracion)) + "," + Trim(Str(cant_mincuotas)) + "," + Trim(Str(cant_maxcuotas)) + "," + Trim(Str(valor_cobro)) + "); end;"
    InsPromCobro = fn_error(Err.Description, sLlamadaProc)
    InsPromCobro = False
End Function

Function InsPromPlan(ByVal codi_promocion As Long, _
                     ByVal codi_plan As String, _
                     ByVal cant_perm As Integer) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsPromPlan As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoPlanAgrega(?,?,?); end;"

    Set vrInsPromPlan = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_perm)
    End With
    On Error GoTo Error
    vrInsPromPlan.Open adoCmd

    Set vrInsPromPlan = Nothing

    InsPromPlan = True
    Exit Function

Error:
    Set vrInsPromPlan = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoPlanAgrega(" + Trim(Str(codi_promocion)) + "," + codi_plan + "," + Trim(Str(cant_perm)) + "); end;"
    InsPromPlan = fn_error(Err.Description, sLlamadaProc)
    InsPromPlan = False
End Function

Function InsPromEqui(ByVal codi_promocion As Long, ByVal codi_producto As String, ByVal codi_equipo As String, ByVal tipo_relacoin As String, ByVal cant_permanencia As Integer, ByVal fecha_inicio As String, ByVal fecha_termino As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsPromEqui As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoEquipoAgrega(?,?,?,?,?,?,?); end;"

    Set vrInsPromEqui = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_equipo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, tipo_relacoin)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_permanencia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_inicio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_termino)
    End With
    On Error GoTo Error
    vrInsPromEqui.Open adoCmd

    Set vrInsPromEqui = Nothing

    InsPromEqui = True
    Exit Function

Error:
    Set vrInsPromEqui = Nothing
    'sLlamadaProc = "begin SIAA_PROMOCIONES.PromoEquipoAgrega(" + codi_promocion + "," + codi_producto + "," + codi_equipo + "," + tipo_relacoin + "," + cant_permanencia + "," + fecha_inicio + "," + fecha_termino + "); end;"
    InsPromEqui = fn_error(Err.Description, sLlamadaProc)
    InsPromEqui = False
End Function

Function CopiaPromo(ByVal CodiPromocion As Long) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCopiaPromo As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromocionCopia(?); end;"

    Set vrCopiaPromo = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, CodiPromocion)
    End With
    On Error GoTo Error
    vrCopiaPromo.Open adoCmd

    Set vrCopiaPromo = Nothing

    CopiaPromo = True
    Exit Function

Error:
    Set vrCopiaPromo = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromocionCopia(" + Trim(Str(CodiPromocion)) + "); end;"
    CopiaPromo = fn_error(Err.Description, sLlamadaProc)
    CopiaPromo = False
End Function



