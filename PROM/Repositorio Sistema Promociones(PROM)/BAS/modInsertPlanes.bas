Attribute VB_Name = "modInsertPlanes"
Option Explicit

Function InsPlan(ByVal codi_plan As String, _
                 ByVal desc_plan As String, _
                 ByVal fecha_fin As String, _
                 ByVal codi_facilidad As String, _
                 ByVal flag_control As String, _
                 ByVal codi_renta As String, _
                 ByVal central As String, _
                 ByVal prop_cargaini As String, _
                 ByVal valor_plan As Long, _
                 ByVal tipo_saldo As String, _
                 ByVal valor_cargaini As Long, _
                 ByVal uso As String, _
                 ByVal rut_empresa As String, _
                 ByVal flag_acumula As String, _
                 ByVal Codi_Agrupa As String, _
                 ByVal paq_portal As String, _
                 ByVal cant_lineas As String, _
                 ByVal flag_4g As String, _
                 ByVal flag_cargoboleta As String _
                 ) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsPlan As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanAgrega(?,?,?,?,?,?,?,?,?,?,?,?,?,?,null,null,null,null,?,?,?,?,?); end;"

    Set vrInsPlan = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_fin)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, codi_facilidad)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, flag_control)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_renta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, central)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, prop_cargaini)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 13, valor_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, tipo_saldo)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 13, valor_cargaini)
        ' incorpora campo uso. JPABLOS. Proyecto OMV. 17-jun-2011.
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 15, uso)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 15, rut_empresa)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, flag_acumula)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 5, Codi_Agrupa)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, paq_portal)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 2, cant_lineas)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 1, flag_4g)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 1, flag_cargoboleta)
    End With
    On Error GoTo Error
    vrInsPlan.Open adoCmd

    Set vrInsPlan = Nothing

    InsPlan = True
    Exit Function

Error:
    Set vrInsPlan = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanAgregar(" + codi_plan + "," + desc_plan + "," + fecha_fin + "," + codi_facilidad + "," + flag_control + "," + codi_renta + "," + central + "," + prop_cargaini + "," + Trim(Str(valor_plan)) + "," + tipo_saldo + "," + Trim(Str(valor_cargaini)) + "," + paq_portal + "," + Trim(Str(cant_lineas)) + "," + Trim(flag_4g) + "," + Trim(flag_cargoboleta) + " ); end;"
    InsPlan = fn_error(Err.Description, sLlamadaProc)
    InsPlan = False
End Function

Function InsPlanFaci(ByVal codi_plan As String, _
                     ByVal codi_facilidad As String, _
                     ByVal cant_facilidad As Long, _
                     ByVal codi_exclusion As String, _
                     ByVal prog_central As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsPlanFaci As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanFaciAgrega(?,?,?,?,?); end;"

    Set vrInsPlanFaci = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, codi_facilidad)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, cant_facilidad)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_exclusion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, prog_central)
    End With
    On Error GoTo Error
    vrInsPlanFaci.Open adoCmd

    Set vrInsPlanFaci = Nothing

    InsPlanFaci = True
    Exit Function

Error:
    Set vrInsPlanFaci = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanFaciAgregar(" + codi_plan + "," + codi_facilidad + "," + Trim(Str(cant_facilidad)) + "," + codi_exclusion + "," + prog_central + "); end;"
    InsPlanFaci = fn_error(Err.Description, sLlamadaProc)
    InsPlanFaci = False
End Function

Function CopiaPlan(ByVal CodiPlan As String, ByVal CodiNuevo As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCopiaPlan As ADODB.Recordset

    sLlamadaProc = "begin siaa_promociones.PlanCopia(?,?); end;"

    Set vrCopiaPlan = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, CodiPlan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, CodiNuevo)
    End With
    On Error GoTo Error
    vrCopiaPlan.Open adoCmd

    Set vrCopiaPlan = Nothing

    CopiaPlan = True
    Exit Function

Error:
    Set vrCopiaPlan = Nothing
    sLlamadaProc = "begin siaa_promociones.PlanCopiar(" + CodiPlan + "," + CodiNuevo + "); end;"
    CopiaPlan = fn_error(Err.Description, sLlamadaProc)
    CopiaPlan = False
End Function
