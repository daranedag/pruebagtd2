Attribute VB_Name = "modDeleteFrecuentes"
Option Explicit

Function DelFrecuentes(ByVal iFono As String, _
                  ByRef osbResult As String _
                 ) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelFrecuentes As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.FrecuentesElimina(?,?); end;"

    Set vrDelFrecuentes = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, iFono)
        .Parameters.Append adoCmd.CreateParameter("osbResult", adVarChar, adParamOutput, 32767)
    End With
    On Error GoTo Error
    vrDelFrecuentes.Open adoCmd
    
    osbResult = adoCmd.Parameters("osbResult")

    Set vrDelFrecuentes = Nothing

    DelFrecuentes = True
    Exit Function

Error:
    Set vrDelFrecuentes = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.FrecuentesElimina(" & iFono & ", :osbResult); end;"
    DelFrecuentes = fn_error(Err.Description, sLlamadaProc)
    DelFrecuentes = False
End Function


Function BajaFrecuentes(ByVal iFono As String, _
                        ByVal iFechaBaja As String, _
                        ByVal iUsuario As String, _
                        ByRef osbResult As String _
                 ) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrBajaFrecuentes As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.FrecuentesModifica(?,?,?,?); end;"

    Set vrBajaFrecuentes = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, iFono)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, iFechaBaja)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, iUsuario)
        .Parameters.Append adoCmd.CreateParameter("osbResult", adVarChar, adParamOutput, 32767)
    End With
    On Error GoTo Error
    vrBajaFrecuentes.Open adoCmd
    
    osbResult = adoCmd.Parameters("osbResult")

    Set vrBajaFrecuentes = Nothing

    BajaFrecuentes = True
    Exit Function

Error:
    Set vrBajaFrecuentes = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.FrecuentesModifica(" & iFono & ", :osbResult); end;"
    BajaFrecuentes = fn_error(Err.Description, sLlamadaProc)
    BajaFrecuentes = False
End Function








