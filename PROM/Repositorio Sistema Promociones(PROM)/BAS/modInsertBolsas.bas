Attribute VB_Name = "modInsertBolsas"
Option Explicit

Function InsBolsa(ByVal inuIdBolsa As Long, _
                  ByVal isbDescBolsa As String, _
                  ByVal isbCodiFacilida As String, _
                  ByVal inuPrecio As Long, _
                  ByVal inuDiasVigencia As Long, _
                  ByVal inuMinutosOnNet As Long, _
                  ByVal inuMinutosOffNet As Long, _
                  ByVal inuMinutosTodoDestino As Long, _
                  ByVal inuMinutosRural As Long, _
                  ByVal inuMinutosIslaPascua As Long, _
                  ByVal inuMinutosRoaming As Long, _
                  ByVal inuQSms As Long, _
                  ByVal isbNombreMswitch As String, _
                  ByVal inuQMBDatos As Long, _
                  ByVal inuQMBDatosRoaming As Long, _
                  ByVal isbNombreRadius As String, _
                  ByVal isbVisibleIvr As String, _
                  ByVal isbVisibleWeb As String, _
                  ByVal inuCodiPaquete As Variant, _
                  ByVal inuDinero As Long, _
                  ByVal isbCargaAutoIniMes As String, _
                  ByRef osbResult As String, _
                  ByVal flag_cargaboleta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsBolsa As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.BolsaAgrega(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;"

    Set vrInsBolsa = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuIdBolsa)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 200, isbDescBolsa)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, isbCodiFacilida)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuPrecio)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuDiasVigencia)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuMinutosOnNet)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuMinutosOffNet)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuMinutosTodoDestino)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuMinutosRural)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuMinutosIslaPascua)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuMinutosRoaming)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuQSms)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, isbNombreMswitch)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuQMBDatos)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuQMBDatosRoaming)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, isbNombreRadius)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, isbVisibleIvr)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, isbVisibleWeb)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuCodiPaquete)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuDinero)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, isbCargaAutoIniMes)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 1, flag_cargaboleta)
        .Parameters.Append adoCmd.CreateParameter("osbResult", adVarChar, adParamOutput, 32767)
    End With
    On Error GoTo Error
    vrInsBolsa.Open adoCmd
    
    osbResult = adoCmd.Parameters("osbResult")

    Set vrInsBolsa = Nothing

    InsBolsa = True
    Exit Function

Error:
    Set vrInsBolsa = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.BolsaAgrega(" & inuIdBolsa & ",'" & isbDescBolsa & "','" & isbCodiFacilida & "'," & inuPrecio & "," & inuDiasVigencia _
                    & "," & inuMinutosOnNet & "," & inuMinutosOffNet & "," & inuMinutosTodoDestino & "," & inuMinutosRural & "," _
                    & inuMinutosIslaPascua & "," & inuMinutosRoaming & "," & inuQSms & ",'" _
                    & isbNombreMswitch & "'," & inuQMBDatos & "," & inuQMBDatosRoaming & ",'" _
                    & isbNombreRadius & "','" & isbVisibleIvr & "','" & isbVisibleWeb & "'," _
                    & inuCodiPaquete & "," & inuDinero & ",'" & isbCargaAutoIniMes & "','" & flag_cargaboleta & "', :osbResult); end;"
    InsBolsa = fn_error(Err.Description, sLlamadaProc)
    InsBolsa = False
End Function


Function CopiaBolsa(ByVal inuIdBolsa As Long, _
                    ByVal inuIdBolsaNue As Long, _
                    ByVal isbCodiFacilidaNue As String, _
                    ByRef osbResult As String _
                   ) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCopiaBolsa As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.BolsaCopia(?,?,?,?); end;"

    Set vrCopiaBolsa = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuIdBolsa)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, , inuIdBolsaNue)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, isbCodiFacilidaNue)
        .Parameters.Append adoCmd.CreateParameter("osbResult", adVarChar, adParamOutput, 32767)
    End With
    On Error GoTo Error
    vrCopiaBolsa.Open adoCmd
    
    osbResult = adoCmd.Parameters("osbResult")

    Set vrCopiaBolsa = Nothing

    CopiaBolsa = True
    Exit Function

Error:
    Set vrCopiaBolsa = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.BolsaCopia(" & inuIdBolsa & "," & inuIdBolsaNue & ",'" & isbCodiFacilidaNue & "', :osbResult); end;"
    CopiaBolsa = fn_error(Err.Description, sLlamadaProc)
    CopiaBolsa = False
End Function




