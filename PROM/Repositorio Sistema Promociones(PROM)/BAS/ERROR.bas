Attribute VB_Name = "ERROR"
Option Explicit
Global msgerr2$
Global gp_nombre$
Global gp_anexo$
Global vrMANTENCION As ADODB.Recordset

Function fn_error(ByVal tuxerr As String, ByVal sql As String)
Dim resp%
Dim Desc_Error As String
Dim num_error%
   
   Desc_Error = Mid(tuxerr, 1, Len(tuxerr) - 1)
   If "Cerrar Aplicación, Apps.exe no ha sido encontrado" = Trim(Desc_Error) Then
       Gp_Username_Aplicacion = Trim(Gp_Username_Aplicacion)
   End If
   
   num_error% = 0
   Call SU_Mantencion(Gp_My_Aplicacion)
   resp% = Srv_ErrorAplicacion(Gp_My_Aplicacion, Gp_Username_Aplicacion, Gp_Zonal_Aplicacion, 0, Desc_Error, Mid(Screen.ActiveForm.Name, 1, 15), sql)
   fn_error = -num_error%

End Function

Function Srv_ErrorAplicacion(ByVal codi_aplicacion As String, _
                             ByVal Iden_Username As String, _
                             ByVal codi_admzonal As String, _
                             ByVal codi_error As String, _
                             ByVal Desc_Error As String, _
                             ByVal ActiveForm As String, _
                             ByVal desc_sql As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrSERV133 As ADODB.Recordset
    Dim resp As Long

    sLlamadaProc = "begin syan_errores01(?,?,?,?,?,?,?); end;"

    Set vrSERV133 = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_aplicacion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Iden_Username)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_admzonal)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_error)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 500, Desc_Error)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 15, ActiveForm)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 2000, desc_sql)
    End With
    On Error GoTo Error
    Screen.MousePointer = vbHourglass
    vrSERV133.Open adoCmd
    Screen.MousePointer = vbDefault
    Set vrSERV133 = Nothing
    Call SU_despliega_msg(2)
    Srv_ErrorAplicacion = True
    Exit Function

Error:
    Call SU_despliega_msg(3)
    resp = fn_error(Err.Description, sLlamadaProc)
    Srv_ErrorAplicacion = False
    
End Function

Function Srv_Mantencion_Close() As Integer

    If Not vrMANTENCION Is Nothing Then
        vrMANTENCION.Close
        Set vrMANTENCION = Nothing
    End If
End Function

Public Function Srv_Mantencion_Open(ByVal sistema As String) As Integer
    '
    Dim sSql As String
    Dim adoCmd As New ADODB.Command

    sSql = "select "
    sSql = sSql & "nomb_mantenc, fono_mantenc "
    sSql = sSql + " from "
    sSql = sSql & "syat_mantenc "
    sSql = sSql + " where "
    sSql = sSql & "codi_aplicacion = ? "
    On Error GoTo Error
    Set vrMANTENCION = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter("", adVarChar, adParamInput, 4, sistema)
    End With
    vrMANTENCION.Open adoCmd

    Srv_Mantencion_Open = True
    Exit Function

Error:
    'Srv_Mantencion_Open = fn_error(Err.Description, sSql)
    ' se comenta ya que no puede obtener datos y entra en loop
    MsgBox Err.Description
    Srv_Mantencion_Open = False
End Function

Function Srv_Mantencion_Read(ByRef nomb_mantenc As String, ByRef fono_mantenc As String) As Integer
    Dim res As Integer

    Srv_Mantencion_Read = True

    If vrMANTENCION.EOF Then
        res = Srv_Mantencion_Close()
        Srv_Mantencion_Read = False
        Exit Function
    End If

    nomb_mantenc = gfnDevuelveValorCampo("STRING", vrMANTENCION.Fields(0).Type, vrMANTENCION.Fields(0))
    fono_mantenc = gfnDevuelveValorCampo("STRING", vrMANTENCION.Fields(1).Type, vrMANTENCION.Fields(1))

    vrMANTENCION.MoveNext

End Function

Sub SU_despliega_msg(ByVal opcion As Integer)
Select Case (opcion)
  Case 2:
     msgerr2$ = " Avise inmediatamente a  "
     msgerr2$ = msgerr2$ + gp_nombre$
     msgerr2$ = msgerr2$ + " Anexo : "
     msgerr2$ = msgerr2$ + Trim(gp_anexo$)
     msgerr2$ = msgerr2$ + " y presione Aceptar"
     MsgBox msgerr2$, 5014, "HA OCURRIDO UN ERROR EN LA APLICACION"
  Case 3:
     msgerr2$ = "Debe Abandonar la Aplicación, avisar a  "
     msgerr2$ = msgerr2$ + gp_nombre$
     msgerr2$ = msgerr2$ + " Anexo : "
     msgerr2$ = msgerr2$ + Trim(gp_anexo$)
     msgerr2$ = msgerr2$ + " y presione Aceptar"
     MsgBox msgerr2$, 5014, "HAY UN ERROR EN LA BASE DE DATOS O DE CONEXION EN LA RED"
     End
  End Select
 
End Sub

Sub SU_Mantencion(ByVal gp_sistema$)
Dim resp%
     resp% = Srv_Mantencion_Open(gp_sistema$)
     resp% = Srv_Mantencion_Read(gp_nombre$, gp_anexo$)
     resp% = Srv_Mantencion_Close()
End Sub



