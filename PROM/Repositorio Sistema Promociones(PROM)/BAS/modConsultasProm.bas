Attribute VB_Name = "modConsultasProm"
Option Explicit

Global vrGetPromocion As ADODB.Recordset
Global vrGetPromLoca As ADODB.Recordset
Global vrGetPromProd As ADODB.Recordset
Global vrGetPromEqui As ADODB.Recordset
Global vrGetPromCobr As ADODB.Recordset
Global vrGetPromPlan As ADODB.Recordset
Global vrCODIELEM As ADODB.Recordset
Global vrCARGAPROYECTOS As ADODB.Recordset


'Global gpbase As New ADODB.Connection

Dim Dy As Recordset
Dim Dy_Desc As Recordset
Dim Dy_Ayuda As Recordset
Dim Dy_Seleccionados As Recordset
Dim Dy_Disponibles As Recordset

Dim Dy_Loca_Disponibles As Recordset
Dim Dy_Loca_Seleccionados As Recordset

Dim Dy_Canal_Disponibles As Recordset
Dim Dy_Canal_Seleccionados As Recordset

Dim Dy_CanalOferta_Disponibles As Recordset
Dim Dy_CanalOferta_Seleccionados As Recordset

Global Const C_PAQ_RENTA = "R"
Global Const C_PAQ_VENTA = "V"
Global Const C_PAQ_EQUIPO = "E"
Global Const C_PAQ_SERVICIO = "S"
Global Const C_MAX_ELEMENTO = 100
Global Const C_MAX_ACCESOS = 10
'TIPOS DE AYUDA
Global Const C_A_CONCEPTOS = 0
Global Const C_A_EQUIPOS = 1
Global Const C_A_FACILIDADES = 2
Global Const C_A_VENDEDORES = 3


Type TElemento
    Tipo_Elemento As String
    Codi_Elemento As String
    desc_elemento As String
    Peri_Gracia As Integer
    Dura_Vigen As Integer
    Peri_Cobro As Integer
    Prop_Elemento As String
End Type

Type TPaquete
    Sino As Integer
    codi_paquete As Integer
    Total As Integer
    Elemento(C_MAX_ELEMENTO) As TElemento
End Type
Global Gp_Paquete As TPaquete

'BUZON PARA MANTENER DATOS DE LA APLICACION EN GENERAL
Type Buzon_VAplicacion
    Username As String
    Rol As String
    Zonal As String
    Data_Source As String
    Uid As String
    Pwd As String
    Interes_My_90 As Double 'INTERESES PACTADOS A MENOS DE 90 DIAS
    Interes_Mn_90 As Double 'INTERESES PACTADOS A MAS DE 90 DIAS
    Valor_Iva As Double     'VALOR DEL IVA
    Impresora_Puntos As Integer 'POSICION EN EL LISTADO DE IMPRESORAS DEL PRINT MANAGER
                'DE LA IMPRESORA DE PUNTOS
    Impresora_Tinta As Integer
    codi_empresa As Integer
    hModule As Integer          'IDENTIFICACION MODULO DE WINRSH
    Impresora_BackOffice As Integer  'IMPRESORA DE PUNTOS c/FACTURAS VTR-EN BOf
End Type
Global Gp_VAplicacion As Buzon_VAplicacion

'BUZON PARA AYUDAS
Type Buzon_Ayuda
    Tipo_Ayuda As Integer
    Codi_Vuelta As String
    Desc_Codi_Vuelta As String
    Sino As Integer
End Type
Global Gp_Ayuda As Buzon_Ayuda

' BUZON PARA CODIGOS Y DESCRIPCIONES DE ACCESOS
' PART_PARAME CODI_GRUPO = 20

Type Accesos
     cantidad                  As Integer  ' indica numero de accesos leidos de part_parame
     Accesos                   As String   ' indica accesos con que inicio sesi�n
     Agrupa                    As String   ' indica codigo de agrupaci�n vigente
     codi_elem(C_MAX_ACCESOS)  As String
     desc_elem(C_MAX_ACCESOS)  As String
     vlor_uno(C_MAX_ACCESOS)   As Long
     Item_Uno(C_MAX_ACCESOS)   As String
     vlor_dos(C_MAX_ACCESOS)   As Long
     item_dos(C_MAX_ACCESOS)   As String
End Type
Global Gp_VAccesos As Accesos

'Dim Dy As Recordset
'Dim Dy_Desc As Recordset
Dim dyna As Recordset

Dim vrEQUIPODESC As ADODB.Recordset
Dim vrFACILIDADESC As ADODB.Recordset
Dim vrDESCCONCEPTO As ADODB.Recordset
Dim vrCONCEPTOEXCLUYENTE As ADODB.Recordset


Function Fn_Valor_Combo(XCombo As ComboBox, Optional ByVal Separador As String = " - ") As String
    Fn_Valor_Combo = Trim$(Left$(XCombo.Text, IIf(Separador = "", 1, InStr(XCombo.Text, Separador))))
End Function

'Function Fn_Valor_Combo_Prio(XCombo As ComboBox) As String
'Fn_Valor_Combo_Prio = XCombo.Text
'End Function

Function Srv_Siad_DetaPaq01(ByVal codi_paquete As Integer, ByVal Tipo_Elemento As String, ByVal Codi_Elemento As String)
    Screen.MousePointer = vbHourglass
    Dim sql As String
    Dim Dy_Ret As Recordset
    
    On Error Resume Next

    sql = "begin SIAA_PAQUETES.prceliminadetallepaquete("
    sql = sql + Str(codi_paquete)
    sql = sql + ",'" + Trim(Tipo_Elemento) + "'"
    sql = sql + ",'" + Trim(Codi_Elemento) + "'"
    sql = sql + "); End;"

    Set Dy_Ret = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      'Srv_Paquetes_Open = fn_error(sql)
      MsgBox Err.Description + " (Paquete:" + codi_paquete + ")"
      Srv_Siad_DetaPaq01 = False
    Else
      Srv_Siad_DetaPaq01 = True
    End If

    Dy_Ret.Close
    Screen.MousePointer = vbDefault

End Function

Function EliminaPaquete(ByVal codi_paquete As Integer) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrPaquete As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PAQUETES.prceliminapaquete(?); end;"

    Set vrPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_paquete)
    End With
    On Error GoTo Error
    vrPaquete.Open adoCmd
        
    Set vrPaquete = Nothing

    EliminaPaquete = True
    Exit Function

Error:
    Set vrPaquete = Nothing
    sLlamadaProc = "begin SIAA_PAQUETES.prceliminapaquete(); end;"
    EliminaPaquete = fn_error(Err.Description, sLlamadaProc)
    MsgBox Err.Description
    EliminaPaquete = False
End Function

Function IngresaDetallePaquete(ByVal codi_paquete As Integer, _
                                ByVal Tipo_Elemento As String, _
                                ByVal Codi_Elemento As String, _
                                ByVal Peri_Gracia As Integer, _
                                ByVal Dura_Vigen As Integer, _
                                ByVal Peri_Cobro As Integer, _
                                ByVal Prop_Elemento As String, _
                                ByVal interes As Double, _
                                ByVal Flag_Competencia As String, _
                                ByVal flag_obligatorio As String, _
                                ByVal fechgracia As String, _
                                ByVal codiarea As String, _
                                ByVal Adelanta_Peri_Gracia As String, _
                                ByVal Adelanta_Dura_Vigen As String, _
                                ByVal Dias_Duracion As Integer, _
                                ByVal Fech_Inicio As String, _
                                ByVal Fech_Termino As String, _
                                ByVal Tipo_Elem_Rel As String, _
                                ByVal Codi_Elem_Rel As String, _
                                ByVal Tecnologia As String, _
                                ByVal Tipo_Peticion As String, _
                                ByRef id_elemento As Long, _
                                ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDetallePaquete As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PAQUETES.prcingresadetallepaquete(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;"

    Set vrDetallePaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Tipo_Elemento)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Trim(Codi_Elemento))
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, Peri_Gracia)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 1, Dura_Vigen)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 1, Peri_Cobro)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Prop_Elemento)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, interes)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Flag_Competencia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Trim(flag_obligatorio))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fechgracia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, codiarea)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, Trim(Adelanta_Peri_Gracia))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, Trim(Adelanta_Dura_Vigen))
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, Dias_Duracion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Fech_Inicio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Fech_Termino)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Tipo_Elem_Rel)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Codi_Elem_Rel)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Tecnologia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Tipo_Peticion)
        .Parameters.Append adoCmd.CreateParameter("o_id_elemento", adNumeric, adParamOutput, 9)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrDetallePaquete.Open adoCmd
    
    If Not IsNull(adoCmd.Parameters("o_id_elemento")) Then
        id_elemento = CVar(adoCmd.Parameters("o_id_elemento"))
    Else
        id_elemento = 0
    End If
        
    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrDetallePaquete = Nothing

    IngresaDetallePaquete = True
    Exit Function

Error:
    Set vrDetallePaquete = Nothing
    sLlamadaProc = "begin SIAA_PAQUETES.prcingresadetallepaquete (); end;"
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    IngresaDetallePaquete = False
End Function

Function EliminaDetallePaquete(ByVal id_elemento As Long) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDetallePaquete As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PAQUETES.prceliminadetallepaqueter(?); end;"

    Set vrDetallePaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 9, id_elemento)
    End With
    On Error GoTo Error
    vrDetallePaquete.Open adoCmd
        
    Set vrDetallePaquete = Nothing

    EliminaDetallePaquete = True
    Exit Function

Error:
    Set vrDetallePaquete = Nothing
    sLlamadaProc = "begin SIAA_PAQUETES.prceliminadetallepaqueter(); end;"
    EliminaDetallePaquete = fn_error(Err.Description, sLlamadaProc)
    EliminaDetallePaquete = False
End Function

Function ActualizaDetallePaquete(ByVal id_elemento As Long, _
                                ByVal Tipo_Elemento As String, _
                                ByVal Codi_Elemento As String, _
                                ByVal Peri_Gracia As Integer, _
                                ByVal Dura_Vigen As Integer, _
                                ByVal Peri_Cobro As Integer, _
                                ByVal Prop_Elemento As String, _
                                ByVal interes As Double, _
                                ByVal Flag_Competencia As String, _
                                ByVal flag_obligatorio As String, _
                                ByVal fechgracia As String, _
                                ByVal codiarea As String, _
                                ByVal Adelanta_Peri_Gracia As String, _
                                ByVal Adelanta_Dura_Vigen As String, _
                                ByVal Dias_Duracion As Integer, _
                                ByVal Fech_Inicio As String, _
                                ByVal Fech_Termino As String, _
                                ByVal Tipo_Elem_Rel As String, _
                                ByVal Codi_Elem_Rel As String, _
                                ByVal Tecnologia As String, _
                                ByVal Tipo_Peticion As String _
                                ) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDetallePaquete As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PAQUETES.prcactualizadetallepaquete(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;"

    Set vrDetallePaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 12, id_elemento)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Tipo_Elemento)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Codi_Elemento)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, Peri_Gracia)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, Dura_Vigen)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, Peri_Cobro)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Prop_Elemento)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, interes)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Flag_Competencia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, flag_obligatorio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, fechgracia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, codiarea)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Adelanta_Peri_Gracia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Adelanta_Dura_Vigen)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, Dias_Duracion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Fech_Inicio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Fech_Termino)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Tipo_Elem_Rel)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Codi_Elem_Rel)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Tecnologia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Tipo_Peticion)
    End With
    On Error GoTo Error
    vrDetallePaquete.Open adoCmd
        
    Set vrDetallePaquete = Nothing

    ActualizaDetallePaquete = True
    Exit Function

Error:
    Set vrDetallePaquete = Nothing
    sLlamadaProc = "begin SIAA_PAQUETES.prcingresadetallepaquete (); end;"
    ActualizaDetallePaquete = fn_error(Err.Description, sLlamadaProc)
    ActualizaDetallePaquete = False
End Function

'Function Srv_Sian_Paquete01(ByVal codi_paquete As Integer, ByVal Desc_Paquete As String, ByVal fech_inivigen As String, ByVal fech_finvigen As String, ByVal Max_Venta As Integer, ByVal Max_Renta As Integer, ByVal Max_Equipo As Integer, ByVal Max_Servicio As Integer, Agrupa As String, ByVal Tipo_Producto As Integer)
'    Screen.MousePointer = vbHourglass
'    Dim sql As String
'    Dim Dy_Ret As Recordset
'
'    On Error Resume Next
'
'    sql = "begin sian_paquete01("
'    sql = sql + Str(codi_paquete)
'    sql = sql + ",'" + Desc_Paquete + "'"
'    sql = sql + ",to_date('" + Trim(fech_inivigen) + "','dd-mm-yy')"
'    sql = sql + ",to_date('" + Trim(fech_finvigen) + "','dd-mm-yy')"
'    sql = sql + "," + Str(Max_Venta)
'    sql = sql + "," + Str(Max_Renta)
'    sql = sql + "," + Str(Max_Equipo)
'    sql = sql + "," + Str(Max_Servicio)
'    sql = sql + ",'" + Agrupa
'    sql = sql + ",'" + Str(Tipo_Producto)
'    sql = sql + "'); End;"
'
'    Set Dy_Ret = GPBase.Execute(sql, 64)
'
'    If Err <> 0 Then
'      'Srv_Paquetes_Open = fn_error(sql)
'      MsgBox Err.Description
'      Srv_Sian_Paquete01 = False
'    Else
'      Srv_Sian_Paquete01 = True
'    End If
'
'    Dy_Ret.Close
'    Screen.MousePointer = vbDefault
'
'End Function

Function CopiaPaquete(ByVal CodiPaquete As Long, ByRef oCodiPaquete As Long) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCopiaPaquete As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PAQUETES.prcclonapaquete(?,?); end;"

    Set vrCopiaPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, CodiPaquete)
        .Parameters.Append adoCmd.CreateParameter("o_codi_paquete", adInteger, adParamOutput, 6)
    End With
    On Error GoTo Error
    vrCopiaPaquete.Open adoCmd
    
    If Not IsNull(adoCmd.Parameters("o_codi_paquete")) Then
        oCodiPaquete = CVar(adoCmd.Parameters("o_codi_paquete"))
    Else
        oCodiPaquete = 0
    End If
        
    Set vrCopiaPaquete = Nothing

    CopiaPaquete = True
    Exit Function

Error:
    Set vrCopiaPaquete = Nothing
    sLlamadaProc = "begin SIAA_PAQUETES.prcclonapaquete(" + Str(CodiPaquete) + "," + Str(oCodiPaquete) + "); end;"
    CopiaPaquete = fn_error(Err.Description, sLlamadaProc)
    CopiaPaquete = False
End Function


Function Srv_Siaq_Paquete_Close()
    On Error Resume Next
    Dy.Close
End Function

Function Srv_Siaq_Paquete_Open()
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next

    's = "select siaq_paquete.nextval"
    'f = " from dual "

    'sql = s + f + w

    sql = "select min(codi_paquete) + 1 "
    sql = sql & "from siat_paquete p "
    sql = sql & "where p.codi_paquete not in ( 0 ) "
    sql = sql & "and not exists "
    sql = sql & "( select * from siat_paquete a where a.codi_paquete = p.codi_paquete +1 )"

    Set Dy = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      'Srv_Paquetes_Open = fn_error(sql)
      Srv_Siaq_Paquete_Open = -1
    Else
      Dy.MoveLast
      Srv_Siaq_Paquete_Open = Dy.RecordCount
      Dy.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_Siaq_Paquete_Read(codi_paquete As Long)
  Screen.MousePointer = vbHourglass
  Srv_Siaq_Paquete_Read = True

  On Error Resume Next
  
  If Dy.EOF Then
    Srv_Siaq_Paquete_Read = False
    Exit Function
  End If

  codi_paquete = 0

  codi_paquete = Dy(0)

  Dy.MoveNext

  Screen.MousePointer = vbDefault

End Function
'se agrega ByVal prioritario As String
Function IngresaPaquete(ByVal codi_paquete As Integer, ByVal Desc_Paquete As String, ByVal fech_inivigen As String, ByVal fech_finvigen As String, ByVal Max_Venta As Integer, ByVal Max_Renta As Integer, ByVal Max_Equipo As Integer, ByVal Max_Servicio As Integer, ByVal habilitado As String, Agrupa As String, ByVal Tipo_Producto As Integer, ByVal Tipo_Segmento As Integer, ByVal prioritario As String, ByVal promocion As String, altaSVA As String, ByRef psRespuesta As String) As Integer
    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrPaquete As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.prcingresapaquete(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;" 'agregado campo extra

    Set vrPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, Desc_Paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Trim(fech_inivigen))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Trim(fech_finvigen))
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Max_Venta)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Max_Renta)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Max_Equipo)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Max_Servicio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, habilitado)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Agrupa)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Tipo_Producto)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Tipo_Segmento)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, prioritario)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, altaSVA)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrPaquete.Open adoCmd

    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrPaquete = Nothing

    IngresaPaquete = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrPaquete = Nothing
    Screen.MousePointer = vbDefault
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    IngresaPaquete = False
End Function

Function ActualizaPaquete(ByVal codi_paquete As Integer, ByVal Desc_Paquete As String, ByVal fech_inivigen As String, ByVal fech_finvigen As String, ByVal Max_Venta As Integer, ByVal Max_Renta As Integer, ByVal Max_Equipo As Integer, ByVal Max_Servicio As Integer, Agrupa As String, ByVal Tipo_Producto As Integer, ByVal Tipo_Segmento As Integer, ByVal prioritario As String, ByVal habilitado As String, ByVal promocion As String, ByVal altaSVA As String, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrPaquete As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.prcactualizapaquete(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;"

    Set vrPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, Desc_Paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Trim(fech_inivigen))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Trim(fech_finvigen))
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Max_Venta)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Max_Renta)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Max_Equipo)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Max_Servicio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, habilitado)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, Agrupa)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Tipo_Producto)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 10, Tipo_Segmento)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, prioritario)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, altaSVA)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrPaquete.Open adoCmd

    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrPaquete = Nothing

    ActualizaPaquete = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrPaquete = Nothing
    Screen.MousePointer = vbDefault
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    ActualizaPaquete = False
End Function


Function IngresaLocaPaquete(ByVal i_codi_paquete As Integer, ByVal i_codi_localida As String, ByVal i_user_name As String, ByVal i_fech_ini As String, ByVal i_fech_fin As String, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrLocaPaquete As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.prcingresalocapaquete(?,?,?,?,?,?); end;"

    Set vrLocaPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_localida)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 20, i_user_name)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 20, i_fech_ini)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 20, i_fech_fin)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrLocaPaquete.Open adoCmd

    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrLocaPaquete = Nothing

    IngresaLocaPaquete = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrLocaPaquete = Nothing
    Screen.MousePointer = vbDefault
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    IngresaLocaPaquete = False
End Function


Function ActualizaLocaPaquete(ByVal i_codi_paquete As Integer, ByVal i_codi_localida As String, ByVal i_user_name As String, ByVal i_fech_ini As String, ByVal i_fech_fin As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrLocaPaquete As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.prcactualizalocapaquete(?,?,?,?,?); end;"

    Set vrLocaPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_localida)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 20, i_user_name)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 20, i_fech_ini)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 20, i_fech_fin)
    End With
    On Error GoTo Error
    vrLocaPaquete.Open adoCmd

    Set vrLocaPaquete = Nothing

    ActualizaLocaPaquete = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrLocaPaquete = Nothing
    Screen.MousePointer = vbDefault
    ActualizaLocaPaquete = False
End Function

Function EliminaLocaPaquete(ByVal i_codi_paquete As Integer, ByVal i_codi_localida As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrLocaPaquete As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.prceliminalocapaquete(?,?); end;"

    Set vrLocaPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_localida)
    End With
    On Error GoTo Error
    vrLocaPaquete.Open adoCmd

    Set vrLocaPaquete = Nothing

    EliminaLocaPaquete = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrLocaPaquete = Nothing
    Screen.MousePointer = vbDefault
    EliminaLocaPaquete = False
End Function

Function IngresaCanalPaquete(ByVal i_codi_paquete As Integer, ByVal i_codi_canal_vta As String, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCanalPaquete As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.prcingresacanalpaquete(?,?,?); end;"

    Set vrCanalPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 5, i_codi_canal_vta)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrCanalPaquete.Open adoCmd

    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrCanalPaquete = Nothing

    IngresaCanalPaquete = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrCanalPaquete = Nothing
    Screen.MousePointer = vbDefault
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    IngresaCanalPaquete = False
End Function

Function EliminaCanalPaquete(ByVal i_codi_paquete As Integer, ByVal i_codi_canal_vta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCanalPaquete As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.prceliminacanalpaquete(?,?); end;"

    Set vrCanalPaquete = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 5, i_codi_canal_vta)
    End With
    On Error GoTo Error
    vrCanalPaquete.Open adoCmd

    Set vrCanalPaquete = Nothing

    EliminaCanalPaquete = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrCanalPaquete = Nothing
    Screen.MousePointer = vbDefault
    EliminaCanalPaquete = False
End Function

'SI 16415. JMARTINE. RF3
Function IngresaCanalOferta(ByVal i_codi_oferta As Integer, ByVal i_codi_canal_vta As String, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCanalOferta As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_OFERTAS.prcingresacanaloferta(?,?,?); end;"

    Set vrCanalOferta = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 5, i_codi_canal_vta)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrCanalOferta.Open adoCmd

    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrCanalOferta = Nothing

    IngresaCanalOferta = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrCanalOferta = Nothing
    Screen.MousePointer = vbDefault
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    IngresaCanalOferta = False
End Function
'SI 16415. JMARTINE. RF3
Function EliminaCanalOferta(ByVal i_codi_oferta As Integer, ByVal i_codi_canal_vta As String) As Integer
    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCanalOferta As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_OFERTAS.prceliminacanaloferta(?,?); end;"

    Set vrCanalOferta = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 5, i_codi_canal_vta)
    End With
    On Error GoTo Error
    vrCanalOferta.Open adoCmd

    Set vrCanalOferta = Nothing

    EliminaCanalOferta = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrCanalOferta = Nothing
    Screen.MousePointer = vbDefault
    EliminaCanalOferta = False
End Function

Function Srv_parp_relacion_elimina(ByVal i_codi_grupop As Integer, ByVal i_codi_elemp As String, ByVal i_codi_grupoh As Integer, ByVal i_codi_elemh As String, ByVal i_item_uno As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrRelacion As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin ptar.parp_int_prom.prcdelrelacion(?,?,?,?,?,?); end;"

    Set vrRelacion = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_grupop)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_elemp)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_grupoh)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_elemh)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_item_uno)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamOutput, 4, o_codi_error)
    End With
    On Error GoTo Error
    vrRelacion.Open adoCmd

    Set vrRelacion = Nothing

    Srv_parp_relacion_elimina = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrRelacion = Nothing
    Screen.MousePointer = vbDefault
    Srv_parp_relacion_elimina = False
End Function

Function Srv_parp_relacion_ingresa(ByVal i_codi_grupop As Integer, ByVal i_codi_elemp As String, ByVal i_codi_grupoh As Integer, ByVal i_codi_elemh As String, ByVal i_item_uno As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrRelacion As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin ptar.parp_int_prom.prcinsrelacion(?,?,?,?,?,?); end;"

    Set vrRelacion = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_grupop)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_elemp)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, i_codi_grupoh)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_elemh)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_item_uno)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamOutput, 4, o_codi_error)

    End With
    On Error GoTo Error
    vrRelacion.Open adoCmd

    Set vrRelacion = Nothing

    Srv_parp_relacion_ingresa = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrRelacion = Nothing
    Screen.MousePointer = vbDefault
    Srv_parp_relacion_ingresa = False
End Function

Sub Su_Asigna_Combo(XCombo As ComboBox, X As String, Optional ByVal default As Boolean = False)
    Dim i As Integer
    XCombo.ListIndex = -1
    For i = 0 To XCombo.ListCount - 1
        If Trim$(Left$(XCombo.List(i), InStr(XCombo.List(i), " "))) = Trim$(X) Then
            XCombo.ListIndex = i
        End If
    Next i
    
    If XCombo.ListIndex < 0 And default = True And XCombo.ListCount > 0 Then
        XCombo.ListIndex = 0
    End If

End Sub
'para comboBox cb_prioritario, actualiza campo al hacer click en la tabla
'Sub Su_Asigna_Combo_Prio(XCombo As ComboBox, X As String)
 '   Dim i As Integer
  '  XCombo.ListIndex = -1
   ' For i = 0 To XCombo.ListCount - 1
    '    If Trim$(XCombo.List(i)) = Trim$(X) Then
     '       XCombo.ListIndex = i
      '  End If
    'Next i
   '
    'If XCombo.ListIndex < 0 And XCombo.ListCount > 0 Then
    '    XCombo.ListIndex = -1
    'End If

'End Sub

Sub Su_Limpiar_Grilla(grilla As MSFlexGrid)
    grilla.AddItem "", 1
    grilla.Rows = 2
End Sub

Function Srv_parn_parame07(ByVal i_codi_grupo As Integer, _
ByVal i_codi_elem As String, _
ByVal i_desc_parame As String, _
Optional ByVal i_vlor_uno As Long, _
Optional ByVal i_item_uno As String, _
Optional ByVal i_vlor_dos As Long, _
Optional ByVal i_item_dos As String) As Integer


    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrParame As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin ptar.parp_int_prom.prcinselemparame(?,?,?,?,?,?,?); end;"

    Set vrParame = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 10, i_codi_grupo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_elem)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 120, i_desc_parame)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 12, i_vlor_uno)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_item_uno)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 12, i_vlor_dos)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_item_dos)
    End With
    On Error GoTo Error
    vrParame.Open adoCmd

    Set vrParame = Nothing

    Srv_parn_parame07 = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrParame = Nothing
    Srv_parn_parame07 = False
End Function


Function Srv_paru_parame06(ByVal i_codi_grupo As Integer, _
ByVal i_codi_elem As String, _
ByVal i_desc_parame As String, _
Optional ByVal i_vlor_uno As Long, _
Optional ByVal i_item_uno As String, _
Optional ByVal i_vlor_dos As Long, _
Optional ByVal i_item_dos As String) As Integer
    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrParame As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin ptar.parp_int_prom.prcupddescelemparame(?,?,?,?,?,?,?); end;"

    Set vrParame = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 10, i_codi_grupo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_elem)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 120, i_desc_parame)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 12, i_vlor_uno)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_item_uno)
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 12, i_vlor_dos)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_item_dos)
    End With
    On Error GoTo Error
    vrParame.Open adoCmd

    Set vrParame = Nothing

    Srv_paru_parame06 = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrParame = Nothing
    Srv_paru_parame06 = False
End Function

Function Srv_pard_parame07(ByVal i_codi_grupo As Integer, ByVal i_codi_elem As String, Optional ByVal i_codi_grupop As Variant = "") As Integer
                                                
    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrParame As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin ptar.parp_int_prom.prcdelelemparame(?,?,?); end;"

    Set vrParame = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 10, i_codi_grupo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, i_codi_elem)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, i_codi_grupop)
    End With
    On Error GoTo Error
    vrParame.Open adoCmd

    Set vrParame = Nothing

    Srv_pard_parame07 = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrParame = Nothing
    Srv_pard_parame07 = False
End Function

Public Sub gsuScanStringLW(Lista As Control, Patron As Control, Item As Integer, largo As Integer)
    ' Compara el patr�n de entrada "token"
    ' con los nombres de las consultas y mueve
    ' la grilla dejando al vista el priemr registro
    ' que haga el match.
    
    Dim i As Integer, ColBusqueda As Integer, Actual As Integer
    Dim Encontrado As Boolean
    If Trim(Patron.Text) <> "" Then

        Encontrado = False
        
        Actual = Lista.SelectedItem.Index
        
        For i = Actual + 1 To Lista.ListItems.Count
            If IsNumeric(Patron.Text) Then
                If Trim(Lista.ListItems(i)) = Trim(Patron.Text) Then
                    Lista.ListItems(i).EnsureVisible
                    Lista.ListItems(i).Selected = True
                    Exit For
                End If
            Else
                If Item = 0 Then
                    If InStr(1, UCase$(Trim(Lista.ListItems(i))), UCase(Trim(Patron.Text)), vbTextCompare) > 0 Then
                        Lista.ListItems(i).EnsureVisible
                        Lista.ListItems(i).Selected = True
                        Exit For
                    End If
                Else
                    If InStr(1, UCase$(Trim(Lista.ListItems(i).SubItems(Item))), UCase(Trim(Patron.Text)), vbTextCompare) > 0 Then
                        Lista.ListItems(i).EnsureVisible
                        Lista.ListItems(i).Selected = True
                        Exit For
                    End If
                End If
            End If
        Next i
        
        If i = Lista.ListItems.Count + 1 Then
            For i = 1 To Actual
                If IsNumeric(Patron.Text) Then
                    If Trim(Lista.ListItems(i)) = Trim(Patron.Text) Then
                        Lista.ListItems(i).EnsureVisible
                        Lista.ListItems(i).Selected = True
                        Exit For
                    End If
                Else
                    If Item = 0 Then
                        If InStr(1, UCase$(Trim(Lista.ListItems(i))), UCase(Trim(Patron.Text)), vbTextCompare) > 0 Then
                            Lista.ListItems(i).EnsureVisible
                            Lista.ListItems(i).Selected = True
                            Exit For
                        End If
                    Else
                        If InStr(1, UCase$(Trim(Lista.ListItems(i).SubItems(Item))), UCase(Trim(Patron.Text)), vbTextCompare) > 0 Then
                            Lista.ListItems(i).EnsureVisible
                            Lista.ListItems(i).Selected = True
                            Exit For
                        End If
                    End If
                End If
            Next i
        End If
    End If
    
End Sub

Function Srv_Concpto_Lik_Close()
    On Error Resume Next
    Dy.Close
End Function

Function Srv_Concpto_Lik_Open(desc_busqueda As String, Mayor As Long, Menor As Long, codi_admzonal As String, codi_empresa As Integer) As Integer
   Dim s As String
   
   Err = 0
   On Error Resume Next
   
        s = " select distinct "
        s = s & " a.codi_concepto,a.desc_concepto,a.vlor_concepto, "
        s = s & " round(decode(a.flag_afectiva,'S', vlor_concepto*ptar.parp_iva.tasa,0),2), "
        s = s & " round(decode(a.flag_afectiva,'S', vlor_concepto*(1+ptar.parp_iva.tasa),vlor_concepto),2) "
        s = s + " from "
        s = s & " typv_cptoszonaemp a, part_parame b "
        s = s + " where "
        s = s + " desc_concepto like '%'||decode(rtrim('" + desc_busqueda + "'),'',desc_concepto,rtrim('" + desc_busqueda + "'))||'%' and"
        s = s + " codi_concepto between " + Str(Mayor) + " and"
        s = s + " decode(" + Str(Menor) + ",0,999999," + Str(Menor) + ")"
        's = s & " and codi_admzonal='" + Trim(codi_admzonal) + "'"
        s = s & " and codi_empresa=" + Str(codi_empresa)
        s = s & " and unid_monetar = codi_elem "
        s = s & " and codi_grupo = 9 "
        s = s & " order by codi_concepto "
        
    
   Set Dy = GPBase.Execute(s, 64)
   If Err <> 0 Then
      Srv_Concpto_Lik_Open = -1
      MsgBox "Err #: " + Str(Err) + Chr(13) + "Error : " + Err.Description
      Exit Function
   End If
   
   If Dy.RecordCount = 0 Then
       Srv_Concpto_Lik_Open = 0
       Exit Function
     Else
       Dy.MoveLast
       Srv_Concpto_Lik_Open = Dy.RecordCount
       Dy.MoveFirst
   End If

End Function

Function Srv_Concpto_Lik_Read(Codi_Concepto As Long, desc_concepto As String, vlor_concepto As Double, vlor_iva As Double, vlor_Total As Double) As Integer
    
    Srv_Concpto_Lik_Read = True

    If Dy.EOF Then
       Srv_Concpto_Lik_Read = False
       Exit Function
    End If

    Codi_Concepto = Dy(0)
    desc_concepto = Dy(1)
    vlor_concepto = Dy(2)
    vlor_iva = Dy(3)
    vlor_Total = Dy(4)
    
    Dy.MoveNext

End Function

Function Srv_Elementos_Close()
    On Error Resume Next
    Dy.Close
End Function

Function Srv_Elementos_Open(codi_paquete As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next

    s = "Select /*+rule*/ tipo_elemento,codi_elemento,peri_gracia,dura_vigen,peri_cobro,prop_elemento,vlor_uno,"
    s = s + "Flag_Competencia,Flag_Obligatorio,fech_gracia,id_elemento,desc_elemento,codi_area"
    s = s + ", adelanta_peri_gracia, adelanta_dura_vigen" ' SSIN#11087. JPABLOS. 17-ago-2009.
    s = s + ", dias_duracion" ' Proyecto OMV. JPABLOS. 22-jun-2011.
    s = s + ", fech_inicio, fech_termino" ' Se agrega fecha de inicio y fin. SI14323 MJPEREZ 14-mar-2012
    s = s + ", tipo_elem_rel, codi_elem_rel, tecnologia, tipo_peticion "
    f = " from siav_detapaq1 "
    w = " where codi_paquete=" + Str(codi_paquete)
    w = w + " order by tipo_elemento,codi_elemento"

    sql = s + f + w

    Set Dy = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_Elementos_Open = -1
      MsgBox Err.Description
    Else
      Dy.MoveLast
      Srv_Elementos_Open = Dy.RecordCount
      Dy.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_Elementos_Read(Tipo_Elemento As String, Codi_Elemento As String, Peri_Gracia As Integer, Dura_Vigen As Integer, Peri_Cobro As Integer, Prop_Elemento As String, interes As Double, Flag_Competencia As String, flag_obligatorio As String, Fech_Gracia As String, id_elemento As Long, desc_elemento As String, codi_area As String, Adelanta_Peri_Gracia As String, Adelanta_Dura_Vigen As String, Dias_Duracion As Integer, Fech_Inicio As String, Fech_Termino As String, Tipo_Elem_Rel As String, Codi_Elem_Rel As String, Tecnologia As String, Tipo_Peticion As String)
  Screen.MousePointer = vbHourglass
  Srv_Elementos_Read = True

  On Error Resume Next
  
  If Dy.EOF Then
    Srv_Elementos_Read = False
    Exit Function
  End If

  Tipo_Elemento = ""
  Codi_Elemento = ""
  Peri_Gracia = 0
  Dura_Vigen = 0
  Peri_Cobro = 0
  Prop_Elemento = ""
  interes = 0
  Flag_Competencia = ""
  flag_obligatorio = ""
  Fech_Gracia = ""
  desc_elemento = ""
  codi_area = ""
  id_elemento = ""
  ' SSIN#11087. JPABLOS. 17-ago-2009.
  Adelanta_Peri_Gracia = ""
  Adelanta_Dura_Vigen = ""
  ' Se agrega campo dias_duracion. Proyecto OMV. JPABLOS. 22-jun-2011.
  Dias_Duracion = 0
  ' Se agrega fecha de inicio y fin. SI14323 MJPEREZ 14-mar-2012
  Fech_Inicio = ""
  Fech_Termino = ""
  'SI 15664 JMARTINE
  Tipo_Elem_Rel = ""
  Codi_Elem_Rel = ""
  Tecnologia = ""
  Tipo_Peticion = ""

  Tipo_Elemento = Dy(0)
  Codi_Elemento = Dy(1)
  Peri_Gracia = Dy(2)
  Dura_Vigen = Dy(3)
  Peri_Cobro = Dy(4)
  Prop_Elemento = Dy(5)
  interes = Dy(6)
  Flag_Competencia = Dy(7)
  flag_obligatorio = Dy(8)
  Fech_Gracia = Dy(9)
  id_elemento = Dy(10)
  desc_elemento = Dy(11)
  codi_area = Dy(12)
  ' SSIN#11087. JPABLOS. 17-ago-2009.
  Adelanta_Peri_Gracia = Dy(13)
  Adelanta_Dura_Vigen = Dy(14)
  ' Se agrega campo dias_duracion. Proyecto OMV. JPABLOS. 22-jun-2011.
  Dias_Duracion = Dy(15)
' Se agrega fecha de inicio y fin. SI14323 MJPEREZ 14-mar-2012
  Fech_Inicio = Dy(16)
  Fech_Termino = Dy(17)
  
  'SI 15664 JMARTINE
  Tipo_Elem_Rel = Dy(18)
  Codi_Elem_Rel = Dy(19)
  Tecnologia = Dy(20)
  Tipo_Peticion = Dy(21)
  
  Dy.MoveNext

  Screen.MousePointer = vbDefault


End Function

Function Srv_ElementosRel_Close()
    On Error Resume Next
    Dy.Close
End Function

Function Srv_ElementosRel_Open(codi_paquete As Integer, Tipo_Elemento As String)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next

    s = "Select /*+rule*/ tipo_elemento,codi_elemento, tipo_elem_rel, codi_elem_rel"
    f = " from siav_detapaq1 "
    w = " where codi_paquete=" + Str(codi_paquete)
    w = w + "and tipo_elemento='" + Tipo_Elemento + "'"
    
    sql = s + f + w

    Set Dy = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_ElementosRel_Open = -1
      MsgBox Err.Description
    Else
      Dy.MoveLast
      Srv_ElementosRel_Open = Dy.RecordCount
      Dy.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_ElementosRel_Read(Tipo_Elemento As String, Codi_Elemento As String, Tipo_Elem_Rel As String, Codi_Elem_Rel As String)
  Screen.MousePointer = vbHourglass
  Srv_ElementosRel_Read = True

  On Error Resume Next
  
  If Dy.EOF Then
    Srv_ElementosRel_Read = False
    Exit Function
  End If

  Tipo_Elemento = ""
  Codi_Elemento = ""
  Tipo_Elem_Rel = ""
  Codi_Elem_Rel = ""

  Tipo_Elemento = Dy(0)
  Codi_Elemento = Dy(1)
  Tipo_Elem_Rel = Dy(2)
  Codi_Elem_Rel = Dy(3)
  
  Dy.MoveNext

  Screen.MousePointer = vbDefault


End Function


Function Srv_Init_Db(ByVal DBName As String, ByVal Uid As String, ByVal Pwd As String)
  On Error Resume Next
  Set GPBase = New ADODB.Connection
  With GPBase
        .CursorLocation = adUseClient
        .CommandTimeout = 0
        .ConnectionString = "Provider=MSDAORA.1;User ID=" & Uid & ";Data Source=" & DBName & ";Password=" & Pwd
        .Open
  End With
  If Err <> 0 Then
    MsgBox "      Se produjo en error al abrir la BASE DE DATOS :" + Chr(13) + Chr(13) + (Err.Description)
    Srv_Init_Db = False
  Else
    Srv_Init_Db = True
  End If
End Function

Function Srv_Paquete_Close()
    On Error Resume Next
    Dy.Close
End Function

Function Srv_Paquete_Open(ByVal codi_paquete As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next

    s = "select codi_paquete,desc_paquete,fech_inivigen,fech_finvigen,max_venta,max_renta,max_equipo,max_servicio"
    f = " from siat_paquete "

    sql = s + f + w

    Set Dy = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_Paquete_Open = -1
    Else
      Dy.MoveLast
      Srv_Paquete_Open = Dy.RecordCount
      Dy.MoveFirst
    End If
    Screen.MousePointer = vbDefault


End Function

Function Srv_Paquete_Read(codi_paquete As Integer, Desc_Paquete As String, fech_inivigen As String, fech_finvigen As String, Max_Venta As Integer, Max_Renta As Integer, Max_Equipo As Integer, Max_Servicio As Integer)
  Screen.MousePointer = vbHourglass
  Srv_Paquete_Read = True

  On Error Resume Next
  
  If Dy.EOF Then
    Srv_Paquete_Read = False
    Exit Function
  End If

  codi_paquete = 0
  Desc_Paquete = ""
  fech_inivigen = ""
  fech_finvigen = ""
  Max_Venta = 0
  Max_Renta = 0
  Max_Equipo = 0
  Max_Servicio = 0

  codi_paquete = Dy(0)
  Desc_Paquete = Dy(1)
  fech_inivigen = Dy(2)
  fech_finvigen = Dy(3)
  Max_Venta = Dy(4)
  Max_Renta = Dy(5)
  Max_Equipo = Dy(6)
  Max_Servicio = Dy(7)

  Dy.MoveNext

  Screen.MousePointer = vbDefault

End Function

Function Srv_Paquetes_Close()
    On Error Resume Next
    Dy.Close
End Function

Function Srv_Paquetes_Open(ByVal p_vigentes As Integer, ByVal p_novigentes As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next

    s = "select codi_paquete,desc_paquete,fech_inivigen,fech_finvigen,max_venta,max_renta,max_equipo,max_servicio,codi_Agrupa, tipo_producto, segmento, prioritario, flag_habilitado, flag_promo, alta_sva"
    f = " from siat_paquete p "
        
    If p_vigentes = 0 And p_novigentes = 0 Then
        w = w + " where  1<>1 "
    End If
                
    If p_vigentes = 1 And p_novigentes = 0 Then
        w = w + " where trunc(sysdate,'dd') between p.fech_inivigen and nvl(p.fech_finvigen,sysdate)"
    End If
    
    If p_vigentes = 0 And p_novigentes = 1 Then
        w = w + " where not (trunc(sysdate,'dd') between p.fech_inivigen and nvl(p.fech_finvigen,sysdate)) "
    End If
    
    w = w + " order by codi_paquete"


    sql = s + f + w

    Set Dy = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_Paquetes_Open = -1
    Else
      Dy.MoveLast
      Srv_Paquetes_Open = Dy.RecordCount
      Dy.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_Paquetes_Read(codi_paquete As Integer, Desc_Paquete As String, fech_inivigen As String, fech_finvigen As String, Max_Venta As Integer, Max_Renta As Integer, Max_Equipo As Integer, Max_Servicio As Integer, Codi_Agrupa As String, Tipo_Producto As Integer, Tipo_Segmento As Integer, prioritario As String, habilitado As String, promocion As String, altaSVA As String)

  Srv_Paquetes_Read = True

  On Error Resume Next
  
  If Dy.EOF Then
    Srv_Paquetes_Read = False
    Exit Function
  End If

  codi_paquete = 0
  Desc_Paquete = ""
  fech_inivigen = ""
  fech_finvigen = ""
  Max_Venta = 0
  Max_Renta = 0
  Max_Equipo = 0
  Max_Servicio = 0
  Tipo_Producto = 0
  Tipo_Segmento = 0
  prioritario = ""
  

  Codi_Agrupa = ""
  habilitado = ""
  promocion = ""
  altaSVA = ""

  codi_paquete = Dy(0)
  Desc_Paquete = Dy(1)
  fech_inivigen = Dy(2)
  fech_finvigen = Dy(3)
  Max_Venta = Dy(4)
  Max_Renta = Dy(5)
  Max_Equipo = Dy(6)
  Max_Servicio = Dy(7)

  Codi_Agrupa = Dy(8)
  Tipo_Producto = Dy(9)
  Tipo_Segmento = Dy(10)
  prioritario = Dy(11)
  habilitado = Dy(12)
  promocion = Dy(13)
  altaSVA = Dy(14)

  Dy.MoveNext
End Function

Function Srv_Parame_Close()
    On Error Resume Next
    Dy_Desc.Close
End Function

Function Srv_Parame_Open(ByVal codi_grupo As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next
        'si es cb_prioritario ejecuta este SQL
        If codi_grupo = 265 Then
        s = "SELECT '0' CODI_ELEM,'SIN PRIORIDAD' DESC_PARAME FROM DUAL UNION SELECT ITEM_UNO, DESC_PARAME"
    f = " FROM PART_PARAME "
    w = " WHERE CODI_GRUPO=" + Str(codi_grupo)
    w = w + " ORDER BY CODI_ELEM DESC"
        sql = s + f + w
        Else
        s = "select codi_elem,desc_parame"
    f = " from part_parame "
    w = " where codi_grupo=" + Str(codi_grupo)
    w = w + " order by to_number(decode(codi_elem,'U','0',codi_elem))"
    sql = s + f + w
        End If
    
    Set Dy_Desc = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      'Srv_Paquetes_Open = fn_error(sql)
      Srv_Parame_Open = -1
    Else
      Dy_Desc.MoveLast
      Srv_Parame_Open = Dy_Desc.RecordCount
      Dy_Desc.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_Parame_Read(codi_elem As String, desc_parame As String)
  Screen.MousePointer = vbHourglass
  Srv_Parame_Read = True

  On Error Resume Next
  
  If Dy_Desc.EOF Then
    Srv_Parame_Read = False
    Exit Function
  End If

  codi_elem = ""
  desc_parame = ""

  codi_elem = Dy_Desc(0)
  desc_parame = Dy_Desc(1)

  Dy_Desc.MoveNext

  Screen.MousePointer = vbDefault

End Function

Function Srv_PqConcepto_Close()
    On Error Resume Next
    Dy_Desc.Close
End Function

Function Srv_PqConcepto_Open(ByVal Codi_Concepto As Long) As Integer
    Dim s As String
    On Error Resume Next
    
    s = "select"
    s = s + " desc_concepto,vlor_concepto,flag_afectiva,tasa_interes,codi_interno,unid_monetar "
    s = s + " from typv_cptostodo "
    s = s + " where codi_concepto = " + Str(Codi_Concepto)
    
    Set Dy_Desc = GPBase.Execute(s, 64)
    
    If Err <> 0 Then
        'Srv_Concepto_Open = fn_error(s)
        Srv_PqConcepto_Open = -1
    Else
        Srv_PqConcepto_Open = Dy_Desc.RecordCount
    End If


End Function

Function Srv_PqConcepto_Read(desc As String) As Integer
    On Error Resume Next

    Srv_PqConcepto_Read = True

    If Dy_Desc.EOF Then
      Srv_PqConcepto_Read = False
      Exit Function
    End If

    desc = Dy_Desc(0)

    Dy_Desc.MoveNext

End Function

Function Srv_PqTipoEqui_Close()
  On Error Resume Next
  Dy_Desc.Close
End Function

Function Srv_PqTipoEqui_Open(ByVal codigo As String)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String

    On Error Resume Next

    s = "select desc_tipoequi,flag_pidecant,flag_estandar "
    f = " from SIAT_TIPOEQUI"
    w = " where codi_tipoequi='" + codigo + "'"
    w = w + " and fech_vigencia is null"

    sql = s + f + w

    Set Dy_Desc = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      'Srv_TipoEqui_Open = fn_error(sql)
      Srv_PqTipoEqui_Open = -1
    Else
      Dy_Desc.MoveLast
      Srv_PqTipoEqui_Open = Dy_Desc.RecordCount
      Dy_Desc.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_PqTipoEqui_Read(descripcion As String)
  Srv_PqTipoEqui_Read = True

  On Error Resume Next
  
  If Dy_Desc.EOF Then
    Srv_PqTipoEqui_Read = False
    Exit Function
  End If

  descripcion = ""
  
  descripcion = Dy_Desc(0)

  Dy_Desc.MoveNext


End Function

Function Srv_PqTipoServ_Close()
    On Error Resume Next
    Dy_Desc.Close
End Function

Function Srv_PqTipoServ_Open(ByVal codigo As String)
    Dim s As String, f As String, w As String, sql As String
    
    On Error Resume Next
    
    s = "select "
    s = s + "desc_tiposerv"
    f = " from siat_tiposerv t"
    w = " where "
    w = w + "  tipo_producto='S'"
    w = w + "  and t.codi_tiposerv='" + Trim$(codigo) + "'"
    
    sql = s + f + w + " order by to_number(codi_tiposerv)"
    
    Err = 0
    Set Dy_Desc = GPBase.Execute(sql, 64)
    
    If Err <> 0 Then
        'Srv_Tiposerv_Open = fn_error(sql)
        Srv_PqTipoServ_Open = -1
    Else
        Srv_PqTipoServ_Open = Dy_Desc.RecordCount
    End If

End Function

Function Srv_PqTipoServ_Read(desc_tiposerv As String)
    Dim Ret As Integer

    On Error Resume Next

    Srv_PqTipoServ_Read = True

    If Dy_Desc.EOF Then
      Srv_PqTipoServ_Read = False
      Exit Function
    End If

    
    desc_tiposerv = ""

    desc_tiposerv = Dy_Desc(0)

    Dy_Desc.MoveNext


End Function

Function Srv_Tipoequi_Close()
    On Error Resume Next
    Dy_Ayuda.Close
End Function

Function Srv_Tipoequi_Open(ByVal codigo As String, ByVal descripcion As String)
    Dim s As String, f As String, w As String, sql As String
    
    On Error Resume Next
    
    s = "select "
    s = s + "codi_tipoequi,desc_tipoequi"
    f = " from siat_tipoequi t"
    w = " where "
    w = w + " t.codi_tipoequi like decode(rtrim(ltrim('" + codigo + "')),'',t.codi_tipoequi,rtrim(ltrim('" + codigo + "'))) and"
    w = w + " t.desc_tipoequi like '%'||decode(rtrim('" + descripcion + "'),'',t.desc_tipoequi,rtrim('" + descripcion + "'))||'%'"
    w = w + " order by to_number(codi_tipoequi)"

    sql = s + f + w
    
    Err = 0
    Set Dy_Ayuda = GPBase.Execute(sql, 64)
    
    If Err <> 0 Then
        Srv_Tipoequi_Open = fn_error(Err.Description, sql)
        Srv_Tipoequi_Open = -1
    Else
        Srv_Tipoequi_Open = Dy_Ayuda.RecordCount
    End If
    
    
End Function

Function Srv_Tipoequi_Read(Codi_Tipoequi As String, Desc_Tipoequi As String)
    Dim Ret As Integer

    On Error Resume Next

    Srv_Tipoequi_Read = True

    If Dy_Ayuda.EOF Then
      Srv_Tipoequi_Read = False
      Exit Function
    End If
    Codi_Tipoequi = ""
    Desc_Tipoequi = ""

    Codi_Tipoequi = Dy_Ayuda(0)
    Desc_Tipoequi = Dy_Ayuda(1)

    Dy_Ayuda.MoveNext

End Function

Function Srv_Tiposerv_Close()
    On Error Resume Next
    Dy_Ayuda.Close
End Function

Function Srv_Tiposerv_Open(ByVal codigo As String, ByVal descripcion As String, Codi_Agrupa As String)
    Dim s As String, f As String, w As String, sql As String
    
    On Error Resume Next
    
    s = "select "
    s = s + "codi_tiposerv,desc_tiposerv"
    f = " from siat_tiposerv t"
    w = " where "
    w = w + " t.tipo_producto = 'S' and"
    w = w + " t.codi_tiposerv like decode(rtrim(ltrim('" + codigo + "')),'',t.codi_tiposerv,rtrim(ltrim('" + codigo + "'))) and"
    w = w + " t.desc_tiposerv like '%'||decode(rtrim('" + descripcion + "'),'',t.desc_tiposerv,rtrim('" + descripcion + "'))||'%' and"
    w = w + " t.codi_agrupa like '%'||decode(rtrim('" + Codi_Agrupa + "'),'',t.codi_agrupa,rtrim('" + Codi_Agrupa + "'))||'%'"
    w = w + " order by to_number(codi_tiposerv)"
    sql = s + f + w
    Err = 0
    Set Dy_Ayuda = GPBase.Execute(sql, 64)
    
    If Err <> 0 Then
        Srv_Tiposerv_Open = fn_error(Err.Description, sql)
        Srv_Tiposerv_Open = -1
    Else
        Srv_Tiposerv_Open = Dy_Ayuda.RecordCount
    End If

End Function

Function Srv_Tiposerv_Read(codi_tiposerv As String, desc_tiposerv As String)
    Dim Ret As Integer

    On Error Resume Next

    Srv_Tiposerv_Read = True

    If Dy_Ayuda.EOF Then
      Srv_Tiposerv_Read = False
      Exit Function
    End If
    codi_tiposerv = ""
    desc_tiposerv = ""

    codi_tiposerv = Dy_Ayuda(0)
    desc_tiposerv = Dy_Ayuda(1)

    Dy_Ayuda.MoveNext


End Function

Function Srv_Vendedores_Close()
    On Error Resume Next
    Dy.Close

End Function

Function Srv_Vendedores_Open(ByVal codigo As String, ByVal descripcion As String)
    Dim s As String, f As String, w As String, sql As String
    
    On Error Resume Next
    
    s = "select "
    s = s + "codi_vendedor,nomb_vendedor"
    f = " from siat_vendedor t"
    w = " where "
    w = w + " t.codi_vendedor like decode(rtrim(ltrim('" + codigo + "')),'',t.codi_vendedor,rtrim(ltrim('" + codigo + "'))) and"
    w = w + " t.nomb_vendedor like '%'||decode(rtrim('" + descripcion + "'),'',t.nomb_vendedor,rtrim('" + descripcion + "'))||'%'"
    w = w + " order by codi_vendedor"

    sql = s + f + w

    Err = 0
    Set Dy = GPBase.Execute(sql, 64)
    
    If Err <> 0 Then
        Srv_Vendedores_Open = fn_error(Err.Description, sql)
        Srv_Vendedores_Open = -1
    Else
        Srv_Vendedores_Open = Dy.RecordCount
    End If

End Function

Function Srv_Vendedores_Read(codi_vendedor As String, nomb_vendedor As String)
    Dim Ret As Integer

    On Error Resume Next

    Srv_Vendedores_Read = True

    If Dy.EOF Then
      Srv_Vendedores_Read = False
      Exit Function
    End If

    codi_vendedor = ""
    nomb_vendedor = ""

    codi_vendedor = Dy(0)
    nomb_vendedor = Dy(1)

    Dy.MoveNext

End Function

Function SrvCloseDb()
    On Error Resume Next
    GPBase.Close
End Function

Sub Su_InFecha(Texto As TextBox)
    Dim X As String, P As Integer, N As Integer
    
    X = Texto.Text
    While InStr(X, "-") <> 0
    N = N + 1
    X = Mid(X, 1, InStr(X, "-") - 1) + Mid(X, InStr(X, "-") + 1)
    Wend
    
    P = Texto.SelStart
    Select Case Len(X)
    Case 3 To 4:
        If P > 2 Then
        P = P + 1 - N
        End If
        Texto.Text = Left$(X, 2) + "-" + Mid(X, 3)
    Case 5 To 6:
        If P > 2 Then
        P = P + 2 - N
        End If
        Texto.Text = Left$(X, 2) + "-" + Mid(X, 3, 2) + "-" + Mid(X, 5)
    End Select

    Texto.SelStart = P

End Sub

'Sub su_mensaje(Mensaje As String)
'    fmensaje.LB_MENSAJE.Caption = Mensaje
'    fmensaje.Refresh
'End Sub


Function Srv_Seleccionados_Close()
    On Error Resume Next
    Dy_Seleccionados.Close
End Function

Function Srv_Seleccionados_Open(ByVal codi_elemh As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next
 
    s = "select tipo_peticion, clase_peticion, subclase, clase "
    f = "   from siav_clases_peti t "
    w = "  where t.clase_peticion in (select r.codi_elemp "
    w = w + "   from part_relacion r "
    w = w + "  Where r.codi_grupop = 11 "
    w = w + "    and r.codi_grupoh = 102 "
    w = w + "    and r.codi_elemh = " + Str(codi_elemh) + ")"
    w = w + "  order by t.tipo_peticion, t.clase_peticion "
 
    sql = s + f + w

    Set Dy_Seleccionados = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_Seleccionados_Open = -1
    Else
      Dy_Seleccionados.MoveLast
      Srv_Seleccionados_Open = Dy_Seleccionados.RecordCount
      Dy_Seleccionados.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_Seleccionados_Read(Tipo_Peticion As String, clase_peticion As String, subclase As String, clase As String)
  Screen.MousePointer = vbHourglass
  Srv_Seleccionados_Read = True

  On Error Resume Next
  
  If Dy_Seleccionados.EOF Then
    Srv_Seleccionados_Read = False
    Exit Function
  End If

  Tipo_Peticion = ""
  clase_peticion = ""
  subclase = ""
  clase = ""

  Tipo_Peticion = Dy_Seleccionados(0)
  clase_peticion = Dy_Seleccionados(1)
  subclase = Dy_Seleccionados(2)
  clase = Dy_Seleccionados(3)

  Dy_Seleccionados.MoveNext

  Screen.MousePointer = vbDefault

End Function

Function Srv_Disponibles_Close()
    On Error Resume Next
    Dy_Seleccionados.Close
End Function

Function Srv_Disponibles_Open(ByVal codi_elemh As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next
    
    s = "select tipo_peticion, clase_peticion, subclase, clase "
    f = "   from siav_clases_peti t "
    w = "  where t.clase_peticion not in (select r.codi_elemp "
    w = w + "   from part_relacion r "
    w = w + "  Where r.codi_grupop = 11 "
    w = w + "    and r.codi_grupoh = 102 "
    w = w + "    and r.codi_elemh = " + Str(codi_elemh) + ")"
    w = w + "  order by t.tipo_peticion, t.clase_peticion "
 
    sql = s + f + w

    Set Dy_Disponibles = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_Disponibles_Open = -1
    Else
      Dy_Disponibles.MoveLast
      Srv_Disponibles_Open = Dy_Disponibles.RecordCount
      Dy_Disponibles.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_Disponibles_Read(Tipo_Peticion As String, clase_peticion As String, subclase As String, clase As String)
  Screen.MousePointer = vbHourglass
  Srv_Disponibles_Read = True

  On Error Resume Next
  
  If Dy_Disponibles.EOF Then
    Srv_Disponibles_Read = False
    Exit Function
  End If

  Tipo_Peticion = ""
  clase_peticion = ""
  subclase = ""
  clase = ""

  Tipo_Peticion = Dy_Disponibles(0)
  clase_peticion = Dy_Disponibles(1)
  subclase = Dy_Disponibles(2)
  clase = Dy_Disponibles(3)
  
  Dy_Disponibles.MoveNext

  Screen.MousePointer = vbDefault

End Function


Function Srv_Canal_Seleccionados_Close()
    On Error Resume Next
    Dy_Canal_Seleccionados.Close
End Function

Function Srv_Canal_Seleccionados_Open(ByVal codi_paquete As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next
    
    s = " select c.codi_canal_vta,"
    s = s + " c.desc_canal_vta,"
    s = s + " decode(pc.codi_canal_vta, null, 0, 1) seleccionada"
    f = " from siat_paquete_canal_vta pc, sivt_canal_vtas c"
    w = " where c.codi_canal_vta = pc.codi_canal_vta(+) and c.codi_estado=1 "
    w = w + " and pc.codi_paquete(+) = " + Str(codi_paquete)
    w = w + " order by c.codi_canal_vta , c.desc_canal_vta"
   
    sql = s + f + w

    Set Dy_Canal_Seleccionados = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_Canal_Seleccionados_Open = -1
    Else
      Dy_Canal_Seleccionados.MoveLast
      Srv_Canal_Seleccionados_Open = Dy_Canal_Seleccionados.RecordCount
      Dy_Canal_Seleccionados.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_Canal_Seleccionados_Read(codi_canal_vta As Integer, desc_canal_vta As String, ByRef seleccionado As Integer)
  Srv_Canal_Seleccionados_Read = True

  On Error Resume Next
  
  If Dy_Canal_Seleccionados.EOF Then
    Srv_Canal_Seleccionados_Read = False
    Exit Function
  End If

  codi_canal_vta = 0
  desc_canal_vta = ""
  seleccionado = ""

  codi_canal_vta = Val(Dy_Canal_Seleccionados(0))
  desc_canal_vta = Dy_Canal_Seleccionados(1)
  seleccionado = Dy_Canal_Seleccionados(2)

  Dy_Canal_Seleccionados.MoveNext
End Function
'
Function Srv_CanalOferta_Seleccionados_Close()
    On Error Resume Next
    Dy_CanalOferta_Seleccionados.Close
End Function

Function Srv_CanalOferta_Seleccionados_Open(ByVal codi_oferta As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next
    
    s = " select c.codi_canal_vta,"
    s = s + " c.desc_canal_vta,"
    s = s + " decode(pc.codi_canal_vta, null, 0, 1) seleccionada"
    f = " from siat_oferta_canal_vta pc, sivt_canal_vtas c"
    w = " where c.codi_canal_vta = pc.codi_canal_vta(+) and c.codi_estado=1 "
    w = w + " and pc.codi_oferta(+) = " + Str(codi_oferta)
    w = w + " order by c.codi_canal_vta , c.desc_canal_vta"
   
    sql = s + f + w

    Set Dy_CanalOferta_Seleccionados = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_CanalOferta_Seleccionados_Open = -1
    Else
      Dy_CanalOferta_Seleccionados.MoveLast
      Srv_CanalOferta_Seleccionados_Open = Dy_CanalOferta_Seleccionados.RecordCount
      Dy_CanalOferta_Seleccionados.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_CanalOferta_Seleccionados_Read(codi_canal_vta As Integer, desc_canal_vta As String, ByRef seleccionado As Integer)
  Srv_CanalOferta_Seleccionados_Read = True

  On Error Resume Next
  
  If Dy_CanalOferta_Seleccionados.EOF Then
    Srv_CanalOferta_Seleccionados_Read = False
    Exit Function
  End If

  codi_canal_vta = 0
  desc_canal_vta = ""
  seleccionado = ""

  codi_canal_vta = Val(Dy_CanalOferta_Seleccionados(0))
  desc_canal_vta = Dy_CanalOferta_Seleccionados(1)
  seleccionado = Dy_CanalOferta_Seleccionados(2)

  Dy_CanalOferta_Seleccionados.MoveNext
End Function

'
Function Srv_Loca_Seleccionados_Close()
    On Error Resume Next
    Dy_Loca_Seleccionados.Close
End Function

Function Srv_Loca_Seleccionados_Open(ByVal codi_paquete As Integer)
    Screen.MousePointer = vbHourglass
    Dim s As String, f  As String, w As String, sql As String
    
    On Error Resume Next
    
    s = " select l.codi_localida,"
    s = s + " l.desc_localida,"
    s = s + " to_char(pl.fech_inivigen,'dd-mm-yyyy'),"
    s = s + " to_char(pl.fech_finvigen,'dd-mm-yyyy'),"
    s = s + " decode(pl.codi_localida, null, 0, 1) seleccionada"
    f = " from siat_paquete_localida pl, part_localida l"
    w = " where l.codi_localida = pl.codi_localida(+)"
    w = w + " and pl.codi_paquete(+) = " + Str(codi_paquete)
    w = w + " order by l.codi_localida , l.desc_localida"
   
    sql = s + f + w

    Set Dy_Loca_Seleccionados = GPBase.Execute(sql, 64)

    If Err <> 0 Then
      Srv_Loca_Seleccionados_Open = -1
    Else
      Dy_Loca_Seleccionados.MoveLast
      Srv_Loca_Seleccionados_Open = Dy_Loca_Seleccionados.RecordCount
      Dy_Loca_Seleccionados.MoveFirst
    End If
    Screen.MousePointer = vbDefault

End Function

Function Srv_Loca_Seleccionados_Read(codi_localida As String, desc_localida As String, fech_ini As String, fech_fin As String, seleccionada As Integer)
  Srv_Loca_Seleccionados_Read = True

  On Error Resume Next
  
  If Dy_Loca_Seleccionados.EOF Then
    Srv_Loca_Seleccionados_Read = False
    Exit Function
  End If

  codi_localida = ""
  desc_localida = ""
  fech_ini = ""
  fech_fin = ""
  seleccionada = ""

  codi_localida = Dy_Loca_Seleccionados(0)
  desc_localida = Dy_Loca_Seleccionados(1)
  fech_ini = Dy_Loca_Seleccionados(2)
  fech_fin = Dy_Loca_Seleccionados(3)
  seleccionada = Dy_Loca_Seleccionados(4)

  Dy_Loca_Seleccionados.MoveNext

  

End Function


Public Function GetPromociones(ByVal Filtro As Integer) As Long
    'Extrae las promociones del sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_promocion, desc_promocion,  "
    sSql1 = sSql1 & "codi_promocion, desc_promocion,  "
    sSql = sSql & "to_char(fech_creacion,'dd-mm-yyyy') creacion,  "
    sSql1 = sSql1 & "to_char(fech_creacion,'dd-mm-yyyy') creacion,  "
    sSql = sSql & "to_char(fech_inicio,'dd-mm-yyyy') inicio,  "
    sSql1 = sSql1 & "to_char(fech_inicio,'dd-mm-yyyy') inicio,  "
    sSql = sSql & "to_char(fech_fin,'dd-mm-yyyy') fin "
    sSql1 = sSql1 & "to_char(fech_fin,'dd-mm-yyyy') fin "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_promocion  "
    sSql1 = sSql1 & "siat_pr_promocion  "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "siaa_promociones.PromocionFiltro(?,codi_promocion) = 1  "
    sSql1 = sSql1 & "siaa_promociones.PromocionFiltro(" & Str(Filtro) & ",codi_promocion) = 1  "
    sSql = sSql & "order by codi_promocion "
    sSql1 = sSql1 & "order by codi_promocion "
    On Error GoTo Error
    Set vrGetPromocion = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-filtro", adInteger, adParamInput, 1, Filtro)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPromocion.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPromocion.EOF Then
        vrGetPromocion.MoveLast
        CantidadTuplas = vrGetPromocion.RecordCount
        vrGetPromocion.MoveFirst
        GetPromociones = CantidadTuplas
    Else
        GetPromociones = 0
    End If
    Exit Function

Error:
    Set vrGetPromocion = Nothing
    Screen.MousePointer = vbDefault
    GetPromociones = fn_error(Err.Description, sSql1)
    GetPromociones = -1
End Function

Function GetPromocionesClose() As Integer

    If Not vrGetPromocion Is Nothing Then
        vrGetPromocion.Close
        Set vrGetPromocion = Nothing
    End If
End Function

Function GetPromocionesRead(ByRef codi_promocion As Long, ByRef desc_promocion As String, ByRef fecha_creacion As String, ByRef fecha_inicio As String, ByRef fecha_fin As String) As Integer
    Dim res As Integer

    GetPromocionesRead = True

    If vrGetPromocion Is Nothing Then
        GetPromocionesRead = False
        Exit Function
    End If
    If vrGetPromocion.EOF Then
        res = GetPromocionesClose()
        GetPromocionesRead = False
        Exit Function
    End If

    codi_promocion = gfnDevuelveValorCampo("LONG", vrGetPromocion.Fields(0).Type, vrGetPromocion.Fields(0))
    desc_promocion = gfnDevuelveValorCampo("STRING", vrGetPromocion.Fields(1).Type, vrGetPromocion.Fields(1))
    fecha_creacion = gfnDevuelveValorCampo("STRING", vrGetPromocion.Fields(2).Type, vrGetPromocion.Fields(2))
    fecha_inicio = gfnDevuelveValorCampo("STRING", vrGetPromocion.Fields(3).Type, vrGetPromocion.Fields(3))
    fecha_fin = gfnDevuelveValorCampo("STRING", vrGetPromocion.Fields(4).Type, vrGetPromocion.Fields(4))

    vrGetPromocion.MoveNext

End Function

Public Function GetPromLoca(ByVal CodiPromocion As Long) As Long
    'Lista las localidades asociadas a la promocion
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "l.codi_localida, l.desc_localida, decode(pl.codi_localida, null, 0, 1) seleccionada "
    sSql1 = sSql1 & "l.codi_localida, l.desc_localida "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_promo_localidad pl, part_localida l "
    sSql1 = sSql1 & "siat_pr_promo_localidad pl, part_localida l "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_promocion= ? "
    sSql1 = sSql1 & "codi_promocion= " & Str(CodiPromocion) & " "
    sSql = sSql & "and pl.codi_localida = l.codi_localida  "
    sSql1 = sSql1 & "and pl.codi_localida = l.codi_localida  "
    sSql = sSql & "order by l.codi_localida "
    sSql1 = sSql1 & "order by l.codi_localida "
    
    On Error GoTo Error
    Set vrGetPromLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiPromocion", adInteger, adParamInput, 6, CodiPromocion)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPromLoca.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPromLoca.EOF Then
        vrGetPromLoca.MoveLast
        CantidadTuplas = vrGetPromLoca.RecordCount
        vrGetPromLoca.MoveFirst
        GetPromLoca = CantidadTuplas
    Else
        GetPromLoca = -1
    End If
    Exit Function

Error:
    Set vrGetPromLoca = Nothing
    Screen.MousePointer = vbDefault
    GetPromLoca = fn_error(Err.Description, sSql1)
    GetPromLoca = -1
End Function

Function GetPromLocaClose() As Integer

    If Not vrGetPromLoca Is Nothing Then
        vrGetPromLoca.Close
        Set vrGetPromLoca = Nothing
    End If
End Function

Function GetPromLocaRead(ByRef codi_localida As String, ByRef desc_localidad As String) As Integer
    Dim res As Integer

    GetPromLocaRead = True

    If vrGetPromLoca Is Nothing Then
        GetPromLocaRead = False
        Exit Function
    End If
    If vrGetPromLoca.EOF Then
        res = GetPromLocaClose()
        GetPromLocaRead = False
        Exit Function
    End If

    codi_localida = gfnDevuelveValorCampo("STRING", vrGetPromLoca.Fields(0).Type, vrGetPromLoca.Fields(0))
    desc_localidad = gfnDevuelveValorCampo("STRING", vrGetPromLoca.Fields(1).Type, vrGetPromLoca.Fields(1))
    'seleccionada = gfnDevuelveValorCampo("LONG", vrGetPromLoca.Fields(2).Type, vrGetPromLoca.Fields(2))

    vrGetPromLoca.MoveNext

End Function

Public Function GetPromProd(ByVal CodiPromocion As Long) As Long
    'Extrae el producto asociado a una promocion
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_producto "
    sSql1 = sSql1 & "codi_producto "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_promo_produ "
    sSql1 = sSql1 & "siat_pr_promo_produ "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_promocion= ? "
    sSql1 = sSql1 & "codi_promocion= " & Str(CodiPromocion) & " "
    On Error GoTo Error
    Set vrGetPromProd = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiPromocion", adInteger, adParamInput, 6, CodiPromocion)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPromProd.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPromProd.EOF Then
        vrGetPromProd.MoveLast
        CantidadTuplas = vrGetPromProd.RecordCount
        vrGetPromProd.MoveFirst
        GetPromProd = CantidadTuplas
    Else
        GetPromProd = -1
    End If
    Exit Function

Error:
    Set vrGetPromProd = Nothing
    Screen.MousePointer = vbDefault
    GetPromProd = fn_error(Err.Description, sSql1)
    GetPromProd = -1
End Function

Function GetPromProdClose() As Integer

    If Not vrGetPromProd Is Nothing Then
        vrGetPromProd.Close
        Set vrGetPromProd = Nothing
    End If
End Function

Function GetPromProdRead(ByRef codi_producto As String) As Integer
    Dim res As Integer

    GetPromProdRead = True

    If vrGetPromProd Is Nothing Then
        GetPromProdRead = False
        Exit Function
    End If
    If vrGetPromProd.EOF Then
        res = GetPromProdClose()
        GetPromProdRead = False
        Exit Function
    End If

    codi_producto = gfnDevuelveValorCampo("STRING", vrGetPromProd.Fields(0).Type, vrGetPromProd.Fields(0))

    vrGetPromProd.MoveNext

End Function

Public Function GetPromEqui(ByVal CodiPromocion As Long) As Long
    'Extrae los equipos asociados a un producto de una promocion
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "pe.codi_equipo, m.desc_modelo, pe.tipo_relacion,  "
    sSql1 = sSql1 & "pe.codi_equipo, m.desc_modelo, pe.tipo_relacion,  "
    sSql = sSql & "pe.cant_permanencia,  "
    sSql1 = sSql1 & "pe.cant_permanencia,  "
    sSql = sSql & "to_char(fech_inicio,'dd-mm-yyyy'), "
    sSql1 = sSql1 & "to_char(fech_inicio,'dd-mm-yyyy'), "
    sSql = sSql & "to_char(fech_termino,'dd-mm-yyyy') "
    sSql1 = sSql1 & "to_char(fech_termino,'dd-mm-yyyy') "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_promo_equi pe, siat_inv_modelo m "
    sSql1 = sSql1 & "siat_pr_promo_equi pe, siat_inv_modelo m "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_promocion= ? "
    sSql1 = sSql1 & "codi_promocion= " & Str(CodiPromocion) & " "
    sSql = sSql & "and pe.codi_equipo = m.codi_tipoequi  "
    sSql1 = sSql1 & "and pe.codi_equipo = m.codi_tipoequi  "
    sSql = sSql & "order by m.desc_modelo "
    sSql1 = sSql1 & "order by m.desc_modelo "
    On Error GoTo Error
    Set vrGetPromEqui = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiPromocion", adInteger, adParamInput, 6, CodiPromocion)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPromEqui.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPromEqui.EOF Then
        vrGetPromEqui.MoveLast
        CantidadTuplas = vrGetPromEqui.RecordCount
        vrGetPromEqui.MoveFirst
        GetPromEqui = CantidadTuplas
    Else
        GetPromEqui = 0
    End If
    Exit Function

Error:
    Set vrGetPromEqui = Nothing
    Screen.MousePointer = vbDefault
    GetPromEqui = fn_error(Err.Description, sSql1)
    GetPromEqui = -1
End Function

Function GetPromEquiClose() As Integer

    If Not vrGetPromEqui Is Nothing Then
        vrGetPromEqui.Close
        Set vrGetPromEqui = Nothing
    End If
End Function

Function GetPromEquiRead(ByRef codi_equipo As String, ByRef desc_modelo As String, ByRef tipo_relacion As String, ByRef cant_permanencia As String, ByRef fecha_inicio As String, ByRef fecha_termino As String) As Integer
    Dim res As Integer

    GetPromEquiRead = True

    If vrGetPromEqui Is Nothing Then
        GetPromEquiRead = False
        Exit Function
    End If
    If vrGetPromEqui.EOF Then
        res = GetPromEquiClose()
        GetPromEquiRead = False
        Exit Function
    End If

    codi_equipo = gfnDevuelveValorCampo("STRING", vrGetPromEqui.Fields(0).Type, vrGetPromEqui.Fields(0))
    desc_modelo = gfnDevuelveValorCampo("STRING", vrGetPromEqui.Fields(1).Type, vrGetPromEqui.Fields(1))
    tipo_relacion = gfnDevuelveValorCampo("STRING", vrGetPromEqui.Fields(2).Type, vrGetPromEqui.Fields(2))
    cant_permanencia = gfnDevuelveValorCampo("STRING", vrGetPromEqui.Fields(3).Type, vrGetPromEqui.Fields(3))
    fecha_inicio = gfnDevuelveValorCampo("STRING", vrGetPromEqui.Fields(4).Type, vrGetPromEqui.Fields(4))
    fecha_termino = gfnDevuelveValorCampo("STRING", vrGetPromEqui.Fields(5).Type, vrGetPromEqui.Fields(5))

    vrGetPromEqui.MoveNext

End Function

Public Function GetPromCobr(ByVal CodiPromocion As Long) As Long
    'Extrae los cobros asociados a una Promocion
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "pc.codi_cobro, c.desc_concepto, pc.tipo_cobro,  "
    sSql1 = sSql1 & "pc.codi_cobro, c.desc_concepto, pc.tipo_cobro,  "
    sSql = sSql & "pc.tipo_origen, pc.codi_origen, pc.cant_gracia,  "
    sSql1 = sSql1 & "pc.tipo_origen, pc.codi_origen, pc.cant_gracia,  "
    sSql = sSql & "pc.cant_duracion, pc.cant_mincuotas, pc.cant_maxcuotas,  "
    sSql1 = sSql1 & "pc.cant_duracion, pc.cant_mincuotas, pc.cant_maxcuotas,  "
    sSql = sSql & "pc.vlor_cobro "
    sSql1 = sSql1 & "pc.vlor_cobro "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_promo_cobros pc, typv_cptosvig c "
    sSql1 = sSql1 & "siat_pr_promo_cobros pc, typv_cptosvig c "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "pc.codi_promocion = ? "
    sSql1 = sSql1 & "pc.codi_promocion = " & Str(CodiPromocion) & " "
    sSql = sSql & "and pc.codi_cobro = c.codi_interno "
    sSql1 = sSql1 & "and pc.codi_cobro = c.codi_interno "
    sSql = sSql & " order by pc.tipo_cobro, pc.tipo_origen, pc.codi_origen"
    sSql1 = sSql1 & " order by pc.tipo_cobro, pc.tipo_origen, pc.codi_origen"
    On Error GoTo Error
    Set vrGetPromCobr = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiPromocion", adInteger, adParamInput, 6, CodiPromocion)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPromCobr.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPromCobr.EOF Then
        vrGetPromCobr.MoveLast
        CantidadTuplas = vrGetPromCobr.RecordCount
        vrGetPromCobr.MoveFirst
        GetPromCobr = CantidadTuplas
    Else
        GetPromCobr = 0
    End If
    Exit Function

Error:
    Set vrGetPromCobr = Nothing
    Screen.MousePointer = vbDefault
    GetPromCobr = fn_error(Err.Description, sSql1)
    GetPromCobr = -1
End Function

Function GetPromCobrClose() As Integer

    If Not vrGetPromCobr Is Nothing Then
        vrGetPromCobr.Close
        Set vrGetPromCobr = Nothing
    End If
End Function

Function GetPromCobrRead(ByRef codi_cobro As String, ByRef desc_concepto As String, ByRef tipo_cobro As String, ByRef tipo_origen As String, ByRef codi_origen As String, ByRef cant_gracia As String, ByRef cant_duracion As String, ByRef min_cuotas As String, ByRef max_cuotas As String, ByRef valor_cobro As String) As Integer
    Dim res As Integer

    GetPromCobrRead = True

    If vrGetPromCobr Is Nothing Then
        GetPromCobrRead = False
        Exit Function
    End If
    If vrGetPromCobr.EOF Then
        res = GetPromCobrClose()
        GetPromCobrRead = False
        Exit Function
    End If

    codi_cobro = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(0).Type, vrGetPromCobr.Fields(0))
    desc_concepto = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(1).Type, vrGetPromCobr.Fields(1))
    tipo_cobro = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(2).Type, vrGetPromCobr.Fields(2))
    tipo_origen = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(3).Type, vrGetPromCobr.Fields(3))
    codi_origen = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(4).Type, vrGetPromCobr.Fields(4))
    cant_gracia = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(5).Type, vrGetPromCobr.Fields(5))
    cant_duracion = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(6).Type, vrGetPromCobr.Fields(6))
    min_cuotas = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(7).Type, vrGetPromCobr.Fields(7))
    max_cuotas = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(8).Type, vrGetPromCobr.Fields(8))
    valor_cobro = gfnDevuelveValorCampo("STRING", vrGetPromCobr.Fields(9).Type, vrGetPromCobr.Fields(9))

    vrGetPromCobr.MoveNext

End Function


Public Function GetPromPlan(ByVal CodiPromocion As Long) As Long
    'Lista los planes asociados a la promocion
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "pp.codi_plan, p.desc_plan, pp.cant_permanencia "
    sSql1 = sSql1 & "pp.codi_plan, p.desc_plan, pp.cant_permanencia "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_promo_plan pp, siat_pr_plan p "
    sSql1 = sSql1 & "siat_pr_promo_plan pp, siat_pr_plan p "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "pp.codi_promocion= ? "
    sSql1 = sSql1 & "pp.codi_promocion= " & Str(CodiPromocion) & " "
    sSql = sSql & "and pp.codi_plan = p.codi_plan "
    sSql1 = sSql1 & "and pp.codi_plan = p.codi_plan "
    On Error GoTo Error
    Set vrGetPromPlan = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiPromocion", adInteger, adParamInput, 6, CodiPromocion)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPromPlan.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPromPlan.EOF Then
        vrGetPromPlan.MoveLast
        CantidadTuplas = vrGetPromPlan.RecordCount
        vrGetPromPlan.MoveFirst
        GetPromPlan = CantidadTuplas
    Else
        GetPromPlan = 0
    End If
    Exit Function

Error:
    Set vrGetPromPlan = Nothing
    Screen.MousePointer = vbDefault
    GetPromPlan = fn_error(Err.Description, sSql1)
    GetPromPlan = -1
End Function

Function GetPromPlanClose() As Integer

    If Not vrGetPromPlan Is Nothing Then
        vrGetPromPlan.Close
        Set vrGetPromPlan = Nothing
    End If
End Function

Function GetPromPlanRead(ByRef codi_plan As String, ByRef desc_plan As String, ByRef cant_permanencia As String) As Integer
    Dim res As Integer

    GetPromPlanRead = True

    If vrGetPromPlan Is Nothing Then
        GetPromPlanRead = False
        Exit Function
    End If
    If vrGetPromPlan.EOF Then
        res = GetPromPlanClose()
        GetPromPlanRead = False
        Exit Function
    End If

    codi_plan = gfnDevuelveValorCampo("STRING", vrGetPromPlan.Fields(0).Type, vrGetPromPlan.Fields(0))
    desc_plan = gfnDevuelveValorCampo("STRING", vrGetPromPlan.Fields(1).Type, vrGetPromPlan.Fields(1))
    cant_permanencia = gfnDevuelveValorCampo("STRING", vrGetPromPlan.Fields(2).Type, vrGetPromPlan.Fields(2))

    vrGetPromPlan.MoveNext

End Function


Public Function SrvEquipoDescOpen(ByVal codigo As String) As Integer
        '
        Dim sSql As String
        Dim adoCmd As New ADODB.Command
        Dim CantidadTuplas As Integer

        sSql = "select "
        sSql = sSql & "desc_tipoequi,flag_pidecant,flag_estandar,codi_tipo "
        sSql = sSql + " from "
        sSql = sSql & "siat_tipoequi "
        sSql = sSql + " where "
        sSql = sSql & "codi_tipoequi=? and  "
        sSql = sSql & "fech_vigencia is null "
        On Error GoTo Error
        Set vrEQUIPODESC = New ADODB.Recordset
        With adoCmd
                .ActiveConnection = GPBase 'RS 4-MAR-05
                .CommandType = adCmdText
                .CommandText = sSql
                .Parameters.Append adoCmd.CreateParameter("", adVarChar, adParamInput, 4, codigo)
        End With
        Screen.MousePointer = vbHourglass
         vrEQUIPODESC.Open adoCmd 'RS1 4-MAR-05
        Screen.MousePointer = vbDefault

        If Not vrEQUIPODESC.EOF Then
                vrEQUIPODESC.MoveLast
                CantidadTuplas = vrEQUIPODESC.RecordCount
                vrEQUIPODESC.MoveFirst
                SrvEquipoDescOpen = CantidadTuplas
        Else
                SrvEquipoDescOpen = -1
        End If
        Exit Function

Error:
        Set vrEQUIPODESC = Nothing
        Screen.MousePointer = vbDefault
        SrvEquipoDescOpen = fn_error(Err.Description, sSql)
        SrvEquipoDescOpen = -1
End Function

Function SrvEquipoDescClose() As Integer

        If Not vrEQUIPODESC Is Nothing Then
                vrEQUIPODESC.Close
                Set vrEQUIPODESC = Nothing
        End If
End Function

Function SrvEquipoDescRead(ByRef descripcion As String, ByRef pide_cantidad As String, ByRef flag_estandar As String, ByRef Codi_Tipo As Integer) As Integer
        Dim res As Integer

        SrvEquipoDescRead = True

        If vrEQUIPODESC Is Nothing Then
                SrvEquipoDescRead = False
                Exit Function
        End If
        If vrEQUIPODESC.EOF Then
                res = SrvEquipoDescClose()
                SrvEquipoDescRead = False
                Exit Function
        End If

        descripcion = gfnDevuelveValorCampo("STRING", vrEQUIPODESC.Fields(0).Type, vrEQUIPODESC.Fields(0))
        pide_cantidad = gfnDevuelveValorCampo("STRING", vrEQUIPODESC.Fields(1).Type, vrEQUIPODESC.Fields(1))
        flag_estandar = gfnDevuelveValorCampo("STRING", vrEQUIPODESC.Fields(2).Type, vrEQUIPODESC.Fields(2))
        Codi_Tipo = gfnDevuelveValorCampo("INTEGER", vrEQUIPODESC.Fields(3).Type, vrEQUIPODESC.Fields(3))

        vrEQUIPODESC.MoveNext
End Function

Public Function Srv_FacilidaDesc_Open(ByVal codi_facilida As String) As Integer
        '
        Dim sSql As String
        Dim adoCmd As New ADODB.Command
        Dim CantidadTuplas As Integer

        sSql = "select "
        sSql = sSql & "desc_tiposerv "
        sSql = sSql & ",desc_fonofacilida "
        sSql = sSql & ",tipo_validacion "
        sSql = sSql + " from "
        sSql = sSql & "siat_tiposerv "
        sSql = sSql + " where "
        sSql = sSql & "codi_tiposerv = ? and "
        sSql = sSql & "tipo_producto = 'S'               and "
        sSql = sSql & "codi_agrupa   = '5'               and "
        sSql = sSql & "fech_vigencia is null "
        On Error GoTo Error
        Set vrFACILIDADESC = New ADODB.Recordset
        With adoCmd
                .ActiveConnection = GPBase 'RS 4-MAR-05
                .CommandType = adCmdText
                .CommandText = sSql
                .Parameters.Append adoCmd.CreateParameter("", adVarChar, adParamInput, 6, codi_facilida)
        End With
        Screen.MousePointer = vbHourglass
         vrFACILIDADESC.Open adoCmd 'RS1 4-MAR-05
        Screen.MousePointer = vbDefault

        If Not vrFACILIDADESC.EOF Then
                vrFACILIDADESC.MoveLast
                CantidadTuplas = vrFACILIDADESC.RecordCount
                vrFACILIDADESC.MoveFirst
                Srv_FacilidaDesc_Open = CantidadTuplas
        Else
                Srv_FacilidaDesc_Open = -1
        End If
        Exit Function

Error:
        Set vrFACILIDADESC = Nothing
        Screen.MousePointer = vbDefault
        Srv_FacilidaDesc_Open = fn_error(Err.Description, sSql)
        Srv_FacilidaDesc_Open = -1
End Function

Function Srv_FacilidaDesc_Close() As Integer

        If Not vrFACILIDADESC Is Nothing Then
                vrFACILIDADESC.Close
                Set vrFACILIDADESC = Nothing
        End If
End Function

Function Srv_FacilidaDesc_Read(ByRef desc_facilida As String, ByRef desc_fonofacilida As String, ByRef tipo_validacion As String) As Integer
        Dim res As Integer

        Srv_FacilidaDesc_Read = True

        If vrFACILIDADESC Is Nothing Then
                Srv_FacilidaDesc_Read = False
                Exit Function
        End If
        If vrFACILIDADESC.EOF Then
                res = Srv_FacilidaDesc_Close()
                Srv_FacilidaDesc_Read = False
                Exit Function
        End If

        desc_facilida = gfnDevuelveValorCampo("STRING", vrFACILIDADESC.Fields(0).Type, vrFACILIDADESC.Fields(0))
        desc_fonofacilida = gfnDevuelveValorCampo("STRING", vrFACILIDADESC.Fields(1).Type, vrFACILIDADESC.Fields(1))
        tipo_validacion = gfnDevuelveValorCampo("STRING", vrFACILIDADESC.Fields(2).Type, vrFACILIDADESC.Fields(2))

        vrFACILIDADESC.MoveNext
End Function


Public Function Srv_DescConcepto_Open(ByVal Codi_Concepto As Long, ByVal codi_empresa As Integer) As Integer
        '
        Dim sSql As String
        Dim sSql1 As String
        Dim adoCmd As New ADODB.Command
        Dim CantidadTuplas As Integer

        sSql = "select "
        sSql1 = "select "
        sSql = sSql & "/*+ rule */  "
        sSql1 = sSql1 & "/*+ rule */  "
        sSql = sSql & " "
        sSql1 = sSql1 & " "
        sSql = sSql & " t.desc_concepto "
        sSql1 = sSql1 & " t.desc_concepto "
        sSql = sSql & ",t.vlor_concepto "
        sSql1 = sSql1 & ",t.vlor_concepto "
        sSql = sSql & ",t.flag_afectiva "
        sSql1 = sSql1 & ",t.flag_afectiva "
        sSql = sSql & ",t.tasa_interes "
        sSql1 = sSql1 & ",t.tasa_interes "
        sSql = sSql & ",t.codi_interno "
        sSql1 = sSql1 & ",t.codi_interno "
        sSql = sSql & ",t.unid_monetar "
        sSql1 = sSql1 & ",t.unid_monetar "
        sSql = sSql & ",t.inst_renta "
        sSql1 = sSql1 & ",t.inst_renta "
        sSql = sSql & ",nvl(t.codi_hijo,0) "
        sSql1 = sSql1 & ",nvl(t.codi_hijo,0) "
        sSql = sSql & ",( select decode(count(*),0,'N','S') from siat_pr_promo_cobros c "
        sSql1 = sSql1 & ",( select decode(count(*),0,'N','S') from siat_pr_promo_cobros c "
        sSql = sSql & "where c.tipo_cobro = 'V' and c.tipo_origen = 'EQUI' and c.codi_cobro = t.codi_interno ) "
        sSql1 = sSql1 & "where c.tipo_cobro = 'V' and c.tipo_origen = 'EQUI' and c.codi_cobro = t.codi_interno ) "
        sSql = sSql & " from "
        sSql1 = sSql1 & " from "
        sSql = sSql & "TYPV_CPTOSzonaemp T "
        sSql1 = sSql1 & "TYPV_CPTOSzonaemp T "
        sSql = sSql & " where "
        sSql1 = sSql1 & " where "
        sSql = sSql & "codi_concepto = ? "
        sSql1 = sSql1 & "codi_concepto = " & Str(Codi_Concepto) & " "
'        sSql = sSql & "and codi_Admzonal = rpad(?,4) "
'        sSql1 = sSql1 & "and codi_Admzonal = rpad('" & codi_admzonal & "',4) "
        sSql = sSql & "and codi_empresa  = ? "
        sSql1 = sSql1 & "and codi_empresa  = " & Str(codi_empresa) & " "
        On Error GoTo Error
        Set vrDESCCONCEPTO = New ADODB.Recordset
        With adoCmd
                .ActiveConnection = GPBase 'RS 4-MAR-05
                .CommandType = adCmdText
                .CommandText = sSql
                .Parameters.Append adoCmd.CreateParameter("", adInteger, adParamInput, 6, Codi_Concepto)
'                .Parameters.Append adoCmd.CreateParameter("", adVarChar, adParamInput, 4, codi_admzonal)
                .Parameters.Append adoCmd.CreateParameter("", adInteger, adParamInput, 2, codi_empresa)
        End With
        Screen.MousePointer = vbHourglass
         vrDESCCONCEPTO.Open adoCmd 'RS1 4-MAR-05
        Screen.MousePointer = vbDefault

        If Not vrDESCCONCEPTO.EOF Then
                vrDESCCONCEPTO.MoveLast
                CantidadTuplas = vrDESCCONCEPTO.RecordCount
                vrDESCCONCEPTO.MoveFirst
                Srv_DescConcepto_Open = CantidadTuplas
        Else
                Srv_DescConcepto_Open = -1
        End If
        Exit Function

Error:
        Set vrDESCCONCEPTO = Nothing
        Screen.MousePointer = vbDefault
        Srv_DescConcepto_Open = fn_error(Err.Description, sSql)
        Srv_DescConcepto_Open = -1
End Function

Function Srv_DescConcepto_Close() As Integer
    If Not vrDESCCONCEPTO Is Nothing Then
        vrDESCCONCEPTO.Close
        Set vrDESCCONCEPTO = Nothing
    End If
End Function

Function Srv_DescConcepto_Read(ByRef desc As String, ByRef Valor As Double, ByRef flag As String, ByRef interes As Double, ByRef codi_interno As String, ByRef unid_monetar As String, ByRef inst_renta As String, ByRef codi_hijo As Long, ByRef flag_equipo As String) As Integer
        Dim res As Integer

        Srv_DescConcepto_Read = True

        If vrDESCCONCEPTO Is Nothing Then
            Srv_DescConcepto_Read = False
            Exit Function
        End If
        
        If vrDESCCONCEPTO.EOF Then
            res = Srv_DescConcepto_Close()
            Srv_DescConcepto_Read = False
            Exit Function
        End If

        desc = gfnDevuelveValorCampo("STRING", vrDESCCONCEPTO.Fields(0).Type, vrDESCCONCEPTO.Fields(0))
        Valor = gfnDevuelveValorCampo("DOUBLE", vrDESCCONCEPTO.Fields(1).Type, vrDESCCONCEPTO.Fields(1))
        flag = gfnDevuelveValorCampo("STRING", vrDESCCONCEPTO.Fields(2).Type, vrDESCCONCEPTO.Fields(2))
        interes = gfnDevuelveValorCampo("DOUBLE", vrDESCCONCEPTO.Fields(3).Type, vrDESCCONCEPTO.Fields(3))
        codi_interno = gfnDevuelveValorCampo("STRING", vrDESCCONCEPTO.Fields(4).Type, vrDESCCONCEPTO.Fields(4))
        unid_monetar = gfnDevuelveValorCampo("STRING", vrDESCCONCEPTO.Fields(5).Type, vrDESCCONCEPTO.Fields(5))
        inst_renta = gfnDevuelveValorCampo("STRING", vrDESCCONCEPTO.Fields(6).Type, vrDESCCONCEPTO.Fields(6))
        codi_hijo = gfnDevuelveValorCampo("INTEGER", vrDESCCONCEPTO.Fields(7).Type, vrDESCCONCEPTO.Fields(7))
        flag_equipo = gfnDevuelveValorCampo("STRING", vrDESCCONCEPTO.Fields(8).Type, vrDESCCONCEPTO.Fields(8))
        
        vrDESCCONCEPTO.MoveNext
End Function

Public Function Srv_ConceptoExcluyente_Open() As Integer
        '
        Dim sSql As String
        Dim sSql1 As String
        Dim adoCmd As New ADODB.Command
        Dim CantidadTuplas As Integer

        sSql = "select t.codi_elem, "
        sSql = sSql & " v.codi_concepto, "
        sSql = sSql & " v.desc_concepto, "
        sSql = sSql & " trim(t.item_uno) prioridad "
        sSql = sSql & " from part_parame t, typv_cptosvig v "
        sSql = sSql & " Where t.Codi_Grupo = 346 "
        sSql = sSql & " and trim(t.item_dos) = 'V' "
        sSql = sSql & " and v.codi_concepto = t.vlor_uno "
        'sSql = sSql & " and t.vlor_dos = ? "
        sSql = sSql & " order by to_number(t.codi_elem) "
        
        On Error GoTo Error
        Set vrCONCEPTOEXCLUYENTE = New ADODB.Recordset
        With adoCmd
                .ActiveConnection = GPBase 'RS 4-MAR-05
                .CommandType = adCmdText
                .CommandText = sSql
                '.Parameters.Append adoCmd.CreateParameter("", adInteger, adParamInput, 6, CodiGrupo)
        End With
        Screen.MousePointer = vbHourglass
        vrCONCEPTOEXCLUYENTE.Open adoCmd 'RS1 4-MAR-05
        Screen.MousePointer = vbDefault

        If Not vrCONCEPTOEXCLUYENTE.EOF Then
                vrCONCEPTOEXCLUYENTE.MoveLast
                CantidadTuplas = vrCONCEPTOEXCLUYENTE.RecordCount
                vrCONCEPTOEXCLUYENTE.MoveFirst
                Srv_ConceptoExcluyente_Open = CantidadTuplas
        Else
                Srv_ConceptoExcluyente_Open = -1
        End If
        Exit Function

Error:
        Set vrCONCEPTOEXCLUYENTE = Nothing
        Screen.MousePointer = vbDefault
        Srv_ConceptoExcluyente_Open = fn_error(Err.Description, sSql)
        Srv_ConceptoExcluyente_Open = -1
End Function

Function Srv_ConceptoExcluyente_Close() As Integer
    If Not vrCONCEPTOEXCLUYENTE Is Nothing Then
        vrCONCEPTOEXCLUYENTE.Close
        Set vrCONCEPTOEXCLUYENTE = Nothing
    End If
End Function

Function Srv_ConceptoExcluyente_Read(ByRef codi_elem As Integer, ByRef Codi_Concepto As Long, ByRef desc_concepto As String, ByRef prioridad As String) As Integer
        Dim res As Integer

        Srv_ConceptoExcluyente_Read = True

        If vrCONCEPTOEXCLUYENTE Is Nothing Then
            Srv_ConceptoExcluyente_Read = False
            Exit Function
        End If
        
        If vrCONCEPTOEXCLUYENTE.EOF Then
            res = Srv_ConceptoExcluyente_Close()
            Srv_ConceptoExcluyente_Read = False
            Exit Function
        End If

        codi_elem = gfnDevuelveValorCampo("INTEGER", vrCONCEPTOEXCLUYENTE.Fields(0).Type, vrCONCEPTOEXCLUYENTE.Fields(0))
        Codi_Concepto = gfnDevuelveValorCampo("LONG", vrCONCEPTOEXCLUYENTE.Fields(1).Type, vrCONCEPTOEXCLUYENTE.Fields(1))
        desc_concepto = gfnDevuelveValorCampo("STRING", vrCONCEPTOEXCLUYENTE.Fields(2).Type, vrCONCEPTOEXCLUYENTE.Fields(2))
        prioridad = gfnDevuelveValorCampo("STRING", vrCONCEPTOEXCLUYENTE.Fields(3).Type, vrCONCEPTOEXCLUYENTE.Fields(3))
                
        vrCONCEPTOEXCLUYENTE.MoveNext
End Function

Public Function Srv_CodiElem_Open(ByVal CodiGrupo As Integer) As Integer
        '
        Dim sSql As String
        Dim adoCmd As New ADODB.Command
        Dim CantidadTuplas As Integer

        sSql = "select "
        sSql = sSql & " ptar.parp_int_prom.fncodielem(?) "
        sSql = sSql + " from "
        sSql = sSql & "dual "
        On Error GoTo Error
        Set vrCODIELEM = New ADODB.Recordset
        With adoCmd
                .ActiveConnection = GPBase 'RS 4-MAR-05
                .CommandType = adCmdText
                .CommandText = sSql
                .Parameters.Append adoCmd.CreateParameter("", adInteger, adParamInput, 10, CodiGrupo)
        End With
        Screen.MousePointer = vbHourglass
         vrCODIELEM.Open adoCmd 'RS1 4-MAR-05
        Screen.MousePointer = vbDefault

        If Not vrCODIELEM.EOF Then
                vrCODIELEM.MoveLast
                CantidadTuplas = vrCODIELEM.RecordCount
                vrCODIELEM.MoveFirst
                Srv_CodiElem_Open = CantidadTuplas
        Else
                Srv_CodiElem_Open = -1
        End If
        Exit Function

Error:
        Set vrCODIELEM = Nothing
        Screen.MousePointer = vbDefault
        Srv_CodiElem_Open = fn_error(Err.Description, sSql)
        Srv_CodiElem_Open = -1
End Function

Function Srv_CodiElem_Close() As Integer

        If Not vrCODIELEM Is Nothing Then
                vrCODIELEM.Close
                Set vrCODIELEM = Nothing
        End If
End Function

Function Srv_CodiElem_Read(ByRef CodiElem As String) As Integer
        Dim res As Integer

        Srv_CodiElem_Read = True

        If vrCODIELEM Is Nothing Then
                Srv_CodiElem_Read = False
                Exit Function
        End If
        
        If vrCODIELEM.EOF Then
                res = Srv_CodiElem_Close()
                Srv_CodiElem_Read = False
                Exit Function
        End If

        CodiElem = gfnDevuelveValorCampo("STRING", vrCODIELEM.Fields(0).Type, vrCODIELEM.Fields(0))

        vrCODIELEM.MoveNext
End Function

Function IngresaProyecto(ByVal i_codi_paquete As Integer, ByVal id_proyecto As String, ByVal desc_proyecto As String, ByVal concepto As String, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrProyecto As ADODB.Recordset
    Dim o_codi_error As String

    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.ingresa_proyecto(?,?,?,?,?); end;"
    

    Set vrProyecto = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 50, i_codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, id_proyecto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_proyecto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, concepto)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrProyecto.Open adoCmd

    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrProyecto = Nothing

    IngresaProyecto = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function
 
    
Error:
    Set vrProyecto = Nothing
    Screen.MousePointer = vbDefault
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    IngresaProyecto = False
'     SRV_INFORMURECLAMO_PROC = fn_error(Err.Description, sLlamadaProc)
'    SRV_INFORMURECLAMO_PROC = False
'
End Function

Public Function Srv_CargaProyectos_Open(i_codi_paquete) As Integer
        '
        Dim sSql As String
        Dim sSql1 As String
        Dim adoCmd As New ADODB.Command
        Dim CantidadTuplas As Integer
       
        sSql = "select p.id_proyecto, "
        sSql = sSql & " p.desc_proyecto, "
        sSql = sSql & " p.cod_concepto "
        sSql = sSql & " from SIAT_PROY_INMOBILIARIO_PROM p "
        sSql = sSql & " Where p.id_paquete = ? "
        sSql = sSql & " order by p.id_proyecto "
       
               
        On Error GoTo Error
        Set vrCARGAPROYECTOS = New ADODB.Recordset
        
        With adoCmd
                .ActiveConnection = GPBase
                .CommandType = adCmdText
                .CommandText = sSql
                .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 50, i_codi_paquete)

        End With
        Screen.MousePointer = vbHourglass
         vrCARGAPROYECTOS.Open adoCmd
        Screen.MousePointer = vbDefault


        If Not vrCARGAPROYECTOS.EOF Then
                vrCARGAPROYECTOS.MoveLast
                CantidadTuplas = vrCARGAPROYECTOS.RecordCount
                vrCARGAPROYECTOS.MoveFirst
                Srv_CargaProyectos_Open = CantidadTuplas
        Else
                Srv_CargaProyectos_Open = -1
        End If
        Exit Function

Error:
        Set vrCARGAPROYECTOS = Nothing
        Screen.MousePointer = vbDefault
        Srv_CargaProyectos_Open = fn_error(Err.Description, sSql)
        Srv_CargaProyectos_Open = -1
End Function

Function Srv_CargaProyectos_Read(ByRef cod_proyecto As String, ByRef desc_proyecto As String, ByRef concepto As String) As Integer
        Dim res As Integer
        
        Srv_CargaProyectos_Read = True

        If vrCARGAPROYECTOS Is Nothing Then
            Srv_CargaProyectos_Read = False
            Exit Function
        End If

        If vrCARGAPROYECTOS.EOF Then
            res = Srv_CargaProyectos_Close()
            Srv_CargaProyectos_Read = False
            Exit Function
       End If
       


        cod_proyecto = gfnDevuelveValorCampo("STRING", vrCARGAPROYECTOS.Fields(0).Type, vrCARGAPROYECTOS.Fields(0))
        desc_proyecto = gfnDevuelveValorCampo("STRING", vrCARGAPROYECTOS.Fields(1).Type, vrCARGAPROYECTOS.Fields(1))
        concepto = gfnDevuelveValorCampo("STRING", vrCARGAPROYECTOS.Fields(2).Type, vrCARGAPROYECTOS.Fields(2))
                
        vrCARGAPROYECTOS.MoveNext
End Function

Function Srv_CargaProyectos_Close() As Integer
    If Not vrCARGAPROYECTOS Is Nothing Then
        vrCARGAPROYECTOS.Close
        Set vrCARGAPROYECTOS = Nothing
    End If
End Function

Function ModificaProyecto(ByVal i_codi_paquete As Integer, ByVal id_proyecto As String, ByVal desc_proyecto As String, ByVal concepto As String, ByRef psRespuesta As String) As Integer
    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrProyecto As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.modifica_proyecto(?,?,?,?,?); end;"

    Set vrProyecto = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 50, i_codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, id_proyecto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_proyecto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, concepto)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrProyecto.Open adoCmd

    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrProyecto = Nothing

    ModificaProyecto = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrProyecto = Nothing
    Screen.MousePointer = vbDefault
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    ModificaProyecto = False
End Function

Function EliminaProyecto(ByVal i_codi_paquete As Integer, ByVal id_proyecto As String, ByRef psRespuesta As String) As Integer
    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrProyecto As ADODB.Recordset
    Dim o_codi_error As String
    
    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin SIAA_PAQUETES.elimina_proyecto(?,?,?); end;"

    Set vrProyecto = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 50, i_codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 50, id_proyecto)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrProyecto.Open adoCmd

    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
    
    Set vrProyecto = Nothing

    EliminaProyecto = True
    
    Screen.MousePointer = vbDefault
    
    Exit Function

Error:
    Set vrProyecto = Nothing
    Screen.MousePointer = vbDefault
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    EliminaProyecto = False
End Function
