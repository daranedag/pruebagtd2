Attribute VB_Name = "modConsultasGral"
Option Explicit

Global vrGetLocalidad As ADODB.Recordset
Global vrGetTitParam As ADODB.Recordset
Global vrGetParam As ADODB.Recordset
Global vrGetEquipos As ADODB.Recordset
Global vrGetFacilidad As ADODB.Recordset
Global vrGetRentas As ADODB.Recordset
Global vrBusRentas As ADODB.Recordset
Global vrBusFaci As ADODB.Recordset
Global vrBusLocalidad As ADODB.Recordset
Dim Dy_Desc As Recordset


Public Function GetLocalidades() As Long
    'Lista las Localidades del Sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_localida, desc_localida "
    sSql1 = sSql1 & "codi_localida, desc_localida "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "part_localida  "
    sSql1 = sSql1 & "part_localida  "
    sSql = sSql & " where codi_admzonal is not null "
    sSql1 = sSql1 & " where codi_admzonal is not null "
    sSql = sSql & "  and codi_admzonal not in ('STGO', 'GCLI')"
    sSql1 = sSql1 & "  and codi_admzonal not in ('STGO', 'GCLI')"
    sSql = sSql & " order by codi_localida "
    sSql1 = sSql1 & " order by codi_localida "
    On Error GoTo Error
    Set vrGetLocalidad = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetLocalidad.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetLocalidad.EOF Then
        vrGetLocalidad.MoveLast
        CantidadTuplas = vrGetLocalidad.RecordCount
        vrGetLocalidad.MoveFirst
        GetLocalidades = CantidadTuplas
    Else
        GetLocalidades = -1
    End If
    Exit Function

Error:
    Set vrGetLocalidad = Nothing
    Screen.MousePointer = vbDefault
    GetLocalidades = fn_error(Err.Description, sSql1)
    GetLocalidades = -1
End Function

Function GetLocalidadesClose() As Integer

    If Not vrGetLocalidad Is Nothing Then
        vrGetLocalidad.Close
        Set vrGetLocalidad = Nothing
    End If
End Function

Function GetLocalidadesRead(ByRef codi_localida As String, ByRef desc_localida As String) As Integer
    Dim res As Integer

    GetLocalidadesRead = True

    If vrGetLocalidad Is Nothing Then
        GetLocalidadesRead = False
        Exit Function
    End If
    If vrGetLocalidad.EOF Then
        res = GetLocalidadesClose()
        GetLocalidadesRead = False
        Exit Function
    End If

    codi_localida = gfnDevuelveValorCampo("STRING", vrGetLocalidad.Fields(0).Type, vrGetLocalidad.Fields(0))
    desc_localida = gfnDevuelveValorCampo("STRING", vrGetLocalidad.Fields(1).Type, vrGetLocalidad.Fields(1))

    vrGetLocalidad.MoveNext

End Function

Public Function GetTitParam() As Long
    'Lista los Grupos de Parámetros
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_grupo, desc_grupo "
    sSql1 = sSql1 & "codi_grupo, desc_grupo "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "part_titulos  "
    sSql1 = sSql1 & "part_titulos  "
    sSql = sSql & "order by codi_grupo "
    sSql1 = sSql1 & "order by codi_grupo "
    On Error GoTo Error
    Set vrGetTitParam = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetTitParam.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetTitParam.EOF Then
        vrGetTitParam.MoveLast
        CantidadTuplas = vrGetTitParam.RecordCount
        vrGetTitParam.MoveFirst
        GetTitParam = CantidadTuplas
    Else
        GetTitParam = -1
    End If
    Exit Function

Error:
    Set vrGetTitParam = Nothing
    Screen.MousePointer = vbDefault
    GetTitParam = fn_error(Err.Description, sSql1)
    GetTitParam = -1
End Function

Function GetTitParamClose() As Integer

    If Not vrGetTitParam Is Nothing Then
        vrGetTitParam.Close
        Set vrGetTitParam = Nothing
    End If
End Function

Function GetTitParamRead(ByRef codi_grupo As String, ByRef desc_grupo As String) As Integer
    Dim res As Integer

    GetTitParamRead = True

    If vrGetTitParam Is Nothing Then
        GetTitParamRead = False
        Exit Function
    End If
    If vrGetTitParam.EOF Then
        res = GetTitParamClose()
        GetTitParamRead = False
        Exit Function
    End If

    codi_grupo = gfnDevuelveValorCampo("STRING", vrGetTitParam.Fields(0).Type, vrGetTitParam.Fields(0))
    desc_grupo = gfnDevuelveValorCampo("STRING", vrGetTitParam.Fields(1).Type, vrGetTitParam.Fields(1))

    vrGetTitParam.MoveNext

End Function

Public Function GetParam(ByVal CodiGrupo As String) As Long
    'Extrae los datos de un Grupo de Parametros
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_elem, desc_parame "
    sSql1 = sSql1 & "codi_elem, desc_parame "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "part_parame  "
    sSql1 = sSql1 & "part_parame  "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_grupo= ? "
    sSql1 = sSql1 & "codi_grupo= " & Str(CodiGrupo) & " "
    sSql = sSql & "order by lpad(codi_elem,2)"
    sSql1 = sSql1 & "order by lpad(codi_elem,2)"
    On Error GoTo Error
    Set vrGetParam = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiGrupo", adInteger, adParamInput, 4, CodiGrupo)
    End With
    Screen.MousePointer = vbHourglass
    vrGetParam.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetParam.EOF Then
        vrGetParam.MoveLast
        CantidadTuplas = vrGetParam.RecordCount
        vrGetParam.MoveFirst
        GetParam = CantidadTuplas
    Else
        GetParam = -1
    End If
    Exit Function

Error:
    Set vrGetParam = Nothing
    Screen.MousePointer = vbDefault
    GetParam = fn_error(Err.Description, sSql1)
    GetParam = -1
End Function

Function GetParamClose() As Integer

    If Not vrGetParam Is Nothing Then
        vrGetParam.Close
        Set vrGetParam = Nothing
    End If
End Function

Function GetParamRead(ByRef codi_elem As String, ByRef desc_elem As String) As Integer
    Dim res As Integer

    GetParamRead = True

    If vrGetParam Is Nothing Then
        GetParamRead = False
        Exit Function
    End If
    If vrGetParam.EOF Then
        res = GetParamClose()
        GetParamRead = False
        Exit Function
    End If

    codi_elem = gfnDevuelveValorCampo("STRING", vrGetParam.Fields(0).Type, vrGetParam.Fields(0))
    desc_elem = gfnDevuelveValorCampo("STRING", vrGetParam.Fields(1).Type, vrGetParam.Fields(1))

    vrGetParam.MoveNext

End Function

Public Function GetEquipos() As Long
    'Lista los equipos existentes en el sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select distinct "
    sSql1 = "select distinct "
    sSql = sSql & "desc_modelo, codi_tipoequi "
    sSql1 = sSql1 & "desc_modelo, codi_tipoequi "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_inv_modelo  "
    sSql1 = sSql1 & "siat_inv_modelo  "
    sSql = sSql & "order by desc_modelo "
    sSql1 = sSql1 & "order by desc_modelo "
    On Error GoTo Error
    Set vrGetEquipos = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetEquipos.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetEquipos.EOF Then
        vrGetEquipos.MoveLast
        CantidadTuplas = vrGetEquipos.RecordCount
        vrGetEquipos.MoveFirst
        GetEquipos = CantidadTuplas
    Else
        GetEquipos = -1
    End If
    Exit Function

Error:
    Set vrGetEquipos = Nothing
    Screen.MousePointer = vbDefault
    GetEquipos = fn_error(Err.Description, sSql1)
    GetEquipos = -1
End Function

Function GetEquiposClose() As Integer

    If Not vrGetEquipos Is Nothing Then
        vrGetEquipos.Close
        Set vrGetEquipos = Nothing
    End If
End Function

Function GetEquiposRead(ByRef desc_modelo As String, ByRef codi_modelo As String) As Integer
    Dim res As Integer

    GetEquiposRead = True

    If vrGetEquipos Is Nothing Then
        GetEquiposRead = False
        Exit Function
    End If
    If vrGetEquipos.EOF Then
        res = GetEquiposClose()
        GetEquiposRead = False
        Exit Function
    End If

    desc_modelo = gfnDevuelveValorCampo("STRING", vrGetEquipos.Fields(0).Type, vrGetEquipos.Fields(0))
    codi_modelo = gfnDevuelveValorCampo("STRING", vrGetEquipos.Fields(1).Type, vrGetEquipos.Fields(1))

    vrGetEquipos.MoveNext

End Function

Public Function GetFacilidad() As Long
    'Lista las facilidades existentes en el sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_tiposerv, desc_tiposerv  "
    sSql1 = sSql1 & "codi_tiposerv, desc_tiposerv  "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_tiposerv  "
    sSql1 = sSql1 & "siat_tiposerv  "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "nvl(fech_vigencia,sysdate) >= sysdate "
    sSql1 = sSql1 & "nvl(fech_vigencia,sysdate) >= sysdate "
    sSql = sSql & " and tipo_producto='S'"
    sSql1 = sSql1 & " and tipo_producto='S'"
    sSql = sSql & "order by lpad(codi_tiposerv,6) "
    sSql1 = sSql1 & "order by lpad(codi_tiposerv,6) "
    On Error GoTo Error
    Set vrGetFacilidad = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetFacilidad.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetFacilidad.EOF Then
        vrGetFacilidad.MoveLast
        CantidadTuplas = vrGetFacilidad.RecordCount
        vrGetFacilidad.MoveFirst
        GetFacilidad = CantidadTuplas
    Else
        GetFacilidad = -1
    End If
    Exit Function

Error:
    Set vrGetFacilidad = Nothing
    Screen.MousePointer = vbDefault
    GetFacilidad = fn_error(Err.Description, sSql1)
    GetFacilidad = -1
End Function

Function GetFacilidadClose() As Integer

    If Not vrGetFacilidad Is Nothing Then
        vrGetFacilidad.Close
        Set vrGetFacilidad = Nothing
    End If
End Function

Function GetFacilidadRead(ByRef codi_tiposerv As String, ByRef desc_tiposerv As String) As Integer
    Dim res As Integer

    GetFacilidadRead = True

    If vrGetFacilidad Is Nothing Then
        GetFacilidadRead = False
        Exit Function
    End If
    If vrGetFacilidad.EOF Then
        res = GetFacilidadClose()
        GetFacilidadRead = False
        Exit Function
    End If

    codi_tiposerv = gfnDevuelveValorCampo("STRING", vrGetFacilidad.Fields(0).Type, vrGetFacilidad.Fields(0))
    desc_tiposerv = gfnDevuelveValorCampo("STRING", vrGetFacilidad.Fields(1).Type, vrGetFacilidad.Fields(1))

    vrGetFacilidad.MoveNext

End Function

Public Function GetRentas() As Long
    'Lista las rentas existentes en el sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_interno, codi_concepto, desc_concepto"
    sSql1 = sSql1 & "codi_interno, codi_concepto, desc_concepto "
    sSql = sSql & ", v.unid_monetar ||' '||p.desc_parame unid_monetar "
    sSql1 = sSql1 & ", v.unid_monetar || ' '||p.desc_parame unid_monetar "
    sSql = sSql & " from typv_cptosvig v, ptar.part_valmoned m, ptar.part_parame p "
    sSql1 = sSql1 & " from typv_cptosvig v, ptar.part_valmoned m, ptar.part_parame p "
    sSql = sSql & "WHERE p.codi_grupo = 9 "
    sSql1 = sSql1 & "WHERE p.codi_grupo = 9 "
    sSql = sSql & "AND p.codi_elem  = m.codi_moneda "
    sSql1 = sSql1 & "AND p.codi_elem  = m.codi_moneda "
    sSql = sSql & "AND SYSDATE BETWEEN m.fech_inicio AND nvl(m.fech_fin, SYSDATE) "
    sSql1 = sSql1 & "AND SYSDATE BETWEEN m.fech_inicio AND nvl(m.fech_fin, SYSDATE) "
    sSql = sSql & "AND m.codi_moneda = v.unid_monetar "
    sSql1 = sSql1 & "AND m.codi_moneda = v.unid_monetar "
    sSql = sSql & "order by codi_concepto "
    sSql1 = sSql1 & "order by codi_concepto "
    
    
    On Error GoTo Error
    Set vrGetRentas = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetRentas.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetRentas.EOF Then
        vrGetRentas.MoveLast
        CantidadTuplas = vrGetRentas.RecordCount
        vrGetRentas.MoveFirst
        GetRentas = CantidadTuplas
    Else
        GetRentas = -1
    End If
    Exit Function

Error:
    Set vrGetRentas = Nothing
    Screen.MousePointer = vbDefault
    GetRentas = fn_error(Err.Description, sSql1)
    GetRentas = -1
End Function

Function GetRentasClose() As Integer

    If Not vrGetRentas Is Nothing Then
        vrGetRentas.Close
        Set vrGetRentas = Nothing
    End If
End Function

Function GetRentasRead(ByRef codi_interno As String, ByRef Codi_Concepto As Long, ByRef desc_concepto As String, ByRef unid_monetar As String) As Integer
    Dim res As Integer

    GetRentasRead = True

    If vrGetRentas Is Nothing Then
        GetRentasRead = False
        Exit Function
    End If
    If vrGetRentas.EOF Then
        res = GetRentasClose()
        GetRentasRead = False
        Exit Function
    End If

    codi_interno = gfnDevuelveValorCampo("STRING", vrGetRentas.Fields(0).Type, vrGetRentas.Fields(0))
    Codi_Concepto = gfnDevuelveValorCampo("LONG", vrGetRentas.Fields(1).Type, vrGetRentas.Fields(1))
    desc_concepto = gfnDevuelveValorCampo("STRING", vrGetRentas.Fields(2).Type, vrGetRentas.Fields(2))
    unid_monetar = gfnDevuelveValorCampo("STRING", vrGetRentas.Fields(3).Type, vrGetRentas.Fields(3))
    
    vrGetRentas.MoveNext

End Function


Public Function BusRentas(ByVal Texto As String) As Long
    'Busca una renta por codigo o descripcion
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_interno,  "
    sSql1 = sSql1 & "codi_interno,  "
    sSql = sSql & "codi_concepto||' - '|| desc_concepto "
    sSql1 = sSql1 & "codi_concepto||' - '|| desc_concepto "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "typv_cptosvig "
    sSql1 = sSql1 & "typv_cptosvig "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "upper(codi_concepto) like upper(?) ||'%' "
    sSql1 = sSql1 & "upper(codi_concepto) like upper('" & Texto & "') ||'%' "
    sSql = sSql & "or upper(desc_concepto) like '%' || upper(?) ||'%' "
    sSql1 = sSql1 & "or upper(desc_concepto) like '%' || upper('" & Texto & "') ||'%' "
    sSql = sSql & "or upper(codi_interno) like upper(?) ||'%' "
    sSql1 = sSql1 & "or upper(codi_interno) like upper('" & Texto & "') ||'%' "
    sSql = sSql & "order by codi_concepto "
    sSql1 = sSql1 & "order by codi_concepto "
    On Error GoTo Error
    Set vrBusRentas = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-Texto", adVarChar, adParamInput, 100, Texto)
        .Parameters.Append adoCmd.CreateParameter(":SI-Texto", adVarChar, adParamInput, 100, Texto)
        .Parameters.Append adoCmd.CreateParameter(":SI-Texto", adVarChar, adParamInput, 100, Texto)
    End With
    Screen.MousePointer = vbHourglass
    vrBusRentas.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrBusRentas.EOF Then
        vrBusRentas.MoveLast
        CantidadTuplas = vrBusRentas.RecordCount
        vrBusRentas.MoveFirst
        BusRentas = CantidadTuplas
    Else
        BusRentas = -1
    End If
    Exit Function

Error:
    Set vrBusRentas = Nothing
    Screen.MousePointer = vbDefault
    BusRentas = fn_error(Err.Description, sSql1)
    BusRentas = -1
End Function

Function BusRentasClose() As Integer

    If Not vrBusRentas Is Nothing Then
        vrBusRentas.Close
        Set vrBusRentas = Nothing
    End If
End Function

Function BusRentasRead(ByRef codi_interno As String, ByRef desc_concepto As String) As Integer
    Dim res As Integer

    BusRentasRead = True

    If vrBusRentas Is Nothing Then
        BusRentasRead = False
        Exit Function
    End If
    If vrBusRentas.EOF Then
        res = BusRentasClose()
        BusRentasRead = False
        Exit Function
    End If

    codi_interno = gfnDevuelveValorCampo("STRING", vrBusRentas.Fields(0).Type, vrBusRentas.Fields(0))
    desc_concepto = gfnDevuelveValorCampo("STRING", vrBusRentas.Fields(1).Type, vrBusRentas.Fields(1))

    vrBusRentas.MoveNext

End Function


Public Function BusFaci(ByVal Texto As String) As Long
    'Busca una facilidad por codigo o descripcion
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_tiposerv,  "
    sSql1 = sSql1 & "codi_tiposerv,  "
    sSql = sSql & "lpad(codi_tiposerv,6) || ' - ' || desc_tiposerv "
    sSql1 = sSql1 & "lpad(codi_tiposerv,6) || ' - ' || desc_tiposerv "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_tiposerv "
    sSql1 = sSql1 & "siat_tiposerv "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "(upper(codi_tiposerv) like upper(?) || '%' "
    sSql1 = sSql1 & "(upper(codi_tiposerv) like upper('" & Texto & "') || '%' "
    sSql = sSql & "or upper(desc_tiposerv) like '%' || upper(?) || '%') "
    sSql1 = sSql1 & "or upper(desc_tiposerv) like '%' || upper('" & Texto & "') || '%') "
    sSql = sSql & "and nvl(fech_vigencia,sysdate) >= sysdate "
    sSql1 = sSql1 & "and nvl(fech_vigencia,sysdate) >= sysdate "
    sSql = sSql & "and tipo_producto='S' "
    sSql1 = sSql1 & "and tipo_producto='S' "
    sSql = sSql & "order by lpad(codi_tiposerv,6) "
    sSql1 = sSql1 & "order by lpad(codi_tiposerv,6) "
    On Error GoTo Error
    Set vrBusFaci = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-Texto", adVarChar, adParamInput, 100, Texto)
        .Parameters.Append adoCmd.CreateParameter(":SI-Texto", adVarChar, adParamInput, 100, Texto)
    End With
    Screen.MousePointer = vbHourglass
    vrBusFaci.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrBusFaci.EOF Then
        vrBusFaci.MoveLast
        CantidadTuplas = vrBusFaci.RecordCount
        vrBusFaci.MoveFirst
        BusFaci = CantidadTuplas
    Else
        BusFaci = -1
    End If
    Exit Function

Error:
    Set vrBusFaci = Nothing
    Screen.MousePointer = vbDefault
    BusFaci = fn_error(Err.Description, sSql1)
    BusFaci = -1
End Function

Function BusFaciClose() As Integer

    If Not vrBusFaci Is Nothing Then
        vrBusFaci.Close
        Set vrBusFaci = Nothing
    End If
End Function

Function BusFaciRead(ByRef codigo As String, ByRef descripcion As String) As Integer
    Dim res As Integer

    BusFaciRead = True

    If vrBusFaci Is Nothing Then
        BusFaciRead = False
        Exit Function
    End If
    If vrBusFaci.EOF Then
        res = BusFaciClose()
        BusFaciRead = False
        Exit Function
    End If

    codigo = gfnDevuelveValorCampo("STRING", vrBusFaci.Fields(0).Type, vrBusFaci.Fields(0))
    descripcion = gfnDevuelveValorCampo("STRING", vrBusFaci.Fields(1).Type, vrBusFaci.Fields(1))

    vrBusFaci.MoveNext

End Function

Public Function BusLocalidad(ByVal Busca As String) As Long
    'Busca dentro de las localidades del sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_localida, desc_localida "
    sSql1 = sSql1 & "codi_localida, desc_localida "
    sSql = sSql & " "
    sSql1 = sSql1 & " "
    sSql = sSql & " "
    sSql1 = sSql1 & " "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "part_localida  "
    sSql1 = sSql1 & "part_localida  "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_admzonal is not null "
    sSql1 = sSql1 & "codi_admzonal is not null "
    sSql = sSql & "and (upper(codi_localida) like '%'|| upper(?) ||'%'  "
    sSql1 = sSql1 & "and (upper(codi_localida) like '%'|| upper('" & Busca & "') ||'%'  "
    sSql = sSql & "or upper(desc_localida) like '%'|| upper(?) ||'%') "
    sSql1 = sSql1 & "or upper(desc_localida) like '%'|| upper('" & Busca & "') ||'%') "
    sSql = sSql & "order by codi_localida "
    sSql1 = sSql1 & "order by codi_localida "
    On Error GoTo Error
    Set vrBusLocalidad = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-Busca", adVarChar, adParamInput, 100, Busca)
        .Parameters.Append adoCmd.CreateParameter(":SI-Busca", adVarChar, adParamInput, 100, Busca)
    End With
    Screen.MousePointer = vbHourglass
    vrBusLocalidad.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrBusLocalidad.EOF Then
        vrBusLocalidad.MoveLast
        CantidadTuplas = vrBusLocalidad.RecordCount
        vrBusLocalidad.MoveFirst
        BusLocalidad = CantidadTuplas
    Else
        BusLocalidad = -1
    End If
    Exit Function

Error:
    Set vrBusLocalidad = Nothing
    Screen.MousePointer = vbDefault
    BusLocalidad = fn_error(Err.Description, sSql1)
    BusLocalidad = -1
End Function

Function BusLocalidadClose() As Integer

    If Not vrBusLocalidad Is Nothing Then
        vrBusLocalidad.Close
        Set vrBusLocalidad = Nothing
    End If
End Function

Function BusLocalidadRead(ByRef codi_localida As String, ByRef desc_localida As String) As Integer
    Dim res As Integer

    BusLocalidadRead = True

    If vrBusLocalidad Is Nothing Then
        BusLocalidadRead = False
        Exit Function
    End If
    If vrBusLocalidad.EOF Then
        res = BusLocalidadClose()
        BusLocalidadRead = False
        Exit Function
    End If

    codi_localida = gfnDevuelveValorCampo("STRING", vrBusLocalidad.Fields(0).Type, vrBusLocalidad.Fields(0))
    desc_localida = gfnDevuelveValorCampo("STRING", vrBusLocalidad.Fields(1).Type, vrBusLocalidad.Fields(1))

    vrBusLocalidad.MoveNext

End Function





