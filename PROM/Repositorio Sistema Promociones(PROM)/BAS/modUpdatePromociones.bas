Attribute VB_Name = "modUpdatePromociones"
Option Explicit

Function UpdPromocion(ByVal codi_promocion As Long, _
                      ByVal desc_promocion As String, _
                      ByVal fecha_inicio As String, _
                      ByVal fecha_fin As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrUpdPromocion As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromocionModifica(?,?,?,?); end;"

    Set vrUpdPromocion = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_inicio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_fin)
    End With
    On Error GoTo Error
    vrUpdPromocion.Open adoCmd

    Set vrUpdPromocion = Nothing

    UpdPromocion = True
    Exit Function

Error:
    Set vrUpdPromocion = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromocionModifica(" + Trim(Str(codi_promocion)) + "," + desc_promocion + "," + fecha_fin + "); end;"
    UpdPromocion = fn_error(Err.Description, sLlamadaProc)
    UpdPromocion = False
End Function

Function UpdPromCobro(ByVal codi_promocion As Long, _
                      ByVal codi_cobro As String, _
                      ByVal tipo_cobro As String, _
                      ByVal tipo_origen As String, _
                      ByVal codi_origen As String, _
                      ByVal cant_gracia As Integer, _
                      ByVal cant_duracion As Integer, _
                      ByVal cant_mincuotas As Integer, _
                      ByVal cant_maxcuotas As Integer, _
                      ByVal valor_cobro As Double) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrUpdPromCobro As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoCobroModifica(?,?,?,?,?,?,?,?,?,?); end;"

    Set vrUpdPromCobro = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_cobro)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, tipo_cobro)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, tipo_origen)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_origen)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_gracia)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_duracion)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_mincuotas)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_maxcuotas)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 13, valor_cobro)
    End With
    On Error GoTo Error
    vrUpdPromCobro.Open adoCmd

    Set vrUpdPromCobro = Nothing

    UpdPromCobro = True
    Exit Function

Error:
    Set vrUpdPromCobro = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoCobroModifica(" + Trim(Str(codi_promocion)) + "," + codi_cobro + "," + tipo_cobro + "," + tipo_origen + "," + codi_origen + "," + Trim(Str(cant_gracia)) + "," + Trim(Str(cant_duracion)) + "," + Trim(Str(cant_mincuotas)) + "," + Trim(Str(cant_maxcuotas)) + "," + Trim(Str(valor_cobro)) + "); end;"
    UpdPromCobro = fn_error(Err.Description, sLlamadaProc)
    UpdPromCobro = False
End Function

Function UpdPromProd(ByVal codi_promocion As Long, _
                     ByVal codi_producto As String, _
                     ByVal agrega As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrUpdPromProd As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoProductoActualiza(?,?,?); end;"

    Set vrUpdPromProd = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, agrega)
    End With
    On Error GoTo Error
    vrUpdPromProd.Open adoCmd

    Set vrUpdPromProd = Nothing

    UpdPromProd = True
    Exit Function

Error:
    Set vrUpdPromProd = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoProductoActualiza(" + Trim(Str(codi_promocion)) + "," + codi_producto + "," + agrega + "); end;"
    UpdPromProd = fn_error(Err.Description, sLlamadaProc)
    UpdPromProd = False
End Function

Function UpdPromPlan(ByVal codi_promocion As Long, _
                     ByVal codi_plan As String, _
                     ByVal cant_perm As Integer) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrUpdPromPlan As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoPlanModifica(?,?,?); end;"

    Set vrUpdPromPlan = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_perm)
    End With
    On Error GoTo Error
    vrUpdPromPlan.Open adoCmd

    Set vrUpdPromPlan = Nothing

    UpdPromPlan = True
    Exit Function

Error:
    Set vrUpdPromPlan = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoPlanModifica(" + Trim(Str(codi_promocion)) + "," + codi_plan + "," + Trim(Str(cant_perm)) + "); end;"
    UpdPromPlan = fn_error(Err.Description, sLlamadaProc)
    UpdPromPlan = False
End Function

Function UpdPromEqui(ByVal codi_promocion As Long, ByVal codi_producto As String, ByVal codi_equipo As String, ByVal tipo_relacoin As String, ByVal cant_permanencia As Integer, ByVal fecha_inicio As String, ByVal fecha_termino As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrUpdPromEqui As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoEquipoModifica(?,?,?,?,?,?,?); end;"

    Set vrUpdPromEqui = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_equipo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, tipo_relacoin)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_permanencia)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_inicio)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_termino)
    End With
    On Error GoTo Error
    vrUpdPromEqui.Open adoCmd

    Set vrUpdPromEqui = Nothing

    UpdPromEqui = True
    Exit Function

Error:
    Set vrUpdPromEqui = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoEquipoModifica(" + codi_promocion + "," + codi_producto + "," + codi_equipo + "," + tipo_relacoin + "," + cant_permanencia + "," + fecha_inicio + "," + fecha_termino + "); end;"
    UpdPromEqui = fn_error(Err.Description, sLlamadaProc)
    UpdPromEqui = False
End Function
