Attribute VB_Name = "modInsertFrecuentes"
Option Explicit

Function InsFrecuentes(ByVal iFono As String, ByVal iFechaAlta As String, ByVal iUsuario As String, ByRef osbResult As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsFrecuentes As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.FrecuentesAgrega(?,?,?,?); end;"

    Set vrInsFrecuentes = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, iFono)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, iFechaAlta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 15, iUsuario)
        .Parameters.Append adoCmd.CreateParameter("osbResult", adVarChar, adParamOutput, 32767)
    End With
    On Error GoTo Error
    vrInsFrecuentes.Open adoCmd
    
    osbResult = adoCmd.Parameters("osbResult")

    Set vrInsFrecuentes = Nothing

    InsFrecuentes = True
    Exit Function

Error:
    Set vrInsFrecuentes = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.FrecuentesAgrega(" & iFono & ",'" & iFechaAlta & "','" & iUsuario _
                    & "', :osbResult); end;"
    InsFrecuentes = fn_error(Err.Description, sLlamadaProc)
    InsFrecuentes = False
End Function







