Attribute VB_Name = "modConsultasProductos"
Option Explicit
Global vrGetProductos As ADODB.Recordset
Global vrGetProdRela As ADODB.Recordset
Global vrGetProdLoca As ADODB.Recordset
Global vrGetProdFaci As ADODB.Recordset
Global vrGetProdEquip As ADODB.Recordset

Public Function GetProductos(ByVal filtro As Integer) As Long
    'Extrae los productos Del sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_producto, desc_producto,  "
    sSql1 = sSql1 & "codi_producto, desc_producto,  "
    sSql = sSql & "to_char(fech_creacion,'dd-mm-yyyy'),  "
    sSql1 = sSql1 & "to_char(fech_creacion,'dd-mm-yyyy'),  "
    sSql = sSql & "to_char(fech_fin,'dd-mm-yyyy'), "
    sSql1 = sSql1 & "to_char(fech_fin,'dd-mm-yyyy'), "
    sSql = sSql & "siaa_promociones.ProductoOK(codi_producto) "
    sSql1 = sSql1 & "siaa_promociones.ProductoOK(codi_producto) "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "SIAT_PR_PRODUCTO  "
    sSql1 = sSql1 & "SIAT_PR_PRODUCTO  "
    sSql = sSql & " "
    sSql1 = sSql1 & " "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "decode(?,0,0,decode(sign(nvl(fech_fin,sysdate)- sysdate),-1,-1,1)) = ? "
    sSql1 = sSql1 & "decode(" & Str(filtro) & ",0,0,decode(sign(nvl(fech_fin,sysdate)- sysdate),-1,-1,1)) = " & Str(filtro) & " "
    sSql = sSql & "order by codi_producto "
    sSql1 = sSql1 & "order by codi_producto "
    On Error GoTo Error
    Set vrGetProductos = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-filtro", adInteger, adParamInput, 1, filtro)
        .Parameters.Append adoCmd.CreateParameter(":SI-filtro", adInteger, adParamInput, 1, filtro)
    End With
    Screen.MousePointer = vbHourglass
    vrGetProductos.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetProductos.EOF Then
        vrGetProductos.MoveLast
        CantidadTuplas = vrGetProductos.RecordCount
        vrGetProductos.MoveFirst
        GetProductos = CantidadTuplas
    Else
        GetProductos = -1
    End If
    Exit Function

Error:
    Set vrGetProductos = Nothing
    Screen.MousePointer = vbDefault
    GetProductos = fn_error(Err.Description, sSql1)
    GetProductos = -1
End Function

Function GetProductosClose() As Integer

    If Not vrGetProductos Is Nothing Then
        vrGetProductos.Close
        Set vrGetProductos = Nothing
    End If
End Function

Function GetProductosRead(ByRef codi_producto As String, ByRef desc_producto As String, ByRef fecha_creacion As String, ByRef fecha_fin As String, ByRef producto_ok As Integer) As Integer
    Dim res As Integer

    GetProductosRead = True

    If vrGetProductos Is Nothing Then
        GetProductosRead = False
        Exit Function
    End If
    If vrGetProductos.EOF Then
        res = GetProductosClose()
        GetProductosRead = False
        Exit Function
    End If

    codi_producto = gfnDevuelveValorCampo("STRING", vrGetProductos.Fields(0).Type, vrGetProductos.Fields(0))
    desc_producto = gfnDevuelveValorCampo("STRING", vrGetProductos.Fields(1).Type, vrGetProductos.Fields(1))
    fecha_creacion = gfnDevuelveValorCampo("STRING", vrGetProductos.Fields(2).Type, vrGetProductos.Fields(2))
    fecha_fin = gfnDevuelveValorCampo("STRING", vrGetProductos.Fields(3).Type, vrGetProductos.Fields(3))
    producto_ok = gfnDevuelveValorCampo("INTEGER", vrGetProductos.Fields(4).Type, vrGetProductos.Fields(4))

    vrGetProductos.MoveNext

End Function

Public Function GetProdRela(ByVal CodiProducto As String) As Long
    'Extrae las relaciones del producto
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "t.desc_grupo, p.desc_parame, t.codi_grupo, p.codi_elem "
    sSql1 = sSql1 & "t.desc_grupo, p.desc_parame, t.codi_grupo, p.codi_elem "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_prod_relacion r, part_titulos t, part_parame p "
    sSql1 = sSql1 & "siat_pr_prod_relacion r, part_titulos t, part_parame p "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_producto = ? "
    sSql1 = sSql1 & "codi_producto = '" & CodiProducto & "' "
    sSql = sSql & "and t.codi_grupo(+) = r.codi_grupo  "
    sSql1 = sSql1 & "and t.codi_grupo(+) = r.codi_grupo  "
    sSql = sSql & "and p.codi_grupo = r.codi_grupo  "
    sSql1 = sSql1 & "and p.codi_grupo = r.codi_grupo  "
    sSql = sSql & "and p.codi_elem = r.codi_elem "
    sSql1 = sSql1 & "and p.codi_elem = r.codi_elem "
    sSql = sSql & "order by lpad(t.codi_grupo,2), lpad(p.codi_elem,2) "
    sSql1 = sSql1 & "order by lpad(t.codi_grupo,2), lpad(p.codi_elem,2) "
    On Error GoTo Error
    Set vrGetProdRela = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiProducto", adVarChar, adParamInput, 10, CodiProducto)
    End With
    Screen.MousePointer = vbHourglass
    vrGetProdRela.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetProdRela.EOF Then
        vrGetProdRela.MoveLast
        CantidadTuplas = vrGetProdRela.RecordCount
        vrGetProdRela.MoveFirst
        GetProdRela = CantidadTuplas
    Else
        GetProdRela = -1
    End If
    Exit Function

Error:
    Set vrGetProdRela = Nothing
    Screen.MousePointer = vbDefault
    GetProdRela = fn_error(Err.Description, sSql1)
    GetProdRela = -1
End Function

Function GetProdRelaClose() As Integer

    If Not vrGetProdRela Is Nothing Then
        vrGetProdRela.Close
        Set vrGetProdRela = Nothing
    End If
End Function

Function GetProdRelaRead(ByRef desc_grupo As String, ByRef desc_parame As String, ByRef codi_grupo As String, ByRef codi_elem As String) As Integer
    Dim res As Integer

    GetProdRelaRead = True

    If vrGetProdRela Is Nothing Then
        GetProdRelaRead = False
        Exit Function
    End If
    If vrGetProdRela.EOF Then
        res = GetProdRelaClose()
        GetProdRelaRead = False
        Exit Function
    End If

    desc_grupo = gfnDevuelveValorCampo("STRING", vrGetProdRela.Fields(0).Type, vrGetProdRela.Fields(0))
    desc_parame = gfnDevuelveValorCampo("STRING", vrGetProdRela.Fields(1).Type, vrGetProdRela.Fields(1))
    codi_grupo = gfnDevuelveValorCampo("STRING", vrGetProdRela.Fields(2).Type, vrGetProdRela.Fields(2))
    codi_elem = gfnDevuelveValorCampo("STRING", vrGetProdRela.Fields(3).Type, vrGetProdRela.Fields(3))

    vrGetProdRela.MoveNext

End Function


Public Function GetProdLoca(ByVal CodiProducto As String) As Long
    'Extrae las localidades asociadas a un producto
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "l.desc_localida, l.codi_localida "
    sSql1 = sSql1 & "l.desc_localida, l.codi_localida "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_prod_localidad pl, part_localida l "
    sSql1 = sSql1 & "siat_pr_prod_localidad pl, part_localida l "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_producto = ? "
    sSql1 = sSql1 & "codi_producto = '" & CodiProducto & "' "
    sSql = sSql & "and rpad(pl.codi_localidad,4) = l.codi_localida  "
    sSql1 = sSql1 & "and rpad(pl.codi_localidad,4) = l.codi_localida  "
    sSql = sSql & "order by pl.codi_localidad "
    sSql1 = sSql1 & "order by pl.codi_localidad "
    On Error GoTo Error
    Set vrGetProdLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiProducto", adVarChar, adParamInput, 10, CodiProducto)
    End With
    Screen.MousePointer = vbHourglass
    vrGetProdLoca.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetProdLoca.EOF Then
        vrGetProdLoca.MoveLast
        CantidadTuplas = vrGetProdLoca.RecordCount
        vrGetProdLoca.MoveFirst
        GetProdLoca = CantidadTuplas
    Else
        GetProdLoca = -1
    End If
    Exit Function

Error:
    Set vrGetProdLoca = Nothing
    Screen.MousePointer = vbDefault
    GetProdLoca = fn_error(Err.Description, sSql1)
    GetProdLoca = -1
End Function

Function GetProdLocaClose() As Integer

    If Not vrGetProdLoca Is Nothing Then
        vrGetProdLoca.Close
        Set vrGetProdLoca = Nothing
    End If
End Function

Function GetProdLocaRead(ByRef desc_localidad As String, ByRef codi_localidad As String) As Integer
    Dim res As Integer

    GetProdLocaRead = True

    If vrGetProdLoca Is Nothing Then
        GetProdLocaRead = False
        Exit Function
    End If
    If vrGetProdLoca.EOF Then
        res = GetProdLocaClose()
        GetProdLocaRead = False
        Exit Function
    End If

    desc_localidad = gfnDevuelveValorCampo("STRING", vrGetProdLoca.Fields(0).Type, vrGetProdLoca.Fields(0))
    codi_localidad = gfnDevuelveValorCampo("STRING", vrGetProdLoca.Fields(1).Type, vrGetProdLoca.Fields(1))

    vrGetProdLoca.MoveNext

End Function


Public Function GetProdFaci(ByVal CodiProducto As String, ByVal CodiEquipo As String) As Long
    'Extrae las facilidades asociadas a un equipo de un producto
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_facilida, desc_tiposerv "
    sSql1 = sSql1 & "codi_facilida, desc_tiposerv "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_suplementario ps, siat_tiposerv t "
    sSql1 = sSql1 & "siat_pr_suplementario ps, siat_tiposerv t "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_producto = ? "
    sSql1 = sSql1 & "codi_producto = '" & CodiProducto & "' "
    sSql = sSql & "and codi_equipo = ? "
    sSql1 = sSql1 & "and codi_equipo = '" & CodiEquipo & "' "
    sSql = sSql & "and ps.codi_facilida = t.codi_tiposerv  "
    sSql1 = sSql1 & "and ps.codi_facilida = t.codi_tiposerv  "
    sSql = sSql & "order by lpad(ps.codi_facilida,6) "
    sSql1 = sSql1 & "order by lpad(ps.codi_facilida,6) "
    On Error GoTo Error
    Set vrGetProdFaci = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiProducto", adVarChar, adParamInput, 10, CodiProducto)
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiEquipo", adVarChar, adParamInput, 4, CodiEquipo)
    End With
    Screen.MousePointer = vbHourglass
    vrGetProdFaci.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetProdFaci.EOF Then
        vrGetProdFaci.MoveLast
        CantidadTuplas = vrGetProdFaci.RecordCount
        vrGetProdFaci.MoveFirst
        GetProdFaci = CantidadTuplas
    Else
        GetProdFaci = -1
    End If
    Exit Function

Error:
    Set vrGetProdFaci = Nothing
    Screen.MousePointer = vbDefault
    GetProdFaci = fn_error(Err.Description, sSql1)
    GetProdFaci = -1
End Function

Function GetProdFaciClose() As Integer

    If Not vrGetProdFaci Is Nothing Then
        vrGetProdFaci.Close
        Set vrGetProdFaci = Nothing
    End If
End Function

Function GetProdFaciRead(ByRef codi_facilida As String, ByRef desc_facilidad As String) As Integer
    Dim res As Integer

    GetProdFaciRead = True

    If vrGetProdFaci Is Nothing Then
        GetProdFaciRead = False
        Exit Function
    End If
    If vrGetProdFaci.EOF Then
        res = GetProdFaciClose()
        GetProdFaciRead = False
        Exit Function
    End If

    codi_facilida = gfnDevuelveValorCampo("STRING", vrGetProdFaci.Fields(0).Type, vrGetProdFaci.Fields(0))
    desc_facilidad = gfnDevuelveValorCampo("STRING", vrGetProdFaci.Fields(1).Type, vrGetProdFaci.Fields(1))

    vrGetProdFaci.MoveNext

End Function


Public Function GetProdEquip(ByVal CodiProducto As String) As Long
    'Extrae los equipos asociados a un producto
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select distinct "
    sSql1 = "select distinct "
    sSql = sSql & "im.desc_modelo, pe.cant_equipo, pe.codi_exclusion,  "
    sSql1 = sSql1 & "im.desc_modelo, pe.cant_equipo, pe.codi_exclusion,  "
    sSql = sSql & "pe.codi_equipo "
    sSql1 = sSql1 & "pe.codi_equipo "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_equipo pe, siat_inv_modelo im "
    sSql1 = sSql1 & "siat_pr_equipo pe, siat_inv_modelo im "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_producto = ? "
    sSql1 = sSql1 & "codi_producto = '" & CodiProducto & "' "
    sSql = sSql & "and pe.codi_equipo = im.codi_tipoequi  "
    sSql1 = sSql1 & "and pe.codi_equipo = im.codi_tipoequi  "
    sSql = sSql & "order by im.desc_modelo "
    sSql1 = sSql1 & "order by im.desc_modelo "
    On Error GoTo Error
    Set vrGetProdEquip = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiProducto", adVarChar, adParamInput, 10, CodiProducto)
    End With
    Screen.MousePointer = vbHourglass
    vrGetProdEquip.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetProdEquip.EOF Then
        vrGetProdEquip.MoveLast
        CantidadTuplas = vrGetProdEquip.RecordCount
        vrGetProdEquip.MoveFirst
        GetProdEquip = CantidadTuplas
    Else
        GetProdEquip = -1
    End If
    Exit Function

Error:
    Set vrGetProdEquip = Nothing
    Screen.MousePointer = vbDefault
    GetProdEquip = fn_error(Err.Description, sSql1)
    GetProdEquip = -1
End Function

Function GetProdEquipClose() As Integer

    If Not vrGetProdEquip Is Nothing Then
        vrGetProdEquip.Close
        Set vrGetProdEquip = Nothing
    End If
End Function

Function GetProdEquipRead(ByRef desc_modelo As String, ByRef cant_equipo As String, ByRef codi_exclusion As String, ByRef codi_equipo As String) As Integer
    Dim res As Integer

    GetProdEquipRead = True

    If vrGetProdEquip Is Nothing Then
        GetProdEquipRead = False
        Exit Function
    End If
    If vrGetProdEquip.EOF Then
        res = GetProdEquipClose()
        GetProdEquipRead = False
        Exit Function
    End If

    desc_modelo = gfnDevuelveValorCampo("STRING", vrGetProdEquip.Fields(0).Type, vrGetProdEquip.Fields(0))
    cant_equipo = gfnDevuelveValorCampo("STRING", vrGetProdEquip.Fields(1).Type, vrGetProdEquip.Fields(1))
    codi_exclusion = gfnDevuelveValorCampo("STRING", vrGetProdEquip.Fields(2).Type, vrGetProdEquip.Fields(2))
    codi_equipo = gfnDevuelveValorCampo("STRING", vrGetProdEquip.Fields(3).Type, vrGetProdEquip.Fields(3))

    vrGetProdEquip.MoveNext

End Function




