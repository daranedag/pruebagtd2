Attribute VB_Name = "modInsertProductos"
Option Explicit

Function InsProducto(ByVal codi_producto As String, _
                     ByVal desc_producto As String, _
                     ByVal fecha_fin As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsProducto As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProductoAgrega(?,?,?); end;"

    Set vrInsProducto = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fecha_fin)
    End With
    On Error GoTo Error
    vrInsProducto.Open adoCmd

    Set vrInsProducto = Nothing

    InsProducto = True
    Exit Function

Error:
    Set vrInsProducto = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.NuevoProducto(" + codi_producto + "," + desc_producto + "," + fecha_fin + "); end;"
    InsProducto = fn_error(Err.Description, sLlamadaProc)
    InsProducto = False
End Function

Function InsProdEqui(ByVal codi_producto As String, _
                     ByVal codi_equipo As String, _
                     ByVal cant_equipo As Integer, _
                     ByVal codi_exclusion As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsProdEqui As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdEquipoAgrega(?,?,?,?); end;"

    Set vrInsProdEqui = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_equipo)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, cant_equipo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_exclusion)
    End With
    On Error GoTo Error
    vrInsProdEqui.Open adoCmd

    Set vrInsProdEqui = Nothing

    InsProdEqui = True
    Exit Function

Error:
    Set vrInsProdEqui = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdEquipoAgrega(" + codi_producto + "," + codi_equipo + "," + Trim(Str(cant_equipo)) + "," + codi_exclusion + "); end;"
    InsProdEqui = fn_error(Err.Description, sLlamadaProc)
    InsProdEqui = False
End Function


Function InsProdLoca(ByVal codi_producto As String, _
                     ByVal codi_localidad As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsProdLoca As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdLocalidadAgrega(?,?); end;"

    Set vrInsProdLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_localidad)
    End With
    On Error GoTo Error
    vrInsProdLoca.Open adoCmd

    Set vrInsProdLoca = Nothing

    InsProdLoca = True
    Exit Function

Error:
    Set vrInsProdLoca = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdLocalidadAgrega(" + codi_producto + "," + codi_localidad + "); end;"
    InsProdLoca = fn_error(Err.Description, sLlamadaProc)
    InsProdLoca = False
End Function


Function InsProdRela(ByVal codi_producto As String, _
                     ByVal codi_grupo As String, _
                     ByVal codi_elem As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsProdRela As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdRelacionAgrega(?,?,?); end;"

    Set vrInsProdRela = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, codi_grupo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_elem)
    End With
    On Error GoTo Error
    vrInsProdRela.Open adoCmd

    Set vrInsProdRela = Nothing

    InsProdRela = True
    Exit Function

Error:
    Set vrInsProdRela = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdRelacionAgrega(" + codi_producto + "," + codi_grupo + "," + codi_elem + "); end;"
    InsProdRela = fn_error(Err.Description, sLlamadaProc)
    InsProdRela = False
End Function


Function InsProdSuple(ByVal codi_producto As String, _
                      ByVal codi_equipo As String, _
                      ByVal codi_facilidad As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsProdSuple As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdSuplementarioAgrega(?,?,?); end;"

    Set vrInsProdSuple = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_equipo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, codi_facilidad)
    End With
    On Error GoTo Error
    vrInsProdSuple.Open adoCmd

    Set vrInsProdSuple = Nothing

    InsProdSuple = True
    Exit Function

Error:
    Set vrInsProdSuple = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdSuplementarioAgregar(" + codi_producto + "," + codi_equipo + "," + codi_facilidad + "); end;"
    InsProdSuple = fn_error(Err.Description, sLlamadaProc)
    InsProdSuple = False
End Function

Function CopiaProd(ByVal CodiProd As String, _
                   ByVal CodiNuevo As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCopiaProd As ADODB.Recordset

    sLlamadaProc = "begin siaa_promociones.ProductoCopia(?,?); end;"

    Set vrCopiaProd = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, CodiProd)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, CodiNuevo)
    End With
    On Error GoTo Error
    vrCopiaProd.Open adoCmd

    Set vrCopiaProd = Nothing

    CopiaProd = True
    Exit Function

Error:
    Set vrCopiaProd = Nothing
    sLlamadaProc = "begin siaa_promociones.ProductoCopiar(" + CodiProd + "," + CodiNuevo + "); end;"
    CopiaProd = fn_error(Err.Description, sLlamadaProc)
    CopiaProd = False
End Function



