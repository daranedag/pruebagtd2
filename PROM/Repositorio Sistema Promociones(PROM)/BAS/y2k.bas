'Prueba

Function convierte (date1)

    If UCase(Mid$(date1, 1, 9)) = "29-FEB-00" Or UCase(Mid$(date1, 1, 9)) = "29/FEB/00" Then
	convierte = Mid$(date1, 1, 7) + "20" + Mid$(date1, 8)
	Exit Function
    ElseIf Mid$(date1, 1, 8) = "29-02-00" Or Mid$(date1, 1, 8) = "29/02/00" Then
	convierte = Mid$(date1, 1, 6) + "20" + Mid$(date1, 7)
	Exit Function
    Else
	If Val(Format(date1, "yy")) < 49 Then
	    convierte = Format(date1, "dd-mm-20yy hh:nn:ss")
	    Exit Function
	Else
	    convierte = date1
	    Exit Function
	End If
    End If
	resp = MsgBox("Error en la Función de Conversión de la fecha: " + date1, 48, "MENSAJE DE ERROR")
End Function

Function CVDate_cnt (expresion)
    CVDate_cnt = CVDate(convierte(expresion))
End Function

Function DateAdd_cnt (interval, cantidad, date2) As String
    DateAdd_cnt = DateAdd(interval, cantidad, convierte(date2))
End Function

Function DateDiff_cnt (interval, date1, date2)
    DateDiff_cnt = DateDiff(interval, convierte(date1), convierte(date2))
End Function

Function datepart_cnt (interval, date1)
    datepart_cnt = DatePart(interval, convierte(date1))
End Function

Function DateValue_Cnt (ByVal expresion As String) As Variant
    Dim s
    s = convierte(expresion)
    DateValue_Cnt = DateValue(s)
End Function

Function Format_cnt (date1, formato)                 ' as Variant
    Format_cnt = Format(convierte(date1), formato)
End Function

Function Format_cntS (date1, formato)                ' as String
    Format_cntS = Format$(convierte(date1), formato)
End Function

Function IsDate_Cnt (ByVal date1)
    IsDate_Cnt = IsDate(convierte(date1))
End Function

Function weekday_cnt (date1)
	weekday_cnt = Weekday(convierte(date1))
End Function

