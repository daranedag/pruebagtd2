Attribute VB_Name = "modDeleteProductos"
Option Explicit

Function DelProdRela(ByVal codi_producto As String, _
                     ByVal codi_grupo As String, _
                     ByVal codi_elem As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelProdRela As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdRelacionElimina(?,?,?); end;"

    Set vrDelProdRela = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 4, codi_grupo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_elem)
    End With
    On Error GoTo Error
    vrDelProdRela.Open adoCmd

    Set vrDelProdRela = Nothing

    DelProdRela = True
    Exit Function

Error:
    Set vrDelProdRela = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdRelacionElimina(" + codi_producto + "," + codi_grupo + "," + codi_elem + "); end;"
    DelProdRela = fn_error(Err.Description, sLlamadaProc)
    DelProdRela = False
End Function


Function DelProdLoca(ByVal codi_producto As String, _
                     ByVal codi_localidad As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelProdLoca As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdLocalidadElimina(?,?); end;"

    Set vrDelProdLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_localidad)
    End With
    On Error GoTo Error
    vrDelProdLoca.Open adoCmd

    Set vrDelProdLoca = Nothing

    DelProdLoca = True
    Exit Function

Error:
    Set vrDelProdLoca = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdLocalidadElimina(" + codi_producto + "," + codi_localidad + "); end;"
    DelProdLoca = fn_error(Err.Description, sLlamadaProc)
    DelProdLoca = False
End Function


Function DelProdEqui(ByVal codi_producto As String, _
                     ByVal codi_equipo As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelProdEqui As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdEquipoElimina(?,?); end;"

    Set vrDelProdEqui = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_equipo)
    End With
    On Error GoTo Error
    vrDelProdEqui.Open adoCmd

    Set vrDelProdEqui = Nothing

    DelProdEqui = True
    Exit Function

Error:
    Set vrDelProdEqui = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdEquipoElimina(" + codi_producto + "," + codi_equipo + "); end;"
    DelProdEqui = fn_error(Err.Description, sLlamadaProc)
    DelProdEqui = False
End Function


Function DelProdSuple(ByVal codi_producto As String, _
                      ByVal codi_equipo As String, _
                      ByVal codi_facilidad As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelProdSuple As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdSuplementarioElimina(?,?,?); end;"

    Set vrDelProdSuple = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_equipo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, codi_facilidad)
    End With
    On Error GoTo Error
    vrDelProdSuple.Open adoCmd

    Set vrDelProdSuple = Nothing

    DelProdSuple = True
    Exit Function

Error:
    Set vrDelProdSuple = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.ProdSuplementarioEliminar(" + codi_producto + "," + codi_equipo + "," + codi_facilidad + "); end;"
    DelProdSuple = fn_error(Err.Description, sLlamadaProc)
    DelProdSuple = False
End Function




