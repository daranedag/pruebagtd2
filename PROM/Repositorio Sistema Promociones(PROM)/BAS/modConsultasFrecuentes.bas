Attribute VB_Name = "modConsultasFrecuentes"
Option Explicit

Global vrGetFrecuentes As ADODB.Recordset

Public Function GetFrecuentes() As Long
    'Lista las Frecuentes existentes en el sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select fono_servicio, fech_alta, fech_baja, usuario, fech_aprovisiona "
    sSql1 = "select fono_servicio, fech_alta, fech_baja, usuario, fech_aprovisiona """
    sSql = sSql & "from siat_omv_frecuentes  "
    sSql1 = sSql1 & "from siat_omv_frecuentes  "
    
    On Error GoTo Error
    Set vrGetFrecuentes = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetFrecuentes.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetFrecuentes.EOF Then
        vrGetFrecuentes.MoveLast
        CantidadTuplas = vrGetFrecuentes.RecordCount
        vrGetFrecuentes.MoveFirst
        GetFrecuentes = CantidadTuplas
    Else
        GetFrecuentes = -1
    End If
    Exit Function

Error:
    Set vrGetFrecuentes = Nothing
    Screen.MousePointer = vbDefault
    GetFrecuentes = fn_error(Err.Description, sSql1)
    GetFrecuentes = -1
End Function

Function GetFrecuentesClose() As Integer

    If Not vrGetFrecuentes Is Nothing Then
        vrGetFrecuentes.Close
        Set vrGetFrecuentes = Nothing
    End If
End Function

Function GetFrecuentesRead(ByRef fono_servicio As String, ByRef fech_alta As String, ByRef fech_baja As String, ByRef usuario As String) As Integer
    Dim res As Integer

    GetFrecuentesRead = True

    If vrGetFrecuentes Is Nothing Then
        GetFrecuentesRead = False
        Exit Function
    End If
    If vrGetFrecuentes.EOF Then
        res = GetFrecuentesClose()
        GetFrecuentesRead = False
        Exit Function
    End If

    fono_servicio = gfnDevuelveValorCampo("STRING", vrGetFrecuentes.Fields(0).Type, vrGetFrecuentes.Fields(0))
    fech_alta = gfnDevuelveValorCampo("STRING", vrGetFrecuentes.Fields(1).Type, vrGetFrecuentes.Fields(1))
    fech_baja = gfnDevuelveValorCampo("STRING", vrGetFrecuentes.Fields(2).Type, vrGetFrecuentes.Fields(2))
    usuario = gfnDevuelveValorCampo("STRING", vrGetFrecuentes.Fields(3).Type, vrGetFrecuentes.Fields(3))
    
    vrGetFrecuentes.MoveNext

End Function
