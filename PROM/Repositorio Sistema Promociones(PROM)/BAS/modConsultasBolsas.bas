Attribute VB_Name = "modConsultasBolsas"
Option Explicit

Global vrGetBolsas As ADODB.Recordset

Public Function GetBolsas() As Long
    'Lista las bolsas existentes en el sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select a.id_bolsa, a.desc_bolsa, a.codi_facilida, a.precio, a.dias_vigencia, a.minutos_on_net, a.minutos_off_net, "
    sSql1 = "select a.id_bolsa, a.desc_bolsa, a.codi_facilida, a.precio, a.dias_vigencia, a.minutos_on_net, a.minutos_off_net, "
    sSql = sSql & "a.minutos_todo_destino, a.minutos_rural, a.minutos_isla_pascua, a.minutos_roaming, a.q_sms, a.nombre_mswitch, "
    sSql1 = sSql1 & "a.minutos_todo_destino, a.minutos_rural, a.minutos_isla_pascua, a.minutos_roaming, a.q_sms, a.nombre_mswitch, "
    sSql = sSql & "a.q_mb_datos, a.q_mb_datos_roaming, a.nombre_radius, decode(a.visible_ivr, 'S', 'SI', 'N', 'NO') visible_ivr, "
    sSql1 = sSql1 & "a.q_mb_datos, a.q_mb_datos_roaming, a.nombre_radius, decode(a.visible_ivr, 'S', 'SI', 'N', 'NO') visible_ivr, "
    sSql = sSql & "decode(a.visible_web, 'S', 'SI', 'N', 'NO') visible_web, a.codi_paquete, nvl(a.dinero, 0) dinero, "
    sSql1 = sSql1 & "decode(a.visible_web, 'S', 'SI', 'N', 'NO') visible_web, a.codi_paquete, nvl(a.dinero, 0) dinero, "
    sSql = sSql & "decode(nvl(a.carga_auto_ini_mes, 'N'), 'S', 'SI', 'N', 'NO') carga_auto_ini_mes, "
    sSql1 = sSql1 & "decode(nvl(a.carga_auto_ini_mes, 'N'), 'S', 'SI', 'N', 'NO') carga_auto_ini_mes, "
    sSql = sSql & "decode(nvl(a.carga_boleta, 'N'), 'S', 'SI', 'N', 'NO') carga_boleta "
    sSql1 = sSql1 & "decode(nvl(a.carga_boleta, 'N'), 'S', 'SI', 'N', 'NO') carga_boleta "
    sSql = sSql & "from siat_omv_bolsa a "
    sSql1 = sSql1 & "from siat_omv_bolsa a "
    sSql = sSql & "order by a.id_bolsa "
    sSql1 = sSql1 & "order by a.id_bolsa "

    On Error GoTo Error
    Set vrGetBolsas = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetBolsas.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetBolsas.EOF Then
        vrGetBolsas.MoveLast
        CantidadTuplas = vrGetBolsas.RecordCount
        vrGetBolsas.MoveFirst
        GetBolsas = CantidadTuplas
    Else
        GetBolsas = -1
    End If
    Exit Function

Error:
    Set vrGetBolsas = Nothing
    Screen.MousePointer = vbDefault
    GetBolsas = fn_error(Err.Description, sSql1)
    GetBolsas = -1
End Function

Function GetBolsasClose() As Integer

    If Not vrGetBolsas Is Nothing Then
        vrGetBolsas.Close
        Set vrGetBolsas = Nothing
    End If
End Function

Function GetBolsasRead(ByRef id_bolsa As Long, ByRef desc_bolsa As String, ByRef codi_facilida As String, ByRef precio As Long, ByRef dias_vigencia As Long, ByRef minutos_on_net As Long, ByRef minutos_off_net As Long, ByRef minutos_todo_destino As Long, ByRef minutos_rural As Long, ByRef minutos_isla_pascua As Long, ByRef minutos_roaming As Long, ByRef q_sms As Long, ByRef nombre_mswitch As String, ByRef q_mb_datos As Long, ByRef q_mb_datos_roaming As Long, ByRef nombre_radius As String, ByRef visible_ivr As String, ByRef visible_web As String, ByRef codi_paquete As String, ByRef dinero As Long, ByRef carga_auto_ini_mes As String, ByRef flag_cargaboleta As String) As Integer
    Dim res As Integer

    GetBolsasRead = True

    If vrGetBolsas Is Nothing Then
        GetBolsasRead = False
        Exit Function
    End If
    If vrGetBolsas.EOF Then
        res = GetBolsasClose()
        GetBolsasRead = False
        Exit Function
    End If

    id_bolsa = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(0).Type, vrGetBolsas.Fields(0))
    desc_bolsa = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(1).Type, vrGetBolsas.Fields(1))
    codi_facilida = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(2).Type, vrGetBolsas.Fields(2))
    precio = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(3).Type, vrGetBolsas.Fields(3))
    dias_vigencia = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(4).Type, vrGetBolsas.Fields(4))
    minutos_on_net = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(5).Type, vrGetBolsas.Fields(5))
    minutos_off_net = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(6).Type, vrGetBolsas.Fields(6))
    minutos_todo_destino = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(7).Type, vrGetBolsas.Fields(7))
    minutos_rural = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(8).Type, vrGetBolsas.Fields(8))
    minutos_isla_pascua = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(9).Type, vrGetBolsas.Fields(9))
    minutos_roaming = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(10).Type, vrGetBolsas.Fields(10))
    q_sms = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(11).Type, vrGetBolsas.Fields(11))
    nombre_mswitch = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(12).Type, vrGetBolsas.Fields(12))
    q_mb_datos = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(13).Type, vrGetBolsas.Fields(13))
    q_mb_datos_roaming = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(14).Type, vrGetBolsas.Fields(14))
    nombre_radius = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(15).Type, vrGetBolsas.Fields(15))
    visible_ivr = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(16).Type, vrGetBolsas.Fields(16))
    visible_web = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(17).Type, vrGetBolsas.Fields(17))
    codi_paquete = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(18).Type, vrGetBolsas.Fields(18))
    dinero = gfnDevuelveValorCampo("LONG", vrGetBolsas.Fields(19).Type, vrGetBolsas.Fields(19))
    carga_auto_ini_mes = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(20).Type, vrGetBolsas.Fields(20))
    flag_cargaboleta = gfnDevuelveValorCampo("STRING", vrGetBolsas.Fields(21).Type, vrGetBolsas.Fields(21))
    vrGetBolsas.MoveNext

End Function
