Attribute VB_Name = "modConsultasOfertas"
Option Explicit

Global vrGetOferta As ADODB.Recordset
Global vrGetParam As ADODB.Recordset
Global vrGetOferLoca As ADODB.Recordset
Global vrGetDetOfer As ADODB.Recordset
Global vrSelConcepto As ADODB.Recordset
Global vrSelVConcepto As ADODB.Recordset

Public Function GetOfertas(ByVal p_vigentes As Integer, ByVal p_novigentes As Integer) As Long
    'Extrae las ofertas del sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "o.codi_oferta, o.desc_oferta, o.observacion, "
    sSql1 = sSql1 & "o.codi_oferta, o.desc_oferta, o.observacion, "
    sSql = sSql & "o.vlor_oferta, o.vlor_promocion, "
    sSql1 = sSql1 & "o.vlor_oferta, o.vlor_promocion, "
    sSql = sSql & "to_char(o.fech_creacion,'dd-mm-yyyy') creacion, "
    sSql1 = sSql1 & "to_char(o.fech_creacion,'dd-mm-yyyy') creacion, "
    sSql = sSql & "to_char(o.fech_inivigen,'dd-mm-yyyy') inicio, "
    sSql1 = sSql1 & "to_char(o.fech_inivigen,'dd-mm-yyyy') inicio, "
    sSql = sSql & "to_char(o.fech_finvigen,'dd-mm-yyyy') fin, "
    sSql1 = sSql1 & "to_char(o.fech_finvigen,'dd-mm-yyyy') fin, "
    sSql = sSql & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = o.codi_cargo and c.fech_finvigen is null) cargo, "
    sSql1 = sSql1 & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = o.codi_cargo and c.fech_finvigen is null) cargo, "
    
    sSql = sSql & "(select c.codi_interno from typt_conceptos c where c.codi_interno = o.codi_cargo and c.fech_finvigen is null) codi_cargo, "
    sSql1 = sSql1 & "(select c.codi_interno from typt_conceptos c where c.codi_interno = o.codi_cargo and c.fech_finvigen is null) codi_cargo, "
    
    sSql = sSql & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = o.codi_dcto and c.fech_finvigen is null) dcto, "
    sSql1 = sSql1 & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = o.codi_dcto and c.fech_finvigen is null) dcto, "
    
    sSql = sSql & "(select c.codi_interno from typt_conceptos c where c.codi_interno = o.codi_dcto and c.fech_finvigen is null) codi_dcto, "
    sSql1 = sSql1 & "(select c.codi_interno from typt_conceptos c where c.codi_interno = o.codi_dcto and c.fech_finvigen is null) codi_dcto, "
    
    sSql = sSql & "  o.pack_compartido "
    sSql1 = sSql1 & "  o.pack_compartido "
    
    sSql = sSql & ",(SELECT c.unid_monetar|| ' ' || p.desc_parame FROM typt_conceptos c, ptar.part_valmoned m, part_parame p WHERE p.codi_grupo = 9 AND p.codi_elem = m.codi_moneda AND SYSDATE BETWEEN m.fech_inicio AND nvl(m.fech_fin, SYSDATE) AND m.codi_moneda = c.UNID_MONETAR AND c.codi_interno = o.codi_cargo AND c.fech_finvigen IS NULL) unid_monetar, o.codi_agrupa "
    sSql1 = sSql1 & ",(SELECT c.unid_monetar|| ' ' || p.desc_parame FROM typt_conceptos c, ptar.part_valmoned m, part_parame p WHERE p.codi_grupo = 9 AND p.codi_elem = m.codi_moneda AND SYSDATE BETWEEN m.fech_inicio AND nvl(m.fech_fin, SYSDATE) AND m.codi_moneda = c.UNID_MONETAR AND c.codi_interno = o.codi_cargo AND c.fech_finvigen IS NULL) unid_monetar, o.codi_agrupa "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_of_oferta o "
    sSql1 = sSql1 & "siat_of_oferta o "
    
    If p_vigentes = 0 And p_novigentes = 0 Then
        sSql = sSql + " where 1<>1 "
    End If
                
    If p_vigentes = 1 And p_novigentes = 0 Then
        sSql = sSql + " where sysdate >= o.fech_inivigen and sysdate <= nvl(o.fech_finvigen,sysdate)"
    End If
    
    If p_vigentes = 0 And p_novigentes = 1 Then
        sSql = sSql + " where (sysdate > o.Fech_Finvigen Or sysdate < o.Fech_Finvigen)"
    End If
    
    sSql = sSql & "order by codi_oferta "
    sSql1 = sSql1 & "order by codi_oferta "
    On Error GoTo Error
    Set vrGetOferta = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetOferta.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetOferta.EOF Then
        vrGetOferta.MoveLast
        CantidadTuplas = vrGetOferta.RecordCount
        vrGetOferta.MoveFirst
        GetOfertas = CantidadTuplas
    Else
        GetOfertas = 0
    End If
    Exit Function

Error:
    Set vrGetOferta = Nothing
    Screen.MousePointer = vbDefault
    GetOfertas = fn_error(Err.Description, sSql1)
    GetOfertas = -1
End Function

Function GetOfertasClose() As Integer

    If Not vrGetOferta Is Nothing Then
        vrGetOferta.Close
        Set vrGetOferta = Nothing
    End If
End Function

Function GetOfertasRead(ByRef codi_oferta As Long, _
                        ByRef desc_oferta As String, _
                        ByRef observacion As String, _
                        ByRef vlor_oferta As Double, _
                        ByRef vlor_promocion As Double, _
                        ByRef fecha_creacion As String, _
                        ByRef fecha_inivigen As String, _
                        ByRef fecha_finvigen As String, _
                        ByRef cargo As String, _
                        ByRef codi_cargo As String, _
                        ByRef dcto As String, _
                        ByRef codi_dcto As String, _
                        ByRef pack_compart As String, _
                        ByRef unid_monetar As String, _
                        ByRef Codi_Agrupa) As Integer
    Dim res As Integer

    GetOfertasRead = True

    If vrGetOferta Is Nothing Then
        GetOfertasRead = False
        Exit Function
    End If
    If vrGetOferta.EOF Then
        res = GetOfertasClose()
        GetOfertasRead = False
        Exit Function
    End If

    codi_oferta = gfnDevuelveValorCampo("LONG", vrGetOferta.Fields(0).Type, vrGetOferta.Fields(0))
    desc_oferta = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(1).Type, vrGetOferta.Fields(1))
    observacion = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(2).Type, vrGetOferta.Fields(2))
    vlor_oferta = gfnDevuelveValorCampo("DOUBLE", vrGetOferta.Fields(3).Type, vrGetOferta.Fields(3))
    vlor_promocion = gfnDevuelveValorCampo("DOUBLE", vrGetOferta.Fields(4).Type, vrGetOferta.Fields(4))
    fecha_creacion = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(5).Type, vrGetOferta.Fields(5))
    fecha_inivigen = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(6).Type, vrGetOferta.Fields(6))
    fecha_finvigen = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(7).Type, vrGetOferta.Fields(7))
    cargo = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(8).Type, vrGetOferta.Fields(8))
    codi_cargo = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(8).Type, vrGetOferta.Fields(9))
    dcto = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(9).Type, vrGetOferta.Fields(10))
    codi_dcto = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(9).Type, vrGetOferta.Fields(11))
    pack_compart = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(10).Type, vrGetOferta.Fields(12))
    unid_monetar = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(11).Type, vrGetOferta.Fields(13))
    Codi_Agrupa = gfnDevuelveValorCampo("STRING", vrGetOferta.Fields(12).Type, vrGetOferta.Fields(14))

    vrGetOferta.MoveNext

End Function

Public Function GetOferLoca(ByVal CodOferta As Long) As Long
    'Lista las localidades asociadas a la oferta
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    
    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "l.codi_localida, l.desc_localida, "
    sSql1 = sSql1 & "l.codi_localida, l.desc_localida, "
    sSql = sSql & "to_char(ol.fech_inivigen,'dd-mm-yyyy') FechaF, "
    sSql1 = sSql1 & "to_char(ol.fech_inivigen,'dd-mm-yyyy') FechaF, "
    sSql = sSql & "to_char(ol.fech_finvigen,'dd-mm-yyyy') FechaI, "
    sSql1 = sSql1 & "to_char(ol.fech_finvigen,'dd-mm-yyyy') FechaI, "
    sSql = sSql & "decode(ol.codi_localida, null, 0, 1) seleccionada "
    sSql1 = sSql1 & "decode(ol.codi_localida, null, 0, 1) seleccionada "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_of_localida ol, part_localida l "
    sSql1 = sSql1 & "siat_of_localida ol, part_localida l "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & " ol.codi_oferta(+)= ? "
    sSql1 = sSql1 & "ol.codi_oferta(+)= " & Str(CodOferta) & " "
    sSql = sSql & "and ol.codi_localida(+) = l.codi_localida  "
    sSql1 = sSql1 & "and ol.codi_localida(+) = l.codi_localida  "
    sSql = sSql & "order by l.codi_localida "
    sSql1 = sSql1 & "order by l.codi_localida "
    On Error GoTo Error
    Set vrGetOferLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodOferta", adInteger, adParamInput, 6, CodOferta)
    End With
    Screen.MousePointer = vbHourglass
    vrGetOferLoca.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetOferLoca.EOF Then
        vrGetOferLoca.MoveLast
        CantidadTuplas = vrGetOferLoca.RecordCount
        vrGetOferLoca.MoveFirst
        GetOferLoca = CantidadTuplas
    Else
        GetOferLoca = -1
    End If
    Exit Function

Error:
    Set vrGetOferLoca = Nothing
    Screen.MousePointer = vbDefault
    GetOferLoca = fn_error(Err.Description, sSql1)
    GetOferLoca = -1
End Function

Function GetOferLocaClose() As Integer

    If Not vrGetOferLoca Is Nothing Then
        vrGetOferLoca.Close
        Set vrGetOferLoca = Nothing
    End If
End Function

Function GetOferLocaRead(ByRef codi_localida As String, ByRef desc_localidad As String, ByRef fech_inivigen As String, ByRef fech_finvigen As String, ByRef seleccionada As Long) As Integer
    Dim res As Integer

    GetOferLocaRead = True

    If vrGetOferLoca Is Nothing Then
        GetOferLocaRead = False
        Exit Function
    End If
    If vrGetOferLoca.EOF Then
        res = GetOferLocaClose()
        GetOferLocaRead = False
        Exit Function
    End If

    codi_localida = gfnDevuelveValorCampo("STRING", vrGetOferLoca.Fields(0).Type, vrGetOferLoca.Fields(0))
    desc_localidad = gfnDevuelveValorCampo("STRING", vrGetOferLoca.Fields(1).Type, vrGetOferLoca.Fields(1))
    fech_inivigen = gfnDevuelveValorCampo("STRING", vrGetOferLoca.Fields(2).Type, vrGetOferLoca.Fields(2))
    fech_finvigen = gfnDevuelveValorCampo("STRING", vrGetOferLoca.Fields(3).Type, vrGetOferLoca.Fields(3))

    seleccionada = gfnDevuelveValorCampo("LONG", vrGetOferLoca.Fields(4).Type, vrGetOferLoca.Fields(4))
    
    vrGetOferLoca.MoveNext

End Function

Public Function GetDetOfer(ByVal CodOferta As Long) As Long
    'Lista el detalle asociado a la oferta
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "(select p.desc_parame from part_parame p where p.codi_grupo=8007 and p.codi_elem=d.codi_area) area, "
    sSql1 = sSql1 & "(select p.desc_parame from part_parame p where p.codi_grupo=8007 and p.codi_elem=d.codi_area) area, "
    sSql = sSql & "decode(d.tipo_paquete,'PAQUE','PAQUETE','PROMOCION') desc_tipo_paquete, "
    sSql1 = sSql1 & "decode(d.tipo_paquete,'PAQUE','PAQUETE','PROMOCION') desc_tipo_paquete, "
    sSql = sSql & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = d.codi_cargo and c.fech_finvigen is null) cargo, "
    sSql1 = sSql1 & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = d.codi_cargo and c.fech_finvigen is null) cargo, "
    sSql = sSql & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = d.codi_dcto and c.fech_finvigen is null) dcto, "
    sSql1 = sSql1 & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = d.codi_dcto and c.fech_finvigen is null) dcto, "
    sSql = sSql & "d.vlor_total, d.porc_dcto, d.vlor_cargo, d.vlor_dcto, "
    sSql1 = sSql1 & "d.vlor_total, d.porc_dcto, d.vlor_cargo, d.vlor_dcto, "
    sSql = sSql & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = d.codi_dctoprom and c.fech_finvigen is null) dctoprom, "
    sSql1 = sSql1 & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = d.codi_dctoprom and c.fech_finvigen is null) dctoprom, "
    sSql = sSql & "d.vlor_dctoprom, d.cant_mesesprom, "
    sSql1 = sSql1 & "d.vlor_dctoprom, d.cant_mesesprom, "
    sSql = sSql & "(select p.desc_parame from part_parame p where p.codi_grupo=8007 and p.codi_elem=d.codi_exclusion) exclusion, "
    sSql1 = sSql1 & "(select p.desc_parame from part_parame p where p.codi_grupo=8007 and p.codi_elem=d.codi_exclusion) exclusion, "
    sSql = sSql & "(select p.desc_parame from part_parame p where p.codi_grupo=8008 and p.codi_elem=d.clas_paquete) desc_clas_paquete, "
    sSql1 = sSql1 & "(select p.desc_parame from part_parame p where p.codi_grupo=8008 and p.codi_elem=d.clas_paquete) desc_clas_paquete, "
    sSql = sSql & "d.codi_paquete,"
    sSql1 = sSql1 & "d.codi_paquete,"
    sSql = sSql & "d.codi_paq_adic,"
    sSql1 = sSql1 & "d.codi_paq_adic,"
    sSql = sSql & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = d.codi_dcto_pc and c.fech_finvigen is null) codi_dcto_pc, "
    sSql1 = sSql1 & "(select c.codi_concepto from typt_conceptos c where c.codi_interno = d.codi_dcto_pc and c.fech_finvigen is null) codi_dcto_pc, "
    sSql = sSql & " d.vlor_dcto_pc, d.inicia_cobro, d.rowid , d.codi_area, d.tipo_paquete,d.codi_exclusion, d.clas_paquete, d.flag_obligatorio"
    sSql1 = sSql1 & "d.vlor_dcto_pc, d.inicia_cobro, d.rowid, d.codi_area, d.tipo_paquete,d.codi_exclusion, d.clas_paquete, d.flag_obligatorio "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_of_detaoferta d ,"
    sSql = sSql & "  (select 'PAQUE' tipo_paquete,"
    sSql = sSql & "           pq.codi_paquete,"
    sSql = sSql & "           pq.fech_inivigen,"
    sSql = sSql & "               pq.fech_finvigen"
    sSql = sSql & "      from siat_paquete pq"
    sSql = sSql & "    Union"
    sSql = sSql & "    select 'PROMO' tipo_paquete,"
    sSql = sSql & "           pr.codi_promocion codi_paquete,"
    sSql = sSql & "           pr.fech_inicio fech_inivigen,"
    sSql = sSql & "           pr.fech_fin fech_finvigen"
    sSql = sSql & "      from siat_pr_promocion pr) pa"
    sSql1 = sSql1 & "siat_of_detaoferta d "
    sSql = sSql & " where "
    sSql = sSql & "  pa.codi_paquete = d.codi_paquete"
    sSql = sSql & " and pa.tipo_paquete = d.tipo_paquete"
    sSql = sSql & " and ((sysdate between pa.fech_inivigen and nvl(pa.fech_finvigen,sysdate) and d.clas_paquete='M') or d.clas_paquete='A')"
    sSql = sSql & " and codi_oferta= ? "
    
    sSql1 = sSql1 & " where "
    
    sSql1 = sSql1 & "codi_oferta= " & Str(CodOferta) & " "
    sSql = sSql & "order by 13,1, d.rowid"
    sSql1 = sSql1 & "order by 13,1, d.rowid "
    On Error GoTo Error
    Set vrGetDetOfer = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodOferta", adInteger, adParamInput, 6, CodOferta)
    End With
    Screen.MousePointer = vbHourglass
    vrGetDetOfer.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetDetOfer.EOF Then
        vrGetDetOfer.MoveLast
        CantidadTuplas = vrGetDetOfer.RecordCount
        vrGetDetOfer.MoveFirst
        GetDetOfer = CantidadTuplas
    Else
        GetDetOfer = 0
    End If
    Exit Function

Error:
    Set vrGetDetOfer = Nothing
    Screen.MousePointer = vbDefault
    GetDetOfer = fn_error(Err.Description, sSql1)
    GetDetOfer = -1
End Function

Function GetDetOferClose() As Integer

    If Not vrGetDetOfer Is Nothing Then
        vrGetDetOfer.Close
        Set vrGetDetOfer = Nothing
    End If
End Function

Function GetDetOferRead( _
ByRef area As String, _
ByRef desc_tipo_paquete As String, _
ByRef codi_cargo As String, _
ByRef codi_dcto As String, _
ByRef vlor_Total As Double, _
ByRef porc_dcto As Double, _
ByRef vlor_cargo As Double, _
ByRef vlor_dcto As Double, _
ByRef codi_dctoprom As String, _
ByRef vlor_dctoprom As Double, _
ByRef cant_mesesprom As Long, _
ByRef exclusion As String, _
ByRef desc_clas_paquete As String, _
ByRef codi_paquete As Long, _
ByRef paq_adic As Long, _
ByRef codi_dctoPC As String, _
ByRef vlor_dctoPC As Double, _
ByRef inicia_cobro As String, _
ByRef rowid As String, _
ByRef codi_area As String, _
ByRef tipo_paquete As String, _
ByRef codi_exclusion As String, _
ByRef clas_paquete As String, _
ByRef flag_obligatorio As String) As Integer				   
    Dim res As Integer

    GetDetOferRead = True

    If vrGetDetOfer Is Nothing Then
        GetDetOferRead = False
        Exit Function
    End If
    If vrGetDetOfer.EOF Then
        res = GetDetOferClose()
        GetDetOferRead = False
        Exit Function
    End If

    area = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(0).Type, vrGetDetOfer.Fields(0))
    desc_tipo_paquete = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(1).Type, vrGetDetOfer.Fields(1))
    codi_cargo = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(2).Type, vrGetDetOfer.Fields(2))
    codi_dcto = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(3).Type, vrGetDetOfer.Fields(3))
    vlor_Total = gfnDevuelveValorCampo("DOUBLE", vrGetDetOfer.Fields(4).Type, vrGetDetOfer.Fields(4))
    porc_dcto = gfnDevuelveValorCampo("DOUBLE", vrGetDetOfer.Fields(5).Type, vrGetDetOfer.Fields(5))
    vlor_cargo = gfnDevuelveValorCampo("DOUBLE", vrGetDetOfer.Fields(6).Type, vrGetDetOfer.Fields(6))
    vlor_dcto = gfnDevuelveValorCampo("DOUBLE", vrGetDetOfer.Fields(7).Type, vrGetDetOfer.Fields(7))
    codi_dctoprom = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(8).Type, vrGetDetOfer.Fields(8))
    vlor_dctoprom = gfnDevuelveValorCampo("DOUBLE", vrGetDetOfer.Fields(9).Type, vrGetDetOfer.Fields(9))
    cant_mesesprom = gfnDevuelveValorCampo("LONG", vrGetDetOfer.Fields(10).Type, vrGetDetOfer.Fields(10))
    exclusion = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(11).Type, vrGetDetOfer.Fields(11))
    desc_clas_paquete = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(12).Type, vrGetDetOfer.Fields(12))
    codi_paquete = gfnDevuelveValorCampo("LONG", vrGetDetOfer.Fields(13).Type, vrGetDetOfer.Fields(13))
    paq_adic = gfnDevuelveValorCampo("LONG", vrGetDetOfer.Fields(14).Type, vrGetDetOfer.Fields(14))
    codi_dctoPC = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(15).Type, vrGetDetOfer.Fields(15))
    vlor_dctoPC = gfnDevuelveValorCampo("DOUBLE", vrGetDetOfer.Fields(16).Type, vrGetDetOfer.Fields(16))
    inicia_cobro = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(17).Type, vrGetDetOfer.Fields(17))
    rowid = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(18).Type, vrGetDetOfer.Fields(18))
    
    codi_area = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(18).Type, vrGetDetOfer.Fields(19))
    tipo_paquete = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(18).Type, vrGetDetOfer.Fields(20))
    codi_exclusion = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(18).Type, vrGetDetOfer.Fields(21))
    clas_paquete = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(18).Type, vrGetDetOfer.Fields(22))
	flag_obligatorio = gfnDevuelveValorCampo("STRING", vrGetDetOfer.Fields(19).Type, vrGetDetOfer.Fields(23))																				  
    
    vrGetDetOfer.MoveNext

End Function

Public Function SelConcepto(ByVal vCodInterno As String) As Long
    'Seleccionar el concepto correspondiente
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_concepto "
    sSql1 = sSql1 & "codi_concepto "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "typt_conceptos "
    sSql1 = sSql1 & "typt_conceptos "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_interno =? "
    sSql1 = sSql1 & "codi_interno ='" & vCodInterno & "' "
    On Error GoTo Error
    Set vrSelConcepto = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-vCodInterno", adVarChar, adParamInput, 4, vCodInterno)
    End With
    Screen.MousePointer = vbHourglass
    vrSelConcepto.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrSelConcepto.EOF Then
        vrSelConcepto.MoveLast
        CantidadTuplas = vrSelConcepto.RecordCount
        vrSelConcepto.MoveFirst
        SelConcepto = CantidadTuplas
    Else
        SelConcepto = -1
    End If
    Exit Function

Error:
    Set vrSelConcepto = Nothing
    Screen.MousePointer = vbDefault
    SelConcepto = fn_error(Err.Description, sSql1)
    SelConcepto = -1
End Function

Function SelConceptoClose() As Integer

    If Not vrSelConcepto Is Nothing Then
        vrSelConcepto.Close
        Set vrSelConcepto = Nothing
    End If
End Function

Function SelConceptoRead(ByRef Codi_Concepto As Long) As Integer
    Dim res As Integer

    SelConceptoRead = True

    If vrSelConcepto Is Nothing Then
        SelConceptoRead = False
        Exit Function
    End If
    If vrSelConcepto.EOF Then
        res = SelConceptoClose()
        SelConceptoRead = False
        Exit Function
    End If

    Codi_Concepto = gfnDevuelveValorCampo("LONG", vrSelConcepto.Fields(0).Type, vrSelConcepto.Fields(0))

    vrSelConcepto.MoveNext

End Function

Public Function SelVConcepto(ByVal vCodInterno As String) As Long
    '
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "vlor_concepto "
    sSql1 = sSql1 & "vlor_concepto "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "typt_conceptos "
    sSql1 = sSql1 & "typt_conceptos "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "codi_concepto =? "
    sSql1 = sSql1 & "codi_concepto ='" & vCodInterno & "' "
    sSql = sSql & "and fech_finvigen is null "
    sSql1 = sSql1 & "and fech_finvigen is null "
    On Error GoTo Error
    Set vrSelVConcepto = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-vCodInterno", adVarChar, adParamInput, 10, vCodInterno)
    End With
    Screen.MousePointer = vbHourglass
    vrSelVConcepto.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrSelVConcepto.EOF Then
        vrSelVConcepto.MoveLast
        CantidadTuplas = vrSelVConcepto.RecordCount
        vrSelVConcepto.MoveFirst
        SelVConcepto = CantidadTuplas
    Else
        SelVConcepto = -1
    End If
    Exit Function

Error:
    Set vrSelVConcepto = Nothing
    Screen.MousePointer = vbDefault
    SelVConcepto = fn_error(Err.Description, sSql1)
    SelVConcepto = -1
End Function

Function SelVConceptoClose() As Integer

    If Not vrSelVConcepto Is Nothing Then
        vrSelVConcepto.Close
        Set vrSelVConcepto = Nothing
    End If
End Function

Function SelVConceptoRead(ByRef vlor_concepto As Double) As Integer
    Dim res As Integer

    SelVConceptoRead = True

    If vrSelVConcepto Is Nothing Then
        SelVConceptoRead = False
        Exit Function
    End If
    If vrSelVConcepto.EOF Then
        res = SelVConceptoClose()
        SelVConceptoRead = False
        Exit Function
    End If

    vlor_concepto = gfnDevuelveValorCampo("DOUBLE", vrSelVConcepto.Fields(0).Type, vrSelVConcepto.Fields(0))

    vrSelVConcepto.MoveNext

End Function
