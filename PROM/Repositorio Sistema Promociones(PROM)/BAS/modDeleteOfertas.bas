Attribute VB_Name = "modDeleteOfertas"
Option Explicit

Function DelOferLoca(ByVal codi_promocion As Long, ByVal codi_localidad As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelOferLoca As ADODB.Recordset
    
    Screen.MousePointer = vbHourglass

    sLlamadaProc = "begin siaa_ofertas.del_localidad_oferta(?,?); end;"

    Set vrDelOferLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adNumeric, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, Trim(codi_localidad))
    End With
    On Error GoTo Error
    vrDelOferLoca.Open adoCmd

    Set vrDelOferLoca = Nothing

    DelOferLoca = True
    
    Screen.MousePointer = vbDefault
    Exit Function

Error:
    Set vrDelOferLoca = Nothing
    sLlamadaProc = "begin siaa_ofertas.Del_Localidad_Oferta(" + CStr(codi_promocion) + "," + codi_localidad + "); end;"
    DelOferLoca = fn_error(Err.Description, sLlamadaProc)
    DelOferLoca = False
End Function

Function DelOferta(ByVal codi_oferta As Long) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelOferta As ADODB.Recordset

    Screen.MousePointer = vbHourglass
    
    sLlamadaProc = "begin siaa_ofertas.Del_Oferta(?); end;"

    Set vrDelOferta = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_oferta)
    End With
    On Error GoTo Error
    vrDelOferta.Open adoCmd

    Set vrDelOferta = Nothing

    DelOferta = True
    
    Screen.MousePointer = vbDefault
    Exit Function

Error:
    Set vrDelOferta = Nothing
    sLlamadaProc = "begin siaa_ofertas.Del_Oferta(" + CStr(codi_oferta) + "); end;"
    DelOferta = fn_error(Err.Description, sLlamadaProc)
    DelOferta = False
End Function

' Restringe m�s campos al eliminar. SSIN#15788. JPABLOS. 24-abr-2015.
'Function DelOferDet(ByVal codi_oferta As Long, ByVal codi_paquete As Long) As Integer
Function DelOferDet(ByVal codi_oferta As Long, ByVal codi_area As String, ByVal tipo_paquete As String, ByVal clas_paquete As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelOferDet As ADODB.Recordset
    Dim lInicio As Long
    Dim lFin As Long

    ' Restringe m�s campos al eliminar. SSIN#15788. JPABLOS. 24-abr-2015.
    'sLlamadaProc = "begin siaa_ofertas.Del_Detalle_Oferta(?,?); end;"
    sLlamadaProc = "begin siaa_ofertas.Del_Detalle_Oferta(?,null,?,?,?); end;"

    Set vrDelOferDet = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_oferta)
        '.Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_paquete)
        ' Restringe m�s campos al eliminar. SSIN#15788. JPABLOS. 24-abr-2015.
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_area)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, tipo_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, clas_paquete)
    End With
    On Error GoTo Error
    vrDelOferDet.Open adoCmd

    Set vrDelOferDet = Nothing

    DelOferDet = True
    Exit Function

Error:
    Set vrDelOferDet = Nothing
    ' Si es error Oracle definido por usuario, mostrar mensaje. SSOT#541852. JPABLOS. 24-ago-2015.
    If Mid(Err.Description, 1, 6) = "ORA-20" And Mid(Err.Description, 7, 3) >= "000" And Mid(Err.Description, 7, 3) <= "999" Then
        lInicio = InStr(1, Err.Description, ":") + 2
        lFin = InStr(1, Err.Description & vbLf, vbLf) - 1
        MsgBox Mid(Err.Description, lInicio, lFin - lInicio + 1) + "(Oferta:" + CStr(codi_oferta) + ")", vbCritical, "ERROR"
    Else ' comportamiento "default"
        sLlamadaProc = "begin siaa_ofertas.Del_Detalle_Oferta(" + CStr(codi_oferta) + "); end;"
        sLlamadaProc = "begin siaa_ofertas.Del_Detalle_Oferta(0 , 0 ); end;"
        DelOferDet = fn_error(Err.Description, sLlamadaProc)
    End If
    DelOferDet = False
End Function

Function DelOferDetRowid(ByVal rowid As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelOferDet As ADODB.Recordset
    Dim lInicio As Long
    Dim lFin As Long

    ' Restringe m�s campos al eliminar. SSIN#15788. JPABLOS. 24-abr-2015.
    'sLlamadaProc = "begin siaa_ofertas.Del_Detalle_Oferta(?,?); end;"
    sLlamadaProc = "begin siaa_ofertas.Del_Detalle_Oferta(" + "'" + rowid + "'" + "); end;"

    Set vrDelOferDet = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
       ' .Parameters.Append adoCmd.CreateParameter(, ad, adParamInput, 6, rowid)
    End With
    On Error GoTo Error
    vrDelOferDet.Open adoCmd

    Set vrDelOferDet = Nothing

    DelOferDetRowid = True
    Exit Function

Error:
    Set vrDelOferDet = Nothing
    ' Si es error Oracle definido por usuario, mostrar mensaje. SSOT#541852. JPABLOS. 24-ago-2015.
    If Mid(Err.Description, 1, 6) = "ORA-20" And Mid(Err.Description, 7, 3) >= "000" And Mid(Err.Description, 7, 3) <= "999" Then
        lInicio = InStr(1, Err.Description, ":") + 2
        lFin = InStr(1, Err.Description & vbLf, vbLf) - 1
        MsgBox Mid(Err.Description, lInicio, lFin - lInicio + 1), vbCritical, "ERROR"
    Else ' comportamiento "default"
        sLlamadaProc = "begin siaa_ofertas.Del_Detalle_Oferta(" + CStr(rowid) + "); end;"
        DelOferDetRowid = fn_error(Err.Description, sLlamadaProc)
    End If
    DelOferDetRowid = False
End Function
