Attribute VB_Name = "modDeletePlanes"
Option Explicit

Function DelPlanFaci(ByVal codi_plan As String, ByVal codi_facilidad As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelPlanFaci As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanFaciElimina(?,?); end;"

    Set vrDelPlanFaci = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, codi_facilidad)
    End With
    On Error GoTo Error
    vrDelPlanFaci.Open adoCmd

    Set vrDelPlanFaci = Nothing

    DelPlanFaci = True
    Exit Function

Error:
    Set vrDelPlanFaci = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PlanFaciEliminar(" + codi_plan + "," + codi_facilidad + "); end;"
    DelPlanFaci = fn_error(Err.Description, sLlamadaProc)
    DelPlanFaci = False
End Function



