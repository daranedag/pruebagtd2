Attribute VB_Name = "GINTCONT"
Option Explicit

Global Gp_Ip_Host  As String * 15
Global Gp_My_Ip As String * 15


Global Const C_T_INIAPLI = 200
Global Const C_T_FINAPLI = 201

Global Gp_My_Aplicacion As String * 4
Global Gp_Rol_Aplicacion As String * 4
Global Gp_Zonal_Aplicacion As String * 4
Global Gp_Username_Aplicacion As String * 8

Function Fn_Prepara_Mensaje_Indirecto(ByVal iden_tarea As Long, ByVal tipo_tarea As Integer, ByVal orig_tarea As Integer, ByVal Tipo_Documento As String, ByVal Nmro_documento As String, ByVal dest_username As String, ByVal Dest_Zonal As String, ByVal dest_aplicacion As String, ByVal mensaje As String, ip_cliente As String, Dest_Rol As String, tipo_aviso As Integer, dest_tarea As Integer) As String

  Dim msg As String

  msg = Left(Trim(Str(iden_tarea)) + Space(6), 6)
  msg = msg + Left(Trim(Str(tipo_tarea)) + Space(3), 3)
  msg = msg + Left(Trim(Str(orig_tarea)) + Space(4), 4)
  msg = msg + Left(Trim(Tipo_Documento) + Space(6), 6)
  msg = msg + Left(Trim(Nmro_documento) + Space(12), 12)
  msg = msg + Left(Trim(dest_username) + Space(8), 8)
  msg = msg + Left(Trim(Dest_Zonal) + Space(4), 4)
  msg = msg + Left(Trim(dest_aplicacion) + Space(4), 4)
  msg = msg + Left(mensaje + Space(120), 120)

  msg = msg + Left(ip_cliente + Space(15), 15)
  msg = msg + Left(Dest_Rol + Space(4), 4)
  msg = msg + Left(Trim(Str(tipo_aviso)) + Space(3), 3)
  msg = msg + Left(Trim(Str(dest_tarea)) + Space(4), 4)

  Fn_Prepara_Mensaje_Indirecto = msg

End Function

Sub Su_Env_Control(ByVal msg As String)
  On Error Resume Next
  fintcont.lb_envia.Caption = ""
  fintcont.lb_envia.LinkPoke
  fintcont.lb_envia.Caption = msg
  fintcont.lb_envia.LinkPoke
End Sub

Sub Su_Fin_Aplicacion(ByVal apli As String)
  Dim msg As String

  msg = Fn_Prepara_Mensaje_Indirecto(0, C_T_FINAPLI, 0, "", "", "", "", "", Left(Trim(UCase(apli)) + Space(4), 4), "", "", 0, 0)

  Call Su_Env_Control(msg)

  Unload fintcont
End Sub

Sub Su_Init_Aplicacion(ByVal apli As String)
  Gp_My_Aplicacion = Trim(UCase(apli))
  fintcont.Show vbModal
End Sub

Sub Su_Rcv_Control(ByVal glosa As String)

  Dim codi_error As Integer, Ip As String * 15, iden_tarea As Long
  Dim dest_username As String * 4, Dest_Rol As String * 4, Dest_Zonal As String * 4
  Dim dest_apli As String * 4, orig_username As String * 8, orig_rol As String * 4
  Dim orig_zonal As String * 4, orig_tarea As Integer, orig_apli As String * 4
  Dim tipo_tarea As Integer, tipo_aviso As Integer, fecha_hora As String * 12
  Dim tipo_documen As String * 6, codi_documen As String * 12, mensaje As String * 120
  Dim dest_aplicacion As String * 4, dest_tarea As Integer

  codi_error = Val(Mid(glosa, 1, 5))
  iden_tarea = Val(Mid(glosa, 6, 6))
  orig_username = Mid(glosa, 12, 8)
  orig_rol = Mid(glosa, 20, 4)
  orig_zonal = Mid(glosa, 24, 4)
  orig_apli = Mid(glosa, 28, 4)
  tipo_tarea = Val(Mid(glosa, 32, 3))
  fecha_hora = Mid(glosa, 35, 12)
  tipo_documen = Mid(glosa, 47, 6)
  codi_documen = Mid(glosa, 53, 12)
  mensaje = Mid(glosa, 65, 120)
  dest_aplicacion = Mid(glosa, 185, 4)
  'dest_tarea = Mid(glosa, 189, 4)

  If dest_aplicacion = Gp_My_Aplicacion Then 'ES PARA MI
    Select Case tipo_tarea
        Case C_T_FINAPLI:
            DoEvents
            If Trim(mensaje) <> "X" Then
                MsgBox mensaje
            End If
            End
        Case C_T_INIAPLI:
            Gp_Rol_Aplicacion = UCase(Left(mensaje, 4))
            Gp_Username_Aplicacion = UCase(Mid(mensaje, 5, 8))
            Gp_Zonal_Aplicacion = UCase(Mid(mensaje, 13, 4))
            Gp_My_Ip = UCase(Mid(mensaje, 17, 15))
            Gp_Ip_Host = UCase(Mid(mensaje, 32, 15))
            'Unload fespera
        Case Else
            'Call Su_AdmMsg(codi_error, iden_tarea, orig_username, orig_rol, orig_zonal, orig_apli, tipo_tarea, fecha_hora, tipo_documen, codi_documen, mensaje, dest_apli, dest_tarea)
    End Select
  End If
End Sub


