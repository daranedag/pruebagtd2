Attribute VB_Name = "modInsertOfertas"
Option Explicit

Function InsOferLoca(ByVal codi_promocion As Long, ByVal codi_localidad As String, ByVal fech_inivigen As String, ByVal fech_finvigen As String, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsOferLoca As ADODB.Recordset
    
    Screen.MousePointer = vbHourglass

    sLlamadaProc = "begin siaa_ofertas.Ins_Localidad_Oferta(?,?,?,?,?); end;"

    Set vrInsOferLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_localidad)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fech_inivigen)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fech_finvigen)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
        
    End With
    On Error GoTo Error
    vrInsOferLoca.Open adoCmd
    
    If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
        psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
    Else
        psRespuesta = ""
    End If
        
    Set vrInsOferLoca = Nothing

    InsOferLoca = True
    
     Screen.MousePointer = vbDefault
    Exit Function

Error:
    Set vrInsOferLoca = Nothing
    'sLlamadaProc = "begin siaa_ofertas.Ins_Localidad_Oferta(" + codi_promocion + "," + codi_localidad + "," + fech_inivigen + "," + fech_finvigen + "); end;"
    InsOferLoca = fn_error(Err.Description, sLlamadaProc)
    InsOferLoca = False
End Function

Function InsOferta(ByVal desc_oferta As String, ByVal codi_cargo As String, ByVal codi_dcto As String, ByVal fech_inivigen As String, ByVal fech_finvigen As String, ByVal Iden_Username As String, ByVal observacion As String, ByVal vlor_oferta As Long, ByVal vlor_promocion As Long, ByVal pack_comp As String, ByVal agrupador As String, ByVal piCodiOferta As Long, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsOferta As ADODB.Recordset
    
     Screen.MousePointer = vbHourglass

    sLlamadaProc = "begin siaa_ofertas.Ins_Oferta(?,?,?,?,?,?,?,?,?,?,?,?,?); end;"

    Set vrInsOferta = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 100, desc_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, codi_cargo)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, codi_dcto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fech_inivigen)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, fech_finvigen)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Iden_Username)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 2000, observacion)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 12, vlor_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 12, vlor_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, pack_comp)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, agrupador)
        .Parameters.Append adoCmd.CreateParameter("piCodiOferta", adInteger, adParamOutput, 6)
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrInsOferta.Open adoCmd
        If Not IsNull(adoCmd.Parameters("piCodiOferta")) Then
            piCodiOferta = CVar(adoCmd.Parameters("piCodiOferta"))
        Else
            piCodiOferta = 0
        End If
        If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
            psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
        Else
            psRespuesta = ""
        End If

    Set vrInsOferta = Nothing

    InsOferta = True
    
    Screen.MousePointer = vbDefault
    Exit Function

Error:
    Set vrInsOferta = Nothing
    sLlamadaProc = "begin siaa_ofertas.Ins_Oferta(" + desc_oferta + "," + codi_cargo + "," + codi_dcto + "," + fech_inivigen + "," + fech_finvigen + "," + Iden_Username + "," + observacion + "," + vlor_oferta + "," + vlor_promocion + "); end;"
    InsOferta = fn_error(Err.Description, sLlamadaProc)
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    InsOferta = False
End Function

Function CopiaOferta(ByVal CodiOferta As Long, ByVal Iden_Username As String, ByRef piCodiOferta As Long) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrCopiaOferta As ADODB.Recordset

    sLlamadaProc = "begin siaa_ofertas.cop_oferta(?,?,?); end;"

    Set vrCopiaOferta = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, CodiOferta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 8, Iden_Username)
        .Parameters.Append adoCmd.CreateParameter("piCodiOferta", adInteger, adParamOutput, 6)
    End With
    On Error GoTo Error
    vrCopiaOferta.Open adoCmd
    
    If Not IsNull(adoCmd.Parameters("piCodiOferta")) Then
        piCodiOferta = CVar(adoCmd.Parameters("piCodiOferta"))
    Else
        piCodiOferta = 0
    End If
        
    Set vrCopiaOferta = Nothing

    CopiaOferta = True
    Exit Function

Error:
    Set vrCopiaOferta = Nothing
    sLlamadaProc = "begin siaa_ofertas.cop_oferta(" + Str(CodiOferta) + "," + Iden_Username + "); end;"
    CopiaOferta = fn_error(Err.Description, sLlamadaProc)
    CopiaOferta = False
End Function

Function InsOferDet(ByVal codi_oferta As Long, ByVal codi_area As String, ByVal tipo_paquete As String, ByVal codi_cargo As String, _
ByVal codi_dcto As String, ByVal vlor_Total As Double, ByVal porc_dcto As Double, ByVal vlor_cargo As Double, ByVal vlor_dcto As Double, _
ByVal codi_dctoprom As String, ByVal vlor_dctoprom As Double, ByVal cant_mesesprom As Long, ByVal codi_exclusion As String, _
ByVal clas_paquete As String, ByVal codi_paquete As Long, ByVal paq_adicional As Long, ByVal codi_dctoPC As String, _
ByVal vlor_dctoPC As Double, ByVal inicia_cobro As String, ByVal alta_obligatoria As String, ByRef psRespuesta As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrInsOferDet As ADODB.Recordset

    sLlamadaProc = "begin siaa_ofertas.Ins_Detalle_Oferta(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;"

    Set vrInsOferDet = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_oferta)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, Trim(codi_area))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 6, Trim(tipo_paquete))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_cargo))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_dcto))
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, vlor_Total)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 5, porc_dcto)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, vlor_cargo)
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, vlor_dcto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_dctoprom))
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, vlor_dctoprom)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 2, cant_mesesprom)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, Trim(codi_exclusion))
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, Trim(clas_paquete))
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 10, codi_paquete)
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 10, paq_adicional)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 12, Trim(codi_dctoPC))
        .Parameters.Append adoCmd.CreateParameter(, adDouble, adParamInput, 12, vlor_dctoPC)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, Trim(inicia_cobro))
		.Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 1, Trim(alta_obligatoria)) 
        .Parameters.Append adoCmd.CreateParameter("psRespuesta", adVarChar, adParamOutput, 100)
    End With
    On Error GoTo Error
    vrInsOferDet.Open adoCmd
        If Not IsNull(adoCmd.Parameters("psRespuesta")) Then
            psRespuesta = CVar(adoCmd.Parameters("psRespuesta"))
        Else
            psRespuesta = ""
        End If

    Set vrInsOferDet = Nothing

    InsOferDet = True
    Exit Function

Error:
    Set vrInsOferDet = Nothing
    sLlamadaProc = "begin siaa_ofertas.Ins_Detalle_Oferta(" & codi_oferta & "," & codi_area & "," & tipo_paquete & "," & codi_cargo & "," & codi_dcto & "," & vlor_Total & "," & porc_dcto & "," & vlor_cargo & "," & vlor_dcto & "," & codi_dctoprom & "," & vlor_dctoprom & "," & cant_mesesprom & "," & codi_exclusion & "," & clas_paquete & "," & codi_paquete & "," & paq_adicional & "); end;"
    psRespuesta = Mid(Err.Description, InStr(1, Err.Description, "ORA-", vbTextCompare) + 10)
    psRespuesta = Mid(psRespuesta, 1, InStr(1, psRespuesta, "ORA-", vbTextCompare) - 1)
    InsOferDet = False
End Function
