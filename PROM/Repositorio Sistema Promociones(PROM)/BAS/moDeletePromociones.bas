Attribute VB_Name = "moDeletePromociones"
Option Explicit

Function DelPromLoca(ByVal codi_promocion As Long, _
                     ByVal codi_localidad As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelPromLoca As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoLocalidadElimina(?,?); end;"

    Set vrDelPromLoca = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_localidad)
    End With
    On Error GoTo Error
    vrDelPromLoca.Open adoCmd

    Set vrDelPromLoca = Nothing

    DelPromLoca = True
    Exit Function

Error:
    Set vrDelPromLoca = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoLocalidadElimina(" + Trim(Str(codi_promocion)) + "," + codi_localidad + "); end;"
    DelPromLoca = fn_error(Err.Description, sLlamadaProc)
    DelPromLoca = False
End Function

Function DelPromCobro(ByVal codi_promocion As Long, _
                      ByVal codi_cobro As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelPromCobro As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoCobroElimina(?,?); end;"

    Set vrDelPromCobro = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_cobro)
    End With
    On Error GoTo Error
    vrDelPromCobro.Open adoCmd

    Set vrDelPromCobro = Nothing

    DelPromCobro = True
    Exit Function

Error:
    Set vrDelPromCobro = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoCobroElimina(" + Trim(Str(codi_promocion)) + "," + codi_cobro + "); end;"
    DelPromCobro = fn_error(Err.Description, sLlamadaProc)
    DelPromCobro = False
End Function

Function DelPromPlan(ByVal codi_promocion As Long, _
                     ByVal codi_plan As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelPromPlan As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoPlanElimina(?,?); end;"

    Set vrDelPromPlan = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_plan)
    End With
    On Error GoTo Error
    vrDelPromPlan.Open adoCmd

    Set vrDelPromPlan = Nothing

    DelPromPlan = True
    Exit Function

Error:
    Set vrDelPromPlan = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoPlanElimina(" + Trim(Str(codi_promocion)) + "," + codi_plan + "); end;"
    DelPromPlan = fn_error(Err.Description, sLlamadaProc)
    DelPromPlan = False
End Function

Function DelPromEqui(ByVal codi_promocion As Long, _
                     ByVal codi_producto As String, _
                     ByVal codi_equipo As String) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelPromEqui As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoEquipoElimina(?,?,?); end;"

    Set vrDelPromEqui = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, codi_promocion)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 10, codi_producto)
        .Parameters.Append adoCmd.CreateParameter(, adVarChar, adParamInput, 4, codi_equipo)
    End With
    On Error GoTo Error
    vrDelPromEqui.Open adoCmd

    Set vrDelPromEqui = Nothing

    DelPromEqui = True
    Exit Function

Error:
    Set vrDelPromEqui = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromoEquipoElimina(" + Trim(Str(codi_promocion)) + "," + codi_producto + "," + codi_equipo + "); end;"
    DelPromEqui = fn_error(Err.Description, sLlamadaProc)
    DelPromEqui = False
End Function

Function DelPromo(ByVal CodiPromocion As Long) As Integer

    Dim sParametrosProc As String
    Dim sLlamadaProc As String
    Dim adoCmd As New ADODB.Command
    Dim vrDelPromo As ADODB.Recordset

    sLlamadaProc = "begin SIAA_PROMOCIONES.PromocionElimina(?); end;"

    Set vrDelPromo = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sLlamadaProc
        .Parameters.Append adoCmd.CreateParameter(, adInteger, adParamInput, 6, CodiPromocion)
    End With
    On Error GoTo Error
    vrDelPromo.Open adoCmd

    Set vrDelPromo = Nothing

    DelPromo = True
    Exit Function

Error:
    Set vrDelPromo = Nothing
    sLlamadaProc = "begin SIAA_PROMOCIONES.PromocionElimina(" + Str(CodiPromocion) + "); end;"
    DelPromo = fn_error(Err.Description, sLlamadaProc)
    DelPromo = False
End Function
