Attribute VB_Name = "modConsultasPlanes"
Option Explicit

Global vrGetPlanes As ADODB.Recordset
Global vrGetPlanFaci As ADODB.Recordset
Global vrGetPlanProd As ADODB.Recordset
Global vrGetPlanAgru As ADODB.Recordset

Public Function GetPlanes(ByVal Filtro As Integer) As Long
    'Lista los lpanes existentes en el sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "pp.codi_plan, pp.desc_plan,  "
    sSql1 = sSql1 & "pp.codi_plan, pp.desc_plan,  "
    sSql = sSql & "to_char(pp.fech_creacion,'dd-mm-yyyy') fecha_creacion,  "
    sSql1 = sSql1 & "to_char(pp.fech_creacion,'dd-mm-yyyy') fecha_creacion,  "
    sSql = sSql & "to_char(pp.fech_fin,'dd-mm-yyyy') fecha_fin, "
    sSql1 = sSql1 & "to_char(pp.fech_fin,'dd-mm-yyyy') fecha_fin, "
    sSql = sSql & "pp.codi_facilida, ts.desc_tiposerv,  "
    sSql1 = sSql1 & "pp.codi_facilida, ts.desc_tiposerv,  "
    sSql = sSql & "decode(pp.flag_control,'S','SI','NO'),  "
    sSql1 = sSql1 & "decode(pp.flag_control,'S','SI','NO'),  "
    sSql = sSql & "pp.codi_renta, cv.desc_concepto,  "
    sSql1 = sSql1 & "pp.codi_renta, cv.desc_concepto,  "
    sSql = sSql & "pp.nomb_central,  "
    sSql1 = sSql1 & "pp.nomb_central,  "
    sSql = sSql & "decode(pp.prop_cargaini,'S','SI','NO'),  "
    sSql1 = sSql1 & "decode(pp.prop_cargaini,'S','SI','NO'),  "
    sSql = sSql & "pp.vlor_plan,  "
    sSql1 = sSql1 & "pp.vlor_plan,  "
    sSql = sSql & "decode(pp.tipo_saldo,'MINU','MINUTOS','PESOS'), "
    sSql1 = sSql1 & "decode(pp.tipo_saldo,'MINU','MINUTOS','PESOS'), "
    sSql = sSql & "pp.vlor_cargaini,  "
    sSql1 = sSql1 & "pp.vlor_cargaini,  "
    sSql = sSql & "cv.codi_concepto, "
    sSql1 = sSql1 & "cv.codi_concepto, "
    sSql = sSql & "siaa_promociones.PlanOK(pp.codi_plan) "
    sSql1 = sSql1 & "siaa_promociones.PlanOK(pp.codi_plan) "
    ' incorpora campo uso. JPABLOS. Proyecto OMV. 17-jun-2011.
    sSql = sSql & ", decode(pp.uso, 'PRE', 'PREPAGO', 'POS', 'POSPAGO') "
    sSql1 = sSql1 & ", decode(pp.uso, 'PRE', 'PREPAGO', 'POS', 'POSPAGO') "
    ' incorpora campo RUT Empresa. IALCAINO. Proyecto PORTAL. 29-ABR-2013.
    sSql = sSql & ", decode(pp.flag_acumula,'S','SI','NO') "
    sSql1 = sSql1 & ", decode(pp.flag_acumula,'S','SI','NO') "
    sSql = sSql & ", decode(pp.flag_portal, 'S', 'SI', 'NO') "
    sSql1 = sSql1 & ", decode(pp.flag_portal, 'S', 'SI', 'NO') "
    sSql = sSql & ", pp.rut_empresa "
    sSql1 = sSql1 & ", pp.rut_empresa "
    sSql = sSql & ", pp.codi_rentasms,CVS.desc_concepto, cvs.codi_concepto "
    sSql1 = sSql1 & ", pp.codi_rentasms,CVS.desc_concepto, cvs.codi_concepto "
    sSql = sSql & ", pp.codi_rentadatos,CVD.desc_concepto, cvd.codi_concepto "
    sSql1 = sSql1 & ", pp.codi_rentadatos,CVD.desc_concepto,cvd.codi_concepto  "
    sSql = sSql & ", pp.rut_tercero "
    sSql1 = sSql1 & ", pp.rut_tercero "
    ' MJPEREZ May 2014 - Agrega agrupador de planes
    sSql = sSql & ", pp.codi_agrupa "
    sSql1 = sSql1 & ", pp.codi_agrupa "
    ' IALCAINO Sep 2014 - Agrega Planes Mi negocio
    sSql = sSql & ", pp.paq_portal "
    sSql1 = sSql1 & ", pp.paq_portal "
    sSql = sSql & ", pp.cant_lineas "
    sSql1 = sSql1 & ", pp.cant_lineas "
    sSql = sSql & ", decode(pp.flag_4g,'S','SI','NO') "
    sSql1 = sSql1 & ", decode(pp.flag_4g,'S','SI','NO') "
    sSql = sSql & ", decode(pp.flag_cargoboleta,'S','SI','NO') "
    sSql1 = sSql1 & ", decode(pp.flag_cargoboleta,'S','SI','NO') "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_plan pp, typv_cptosvig cv, siat_tiposerv ts,TYPV_CPTOSVIG CVS,TYPV_CPTOSVIG CVD "
    sSql1 = sSql1 & "siat_pr_plan pp, typv_cptosvig cv, siat_tiposerv ts,TYPV_CPTOSVIG CVS,TYPV_CPTOSVIG CVD "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "pp.codi_renta = cv.codi_interno (+)  AND PP.CODI_RENTASMS = CVS.CODI_INTERNO(+)  AND PP.CODI_RENTADATOS = CVD.CODI_INTERNO(+) "
    sSql1 = sSql1 & "pp.codi_renta = cv.codi_interno (+)  AND PP.CODI_RENTASMS = CVS.CODI_INTERNO(+)  AND PP.CODI_RENTADATOS = CVD.CODI_INTERNO(+)  "
    sSql = sSql & "and pp.codi_facilida = ts.codi_tiposerv  "
    sSql1 = sSql1 & "and pp.codi_facilida = ts.codi_tiposerv "
    sSql = sSql & "and decode(?,0,0,decode(sign(nvl(pp.fech_fin,sysdate)- sysdate),-1,-1,1)) = ? "
    sSql1 = sSql1 & "and decode(" & Str(Filtro) & ",0,0,decode(sign(nvl(pp.fech_fin,sysdate)- sysdate),-1,-1,1)) = " & Str(Filtro) & " "
    sSql = sSql & "order by pp.codi_agrupa, pp.desc_plan "
    sSql1 = sSql1 & "order by pp.codi_agrupa, pp.desc_plan "
    On Error GoTo Error
    Set vrGetPlanes = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-filtro", adInteger, adParamInput, 1, Filtro)
        .Parameters.Append adoCmd.CreateParameter(":SI-filtro", adInteger, adParamInput, 1, Filtro)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPlanes.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPlanes.EOF Then
        vrGetPlanes.MoveLast
        CantidadTuplas = vrGetPlanes.RecordCount
        vrGetPlanes.MoveFirst
        GetPlanes = CantidadTuplas
    Else
        GetPlanes = -1
    End If
    Exit Function

Error:
    Set vrGetPlanes = Nothing
    Screen.MousePointer = vbDefault
    GetPlanes = fn_error(Err.Description, sSql1)
    GetPlanes = -1
End Function

Function GetPlanesClose() As Integer

    If Not vrGetPlanes Is Nothing Then
        vrGetPlanes.Close
        Set vrGetPlanes = Nothing
    End If
End Function

Function GetPlanesRead(ByRef codi_plan As String, ByRef desc_plan As String, ByRef fecha_creacion As String, ByRef fecha_fin As String, ByRef codi_facilida As String, ByRef desc_facilida As String, ByRef flag_control As String, ByRef codi_renta As String, ByRef desc_renta As String, ByRef nomb_central As String, ByRef prop_cargaini As String, ByRef vlor_plan As Long, ByRef tipo_saldo As String, ByRef vlor_cargaini As Long, ByRef Codi_Concepto As String, ByRef plan_ok As Integer, ByRef uso As String, ByRef flag_acumula As String, ByRef flag_portal As String, ByRef rut_empresa As String, ByRef codi_rentasms As String, ByRef desc_rentasms As String, ByRef codi_internosms As String, ByRef codi_rentadatos As String, ByRef desc_rentadatos As String, ByRef codi_internodatos As String, ByRef rut_tercero As String, ByRef agrupador As String, ByRef paq_portal As String, ByRef cant_lineas As Integer, ByRef flag_4g As String, ByRef flag_cargoboleta As String) As Integer
    Dim res As Integer

    GetPlanesRead = True

    If vrGetPlanes Is Nothing Then
        GetPlanesRead = False
        Exit Function
    End If
    If vrGetPlanes.EOF Then
        res = GetPlanesClose()
        GetPlanesRead = False
        Exit Function
    End If

    codi_plan = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(0).Type, vrGetPlanes.Fields(0))
    desc_plan = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(1).Type, vrGetPlanes.Fields(1))
    fecha_creacion = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(2).Type, vrGetPlanes.Fields(2))
    fecha_fin = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(3).Type, vrGetPlanes.Fields(3))
    codi_facilida = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(4).Type, vrGetPlanes.Fields(4))
    desc_facilida = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(5).Type, vrGetPlanes.Fields(5))
    flag_control = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(6).Type, vrGetPlanes.Fields(6))
    codi_renta = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(7).Type, vrGetPlanes.Fields(7))
    desc_renta = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(8).Type, vrGetPlanes.Fields(8))
    nomb_central = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(9).Type, vrGetPlanes.Fields(9))
    prop_cargaini = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(10).Type, vrGetPlanes.Fields(10))
    vlor_plan = gfnDevuelveValorCampo("LONG", vrGetPlanes.Fields(11).Type, vrGetPlanes.Fields(11))
    tipo_saldo = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(12).Type, vrGetPlanes.Fields(12))
    vlor_cargaini = gfnDevuelveValorCampo("LONG", vrGetPlanes.Fields(13).Type, vrGetPlanes.Fields(13))
    Codi_Concepto = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(14).Type, vrGetPlanes.Fields(14))
    plan_ok = gfnDevuelveValorCampo("INTEGER", vrGetPlanes.Fields(15).Type, vrGetPlanes.Fields(15))
    ' incorpora campo uso. JPABLOS. Proyecto OMV. 17-jun-2011.
    uso = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(16).Type, vrGetPlanes.Fields(16))
    ' incorpora campo Rut Empresa. IALCAINO. Proyecto Portal. 29-abr-2013.
    flag_acumula = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(17).Type, vrGetPlanes.Fields(17))
    flag_portal = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(18).Type, vrGetPlanes.Fields(18))
    rut_empresa = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(19).Type, vrGetPlanes.Fields(19))
    codi_rentasms = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(20).Type, vrGetPlanes.Fields(20))
    desc_rentasms = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(21).Type, vrGetPlanes.Fields(21))
    codi_internosms = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(22).Type, vrGetPlanes.Fields(22))
    codi_rentadatos = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(23).Type, vrGetPlanes.Fields(23))
    desc_rentadatos = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(24).Type, vrGetPlanes.Fields(24))
    codi_internodatos = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(25).Type, vrGetPlanes.Fields(25))
    rut_tercero = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(26).Type, vrGetPlanes.Fields(26))
    agrupador = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(27).Type, vrGetPlanes.Fields(27))
    paq_portal = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(28).Type, vrGetPlanes.Fields(28))
    cant_lineas = gfnDevuelveValorCampo("INTEGER", vrGetPlanes.Fields(29).Type, vrGetPlanes.Fields(29))
    flag_4g = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(30).Type, vrGetPlanes.Fields(30))
    flag_cargoboleta = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(31).Type, vrGetPlanes.Fields(31))
    
    vrGetPlanes.MoveNext

End Function

Public Function GetPlanFaci(ByVal CodiPlan As String) As Long
    'Extrae las facilidades de un plan
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "f.codi_facilida, t.desc_tiposerv,  "
    sSql1 = sSql1 & "f.codi_facilida, t.desc_tiposerv,  "
    sSql = sSql & "f.cant_facilida, f.codi_exclusion,  "
    sSql1 = sSql1 & "f.cant_facilida, f.codi_exclusion,  "
    sSql = sSql & "decode(f.prog_central,'S','SI','NO') "
    sSql1 = sSql1 & "decode(f.prog_central,'S','SI','NO') "
    sSql = sSql & " "
    sSql1 = sSql1 & " "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_facilidad f, siat_tiposerv t "
    sSql1 = sSql1 & "siat_pr_facilidad f, siat_tiposerv t "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "f.codi_plan = ? "
    sSql1 = sSql1 & "f.codi_plan = '" & CodiPlan & "' "
    sSql = sSql & "and f.codi_facilida = t.codi_tiposerv "
    sSql1 = sSql1 & "and f.codi_facilida = t.codi_tiposerv "
    On Error GoTo Error
    Set vrGetPlanFaci = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiPlan", adVarChar, adParamInput, 10, CodiPlan)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPlanFaci.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPlanFaci.EOF Then
        vrGetPlanFaci.MoveLast
        CantidadTuplas = vrGetPlanFaci.RecordCount
        vrGetPlanFaci.MoveFirst
        GetPlanFaci = CantidadTuplas
    Else
        GetPlanFaci = -1
    End If
    Exit Function

Error:
    Set vrGetPlanFaci = Nothing
    Screen.MousePointer = vbDefault
    GetPlanFaci = fn_error(Err.Description, sSql1)
    GetPlanFaci = -1
End Function

Function GetPlanFaciClose() As Integer

    If Not vrGetPlanFaci Is Nothing Then
        vrGetPlanFaci.Close
        Set vrGetPlanFaci = Nothing
    End If
End Function

Function GetPlanFaciRead(ByRef codi_facilida As String, ByRef desc_facilida As String, ByRef cant_facilida As Long, ByRef codi_exclusion As String, ByRef prog_central As String) As Integer
    Dim res As Integer

    GetPlanFaciRead = True

    If vrGetPlanFaci Is Nothing Then
        GetPlanFaciRead = False
        Exit Function
    End If
    If vrGetPlanFaci.EOF Then
        res = GetPlanFaciClose()
        GetPlanFaciRead = False
        Exit Function
    End If

    codi_facilida = gfnDevuelveValorCampo("STRING", vrGetPlanFaci.Fields(0).Type, vrGetPlanFaci.Fields(0))
    desc_facilida = gfnDevuelveValorCampo("STRING", vrGetPlanFaci.Fields(1).Type, vrGetPlanFaci.Fields(1))
    cant_facilida = gfnDevuelveValorCampo("LONG", vrGetPlanFaci.Fields(2).Type, vrGetPlanFaci.Fields(2))
    codi_exclusion = gfnDevuelveValorCampo("STRING", vrGetPlanFaci.Fields(3).Type, vrGetPlanFaci.Fields(3))
    prog_central = gfnDevuelveValorCampo("STRING", vrGetPlanFaci.Fields(4).Type, vrGetPlanFaci.Fields(4))

    vrGetPlanFaci.MoveNext

End Function

Public Function GetPlanProd(ByVal CodiPlan As String) As Long
    'Extrae el producto asociado al plan
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "codi_producto,  "
    sSql1 = sSql1 & "codi_producto,  "
    sSql = sSql & "decode(flag_obligatorio,'S','SI','NO'),  "
    sSql1 = sSql1 & "decode(flag_obligatorio,'S','SI','NO'),  "
    sSql = sSql & "codi_exclusion  "
    sSql1 = sSql1 & "codi_exclusion  "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_prod_plan "
    sSql1 = sSql1 & "siat_pr_prod_plan "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & " codi_plan = ? "
    sSql1 = sSql1 & " codi_plan = '" & CodiPlan & "' "
    On Error GoTo Error
    Set vrGetPlanProd = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-CodiPlan", adVarChar, adParamInput, 10, CodiPlan)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPlanProd.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPlanProd.EOF Then
        vrGetPlanProd.MoveLast
        CantidadTuplas = vrGetPlanProd.RecordCount
        vrGetPlanProd.MoveFirst
        GetPlanProd = CantidadTuplas
    Else
        GetPlanProd = -1
    End If
    Exit Function

Error:
    Set vrGetPlanProd = Nothing
    Screen.MousePointer = vbDefault
    GetPlanProd = fn_error(Err.Description, sSql1)
    GetPlanProd = -1
End Function

Function GetPlanProdClose() As Integer

    If Not vrGetPlanProd Is Nothing Then
        vrGetPlanProd.Close
        Set vrGetPlanProd = Nothing
    End If
End Function

Function GetPlanProdRead(ByRef codi_producto As String, ByRef flag_obligatorio As String, ByRef codi_exclusion As String) As Integer
    Dim res As Integer

    GetPlanProdRead = True

    If vrGetPlanProd Is Nothing Then
        GetPlanProdRead = False
        Exit Function
    End If
    If vrGetPlanProd.EOF Then
        res = GetPlanProdClose()
        GetPlanProdRead = False
        Exit Function
    End If

    codi_producto = gfnDevuelveValorCampo("STRING", vrGetPlanProd.Fields(0).Type, vrGetPlanProd.Fields(0))
    flag_obligatorio = gfnDevuelveValorCampo("STRING", vrGetPlanProd.Fields(1).Type, vrGetPlanProd.Fields(1))
    codi_exclusion = gfnDevuelveValorCampo("STRING", vrGetPlanProd.Fields(2).Type, vrGetPlanProd.Fields(2))

    vrGetPlanProd.MoveNext

End Function

Public Function GetPlanesMovil(ByVal Filtro As Integer) As Long
    'Lista los lpanes existentes en el sistema
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select "
    sSql1 = "select "
    sSql = sSql & "pp.codi_plan, pp.desc_plan,  "
    sSql1 = sSql1 & "pp.codi_plan, pp.desc_plan,  "
    sSql = sSql & "to_char(pp.fech_creacion,'dd-mm-yyyy') fecha_creacion,  "
    sSql1 = sSql1 & "to_char(pp.fech_creacion,'dd-mm-yyyy') fecha_creacion,  "
    sSql = sSql & "to_char(pp.fech_fin,'dd-mm-yyyy') fecha_fin, "
    sSql1 = sSql1 & "to_char(pp.fech_fin,'dd-mm-yyyy') fecha_fin, "
    sSql = sSql & "pp.codi_facilida, ts.desc_tiposerv,  "
    sSql1 = sSql1 & "pp.codi_facilida, ts.desc_tiposerv,  "
    sSql = sSql & "decode(pp.flag_control,'S','SI','NO'),  "
    sSql1 = sSql1 & "decode(pp.flag_control,'S','SI','NO'),  "
    sSql = sSql & "pp.codi_renta, cv.desc_concepto,  "
    sSql1 = sSql1 & "pp.codi_renta, cv.desc_concepto,  "
    sSql = sSql & "pp.nomb_central,  "
    sSql1 = sSql1 & "pp.nomb_central,  "
    sSql = sSql & "decode(pp.prop_cargaini,'S','SI','NO'),  "
    sSql1 = sSql1 & "decode(pp.prop_cargaini,'S','SI','NO'),  "
    sSql = sSql & "pp.vlor_plan,  "
    sSql1 = sSql1 & "pp.vlor_plan,  "
    sSql = sSql & "decode(pp.tipo_saldo,'MINU','MINUTOS','PESOS'), "
    sSql1 = sSql1 & "decode(pp.tipo_saldo,'MINU','MINUTOS','PESOS'), "
    sSql = sSql & "pp.vlor_cargaini,  "
    sSql1 = sSql1 & "pp.vlor_cargaini,  "
    sSql = sSql & "cv.codi_concepto, "
    sSql1 = sSql1 & "cv.codi_concepto, "
    sSql = sSql & "siaa_promociones.PlanOK(pp.codi_plan) "
    sSql1 = sSql1 & "siaa_promociones.PlanOK(pp.codi_plan) "
    ' incorpora campo uso. JPABLOS. Proyecto OMV. 17-jun-2011.
    sSql = sSql & ", decode(pp.uso, 'PRE', 'PREPAGO', 'POS', 'POSPAGO') "
    sSql1 = sSql1 & ", decode(pp.uso, 'PRE', 'PREPAGO', 'POS', 'POSPAGO') "
    ' incorpora campo RUT Empresa. IALCAINO. Proyecto PORTAL. 29-ABR-2013.
    sSql = sSql & ", decode(pp.flag_acumula,'S','SI','NO') "
    sSql1 = sSql1 & ", decode(pp.flag_acumula,'S','SI','NO') "
    sSql = sSql & ", decode(pp.flag_portal, 'S', 'SI', 'NO') "
    sSql1 = sSql1 & ", decode(pp.flag_portal, 'S', 'SI', 'NO') "
    sSql = sSql & ", pp.rut_empresa "
    sSql1 = sSql1 & ", pp.rut_empresa "
    sSql = sSql & ", pp.codi_rentasms,CVS.desc_concepto, cvs.codi_concepto "
    sSql1 = sSql1 & ", pp.codi_rentasms,CVS.desc_concepto, cvs.codi_concepto "
    sSql = sSql & ", pp.codi_rentadatos,CVD.desc_concepto, cvd.codi_concepto "
    sSql1 = sSql1 & ", pp.codi_rentadatos,CVD.desc_concepto, cvd.codi_concepto "
    sSql = sSql & ", pp.rut_tercero "
    sSql1 = sSql1 & ", pp.rut_tercero "
    sSql = sSql & ", pp.codi_agrupa "
    sSql1 = sSql1 & ", pp.codi_agrupa "
    sSql = sSql & ", pp.paq_portal "
    sSql1 = sSql1 & ", pp.paq_portal "
    sSql = sSql & ", pp.cant_lineas "
    sSql1 = sSql1 & ", pp.cant_lineas "
    sSql = sSql & ", decode(pp.flag_4g,'S','SI','NO') "
    sSql1 = sSql1 & ", decode(pp.flag_4g,'S','SI','NO') "
    sSql = sSql & ", decode(pp.flag_cargoboleta,'S','SI','NO') "
    sSql1 = sSql1 & ", decode(pp.flag_cargoboleta,'S','SI','NO') "
    sSql = sSql & " from "
    sSql1 = sSql1 & " from "
    sSql = sSql & "siat_pr_plan pp, typv_cptosvig cv, siat_tiposerv ts,TYPV_CPTOSVIG CVS,TYPV_CPTOSVIG CVD "
    sSql1 = sSql1 & "siat_pr_plan pp, typv_cptosvig cv, siat_tiposerv ts,TYPV_CPTOSVIG CVS,TYPV_CPTOSVIG CVD "
    sSql = sSql & " where "
    sSql1 = sSql1 & " where "
    sSql = sSql & "pp.codi_renta = cv.codi_interno (+) "
    sSql1 = sSql1 & "pp.codi_renta = cv.codi_interno (+) "
    sSql = sSql & " AND PP.CODI_RENTASMS = CVS.CODI_INTERNO(+)  AND PP.CODI_RENTADATOS = CVD.CODI_INTERNO(+)"
    sSql1 = sSql1 & " AND PP.CODI_RENTASMS = CVS.CODI_INTERNO(+)  AND PP.CODI_RENTADATOS = CVD.CODI_INTERNO(+)"
    sSql = sSql & "and pp.codi_facilida = ts.codi_tiposerv "
    sSql1 = sSql1 & "and pp.codi_facilida = ts.codi_tiposerv "
    sSql = sSql & "and pp.uso is not null "
    sSql1 = sSql1 & "and pp.uso is not null "
    sSql = sSql & "and decode(?,0,0,decode(sign(nvl(pp.fech_fin,sysdate)- sysdate),-1,-1,1)) = ? "
    sSql1 = sSql1 & "and decode(" & Str(Filtro) & ",0,0,decode(sign(nvl(pp.fech_fin,sysdate)- sysdate),-1,-1,1)) = " & Str(Filtro) & " "
     sSql = sSql & "order by pp.codi_agrupa, pp.desc_plan "
    sSql1 = sSql1 & "order by pp.codi_agrupa, pp.desc_plan "
    On Error GoTo Error
    Set vrGetPlanes = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
        .Parameters.Append adoCmd.CreateParameter(":SI-filtro", adInteger, adParamInput, 1, Filtro)
        .Parameters.Append adoCmd.CreateParameter(":SI-filtro", adInteger, adParamInput, 1, Filtro)
    End With
    Screen.MousePointer = vbHourglass
    vrGetPlanes.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPlanes.EOF Then
        vrGetPlanes.MoveLast
        CantidadTuplas = vrGetPlanes.RecordCount
        vrGetPlanes.MoveFirst
        GetPlanesMovil = CantidadTuplas
    Else
        GetPlanesMovil = -1
    End If
    Exit Function

Error:
    Set vrGetPlanes = Nothing
    Screen.MousePointer = vbDefault
    GetPlanesMovil = fn_error(Err.Description, sSql1)
    GetPlanesMovil = -1
End Function

Function GetPlanesMovilRead(ByRef codi_plan As String, ByRef desc_plan As String, ByRef fecha_creacion As String, ByRef fecha_fin As String, ByRef codi_facilida As String, ByRef desc_facilida As String, ByRef flag_control As String, ByRef codi_renta As String, ByRef desc_renta As String, ByRef nomb_central As String, ByRef prop_cargaini As String, ByRef vlor_plan As Long, ByRef tipo_saldo As String, ByRef vlor_cargaini As Long, ByRef Codi_Concepto As String, ByRef plan_ok As Integer, ByRef uso As String, ByRef flag_acumula As String, ByRef flag_portal As String, ByRef rut_empresa As String, ByRef codi_rentasms As String, ByRef desc_rentasms As String, ByRef codi_internosms As String, ByRef codi_rentadatos As String, ByRef desc_rentadatos As String, ByRef codi_internodatos As String, ByRef rut_tercero As String, ByRef Codi_Agrupa As String, ByRef paq_portal As String, ByRef cant_lineas As Integer, ByRef flag_4g As String, ByRef flag_cargoboleta As String) As Integer
    Dim res As Integer

    GetPlanesMovilRead = True

    If vrGetPlanes Is Nothing Then
        GetPlanesMovilRead = False
        Exit Function
    End If
    If vrGetPlanes.EOF Then
        res = GetPlanesClose()
        GetPlanesMovilRead = False
        Exit Function
    End If

    codi_plan = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(0).Type, vrGetPlanes.Fields(0))
    desc_plan = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(1).Type, vrGetPlanes.Fields(1))
    fecha_creacion = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(2).Type, vrGetPlanes.Fields(2))
    fecha_fin = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(3).Type, vrGetPlanes.Fields(3))
    codi_facilida = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(4).Type, vrGetPlanes.Fields(4))
    desc_facilida = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(5).Type, vrGetPlanes.Fields(5))
    flag_control = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(6).Type, vrGetPlanes.Fields(6))
    codi_renta = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(7).Type, vrGetPlanes.Fields(7))
    desc_renta = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(8).Type, vrGetPlanes.Fields(8))
    nomb_central = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(9).Type, vrGetPlanes.Fields(9))
    prop_cargaini = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(10).Type, vrGetPlanes.Fields(10))
    vlor_plan = gfnDevuelveValorCampo("LONG", vrGetPlanes.Fields(11).Type, vrGetPlanes.Fields(11))
    tipo_saldo = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(12).Type, vrGetPlanes.Fields(12))
    vlor_cargaini = gfnDevuelveValorCampo("LONG", vrGetPlanes.Fields(13).Type, vrGetPlanes.Fields(13))
    Codi_Concepto = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(14).Type, vrGetPlanes.Fields(14))
    plan_ok = gfnDevuelveValorCampo("INTEGER", vrGetPlanes.Fields(15).Type, vrGetPlanes.Fields(15))
    ' incorpora campo uso. JPABLOS. Proyecto OMV. 17-jun-2011.
    uso = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(16).Type, vrGetPlanes.Fields(16))
    ' incorpora campo Rut Empresa. IALCAINO. Proyecto Portal. 29-abr-2013.
    flag_acumula = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(17).Type, vrGetPlanes.Fields(17))
    flag_portal = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(18).Type, vrGetPlanes.Fields(18))
    rut_empresa = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(19).Type, vrGetPlanes.Fields(19))
    codi_rentasms = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(20).Type, vrGetPlanes.Fields(20))
    desc_rentasms = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(21).Type, vrGetPlanes.Fields(21))
    codi_internosms = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(22).Type, vrGetPlanes.Fields(22))
    codi_rentadatos = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(23).Type, vrGetPlanes.Fields(23))
    desc_rentadatos = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(24).Type, vrGetPlanes.Fields(24))
    codi_internodatos = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(25).Type, vrGetPlanes.Fields(25))
    rut_tercero = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(26).Type, vrGetPlanes.Fields(26))
    Codi_Agrupa = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(27).Type, vrGetPlanes.Fields(27))
    paq_portal = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(28).Type, vrGetPlanes.Fields(28))
    cant_lineas = gfnDevuelveValorCampo("INTEGER", vrGetPlanes.Fields(29).Type, vrGetPlanes.Fields(29))
    flag_4g = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(30).Type, vrGetPlanes.Fields(30))
    flag_cargoboleta = gfnDevuelveValorCampo("STRING", vrGetPlanes.Fields(31).Type, vrGetPlanes.Fields(31))

    vrGetPlanes.MoveNext

End Function

Public Function GetPlanesAgrupa() As Long
    'Extrae las facilidades de un plan
    Dim sSql As String
    Dim sSql1 As String
    Dim adoCmd As New ADODB.Command
    Dim CantidadTuplas As Integer

    sSql = "select t.codi_agrupa from SIAT_PR_AGRUPA t order by t.codi_agrupa "
    sSql1 = "select t.codi_agrupa from SIAT_PR_AGRUPA t order by t.codi_agrupa "
    On Error GoTo Error
    Set vrGetPlanAgru = New ADODB.Recordset
    With adoCmd
        .ActiveConnection = GPBase
        .CommandType = adCmdText
        .CommandText = sSql
    End With
    Screen.MousePointer = vbHourglass
    vrGetPlanAgru.Open adoCmd
    Screen.MousePointer = vbDefault

    If Not vrGetPlanAgru.EOF Then
        vrGetPlanAgru.MoveLast
        CantidadTuplas = vrGetPlanAgru.RecordCount
        vrGetPlanAgru.MoveFirst
        GetPlanesAgrupa = CantidadTuplas
    Else
        GetPlanesAgrupa = -1
    End If
    Exit Function

Error:
    Set vrGetPlanAgru = Nothing
    Screen.MousePointer = vbDefault
    GetPlanesAgrupa = fn_error(Err.Description, sSql1)
    GetPlanesAgrupa = -1
End Function

Function GetPlanesAgrupaClose() As Integer

    If Not vrGetPlanAgru Is Nothing Then
        vrGetPlanAgru.Close
        Set vrGetPlanAgru = Nothing
    End If
End Function

Function GetPlanesAgrupaRead(ByRef Codi_Agrupa As String) As Integer
    Dim res As Integer

    GetPlanesAgrupaRead = True

    If vrGetPlanAgru Is Nothing Then
        GetPlanesAgrupaRead = False
        Exit Function
    End If
    If vrGetPlanAgru.EOF Then
        res = GetPlanesAgrupaClose()
        GetPlanesAgrupaRead = False
        Exit Function
    End If

    Codi_Agrupa = gfnDevuelveValorCampo("STRING", vrGetPlanAgru.Fields(0).Type, vrGetPlanAgru.Fields(0))

    vrGetPlanAgru.MoveNext

End Function
