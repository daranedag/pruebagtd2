VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.MDIForm MDIPrincipal 
   BackColor       =   &H8000000C&
   Caption         =   "Sistema de Promociones"
   ClientHeight    =   10350
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11400
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   StartUpPosition =   2  'CenterScreen
   Tag             =   "MPROM"
   WindowState     =   2  'Maximized
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   1164
      ButtonWidth     =   2646
      ButtonHeight    =   1005
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   17
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Productos"
            Description     =   "Mantenedor de Productos"
            Object.ToolTipText     =   "Mantenedor de Productos"
            Object.Tag             =   "PRODUCTOS"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Planes"
            Description     =   "Mantenedor de Planes"
            Object.ToolTipText     =   "Mantenedor de Planes"
            Object.Tag             =   "PLANES"
            Object.Width           =   10
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "   Promociones   "
            Description     =   "Mantenedor de Promociones"
            Object.ToolTipText     =   "Mantenedor de Promociones"
            Object.Tag             =   "PROMOCIONES"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Ofertas"
            Description     =   "Mantenedor de Ofertas"
            Object.ToolTipText     =   "Mantenedor de ofertas"
            Object.Tag             =   "OFERTAS"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Paquetes"
            Object.ToolTipText     =   "Mantenedor de Paquetes"
            Object.Tag             =   "PAQUETES"
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Bolsas"
            Description     =   "Mantenedor de Bolsas"
            Object.ToolTipText     =   "Mantenedor de Bolsas"
            Object.Tag             =   "BOLSAS"
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Frec Funcionarios"
            Object.ToolTipText     =   "Numeros Frecuentes Funcionarios"
            Object.Tag             =   "FRECUENTES"
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "TELSUR"
            Description     =   "Salir de la Aplicacion"
            Object.ToolTipText     =   "Cambiar de Empresa"
            Object.Tag             =   "EMPRESA"
         EndProperty
         BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Salir"
            Description     =   "Salir de la Aplicacion"
            Object.Tag             =   "SALIR"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   10095
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MDIPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''Private Sub MDIForm_Load()
''    Dim Ret As Integer
''    Dim frm As Form
''
''    Gp_VAplicacion.codi_empresa = 1
''    '''''''''''''''''''''''''''''''''''''''''
''    ''' COMENTAR CUANDO SE ESTE EN DESARROLLO
''    '''''''''''''''''''''''''''''''''''''''''
''  ''''  Call Su_Init_Aplicacion("PROM")
''    '''''''''''''''''''''''''''''''''''''''''
''
''    '''''''''''''''''''''''''''''''''''''''''
''    ''' COMENTAR CUANDO SE ESTE EN PRODUCCION
''    '''''''''''''''''''''''''''''''''''''''''
''   ' XDBName = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.68.91)(PORT = 1533)) ) (CONNECT_DATA = (SID = oracntf3) (SERVER = DEDICATED) ) )"
''   XDBName = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.91)(PORT=1533)))(CONNECT_DATA=(SID=desacnt)(SERVER=DEDICATED)))"
''    '''''''''''''''''''''''''''''''''''''''''
''    'RAALTAMI - 05/08/2013 -TI284055: Por defecto carga Telsur.
''
''  '  Call SrvInitDb("(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.91)(PORT=1533)))(CONNECT_DATA=(SID=desacnt)(SERVER=DEDICATED)))")
''
''   ' Call SrvInitDb("(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.201)(PORT=1522)))(CONNECT_DATA=(SID=desatcy2)(SERVER=DEDICATED)))")
''   'Call SrvInitDb("(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.118)(PORT=15123)))(CONNECT_DATA=(SID=desatcy)(SERVER = DEDICATED)))")
''
''    Call SrvInitDb("tns:mtcp_yel")
''
''    Ret = Srv_ZonalRol_Open(Trim$(Gp_Username_Aplicacion), Gp_My_Aplicacion)
''    If Ret >= 1 Then
''       Ret = Srv_ZonalRol_Read(Gp_Zonal_Aplicacion, Gp_Rol_Aplicacion, Gp_VAplicacion.codi_empresa)
''       Ret = Srv_ZonalRol_Close()
''    Else
''       MsgBox "Error de conexi�n y/o permisos de Usuario", vbCritical
''       Ret = Srv_ZonalRol_Close()
''
''       For Each frm In Forms
''          Unload frm
''       Next
''       End
''    End If
''
''    Gp_VAplicacion.Zonal = Gp_Zonal_Aplicacion
''    Gp_VAplicacion.Username = Trim$(Gp_Username_Aplicacion)
''    Gp_VAplicacion.Rol = Gp_Rol_Aplicacion
''
''    Call SuLeeSya(Gp_My_Aplicacion, Gp_Rol_Aplicacion)
''
''    Call suInicializaParametros
''
''End Sub
''
''Sub suInicializaParametros()
''    frmMensaje.Show
''    frmMensaje.Refresh
''    Call suMensaje("Cargando Localidades ...")
''    Call gsuCargaLocalidades
''    Call suMensaje("Cargando Grupos de Par�metros ...")
''    Call gsuCargaGrupos
''    Call suMensaje("Cargando Equipos ...")
''    Call gsuCargaEquipos
''    Call suMensaje("Cargando Facilidades ...")
''    Call gsuCargaFacilidades
''    Call suMensaje("Cargando Rentas ...")
''    Call gsuCargaRentas
''    Call suMensaje("Cargando Productos ...")
''    Call gsuCargaProductos
''    Call suMensaje("Cargando Planes ...")
''    Call gsuCargaPlanes
''    Call suMensaje("Cargando Areas ...")
''    Call gsuCargaAreas
''    Call suMensaje("Cargando Clases paquetes ...")
''    Call gsuCargaClasePaque
''
''    Unload frmMensaje
''
''End Sub
''
''Private Sub suMensaje(ByVal psCualMensaje As String)
''    frmMensaje.lblMensaje.Caption = vbCrLf & vbCrLf & psCualMensaje
''    frmMensaje.Refresh
''End Sub
''
''
''Private Sub MDIForm_Unload(Cancel As Integer)
''    Call Su_Fin_Aplicacion("PROM")
''End Sub
''
''Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
''    Dim oForm As Form
''
''    Select Case Button.Tag
''        Case "PRODUCTOS"
''            frmProductos.Show
''            frmProductos.ZOrder 0
''        Case "PLANES"
''            frmPlanes.Show
''            frmPlanes.ZOrder 0
''        Case "PROMOCIONES"
''            frmPromociones.Show
''            frmPromociones.ZOrder 0
''        Case "OFERTAS"
''            frmOfertas.Show
''            frmOfertas.ZOrder 0
''        Case "PAQUETES"
''            frmPaquetes.Show
''            frmPaquetes.ZOrder 0
''        ' se incorpora bot�n bolsas. JPABLOS. Proyecto OMV. 19-jun-2011.
''        Case "BOLSAS"
''            frmBolsas.Show
''            frmBolsas.ZOrder 0
''        Case "EMPRESA"
''
''            For Each oForm In Forms
''                If oForm.Name <> "MDIPrincipal" Then
''                    Unload oForm
''                End If
''            Next oForm
''
''            If Button.Caption = "TELSUR" Then
''                Button.Caption = "TELCOY"
''                ''' COMENTAR EN DESARROLLO
''             '   XDBName = "tns:MTCP_CAR"
''                ''' COMENTAR EN PRODUCCION
''                XDBName = "tns:ORACNT_DESARROLLO"
''                'cambio de BD en desarrollo, comentar en produccion
''                'XDBName = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.201)(PORT=1522)))(CONNECT_DATA=(SID=desatcy2)(SERVER=DEDICATED)))"
''                XDBName = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.118)(PORT=15123)))(CONNECT_DATA=(SID=desatcy)(SERVER = DEDICATED)))"
''                Gp_VAplicacion.codi_empresa = 18
''            Else
''                Gp_VAplicacion.codi_empresa = 1
''                Button.Caption = "TELSUR"
''                ''' COMENTAR EN DESARROLLO
''              '  XDBName = "tns:MTCP_YEL"
''                ''' COMENTAR EN PRODUCCION
''               XDBName = "tns:ORACNT_DESARROLLO"
''                'cambio de BD en desarrollo, comentar en produccion
''                'XDBName = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.91)(PORT=1533)))(CONNECT_DATA=(SID=oracntf3)(SERVER=DEDICATED)))"
''                XDBName = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.91)(PORT=1533)))(CONNECT_DATA=(SID=desacnt)(SERVER=DEDICATED)))"
''            End If
''
''            'RAALTAMI - 05/08/2013 -TI284055: se env�a variable de entrada, para cargar el string de conexi�n seg�n la empresa.
''
''            Call SrvInitDb(XDBName)
''
''            Call suInicializaParametros
''
''        Case "FRECUENTES"
''            frmFrecuentes.Show
''            frmFrecuentes.ZOrder 0
''
''        Case "SALIR"
''            ' Se cambia por "unload me", y se pasa este c�digo al unload del form. Esto para que funcione bien el bot�n "X" del form, y no quede el proceso corriendo. JPABLOS. 19-jun-2011.
'''            Call Su_Fin_Aplicacion("PROM")
'''            End
''            Unload Me
''    End Select
''End Sub

Private Sub MDIForm_Load()
    Dim Ret As Integer
    Dim frm As Form

    Gp_VAplicacion.codi_empresa = 1
    '''''''''''''''''''''''''''''''''''''''''
    ''' COMENTAR CUANDO SE ESTE EN DESARROLLO
    '''''''''''''''''''''''''''''''''''''''''
    Call Su_Init_Aplicacion("PROM")
    '''''''''''''''''''''''''''''''''''''''''
    
    '''''''''''''''''''''''''''''''''''''''''
    ''' COMENTAR CUANDO SE ESTE EN PRODUCCION
    '''''''''''''''''''''''''''''''''''''''''
    'XDBName = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.68.91)(PORT = 1533)) ) (CONNECT_DATA = (SID = oracntf3) (SERVER = DEDICATED) ) )"
    '''''''''''''''''''''''''''''''''''''''''
    'RAALTAMI - 05/08/2013 -TI284055: Por defecto carga Telsur.
   
    Call SrvInitDb("(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.91)(PORT=1533)))(CONNECT_DATA=(SID=DESACNT)(SERVER=DEDICATED)))")
    
    'Call SrvInitDb("(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.201)(PORT=1522)))(CONNECT_DATA=(SID=desatcy2)(SERVER=DEDICATED)))")
    
    'Call SrvInitDb("tns:mtcp_yel")
    
    Ret = Srv_ZonalRol_Open(Trim$(Gp_Username_Aplicacion), Gp_My_Aplicacion)
    If Ret >= 1 Then
       Ret = Srv_ZonalRol_Read(Gp_Zonal_Aplicacion, Gp_Rol_Aplicacion, Gp_VAplicacion.codi_empresa)
       Ret = Srv_ZonalRol_Close()
    Else
       MsgBox "Error de conexi�n y/o permisos de Usuario", vbCritical
       Ret = Srv_ZonalRol_Close()
       
       For Each frm In Forms
          Unload frm
       Next
       End
    End If
    
    Gp_VAplicacion.Zonal = Gp_Zonal_Aplicacion
    Gp_VAplicacion.Username = Trim$(Gp_Username_Aplicacion)
    Gp_VAplicacion.Rol = Gp_Rol_Aplicacion
    
    Call SuLeeSya(Gp_My_Aplicacion, Gp_Rol_Aplicacion)
    
    Call suInicializaParametros

End Sub

Sub suInicializaParametros()
    frmMensaje.Show
    frmMensaje.Refresh
    Call suMensaje("Cargando Localidades ...")
    Call gsuCargaLocalidades
    Call suMensaje("Cargando Grupos de Par�metros ...")
    Call gsuCargaGrupos
    Call suMensaje("Cargando Equipos ...")
    Call gsuCargaEquipos
    Call suMensaje("Cargando Facilidades ...")
    Call gsuCargaFacilidades
    Call suMensaje("Cargando Rentas ...")
    Call gsuCargaRentas
    Call suMensaje("Cargando Productos ...")
    Call gsuCargaProductos
    Call suMensaje("Cargando Planes ...")
    Call gsuCargaPlanes
    Call suMensaje("Cargando Areas ...")
    Call gsuCargaAreas
    Call suMensaje("Cargando Clases paquetes ...")
    Call gsuCargaClasePaque

    Unload frmMensaje

End Sub

Private Sub suMensaje(ByVal psCualMensaje As String)
    frmMensaje.lblMensaje.Caption = vbCrLf & vbCrLf & psCualMensaje
    frmMensaje.Refresh
End Sub


Private Sub MDIForm_Unload(Cancel As Integer)
    Call Su_Fin_Aplicacion("PROM")
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim oForm As Form
    
    Select Case Button.Tag
        Case "PRODUCTOS"
            frmProductos.Show
            frmProductos.ZOrder 0
        Case "PLANES"
            frmPlanes.Show
            frmPlanes.ZOrder 0
        Case "PROMOCIONES"
            frmPromociones.Show
            frmPromociones.ZOrder 0
        Case "OFERTAS"
            frmOfertas.Show
            frmOfertas.ZOrder 0
        Case "PAQUETES"
            frmPaquetes.Show
            frmPaquetes.ZOrder 0
        ' se incorpora bot�n bolsas. JPABLOS. Proyecto OMV. 19-jun-2011.
        Case "BOLSAS"
            frmBolsas.Show
            frmBolsas.ZOrder 0
        Case "EMPRESA"
            
            For Each oForm In Forms
                If oForm.Name <> "MDIPrincipal" Then
                    Unload oForm
                End If
            Next oForm
            
            If Button.Caption = "TELSUR" Then
                Button.Caption = "TELCOY"
                ''' COMENTAR EN DESARROLLO
                'XDBName = "tns:MTCP_CAR"
                ''' COMENTAR EN PRODUCCION
                'XDBName = "tns:ORACNT_DESARROLLO"
                'cambio de BD en desarrollo, comentar en produccion
                XDBName = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.118)(PORT=15123)))(CONNECT_DATA=(SID=desatcy)(SERVER=DEDICATED)))"
                Gp_VAplicacion.codi_empresa = 18
            Else
                Gp_VAplicacion.codi_empresa = 1
                Button.Caption = "TELSUR"
                ''' COMENTAR EN DESARROLLO
                'XDBName = "tns:MTCP_YEL"
                ''' COMENTAR EN PRODUCCION
                'XDBName = "tns:ORACNT_DESARROLLO"
                'cambio de BD en desarrollo, comentar en produccion
                XDBName = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.16.68.91)(PORT=1533)))(CONNECT_DATA=(SID=desacnt)(SERVER=DEDICATED)))"
            End If
            
            'RAALTAMI - 05/08/2013 -TI284055: se env�a variable de entrada, para cargar el string de conexi�n seg�n la empresa.
            
            Call SrvInitDb(XDBName)
            
            Call suInicializaParametros
        
        Case "FRECUENTES"
            frmFrecuentes.Show
            frmFrecuentes.ZOrder 0
            
        Case "SALIR"
            ' Se cambia por "unload me", y se pasa este c�digo al unload del form. Esto para que funcione bien el bot�n "X" del form, y no quede el proceso corriendo. JPABLOS. 19-jun-2011.
'            Call Su_Fin_Aplicacion("PROM")
'            End
            Unload Me
    End Select
End Sub

