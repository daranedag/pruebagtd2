VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmFiltro 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Filtro"
   ClientHeight    =   1890
   ClientLeft      =   2565
   ClientTop       =   1500
   ClientWidth     =   8685
   Icon            =   "frmFiltro.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1890
   ScaleWidth      =   8685
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "Borrar Filtros"
      Height          =   375
      Left            =   4200
      TabIndex        =   16
      Top             =   1320
      Width           =   1215
   End
   Begin VB.ComboBox cb_filtro 
      Height          =   315
      Left            =   4080
      TabIndex        =   14
      Top             =   600
      Width           =   2655
   End
   Begin VB.TextBox tx_filtro 
      Height          =   315
      Left            =   4080
      TabIndex        =   13
      Top             =   960
      Width           =   4455
   End
   Begin MSComCtl2.DTPicker dt_filtro 
      Height          =   315
      Left            =   5160
      TabIndex        =   12
      Top             =   240
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      Format          =   113180673
      CurrentDate     =   42345
   End
   Begin VB.TextBox tx_campo 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   600
      Width           =   1695
   End
   Begin VB.ComboBox cb_operador 
      Height          =   315
      Left            =   1920
      TabIndex        =   8
      Top             =   600
      Width           =   2055
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   3
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Ejemplo 4"
         Height          =   1785
         Left            =   2100
         TabIndex        =   7
         Top             =   840
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   2
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample3 
         Caption         =   "Ejemplo 3"
         Height          =   1785
         Left            =   1545
         TabIndex        =   6
         Top             =   675
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   1
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample2 
         Caption         =   "Ejemplo 2"
         Height          =   1785
         Left            =   645
         TabIndex        =   5
         Top             =   300
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Aplicar"
      Height          =   375
      Left            =   840
      TabIndex        =   1
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   2520
      TabIndex        =   0
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Valor"
      Height          =   255
      Left            =   4320
      TabIndex        =   15
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Operador"
      Height          =   255
      Left            =   1920
      TabIndex        =   10
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Campo"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   240
      Width           =   1335
   End
End
Attribute VB_Name = "frmFiltro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Valor As String
Public Operador As String
Public Filtro As String
Public TipoFiltro As String
Public Separador As String

Private Sub cmdApply_Click()
    Valor = "Aplicar"
    Operador = cb_operador.Text

    Select Case TipoFiltro
        Case "Date"
            Filtro = Format(dt_filtro.Value, "dd-mm-yyyy")
        Case "Combo"
            Filtro = Fn_Valor_Combo(cb_filtro, Separador)
        Case "Text", "Number"
            Filtro = tx_filtro.Text
    End Select

    Unload Me
End Sub

Private Sub cmdBorrar_Click()
    Valor = "Borrar"
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    Valor = "Cancelar"
    Unload Me
End Sub

Private Sub Form_Load()
    'centrar el formulario
    Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
    
    cb_operador.Clear
    cb_operador.AddItem "Igual"
    cb_operador.AddItem "Distinto"
    Select Case TipoFiltro
        Case "Date", "Number"
            cb_operador.AddItem "Mayor que"
            cb_operador.AddItem "Menor que"
            cb_operador.AddItem "Mayor o Igual que"
            cb_operador.AddItem "Menor o Igual que"
        Case "Text"
            cb_operador.AddItem "Contiene"
            cb_operador.AddItem "No Contiene"
    End Select
    cb_operador.ListIndex = 0
    
    tx_filtro.Visible = False
    cb_filtro.Visible = False
    dt_filtro.Visible = False
    dt_filtro.Value = Now
    
    Select Case TipoFiltro
        Case "Date":
            dt_filtro.Visible = True
            dt_filtro.Top = 600
            dt_filtro.Left = 4080
            Me.Width = 5970
        Case "Combo":
            cb_filtro.Visible = True
            cb_filtro.Top = 600
            cb_filtro.Left = 4080
            Me.Width = 6990
        Case "Text", "Number":
            tx_filtro.Visible = True
            tx_filtro.Top = 600
            tx_filtro.Left = 4080
            Me.Width = 8790
    End Select
    
End Sub

Private Sub tx_filtro_KeyPress(KEYASCII As Integer)
    Select Case TipoFiltro
    Case "Number"
        KEYASCII = SoloNumeros(KEYASCII)
    Case "Text"
        If Chr(KEYASCII) = "'" Then
            KEYASCII = 0
        End If
        If KEYASCII >= Asc("a") And KEYASCII <= Asc("z") Then
            KEYASCII = KEYASCII - 32
        End If
    End Select
End Sub
