VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Begin VB.Form frmBuscaPlan 
   Caption         =   "Buscador de Planes"
   ClientHeight    =   6375
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11730
   LinkTopic       =   "Form1"
   ScaleHeight     =   6375
   ScaleWidth      =   11730
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid grdPlanes 
      Height          =   3735
      Left            =   240
      TabIndex        =   2
      Top             =   2040
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   6588
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1815
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   11535
      Begin VB.CommandButton cmdLimpia 
         Caption         =   "Limpiar"
         Height          =   495
         Left            =   1680
         TabIndex        =   7
         Top             =   1200
         Width           =   1335
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Facilidad"
         Height          =   255
         Left            =   2280
         TabIndex        =   6
         Top             =   360
         Width           =   1695
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Nombre Plan"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   2775
      End
      Begin VB.TextBox txtNombre 
         Height          =   405
         Left            =   240
         TabIndex        =   0
         Top             =   720
         Width           =   3015
      End
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "Filtrar"
         Height          =   495
         Left            =   240
         TabIndex        =   1
         Top             =   1200
         Width           =   1335
      End
      Begin MSFlexGridLib.MSFlexGrid grdOrig 
         Height          =   375
         Left            =   6840
         TabIndex        =   4
         Top             =   600
         Visible         =   0   'False
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   661
         _Version        =   393216
      End
   End
End
Attribute VB_Name = "frmBuscaPlan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdFiltrar_Click()
    Dim i As Long, j As Long
    Dim iFila As Long
    Dim bAgregar As Boolean
    
    MousePointer = vbHourglass
    
    With grdPlanes
        .Visible = False
        .Rows = 1
        For i = 1 To grdOrig.Rows - 1
            .Rows = .Rows + 1
            For j = 0 To grdOrig.Cols - 1
                .TextMatrix(i, j) = grdOrig.TextMatrix(i, j)
            Next j
        Next i
        .Visible = True
    End With
    iFila = 1
    If txtNombre.Text <> "" Then
        With grdPlanes
            .Visible = False
            .Rows = 1
            Me.Refresh
            ' filtrando por el texto ingresado y la opci�n de Nombre Plan
            If txtNombre <> "" And Option1.Value = True Then
                For i = 1 To grdOrig.Rows - 1
                    If InStr(1, grdOrig.TextMatrix(i, 2), txtNombre.Text, 1) > 0 Then
                        .Rows = .Rows + 1
                        For j = 0 To .Cols - 1
                            .TextMatrix(.Rows - 1, j) = grdOrig.TextMatrix(i, j)
                        Next j
                    End If
                Next i
            Else
            ' filtrando por el texto ingresado y la opci�n de Facilidad
                If txtNombre <> "" And Option2.Value = True Then
                For i = 1 To grdOrig.Rows - 1
                    If txtNombre.Text = grdOrig.TextMatrix(i, 5) Then
                        ' Se encontr� una fila con la facilidad
                        ' revisando que esta no se encuentre en las ya seleccionadas
                        bAgregar = True
                        For j = 0 To .Rows - 1
                                If .TextMatrix(j, 14) = grdOrig.TextMatrix(i, 14) Then
                                    bAgregar = False
                                    Exit For
                                End If
                        Next j
                        If bAgregar Then
                            .Rows = .Rows + 1
                            For j = 0 To .Cols - 1
                                .TextMatrix(.Rows - 1, j) = grdOrig.TextMatrix(i, j)
                            Next j
                        End If
                    End If
                Next i
                Else
                MsgBox "Seleccione Nombre de Plan o Facilidades"
                txtNombre.Text = ""
                End If
            End If
            .Visible = True
        End With
    End If
    MousePointer = vbDefault
End Sub


Private Sub cmdLimpia_Click()
txtNombre.Text = ""
Option1 = False
Option2 = False
With grdPlanes
.Visible = False
End With
End Sub

Private Sub Form_Activate()
    txtNombre.Text = ""
End Sub

Private Sub Form_Load()
    Call suInicializaForm
End Sub

Private Sub suInicializaForm()
    
    grdOrig.Cols = 28
    
    With grdPlanes
        .Cols = 28
        .Rows = 2
        
        .TextMatrix(0, 0) = "Agrupador"
        .ColWidth(0) = 1300
        .TextMatrix(0, 1) = "C�digo"
        .ColWidth(1) = 1300
        .TextMatrix(0, 2) = "Descripci�n"
        .ColWidth(2) = 4000
        .TextMatrix(0, 3) = "Inicio"
        .ColWidth(3) = 1000
        .TextMatrix(0, 4) = "Fin"
        .ColWidth(4) = 1000
        .TextMatrix(0, 5) = "Codi_Facilida"
        .ColWidth(5) = 0
        .TextMatrix(0, 6) = "Facilidad"
        .ColWidth(6) = 4000
        .TextMatrix(0, 7) = "Control"
        .ColWidth(7) = 600
        .TextMatrix(0, 8) = "Codi_Renta"
        .ColWidth(8) = 0
        .TextMatrix(0, 9) = "Renta"
        .ColWidth(9) = 4000
        .TextMatrix(0, 10) = "Central"
        .ColWidth(10) = 1000
        .TextMatrix(0, 11) = "Propor."
        .ColWidth(11) = 600
        .TextMatrix(0, 12) = "Valor Plan"
        .ColWidth(12) = 1000
        .TextMatrix(0, 13) = "Tipo Saldo"
        .ColWidth(13) = 1000
        .TextMatrix(0, 14) = "Carga Inicial"
        .ColWidth(14) = 1000
        .TextMatrix(0, 15) = "Fila"
        .ColWidth(15) = 0
        .TextMatrix(0, 16) = "Rut Empresa"
        .ColWidth(16) = 0
         .TextMatrix(0, 17) = "Acumula Mins"
        .ColWidth(17) = 0
        .TextMatrix(0, 18) = "4G"
        .ColWidth(18) = 500
        .TextMatrix(0, 19) = "Cargo Boleta"
        .ColWidth(19) = 1000
        
        .FixedCols = 0
        .FixedRows = 1
    End With
End Sub

Private Sub grdPlanes_DblClick()
    If grdPlanes.Row > 0 Then
        giCualPlanSeleccionado = grdPlanes.TextMatrix(grdPlanes.Row, 14)
        Unload Me
    End If
End Sub

