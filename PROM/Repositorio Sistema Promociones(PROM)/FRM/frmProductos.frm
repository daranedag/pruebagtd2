VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmProductos 
   Caption         =   "Mantenedor de Productos"
   ClientHeight    =   9000
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14955
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9000
   ScaleWidth      =   14955
   Tag             =   "FRMPRODUCTOS"
   WindowState     =   2  'Maximized
   Begin VB.Frame fraFacilidades 
      Caption         =   "Facilidades"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   4215
      Left            =   3960
      TabIndex        =   42
      Top             =   720
      Visible         =   0   'False
      Width           =   6735
      Begin VB.CommandButton cmdVolverFacilidad 
         Caption         =   "Volver"
         Height          =   375
         Left            =   2400
         TabIndex        =   23
         Top             =   1080
         Width           =   1815
      End
      Begin VB.CommandButton cmdEliminarFacilidad 
         Caption         =   "Eliminar"
         Height          =   375
         Left            =   4560
         TabIndex        =   24
         Tag             =   "FA_ELIMINAR"
         Top             =   1080
         Width           =   1815
      End
      Begin VB.CommandButton cmdAgregarFacilidad 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   240
         TabIndex        =   22
         Tag             =   "FA_AGREGAR"
         Top             =   1080
         Width           =   1815
      End
      Begin VB.ComboBox cbxFacilidad 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   600
         Width           =   6375
      End
      Begin MSFlexGridLib.MSFlexGrid grdFacilidades 
         Height          =   2415
         Left            =   120
         TabIndex        =   43
         Top             =   1680
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   4260
         _Version        =   393216
         AllowUserResizing=   1
      End
      Begin VB.Label Label10 
         Caption         =   "Facilidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   44
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Frame fraEquipos 
      Caption         =   "Equipos Asociados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3615
      Left            =   9720
      TabIndex        =   35
      Top             =   5400
      Width           =   5055
      Begin VB.CommandButton cmdLimpiarEquipo 
         Caption         =   "Limpiar"
         Height          =   255
         Left            =   1200
         TabIndex        =   19
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox txtCodiExclusion 
         Height          =   285
         Left            =   1680
         MaxLength       =   10
         TabIndex        =   17
         Text            =   "Text1"
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtCantEquipo 
         Height          =   285
         Left            =   840
         MaxLength       =   4
         TabIndex        =   16
         Text            =   "Text1"
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdEliminaEquipo 
         Caption         =   "Eliminar"
         Height          =   255
         Left            =   2400
         TabIndex        =   20
         Tag             =   "EQ_ELIMINAR"
         Top             =   1320
         Width           =   975
      End
      Begin VB.CommandButton cmdAgregaEquipo 
         Caption         =   "Agregar"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Tag             =   "EQ_AGREGAR"
         Top             =   1320
         Width           =   975
      End
      Begin VB.ComboBox cbxEquipo 
         Height          =   315
         Left            =   840
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   240
         Width           =   3975
      End
      Begin MSFlexGridLib.MSFlexGrid grdEquipos 
         Height          =   1815
         Left            =   120
         TabIndex        =   39
         Top             =   1680
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   3201
         _Version        =   393216
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Label Label9 
         Caption         =   "Codigo Exclusion"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label8 
         Caption         =   "Equipo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label7 
         Caption         =   "Equipo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame fraLocalidades 
      Caption         =   "Localidades en Venta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3615
      Left            =   5760
      TabIndex        =   31
      Top             =   5280
      Width           =   3855
      Begin VB.CommandButton cmdAgregaLocalidad 
         Caption         =   "Agregar"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Tag             =   "LO_AGREGAR"
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton cmdEliminaLocalidad 
         Caption         =   "Eliminar"
         Height          =   255
         Left            =   2400
         TabIndex        =   14
         Tag             =   "LO_ELIMINAR"
         Top             =   840
         Width           =   1215
      End
      Begin MSFlexGridLib.MSFlexGrid grdLocalidades 
         Height          =   2295
         Left            =   120
         TabIndex        =   37
         Top             =   1200
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   4048
         _Version        =   393216
         AllowUserResizing=   1
      End
      Begin VB.ComboBox cbxLocalidad 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   480
         Width           =   3495
      End
      Begin VB.Label Label6 
         Caption         =   "Localidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame fraRelacion 
      Caption         =   "Relaciones Grupos de Par�metros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3615
      Left            =   0
      TabIndex        =   30
      Top             =   5280
      Width           =   5655
      Begin MSFlexGridLib.MSFlexGrid grdRelaciones 
         Height          =   2175
         Left            =   120
         TabIndex        =   34
         Top             =   1320
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   3836
         _Version        =   393216
         AllowUserResizing=   1
      End
      Begin VB.CommandButton cmdEliminarRelacion 
         Caption         =   "Eliminar"
         Height          =   255
         Left            =   3120
         TabIndex        =   11
         Tag             =   "RE_ELIMINAR"
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton cmdAgregarRelacion 
         Caption         =   "Agregar"
         Height          =   255
         Left            =   960
         TabIndex        =   10
         Tag             =   "RE_AGREGAR"
         Top             =   960
         Width           =   1215
      End
      Begin VB.ComboBox cbxParame 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   600
         Width           =   4095
      End
      Begin VB.ComboBox cbxGrupo 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   4095
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Elemento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame fraProductos 
      Caption         =   "Productos Existentes (En Rojo los Productos no Correctos)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5055
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   14775
      Begin VB.CheckBox chkProductosVigentes 
         Caption         =   "Productos Vigentes"
         Height          =   255
         Left            =   5520
         TabIndex        =   47
         Top             =   0
         Width           =   1815
      End
      Begin VB.CheckBox chkProductosNoVigentes 
         Caption         =   "Productos No Vigentes"
         Height          =   255
         Left            =   7440
         TabIndex        =   46
         Top             =   0
         Width           =   2295
      End
      Begin VB.CommandButton cmdCopiarProducto 
         Caption         =   "Copiar Producto"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6840
         TabIndex        =   45
         Tag             =   "PR_COPIAR"
         Top             =   4320
         Width           =   1935
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "Volver"
         Height          =   375
         Left            =   9000
         TabIndex        =   7
         Top             =   4320
         Width           =   1935
      End
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "Limpiar"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   4320
         Width           =   1935
      End
      Begin VB.CheckBox chkSinFechaFin 
         Caption         =   "Sin Fecha de T�rmino"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   10560
         TabIndex        =   3
         Top             =   3840
         Value           =   1  'Checked
         Width           =   2415
      End
      Begin VB.TextBox txtDescProducto 
         Height          =   285
         Left            =   2640
         TabIndex        =   1
         Text            =   "Text1"
         Top             =   3840
         Width           =   6255
      End
      Begin VB.TextBox txtCodiProducto 
         Height          =   285
         Left            =   120
         MaxLength       =   10
         TabIndex        =   0
         Text            =   "Text1"
         Top             =   3840
         Width           =   2415
      End
      Begin VB.CommandButton cmdModificarProducto 
         Caption         =   "Modificar Producto"
         Height          =   375
         Left            =   4680
         TabIndex        =   6
         Tag             =   "PR_MODIFICAR"
         Top             =   4320
         Width           =   1935
      End
      Begin VB.CommandButton cmdNuevoProducto 
         Caption         =   "Nuevo Producto"
         Height          =   375
         Left            =   2400
         TabIndex        =   5
         Tag             =   "PR_NUEVO"
         Top             =   4320
         Width           =   1935
      End
      Begin MSFlexGridLib.MSFlexGrid grdProductos 
         Height          =   3015
         Left            =   120
         TabIndex        =   26
         Top             =   360
         Width           =   14415
         _ExtentX        =   25426
         _ExtentY        =   5318
         _Version        =   393216
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin MSComCtl2.DTPicker dtFechaFin 
         Height          =   255
         Left            =   9000
         TabIndex        =   2
         Top             =   3840
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   450
         _Version        =   393216
         Format          =   90308609
         CurrentDate     =   38915
      End
      Begin VB.Label Label4 
         Caption         =   "FechaFin"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   9000
         TabIndex        =   29
         Top             =   3600
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "Descripci�n del Producto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2640
         TabIndex        =   28
         Top             =   3600
         Width           =   2415
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo del Producto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   3600
         Width           =   2415
      End
   End
End
Attribute VB_Name = "frmProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cbxFacilidad_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Long
        
    If KeyCode = vbKeyF5 Then
        frmBuscador.txtFiltro.Text = "F"
        frmBuscador.Show vbModal
        
        For i = 0 To UBound(arrCodiFacilidades)
            If arrCodiFacilidades(i) = frmBuscador.txtCodigo.Text Then
                cbxFacilidad.ListIndex = i + 1
             End If
        Next i
    End If
End Sub

Private Sub cbxGrupo_Click()
    Dim sCodiElem As String, sDescElem As String
    Dim iCant As Long, i As Long
    
    iCant = GetParam(arrCodiGrupos(cbxGrupo.ListIndex) - 1)
    
    cbxParame.Clear
    cbxParame.AddItem "SELECCIONE"
    If iCant > 0 Then
        ReDim arrCodiParame(iCant - 1) As String
        i = 0
        Do While GetParamRead(sCodiElem, sDescElem)
            cbxParame.AddItem sCodiElem & "  -  " & sDescElem
            arrCodiParame(i) = sCodiElem
            i = i + 1
        Loop
    End If
    cbxParame.ListIndex = 0
End Sub

Private Sub cbxLocalidad_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Long
        
    If KeyCode = vbKeyF5 Then
        frmBuscador.txtFiltro.Text = "L"
        frmBuscador.Show vbModal
        
        For i = 0 To UBound(arrCodiLocalidades)
            If arrCodiLocalidades(i) = frmBuscador.txtCodigo.Text Then
                cbxLocalidad.ListIndex = i + 1
             End If
        Next i
    End If
End Sub

Private Sub chkProductosNoVigentes_Click()
    Call suLlenaProductos
End Sub

Private Sub chkProductosVigentes_Click()
    Call suLlenaProductos
End Sub

Private Sub chkSinFechaFin_Click()
    If chkSinFechaFin.Value = vbChecked Then
        dtFechaFin.Enabled = False
    End If
    If chkSinFechaFin.Value = vbUnchecked Then
        dtFechaFin.Enabled = True
    End If
End Sub

Private Sub cmdAgregaEquipo_Click()
    Dim i As Long
    
    If cmdAgregaEquipo.Caption = "Agregar" Then
        If cbxEquipo.Text = "SELECCIONE" Then
            MsgBox "Debe seleccionar un equipo a agregar"
            Exit Sub
        End If
        
        With grdEquipos
            For i = 1 To .Rows - 1
                If .TextMatrix(i, 3) = arrCodiEquipos(cbxEquipo.ListIndex - 1) Then
                    MsgBox "El equipo seleccionado ya se encuentra asociado"
                    Exit Sub
                End If
            Next i
        End With
        
        If InsProdEqui(txtCodiProducto.Text, _
                       arrCodiEquipos(cbxEquipo.ListIndex - 1), _
                       Val(txtCantEquipo.Text), _
                       txtCodiExclusion.Text) Then
            Call suTraeDatosProducto
        End If
    End If
    
    If cmdAgregaEquipo.Caption = "Modificar" Then
        If ModProdEqui(txtCodiProducto.Text, _
                       arrCodiEquipos(cbxEquipo.ListIndex - 1), _
                       Val(txtCantEquipo.Text), _
                       txtCodiExclusion.Text) Then
            Call suTraeDatosProducto
            
            Call cmdLimpiarEquipo_Click
        End If
    End If
    
    cbxEquipo.SetFocus
End Sub

Private Sub cmdAgregaLocalidad_Click()
    Dim i As Long
    
    If cbxLocalidad.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar una localidad a lo menos"
        Exit Sub
    End If
    
    With grdLocalidades
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 1) = arrCodiLocalidades(cbxLocalidad.ListIndex - 1) Then
                MsgBox "La localidad seleccionada ya esta ingresada"
                Exit Sub
            End If
        Next i
    End With
    
    If InsProdLoca(txtCodiProducto.Text, _
                   arrCodiLocalidades(cbxLocalidad.ListIndex - 1)) Then
        Call suTraeDatosProducto
        
        cbxLocalidad.ListIndex = 0
    End If
    cbxLocalidad.SetFocus
End Sub

Private Sub cmdAgregarFacilidad_Click()
    Dim i As Long
    
    If cbxFacilidad.Text = "SELECCIONE" Then
        MsgBox "debe seleccionar la facilidad a agregar"
        Exit Sub
    End If
    
    With grdFacilidades
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 1) = arrCodiFacilidades(cbxFacilidad.ListIndex - 1) Then
                MsgBox "Esta facilidad ya se encuentra agregada"
                Exit Sub
            End If
        Next i
    End With
    
    If InsProdSuple(txtCodiProducto.Text, _
                    grdEquipos.TextMatrix(grdEquipos.Row, 3), _
                    arrCodiFacilidades(cbxFacilidad.ListIndex - 1)) Then
        Call suTraeFaciEquiProd
    Else
        MsgBox "No se pudo agregar la facilidad"
    End If
    cbxFacilidad.SetFocus
End Sub

Private Sub cmdAgregarRelacion_Click()
    Dim i As Long, j As Long
    
    ' validando que la combinacion no este ingresada con anterioridad
    If cbxGrupo.Text = "SELECCIONE" Or cbxParame.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el Grupo y el Elemento"
        Exit Sub
    End If
    With grdRelaciones
        For i = 1 To .Rows - 1
            If (.TextMatrix(i, 2) = arrCodiGrupos(cbxGrupo.ListIndex - 1)) And _
               (.TextMatrix(i, 3) = arrCodiParame(cbxParame.ListIndex - 1)) Then
                MsgBox "La combinacion de Grupo y Elemento ya esta ingresada para este producto"
                Exit Sub
            End If
        Next i
    End With
    
    
    ' ingresando la nueva relacion
    If InsProdRela(txtCodiProducto.Text, _
                   arrCodiGrupos(cbxGrupo.ListIndex - 1), _
                   arrCodiParame(cbxParame.ListIndex - 1)) Then
        Call suTraeDatosProducto
        cbxGrupo.ListIndex = 0
    Else
        MsgBox "No se ha podido agregar la relacion"
    End If
    
    cbxGrupo.SetFocus
End Sub

Private Sub cmdCopiarProducto_Click()
    Dim sNuevoCodigo As String
    Dim i As Long
    
    If MsgBox("�Esta seguro de copiar la el producto: " & Me.txtDescProducto.Text & "?", vbYesNo) = vbYes Then
        
        sNuevoCodigo = UCase(InputBox("Ingrese el codigo del nuevo producto" & vbCrLf & "S�lo se tomaran los primeros 10 caracteres", "Ingreso de Datos"))
        sNuevoCodigo = Left(sNuevoCodigo, 10)
        
        If sNuevoCodigo <> "" Then
            ' revisando de que el codigo no se repita
            For i = 1 To grdProductos.Rows - 1
                If sNuevoCodigo = grdProductos.TextMatrix(i, 0) Then
                    MsgBox "Este c�digo ya esta siendo utilizado"
                    Exit Sub
                End If
            Next i
            
            If CopiaProd(txtCodiProducto.Text, _
                         sNuevoCodigo) Then
                Call suInicializaProducto
                Call suLlenaProductos
                Call suInicializaForm
                Call suTraeDatosProducto
                ' siempre se recargan los productos
                Call gsuCargaProductos
            End If
        Else
            MsgBox "Debe ingresar el nuevo c�digo del producto"
        End If
    End If

End Sub

Private Sub cmdEliminaEquipo_Click()
    Dim iCant As Long

    With grdEquipos
        'Revisando que no hayan facilidades asociadas
    
        iCant = GetProdFaci(txtCodiProducto.Text, _
                            .TextMatrix(.Row, 3))
        If iCant <= 0 Then
            If MsgBox("�Esta seguro de eliminar el equipo " & cbxEquipo.Text, vbYesNo) = vbYes Then
                If DelProdEqui(txtCodiProducto.Text, _
                               .TextMatrix(.Row, 3)) Then
                    Call cmdLimpiarEquipo_Click
                    Call suTraeDatosProducto
                    cbxEquipo.SetFocus
                Else
                    MsgBox "No se pudo eliminar el equipo"
                End If
            End If
        Else
            MsgBox "No se puede eliminar el equipo, ya que existen facilidades asociadas"
        End If
    End With
    
    
End Sub

Private Sub cmdEliminaLocalidad_Click()
    Dim i As Long, iCant As Long

    iCant = 0
    For i = 1 To grdLocalidades.Rows - 1
        iCant = iCant + grdLocalidades.TextMatrix(i, 2)
    Next i

    If iCant > 0 Then
        If MsgBox("�Esta seguro de eliminar las " & Trim(Str(iCant)) & " localidades seleccionadas?", vbYesNo) = vbYes Then
            For i = 1 To grdLocalidades.Rows - 1
                If grdLocalidades.TextMatrix(i, 2) = 1 Then
                    Call DelProdLoca(txtCodiProducto.Text, _
                                     grdLocalidades.TextMatrix(i, 1))
                End If
            Next i
            
            Call suTraeDatosProducto
            cbxLocalidad.ListIndex = 0
        End If
    Else
        MsgBox "No hay nada seleccionado para eliminar"
    End If
    cbxLocalidad.SetFocus
End Sub

Private Sub cmdEliminarFacilidad_Click()
    Dim iCant As Long, i As Long
    
    iCant = 0
    With grdFacilidades
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 2) = "1" Then
                iCant = iCant + 1
            End If
        Next i
    End With
    
    If iCant > 0 Then
        If MsgBox("�Esta seguro de eliminar las " & Trim(Str(iCant)) & " Facilidades seleccionadas", vbYesNo) = vbYes Then
            With grdFacilidades
                For i = 1 To .Rows - 1
                    If .TextMatrix(i, 2) = "1" Then
                        Call DelProdSuple(txtCodiProducto.Text, _
                                          grdEquipos.TextMatrix(grdEquipos.Row, 3), _
                                          .TextMatrix(i, 1))
                    End If
                Next i
                Call suTraeFaciEquiProd
            End With
        End If
    Else
        MsgBox "No hay nada seleccionado para eliminar"
    End If
    cbxFacilidad.SetFocus
End Sub

Private Sub cmdEliminarRelacion_Click()
    Dim i As Long, iCant As Long

    iCant = 0
    For i = 1 To grdRelaciones.Rows - 1
        iCant = iCant + grdRelaciones.TextMatrix(i, 4)
    Next i
    
    If iCant > 0 Then
        If MsgBox("�Esta seguro de eliminar las " & Trim(Str(iCant)) & " relaciones seleccionadas?", vbYesNo) = vbYes Then
            For i = 1 To grdRelaciones.Rows - 1
                If grdRelaciones.TextMatrix(i, 4) Then
                    Call DelProdRela(txtCodiProducto.Text, _
                                     grdRelaciones.TextMatrix(i, 2), _
                                     grdRelaciones.TextMatrix(i, 3))
                End If
            Next i
            
            Call suTraeDatosProducto
            cbxGrupo.ListIndex = 0
        End If
    Else
        MsgBox "No hay nada seleccionado para eliminar"
    End If
    
    cbxGrupo.SetFocus
End Sub

Private Sub cmdLimpiar_Click()
    Call suInicializaProducto
    Call suTraeDatosProducto
    
    txtCodiProducto.SetFocus
    grdProductos.Row = 0
End Sub

Private Sub cmdLimpiarEquipo_Click()
    cbxEquipo.ListIndex = 0
    txtCantEquipo.Text = "1"
    txtCodiExclusion = ""
    cbxEquipo.Enabled = True
    cmdAgregaEquipo.Caption = "Agregar"
    cbxEquipo.SetFocus
    grdEquipos.Row = 0
End Sub

Private Sub cmdModificarProducto_Click()
    If fnValidaDatos Then
        If ModProducto(Me.txtCodiProducto.Text, _
                       Me.txtDescProducto.Text, _
                       IIf(Me.chkSinFechaFin.Value = 1, "NADA", Format(dtFechaFin.Value, "dd-mm-yyyy"))) Then
            Call suInicializaProducto
            Call suLlenaProductos
            Call suInicializaForm
            Call suTraeDatosProducto
            ' siempre se recargan los productos
            Call gsuCargaProductos
        Else
            MsgBox "No se pudo actualizar el producto"
        End If
    End If
    If txtCodiProducto.Enabled Then
        txtCodiProducto.SetFocus
    End If

End Sub

Private Sub cmdNuevoProducto_Click()
    If fnValidaDatos Then
        If InsProducto(txtCodiProducto.Text, _
                       txtDescProducto.Text, _
                       IIf(Me.chkSinFechaFin.Value = 1, "NADA", Format(dtFechaFin.Value, "dd-mm-yyyy"))) Then
            Call suLlenaProductos
            Call suInicializaProducto
            Call gsuCargaProductos
        Else
            MsgBox "No se ha podido ingresar el producto"
        End If
    End If
    
    txtCodiProducto.SetFocus
End Sub

Private Function fnValidaDatos() As Boolean
    
    fnValidaDatos = True
    
    If txtCodiProducto.Text = "" Then
        MsgBox "Debe ingresar el codigo del producto"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtDescProducto.Text = "" Then
        MsgBox "Debe ingresar la Descripci�n del producto"
        fnValidaDatos = False
        Exit Function
    End If

'    If chkSinFechaFin.Value = 0 Then
        ' si se encuentra entre 0 y -0.99... significa que es el mismo d�a
'        If dtFechaFin.Value - Now <= -1 Then
'            MsgBox "La Fecha de Fin no puede ser menor a hoy d�a"
'            fnValidaDatos = False
'            Exit Function
'        End If
'    End If
    
End Function

Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub Command1_Click()

End Sub

Private Sub cmdVolverFacilidad_Click()
    fraEquipos.Enabled = True
    fraProductos.Enabled = True
    fraRelacion.Enabled = True
    fraLocalidades.Enabled = True

    fraFacilidades.Visible = False

End Sub

Private Sub Form_Activate()
    Call suInicializaForm
    Call suInicializaProducto
    Call suLlenaProductos
    Call suInicializaRelacion
    Call suInicializaLocalidades
    Call suInicializaEquipos
    Call suInicializaFacilidades
    
    Dim controlesForm As Control
    For Each controlesForm In Controls
      If (TypeOf controlesForm Is CommandButton) Then
          If controlesForm.Tag <> "" Then
              Call suEnable_Ctl1(Me.Tag, controlesForm)
          End If
      End If
    Next
    
End Sub

Private Sub suInicializaFacilidades()
    With grdFacilidades
        .Rows = 2
        .Cols = 3
        
        .TextMatrix(0, 0) = "Facilidad"
        .TextMatrix(0, 1) = "CodiFacilida"
        .TextMatrix(0, 2) = "Marca"
        
        .ColWidth(0) = 6000
        .ColWidth(1) = 0
        .ColWidth(2) = 0
        
        .FixedCols = 0
        .FixedRows = 1
    End With
End Sub
Private Sub suLlenaFacilidades()
    Dim i As Long
    
    cbxFacilidad.Clear
    cbxFacilidad.AddItem "SELECCIONE"
    For i = 0 To UBound(arrFacilidades)
        cbxFacilidad.AddItem arrFacilidades(i)
    Next i
    cbxFacilidad.ListIndex = 0
End Sub

Private Sub suInicializaEquipos()
    With grdEquipos
        .Rows = 2
        .Cols = 5
    
        .TextMatrix(0, 0) = "Equipo"
        .TextMatrix(0, 1) = "Cantidad"
        .TextMatrix(0, 2) = "Exclusion"
        .TextMatrix(0, 3) = "CodiEquipo"
        .TextMatrix(0, 4) = "Marca"
        
        .ColWidth(0) = 1000
        .ColWidth(1) = 1000
        .ColWidth(2) = 1000
        .ColWidth(3) = 0
        .ColWidth(4) = 0
        
        .FixedCols = 0
        .FixedRows = 1
    End With
    
    txtCantEquipo.Text = "1"
    txtCodiExclusion.Text = ""
    
End Sub

Private Sub suInicializaLocalidades()
    With grdLocalidades
        .Rows = 2
        .Cols = 3
    
        .TextMatrix(0, 0) = "Localidad"
        .TextMatrix(0, 1) = "CodiLocalida"
        .TextMatrix(0, 2) = "Marca"
    
        .ColWidth(0) = 2500
        .ColWidth(1) = 0
        .ColWidth(2) = 0
    
        .FixedCols = 0
        .FixedRows = 1
    End With
End Sub

Private Sub suInicializaRelacion()
    With grdRelaciones
        .Rows = 2
        .Cols = 5
    
        .TextMatrix(0, 0) = "GRUPO"
        .TextMatrix(0, 1) = "PARAMETRO"
        .TextMatrix(0, 2) = "CODI_GRUPO"
        .TextMatrix(0, 3) = "CODI_ELEM"
    
        .ColWidth(0) = 2800
        .ColWidth(1) = 2300
        .ColWidth(2) = 0
        .ColWidth(3) = 0
        .ColWidth(4) = 0
    
        .FixedCols = 0
        .FixedRows = 1
    End With
End Sub

Private Sub suLlenaProductos()
    Dim sCodiProducto As String
    Dim sDescProducto As String
    Dim sFechaCreacion As String, sFechaFin As String
    Dim i As Integer, iProductoOk As Integer, ifiltro As Integer

    If Me.chkProductosVigentes.Value = 1 And Me.chkProductosNoVigentes.Value = 1 Then
        ifiltro = 0
    End If
    If Me.chkProductosVigentes.Value = 1 And Me.chkProductosNoVigentes.Value = 0 Then
        ifiltro = 1
    End If
    If Me.chkProductosVigentes.Value = 0 And Me.chkProductosNoVigentes.Value = 1 Then
        ifiltro = -1
    End If
    If Me.chkProductosVigentes.Value = 0 And Me.chkProductosNoVigentes.Value = 0 Then
        ifiltro = -2
    End If
    
    grdProductos.Rows = 1
    If GetProductos(ifiltro) > 0 Then
        i = 1
        Do While GetProductosRead(sCodiProducto, sDescProducto, sFechaCreacion, sFechaFin, iProductoOk)
            With grdProductos
                .Rows = grdProductos.Rows + 1
                .TextMatrix(i, 0) = sCodiProducto
                .TextMatrix(i, 1) = sDescProducto
                .TextMatrix(i, 2) = sFechaCreacion
                .TextMatrix(i, 3) = sFechaFin
                .Row = i
                If iProductoOk = 1 Then
                    ' Producto OK
                    .Col = 0
                    .CellBackColor = vbWhite
                    .CellForeColor = vbBlack
                    .Col = 1
                    .CellBackColor = vbWhite
                    .CellForeColor = vbBlack
                    .Col = 2
                    .CellBackColor = vbWhite
                    .CellForeColor = vbBlack
                    .Col = 3
                    .CellBackColor = vbWhite
                    .CellForeColor = vbBlack
                Else
                    ' Producto con Error
                    .Col = 0
                    .CellBackColor = vbRed
                    .CellForeColor = vbWhite
                    .Col = 1
                    .CellBackColor = vbRed
                    .CellForeColor = vbWhite
                    .Col = 2
                    .CellBackColor = vbRed
                    .CellForeColor = vbWhite
                    .Col = 3
                    .CellBackColor = vbRed
                    .CellForeColor = vbWhite
                End If
                i = i + 1
            End With
        Loop
    End If
    Call cmdLimpiar_Click
End Sub

Private Sub suInicializaProducto()
    
    fraRelacion.Enabled = False
    fraLocalidades.Enabled = False
    fraEquipos.Enabled = False
    
    txtCodiProducto.Text = ""
    txtCodiProducto.Enabled = True
    txtDescProducto.Text = ""
    chkSinFechaFin.Value = 1
    chkSinFechaFin_Click
    cmdModificarProducto.Enabled = False
    cmdCopiarProducto.Enabled = False
    
    Call suEnable_Ctl1(Me.Tag, cmdNuevoProducto)
    
    txtCodiProducto.Enabled = True
    dtFechaFin.Value = Date
End Sub


Private Sub suInicializaForm()
    
    With grdProductos
        .Cols = 4
        .TextMatrix(0, 0) = "C�digo Producto"
        .TextMatrix(0, 1) = "Descripci�n Producto"
        .TextMatrix(0, 2) = "Fecha Creaci�n"
        .TextMatrix(0, 3) = "Fecha Fin"
    
        .ColWidth(0) = 1800
        .ColWidth(1) = 7500
        .ColWidth(2) = 1700
        .ColWidth(3) = 1700
    
        .FixedCols = 0
        .FixedRows = 1
    End With
    
    chkProductosNoVigentes.Value = 1
    chkProductosVigentes.Value = 1
    
    Call suLlenaGrupos
    Call suLlenaLocalidades
    Call suLlenaEquipos
    Call suLlenaFacilidades
    
End Sub

Private Sub suLlenaEquipos()
    Dim i As Integer, Total As Integer, Ret As Integer
    Dim sDescEquipo As String, sCodiEquipo As String
    
    cbxEquipo.Clear
    cbxEquipo.AddItem "SELECCIONE"
    
    Total = GetEquipos()
    For i = 0 To Total
        Ret = GetEquiposRead(sDescEquipo, sCodiEquipo)
        cbxEquipo.AddItem sCodiEquipo + " - " + sDescEquipo
    Next i
    cbxEquipo.ListIndex = 0
    
End Sub

Private Sub suLlenaLocalidades()
    Dim i As Integer

    cbxLocalidad.Clear
    cbxLocalidad.AddItem "SELECCIONE"
    For i = 0 To UBound(arrLocalidades)
        cbxLocalidad.AddItem arrLocalidades(i)
    Next i
    
    cbxLocalidad.ListIndex = 0
End Sub

Private Sub suLlenaGrupos()
    Dim i As Integer
    
    cbxGrupo.Clear
    cbxGrupo.AddItem "SELECCIONE"
    For i = 0 To UBound(arrGrupos)
        cbxGrupo.AddItem arrGrupos(i)
    Next i
    cbxGrupo.ListIndex = 0
End Sub

Private Sub grdEquipos_Click()
'    Call suMarcaFilaGrilla(grdEquipos, 4)
    Dim i As Integer

    If grdEquipos.Row > 0 Then
        With grdEquipos
            txtCantEquipo.Text = .TextMatrix(.Row, 1)
            txtCodiExclusion.Text = .TextMatrix(.Row, 2)
        
            For i = 0 To UBound(arrCodiEquipos)
                If arrCodiEquipos(i) = .TextMatrix(.Row, 3) Then
                    cbxEquipo.ListIndex = i + 1
                    Exit For
                End If
            Next i
            
            cbxEquipo.Enabled = False
        End With
    
        cmdAgregaEquipo.Caption = "Modificar"
    End If
End Sub

Private Sub grdEquipos_DblClick()
    fraEquipos.Enabled = False
    fraProductos.Enabled = False
    fraRelacion.Enabled = False
    fraLocalidades.Enabled = False

    fraFacilidades.Left = 3600
    fraFacilidades.Top = 1900
    fraFacilidades.Visible = True
    Call suTraeFaciEquiProd
    cbxFacilidad.SetFocus
End Sub

Private Sub suTraeFaciEquiProd()
    Dim iCant As Long
    Dim sCodiFacilidad As String, sDescFacilidad As String
    Dim i As Long
    
    iCant = GetProdFaci(txtCodiProducto.Text, _
                        grdEquipos.TextMatrix(grdEquipos.Row, 3))
    
    If iCant > 0 Then
        With grdFacilidades
            .Rows = 1
            i = 1
            Do While GetProdFaciRead(sCodiFacilidad, sDescFacilidad)
                .Rows = .Rows + 1
                .TextMatrix(i, 0) = " " & sCodiFacilidad & " - " & sDescFacilidad
                .TextMatrix(i, 1) = sCodiFacilidad
                .TextMatrix(i, 2) = "0"
                i = i + 1
            Loop
        End With
    Else
        grdFacilidades.Rows = 1
    End If


End Sub

Private Sub grdFacilidades_Click()
    Call gsuMarcaFilaGrilla(grdFacilidades, 2)
End Sub

Private Sub grdLocalidades_Click()
    Call gsuMarcaFilaGrilla(grdLocalidades, 2)
End Sub

Private Sub grdProductos_Click()

    cmdNuevoProducto.Enabled = False
    Call suEnable_Ctl1(Me.Tag, cmdModificarProducto)
    Call suEnable_Ctl1(Me.Tag, cmdCopiarProducto)
    
    With grdProductos
        If .TextMatrix(.Row, 0) <> "" Then
            txtCodiProducto.Text = .TextMatrix(.Row, 0)
            txtDescProducto.Text = .TextMatrix(.Row, 1)
            If .TextMatrix(.Row, 3) <> "" Then
                dtFechaFin.Value = .TextMatrix(.Row, 3)
                chkSinFechaFin.Value = 0
            Else
                chkSinFechaFin.Value = 1
            End If
            Call chkSinFechaFin_Click
        
            Call suTraeDatosProducto
            
            txtDescProducto.SetFocus
        
        Else
            grdRelaciones.Rows = 1
            grdLocalidades.Rows = 1
            grdEquipos.Rows = 1
        End If
    End With
    
    txtCodiProducto.Enabled = False
    
End Sub

Private Sub suTraeDatosProducto()
    Dim i As Integer
    Dim sDescGrupo As String, sDescParame As String
    Dim sCodiGrupo As String, sCodiElem As String
    Dim sDescModelo As String, sCantEquipo As String, sCodiExclusion As String
    Dim sCodiEquipo As String

    fraRelacion.Enabled = True
    fraLocalidades.Enabled = True
    fraEquipos.Enabled = True
    
    grdRelaciones.Rows = 1
    grdLocalidades.Rows = 1
    grdEquipos.Rows = 1
    
    If GetProdRela(txtCodiProducto.Text) > 0 Then
        i = 1
        With grdRelaciones
            .Rows = 1
            Do While GetProdRelaRead(sDescGrupo, sDescParame, sCodiGrupo, sCodiElem)
                    .Rows = .Rows + 1
                    .TextMatrix(i, 0) = " " & Trim(sCodiGrupo) & "  -  " & sDescGrupo
                    .TextMatrix(i, 1) = " " & Trim(sCodiElem) & "  -  " & sDescParame
                    .TextMatrix(i, 2) = sCodiGrupo
                    .TextMatrix(i, 3) = sCodiElem
                    .TextMatrix(i, 4) = 0
                    
                    i = i + 1
            Loop
        End With
    End If
    
    If GetProdLoca(txtCodiProducto.Text) > 0 Then
        i = 1
        With grdLocalidades
            .Rows = 1
            Do While GetProdLocaRead(sDescGrupo, sCodiGrupo)
                .Rows = .Rows + 1
                .TextMatrix(i, 0) = " " & sCodiGrupo & " - " & sDescGrupo
                .TextMatrix(i, 1) = sCodiGrupo
                .TextMatrix(i, 2) = 0
                i = i + 1
            Loop
        End With
    End If
    
    If GetProdEquip(txtCodiProducto.Text) > 0 Then
        i = 1
        With grdEquipos
            .Rows = 1
            Do While GetProdEquipRead(sDescModelo, sCantEquipo, sCodiExclusion, sCodiEquipo)
                .Rows = .Rows + 1
                .TextMatrix(i, 0) = sDescModelo
                .TextMatrix(i, 1) = sCantEquipo
                .TextMatrix(i, 2) = sCodiExclusion
                .TextMatrix(i, 3) = sCodiEquipo
                .TextMatrix(i, 4) = "0"
                i = i + 1
            Loop
        End With
    End If
End Sub

Private Sub grdRelaciones_Click()
    Call gsuMarcaFilaGrilla(grdRelaciones, 4)
End Sub

Private Sub txtCantEquipo_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtCodiExclusion_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))
End Sub

Private Sub txtCodiProducto_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))
End Sub

Private Sub txtDescProducto_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))
End Sub
