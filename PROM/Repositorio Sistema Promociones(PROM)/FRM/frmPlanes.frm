VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmPlanes 
   Caption         =   "Mantenedor de Planes"
   ClientHeight    =   10950
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   18960
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   ScaleHeight     =   10950
   ScaleWidth      =   18960
   Tag             =   "FRMPLANES"
   WindowState     =   2  'Maximized
   Begin VB.Frame fraProducto 
      Caption         =   "Producto Asociado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3495
      Left            =   8640
      TabIndex        =   15
      Top             =   7680
      Width           =   7695
      Begin VB.ComboBox cbxProdObligatorio 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1320
         Width           =   1215
      End
      Begin VB.CommandButton cmdActPlanProd 
         Caption         =   "Asociar Producto"
         Height          =   375
         Left            =   1800
         TabIndex        =   9
         Tag             =   "PR_ASOCIAR"
         Top             =   2880
         Width           =   2655
      End
      Begin VB.TextBox txtProdCodiExclu 
         Height          =   285
         Left            =   240
         TabIndex        =   8
         Text            =   "Text1"
         Top             =   2160
         Width           =   2415
      End
      Begin VB.ComboBox cbxProductos 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   480
         Width           =   6015
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo Exclusi�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   1920
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Obligatorio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame fraFacilidades 
      Caption         =   "Facilidades Asociadas al Plan"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3495
      Left            =   120
      TabIndex        =   12
      Top             =   7680
      Width           =   7935
      Begin VB.CommandButton cmdLimpiarFacilidad 
         Caption         =   "Limpiar"
         Height          =   255
         Left            =   2400
         TabIndex        =   22
         Top             =   840
         Width           =   1815
      End
      Begin VB.ComboBox cbxPlanFaciProgCent 
         Height          =   315
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1350
         Width           =   1095
      End
      Begin VB.TextBox txtPlanFaciCodiExcl 
         Height          =   285
         Left            =   2040
         MaxLength       =   13
         TabIndex        =   4
         Text            =   "Text1"
         Top             =   1400
         Width           =   1455
      End
      Begin VB.TextBox txtPlanCantFaci 
         Height          =   285
         Left            =   240
         MaxLength       =   13
         TabIndex        =   3
         Text            =   "1"
         Top             =   1400
         Width           =   1455
      End
      Begin VB.ComboBox cbxPlanFaci 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   480
         Width           =   6375
      End
      Begin VB.CommandButton cmdAgregarFacilidad 
         Caption         =   "Agregar"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Tag             =   "FA_AGREGAR"
         Top             =   840
         Width           =   1815
      End
      Begin VB.CommandButton cmdEliminarFacilidad 
         Caption         =   "Eliminar"
         Height          =   255
         Left            =   4560
         TabIndex        =   2
         Tag             =   "FA_ELIMINAR"
         Top             =   840
         Width           =   1815
      End
      Begin MSFlexGridLib.MSFlexGrid grdFacilidades 
         Height          =   1695
         Left            =   120
         TabIndex        =   13
         Top             =   1680
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   2990
         _Version        =   393216
         SelectionMode   =   1
      End
      Begin VB.Label Label18 
         Caption         =   "Se Programa en la Central"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   4200
         TabIndex        =   21
         Top             =   1155
         Width           =   2415
      End
      Begin VB.Label Label17 
         Caption         =   "Codigo de Exclusi�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2040
         TabIndex        =   20
         Top             =   1155
         Width           =   1935
      End
      Begin VB.Label Label16 
         Caption         =   "Cantidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   1150
         Width           =   1455
      End
      Begin VB.Label Label10 
         Caption         =   "Facilidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame fraPlanes 
      Caption         =   "Planes Existentes (En Rojo los Planes no Correctos)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6495
      Left            =   120
      TabIndex        =   10
      Top             =   0
      Width           =   16215
      Begin VB.Frame fraContainer 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   2500
         Left            =   120
         TabIndex        =   26
         Top             =   3960
         Width           =   16000
         Begin VB.ComboBox cbxCargoBoleta 
            Height          =   315
            ItemData        =   "frmPlanes.frx":0000
            Left            =   9120
            List            =   "frmPlanes.frx":0002
            Style           =   2  'Dropdown List
            TabIndex        =   82
            Top             =   840
            Width           =   1095
         End
         Begin VB.ComboBox cbx4g 
            Height          =   315
            Left            =   8280
            TabIndex        =   80
            Text            =   "Combo1"
            Top             =   1440
            Width           =   735
         End
         Begin VB.TextBox txCantLineas 
            Height          =   285
            Left            =   14040
            TabIndex        =   79
            Top             =   1440
            Width           =   1575
         End
         Begin VB.TextBox txPaqPortal 
            Height          =   285
            Left            =   12480
            TabIndex        =   78
            Top             =   1440
            Width           =   1335
         End
         Begin VB.ComboBox cbxPortal 
            Height          =   315
            Left            =   9240
            TabIndex        =   55
            Text            =   "Combo1"
            Top             =   1440
            Width           =   1095
         End
         Begin VB.ComboBox cbxAgrupador 
            Height          =   315
            Left            =   0
            Style           =   2  'Dropdown List
            TabIndex        =   54
            Top             =   240
            Width           =   1455
         End
         Begin VB.TextBox txtRut_empresa 
            Height          =   285
            Left            =   10560
            TabIndex        =   53
            Top             =   1440
            Width           =   1815
         End
         Begin VB.ComboBox cbxUso 
            Height          =   315
            ItemData        =   "frmPlanes.frx":0004
            Left            =   5160
            List            =   "frmPlanes.frx":0006
            Style           =   2  'Dropdown List
            TabIndex        =   52
            Top             =   1440
            Width           =   1455
         End
         Begin VB.CommandButton cmdBuscaPlan 
            Caption         =   "Buscar Plan"
            Height          =   375
            Left            =   8160
            TabIndex        =   51
            Top             =   2040
            Width           =   1815
         End
         Begin VB.CommandButton cmdCopiarPlan 
            Caption         =   "Copiar Plan"
            Enabled         =   0   'False
            Height          =   375
            Left            =   6120
            TabIndex        =   50
            Tag             =   "PL_COPIAR"
            Top             =   2040
            Width           =   1815
         End
         Begin VB.ComboBox cbxTipoSaldo 
            Height          =   315
            Left            =   1800
            Style           =   2  'Dropdown List
            TabIndex        =   49
            Top             =   1440
            Width           =   1335
         End
         Begin VB.TextBox txtValorCargaInicial 
            Height          =   285
            Left            =   3360
            MaxLength       =   13
            TabIndex        =   48
            Text            =   "Text1"
            Top             =   1440
            Width           =   1455
         End
         Begin VB.TextBox txtValorPlan 
            Height          =   285
            Left            =   0
            MaxLength       =   13
            TabIndex        =   47
            Text            =   "Text1"
            Top             =   1440
            Width           =   1455
         End
         Begin VB.ComboBox cbxCargaIniProp 
            Height          =   315
            ItemData        =   "frmPlanes.frx":0008
            Left            =   11400
            List            =   "frmPlanes.frx":000A
            Style           =   2  'Dropdown List
            TabIndex        =   46
            Top             =   840
            Width           =   1095
         End
         Begin VB.ComboBox cbxFacilidad 
            Height          =   315
            Left            =   11160
            Style           =   2  'Dropdown List
            TabIndex        =   45
            Top             =   240
            Width           =   4575
         End
         Begin VB.ComboBox cbxComputosCentral 
            Height          =   315
            Left            =   0
            Style           =   2  'Dropdown List
            TabIndex        =   44
            Top             =   840
            Width           =   1095
         End
         Begin VB.CheckBox chkSinFechaFin 
            Caption         =   "Sin Fecha"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   9960
            TabIndex        =   43
            Top             =   240
            Value           =   1  'Checked
            Width           =   1575
         End
         Begin VB.TextBox txtDescPlan 
            Height          =   285
            Left            =   3840
            MaxLength       =   100
            TabIndex        =   42
            Text            =   "Text1"
            Top             =   240
            Width           =   4215
         End
         Begin VB.TextBox txtCodiPlan 
            Height          =   285
            Left            =   2160
            MaxLength       =   10
            TabIndex        =   41
            Text            =   "Text1"
            Top             =   240
            Width           =   1215
         End
         Begin VB.CommandButton cmdVolver 
            Caption         =   "Volver"
            Height          =   375
            Left            =   10200
            TabIndex        =   40
            Top             =   2040
            Width           =   1815
         End
         Begin VB.CommandButton cmdModificaPlan 
            Caption         =   "Modificar Plan"
            Height          =   375
            Left            =   4080
            TabIndex        =   39
            Tag             =   "PL_MODIFICAR"
            Top             =   2040
            Width           =   1815
         End
         Begin VB.CommandButton cmdNuevoPlan 
            Caption         =   "Nuevo Plan"
            Height          =   375
            Left            =   2040
            TabIndex        =   38
            Tag             =   "PL_NUEVO"
            Top             =   2040
            Width           =   1815
         End
         Begin VB.CommandButton cmdLimpiar 
            Caption         =   "Limpiar"
            Height          =   375
            Left            =   0
            TabIndex        =   37
            Top             =   2040
            Width           =   1815
         End
         Begin VB.TextBox txtCentral 
            Height          =   285
            Left            =   5880
            MaxLength       =   50
            TabIndex        =   31
            Text            =   "123456789012345678901234567890"
            Top             =   840
            Width           =   3015
         End
         Begin VB.ComboBox cbxRenta 
            Height          =   315
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   30
            Top             =   840
            Width           =   3255
         End
         Begin VB.ComboBox cbxAcumula 
            Height          =   315
            Left            =   6720
            Style           =   2  'Dropdown List
            TabIndex        =   27
            Top             =   1440
            Width           =   1215
         End
         Begin MSComCtl2.DTPicker dtFechaFin 
            Height          =   255
            Left            =   8280
            TabIndex        =   56
            Top             =   240
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   450
            _Version        =   393216
            Format          =   90046465
            CurrentDate     =   38915
         End
         Begin VB.TextBox txDescRentaSms 
            Enabled         =   0   'False
            Height          =   285
            Left            =   15120
            TabIndex        =   34
            Top             =   2160
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txCptoSMS 
            Height          =   285
            Left            =   15120
            TabIndex        =   28
            Top             =   2160
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.TextBox txCptoDatos 
            Height          =   285
            Left            =   15120
            TabIndex        =   29
            Top             =   2160
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txCodiSMS 
            Height          =   285
            Left            =   15120
            TabIndex        =   32
            Top             =   2160
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txCodiDatos 
            Height          =   285
            Left            =   15120
            TabIndex        =   33
            Top             =   2160
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.TextBox txDescRentaDatos 
            Enabled         =   0   'False
            Height          =   285
            Left            =   15120
            TabIndex        =   35
            Top             =   2160
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txRutTercera 
            Height          =   285
            Left            =   15120
            TabIndex        =   36
            Top             =   2160
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label Label9 
            Caption         =   "� Compra cargo boleta?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   9120
            TabIndex        =   83
            Top             =   600
            Width           =   2175
         End
         Begin VB.Label Label29 
            Caption         =   "4G"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   8400
            TabIndex        =   81
            Top             =   1200
            Width           =   495
         End
         Begin VB.Label Label28 
            Caption         =   "Cantidad de l�neas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00008000&
            Height          =   255
            Left            =   14040
            TabIndex        =   77
            Top             =   1200
            Width           =   1815
         End
         Begin VB.Label Label27 
            Caption         =   "Paquete Portal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00008000&
            Height          =   255
            Left            =   12480
            TabIndex        =   76
            Top             =   1200
            Width           =   1335
         End
         Begin VB.Label Label26 
            Caption         =   "Agrupador"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   0
            TabIndex        =   75
            Top             =   0
            Width           =   1215
         End
         Begin VB.Label Label21 
            Caption         =   "Plan Portal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00008000&
            Height          =   255
            Left            =   9240
            TabIndex        =   74
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Label22 
            Caption         =   "�Acumula Minutos?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   6600
            TabIndex        =   73
            Top             =   1200
            Width           =   1815
         End
         Begin VB.Label Label20 
            Caption         =   "Rut Empresa (Portal)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00008000&
            Height          =   255
            Left            =   10560
            TabIndex        =   72
            Top             =   1200
            Width           =   2055
         End
         Begin VB.Label Label19 
            Caption         =   "Uso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   5160
            TabIndex        =   71
            Top             =   1200
            Width           =   1335
         End
         Begin VB.Label Label15 
            Caption         =   "Tipo de Saldo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   1800
            TabIndex        =   70
            Top             =   1200
            Width           =   1335
         End
         Begin VB.Label Label14 
            Caption         =   "Valor Carga Inicial"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   3360
            TabIndex        =   69
            Top             =   1200
            Width           =   1815
         End
         Begin VB.Label Label13 
            Caption         =   "Valor del Plan"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   0
            TabIndex        =   68
            Top             =   1200
            Width           =   1455
         End
         Begin VB.Label Label12 
            Caption         =   "� Carga Inicial Proporcional al Mes ?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   11400
            TabIndex        =   67
            Top             =   600
            Width           =   3255
         End
         Begin VB.Label Label7 
            Caption         =   "Facilidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   11160
            TabIndex        =   66
            Top             =   0
            Width           =   1215
         End
         Begin VB.Label lb_multimedia 
            Caption         =   "�Computos en Central?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   0
            TabIndex        =   65
            Top             =   600
            Width           =   2175
         End
         Begin VB.Label Label6 
            Caption         =   "Fecha T�rmino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   8280
            TabIndex        =   64
            Top             =   0
            Width           =   1575
         End
         Begin VB.Label Label5 
            Caption         =   "Descripci�n del Plan"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   3840
            TabIndex        =   63
            Top             =   0
            Width           =   1935
         End
         Begin VB.Label Label4 
            Caption         =   "C�digo del Plan"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   2160
            TabIndex        =   62
            Top             =   0
            Width           =   1455
         End
         Begin VB.Label Label11 
            Caption         =   "Central"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   5880
            TabIndex        =   60
            Top             =   600
            Width           =   1455
         End
         Begin VB.Label Label8 
            Caption         =   "Renta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   2160
            TabIndex        =   59
            Top             =   600
            Width           =   1215
         End
         Begin VB.Label Label24 
            Caption         =   "Renta Datos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00008000&
            Height          =   255
            Left            =   13440
            TabIndex        =   61
            Top             =   2160
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label Label25 
            Caption         =   "Rut Empresa Tercera"
            Height          =   255
            Left            =   13440
            TabIndex        =   58
            Top             =   2160
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.Label Label23 
            Caption         =   "Renta SMS"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00008000&
            Height          =   255
            Left            =   13440
            TabIndex        =   57
            Top             =   2160
            Visible         =   0   'False
            Width           =   1095
         End
      End
      Begin VB.CheckBox chkPlanesGTDMovil 
         Caption         =   "Planes GTD Movil"
         Height          =   255
         Left            =   8760
         TabIndex        =   25
         Top             =   0
         Width           =   1815
      End
      Begin VB.CheckBox chkPlanesNoVigentes 
         Caption         =   "Planes No Vigentes"
         Height          =   255
         Left            =   6720
         TabIndex        =   24
         Top             =   0
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin VB.CheckBox chkPlanesVigentes 
         Caption         =   "Planes Vigentes"
         Height          =   255
         Left            =   4800
         TabIndex        =   23
         Top             =   0
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin MSFlexGridLib.MSFlexGrid grdPlanes 
         Height          =   3495
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   15975
         _ExtentX        =   28178
         _ExtentY        =   6165
         _Version        =   393216
         SelectionMode   =   1
      End
   End
End
Attribute VB_Name = "frmPlanes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cbxFacilidad_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Long
        
    If KeyCode = vbKeyF5 Then
        frmBuscador.txtFiltro.Text = "F"
        frmBuscador.Show vbModal
        
        For i = 0 To UBound(arrCodiFacilidades)
            If arrCodiFacilidades(i) = frmBuscador.txtCodigo.Text Then
                cbxFacilidad.ListIndex = i + 1
             End If
        Next i
    End If

End Sub

Private Sub cbxPlanFaci_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Long
        
    If KeyCode = vbKeyF5 Then
        frmBuscador.txtFiltro.Text = "F"
        frmBuscador.Show vbModal
        
        For i = 0 To UBound(arrCodiFacilidades)
            If arrCodiFacilidades(i) = frmBuscador.txtCodigo.Text Then
                cbxPlanFaci.ListIndex = i + 1
             End If
        Next i
    End If
End Sub

Private Sub cbxRenta_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Long
        
    If KeyCode = vbKeyF5 Then
        frmBuscador.txtFiltro.Text = "R"
        frmBuscador.Show vbModal
        
        For i = 0 To UBound(arrCodiRentas)
            If arrCodiRentas(i) = frmBuscador.txtCodigo.Text Then
                cbxRenta.ListIndex = i + 1
             End If
        Next i
    End If
End Sub

Private Sub chkPlanesGTDMovil_Click()
    Call suLlenaPlanes
End Sub

Private Sub chkPlanesNoVigentes_Click()
    Call suLlenaPlanes
End Sub

Private Sub chkPlanesVigentes_Click()
    Call suLlenaPlanes
End Sub

Private Sub chkSinFechaFin_Click()
    If chkSinFechaFin.Value = vbChecked Then
        dtFechaFin.Enabled = False
    End If
    If chkSinFechaFin.Value = vbUnchecked Then
        dtFechaFin.Enabled = True
    End If
End Sub

Private Sub cmdActPlanProd_Click()
    If cbxProductos.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el producto asociado"
        Exit Sub
    End If
    
    If ModPlanProd(txtCodiPlan.Text, _
                       arrCodiProductos(cbxProductos.ListIndex - 1), _
                       Left(cbxProdObligatorio.Text, 1), _
                       txtProdCodiExclu.Text) Then
        MsgBox "Producto Asociado", vbInformation
    Else
        MsgBox "Ha ocurrido un problema a actualizar el producto"
    End If

End Sub

Private Sub cmdAgregarFacilidad_Click()
    Dim i As Long
    
    If Me.cbxPlanFaci.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar la facilidad"
        Exit Sub
    End If
    If Me.txtPlanCantFaci.Text = "" Then
        MsgBox "Debe ingresar la cantidad de facilidades"
        Exit Sub
    Else
        If Not IsNumeric(Me.txtPlanCantFaci.Text) Then
            MsgBox "La cantidad de facilidades debe ser numerico"
            Exit Sub
        End If
    End If
    If Me.cbxPlanFaciProgCent.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si la facilidad se programa en la central"
        Exit Sub
    End If

    If cmdAgregarFacilidad.Caption = "Agregar" Then
    
        With grdFacilidades
            For i = 1 To .Rows - 1
                If arrCodiFacilidades(cbxPlanFaci.ListIndex - 1) = .TextMatrix(i, 4) Then
                    MsgBox "La facilidad seleccionada ya existe"
                    Exit Sub
                End If
            Next i
        End With
    
        If InsPlanFaci(txtCodiPlan.Text, _
                       arrCodiFacilidades(cbxPlanFaci.ListIndex - 1), _
                       Val(txtPlanCantFaci.Text), _
                       txtPlanFaciCodiExcl.Text, _
                       Left(cbxPlanFaciProgCent.Text, 1)) Then
                       
            Call cmdLimpiarFacilidad_Click
            
            Call suTraeDatosPlan
        Else
            MsgBox "No se ha podido Agregar la facilidad"
        End If
    End If

    If cmdAgregarFacilidad.Caption = "Modificar" Then
        If ModPlanFaci(txtCodiPlan.Text, _
                       grdFacilidades.TextMatrix(Me.grdFacilidades.Row, 4), _
                       Val(txtPlanCantFaci.Text), _
                       txtPlanFaciCodiExcl.Text, _
                       Left(cbxPlanFaciProgCent.Text, 1)) Then
            Call cmdLimpiarFacilidad_Click
            
            Call suTraeDatosPlan
        Else
            MsgBox "No se ha podido Modificar la facilidad"
        End If
    End If

End Sub

Private Sub cmdBuscaPlan_Click()
    Dim i As Long, j As Long
    
    Load frmBuscaPlan
    
    MousePointer = vbHourglass
    
    With frmBuscaPlan
    'Se llena la grilla de planes
        For i = 1 To grdPlanes.Rows - 1
            .grdOrig.Rows = .grdOrig.Rows + 1
            .grdPlanes.Rows = .grdOrig.Rows + 1
                
            For j = 0 To grdPlanes.Cols - 1
                .grdOrig.TextMatrix(i, j) = grdPlanes.TextMatrix(i, j)
                .grdPlanes.TextMatrix(i, j) = grdPlanes.TextMatrix(i, j)
            Next j
            .grdOrig.TextMatrix(i, 14) = i
        Next i
    End With
    giCualPlanSeleccionado = 0
    MousePointer = vbDefault
    frmBuscaPlan.Show vbModal
    If giCualPlanSeleccionado > 0 Then
        grdPlanes.TopRow = giCualPlanSeleccionado
        grdPlanes.Row = giCualPlanSeleccionado
        grdPlanes.RowSel = giCualPlanSeleccionado
        grdPlanes.Col = 0
        grdPlanes.ColSel = grdPlanes.Cols - 1
        Call grdPlanes_Click
    End If
End Sub

Private Sub cmdCopiarPlan_Click()
    Dim sNuevoCodigo As String
    Dim i As Long
    
    If MsgBox("�Esta seguro de copiar la el plan: " & Me.txtDescPlan.Text & "?", vbYesNo) = vbYes Then
        
        sNuevoCodigo = UCase(InputBox("Ingrese el codigo del nuevo plan" & vbCrLf & "S�lo se tomaran los primeros 10 caracteres", "Ingreso de Datos"))
        sNuevoCodigo = Left(sNuevoCodigo, 10)
        
        If sNuevoCodigo <> "" Then
            ' revisando de que el codigo no se repita
            For i = 1 To grdPlanes.Rows - 1
                If sNuevoCodigo = grdPlanes.TextMatrix(i, 0) Then
                    MsgBox "Este c�digo ya esta siendo utilizado"
                    Exit Sub
                End If
            Next i
            
            If CopiaPlan(txtCodiPlan.Text, _
                         sNuevoCodigo) Then
                Call suInicializaForm
                Call suInicializaPlanes
                Call suLlenaPlanes
                ' siempre se recargan los productos
                Call gsuCargaProductos
            End If
        Else
            MsgBox "Debe ingresar el nuevo c�digo del plan"
        End If
    End If
End Sub

Private Sub cmdEliminarFacilidad_Click()
    If grdFacilidades.Row > 0 Then
        If grdFacilidades.TextMatrix(grdFacilidades.Row, 4) <> "" Then
        
            If MsgBox("�Esta seguro de elimina la facilidad: " & cbxPlanFaci.Text & "?", vbYesNo) = vbYes Then
                If DelPlanFaci(txtCodiPlan.Text, _
                               grdFacilidades.TextMatrix(grdFacilidades.Row, 4)) Then
                    Call suTraeDatosPlan
                Else
                    MsgBox "No se ha podido eliminar la facilidad"
                End If
            End If
        End If
    End If
End Sub

Private Sub cmdLimpiar_Click()
    Call suInicializaPlanes
End Sub

Private Sub cmdLimpiarFacilidad_Click()
    cbxPlanFaci.ListIndex = 0
    txtPlanCantFaci.Text = "1"
    txtPlanFaciCodiExcl.Text = ""
    Me.cbxPlanFaciProgCent.ListIndex = 0
    
    cmdAgregarFacilidad.Caption = "Agregar"
    cbxPlanFaci.Enabled = True
     
    grdFacilidades.Row = 0
End Sub

Private Sub cmdModificaPlan_Click()

    Dim auxCodRenta
    auxCodRenta = ""
    If Me.cbxRenta.ListIndex > 0 Then
        auxCodRenta = arrCodiRentas(Me.cbxRenta.ListIndex - 1)
    End If
    
    If fnValidaDatos Then
    
        If ModPlan(Me.txtCodiPlan.Text, _
                   Me.txtDescPlan.Text, _
                   IIf(Me.chkSinFechaFin.Value = 1, "NADA", Format(dtFechaFin.Value, "dd-mm-yyyy")), _
                   arrCodiFacilidades(Me.cbxFacilidad.ListIndex - 1), _
                   Left(cbxComputosCentral.Text, 1), _
                   auxCodRenta, _
                   txtCentral.Text, _
                   Left(cbxCargaIniProp.Text, 1), _
                   Val(Me.txtValorPlan.Text), _
                   Left(cbxTipoSaldo.Text, 4), _
                   Val(Me.txtValorCargaInicial.Text), _
                   Left(cbxUso.Text, 3), _
                   Left(cbxAcumula.Text, 1), _
                   Left(cbxPortal.Text, 1), _
                   IIf(txtRut_empresa.Text = "0", "", txtRut_empresa.Text), _
                   txCptoSMS.Text, _
                   txCptoDatos.Text, _
                   txRutTercera, _
                   cbxAgrupador.Text, _
                   txPaqPortal.Text, _
                   txCantLineas.Text, _
                   Left(cbx4g.Text, 1), _
                   Left(cbxCargoBoleta.Text, 1) _
                   ) Then
            Call suInicializaForm
            Call suInicializaPlanes
        
            Call suLlenaPlanes
            
            Call gsuCargaPlanes
            
            'Call suTraeDatosPlan
        End If
    End If
End Sub

Private Function fnValidaDatos() As Boolean
    fnValidaDatos = True
    
    If txtCodiPlan.Text = "" Then
        MsgBox "Debe ingresar el codigo del plan"
        fnValidaDatos = False
        Exit Function
    End If
    
    If Me.txtDescPlan.Text = "" Then
        MsgBox "Debe ingresar la descripci�n del plan"
        fnValidaDatos = False
        Exit Function
    End If
    
    If cbxFacilidad.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar una facilidad para el plan"
        fnValidaDatos = False
        Exit Function
    End If
    
    If cbxComputosCentral.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si los computos son en la central o no"
        fnValidaDatos = False
        Exit Function
    End If
    
    If cbxRenta.Text = "SELECCIONE" And (cbxAgrupador.Text = "PHS" Or cbxAgrupador.Text = "MOV") Then
        MsgBox "Debe seleccionar la renta asociada"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtCentral.Text = "" Then
        MsgBox "Debe ingresar la central en donde estar� el plan"
        fnValidaDatos = False
        Exit Function
    End If
    
    If cbxCargaIniProp.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si la carga inicial es proporcional mensualmente"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtValorPlan.Text = "" Then
        MsgBox "Debe ingresar el valor del plan"
        fnValidaDatos = False
        Exit Function
    Else
        If Not IsNumeric(txtValorPlan.Text) Then
            MsgBox "El valor del plan debe ser numerico"
            fnValidaDatos = False
            Exit Function
        End If
    End If
    
    If cbxTipoSaldo.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si el saldo es en Minutos o Pesos"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtValorCargaInicial.Text = "" Then
        MsgBox "Debe ingresar el valor de la carga inicial"
        fnValidaDatos = False
        Exit Function
    Else
        If Not IsNumeric(txtValorCargaInicial.Text) Then
            MsgBox "El valor de la carga inicial debe ser numerico"
            fnValidaDatos = False
            Exit Function
        End If
    End If
    
    If chkSinFechaFin.Value = 0 Then
        ' si se encuentra entre 0 y -0.99... significa que es el mismo d�a
        If dtFechaFin.Value - Now <= -1 Then
            MsgBox "La Fecha de T�rmino no puede ser menor a hoy d�a"
            fnValidaDatos = False
            Exit Function
        End If
    End If
    
    ' validaci�n para campo Uso. JPABLOS. Proyecto OMV. 17-jun-2011.
    If cbxUso.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el Uso del plan"
        fnValidaDatos = False
        Exit Function
    End If
    
    ' Validaci�n para �Acumula minutos?
    If cbxAcumula.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si acumula minutos o no"
        fnValidaDatos = False
        Exit Function
    End If
    
    ' Validaci�n para Plan Portal y Rut Empresa (Portal)
    If cbxPortal = "SELECCIONE" Then
     MsgBox "Debe seleccionar si es Plan Portal o no"
     fnValidaDatos = False
     Exit Function
    ElseIf cbxPortal = "SI" Then
            If txtRut_empresa = "" Then
              MsgBox "Debe ingresar Rut Empresa (Portal)"
              fnValidaDatos = False
              Exit Function
            End If
        ElseIf txtRut_empresa <> "" Then
              MsgBox "Seleccione SI en Plan Portal"
              fnValidaDatos = False
              Exit Function
    End If
    
    ' Validaci�n para 4g
    If cbx4g.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si es plan 4G o no"
        fnValidaDatos = False
        Exit Function
    End If
    
    If cbxCargoBoleta.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si acepta cargo en boleta"
        fnValidaDatos = False
        Exit Function
    End If
    
End Function

Private Sub cmdNuevoPlan_Click()
    If fnValidaDatos Then
       
        If InsPlan(Me.txtCodiPlan.Text, _
                   Me.txtDescPlan.Text, _
                   IIf(Me.chkSinFechaFin.Value = 1, "NADA", Format(dtFechaFin.Value, "dd-mm-yyyy")), _
                   arrCodiFacilidades(Me.cbxFacilidad.ListIndex - 1), _
                   Left(cbxComputosCentral.Text, 1), _
                   arrCodiRentas(Me.cbxRenta.ListIndex - 1), _
                   txtCentral.Text, _
                   Left(cbxCargaIniProp.Text, 1), _
                   Val(Me.txtValorPlan.Text), _
                   Left(cbxTipoSaldo.Text, 4), _
                   Val(Me.txtValorCargaInicial.Text), _
                   Left(cbxUso.Text, 3), _
                   IIf(txtRut_empresa.Text = "0", "", txtRut_empresa.Text), _
                   Left(cbxAcumula.Text, 1), _
                   cbxAgrupador.Text, _
                   txPaqPortal.Text, _
                   txCantLineas.Text, _
                   Left(cbx4g.Text, 1), _
                   Left(cbxCargoBoleta.Text, 1) _
                   ) Then
            Call suInicializaForm
            Call suInicializaPlanes
        
            Call suLlenaPlanes
            
            Call gsuCargaPlanes
            
            Call suTraeDatosPlan
        End If
    End If

End Sub

Private Sub cmdVolver_Click()
    Unload Me
End Sub



Private Sub Form_Load()
    Call suInicializaForm
    Call suInicializaPlanes
    
    Call suLlenaPlanes
    
    Dim controlesForm As Control
    For Each controlesForm In Controls
      If (TypeOf controlesForm Is CommandButton) Then
          If controlesForm.Tag <> "" Then
              Call suEnable_Ctl1(Me.Tag, controlesForm)
          End If
      End If
    Next
    
End Sub

Private Sub suInicializaPlanes()
    
    txtCodiPlan.Text = ""
    txtDescPlan.Text = ""
    txtProdCodiExclu.Text = ""
    txtCentral.Text = ""
    txtValorCargaInicial.Text = ""
    txtValorPlan.Text = ""
    
    
    cbxCargaIniProp.ListIndex = 0
    cbxComputosCentral.ListIndex = 0
    cbxFacilidad.ListIndex = 0
    cbxProdObligatorio.ListIndex = 0
    cbxProductos.ListIndex = 0
    cbxRenta.ListIndex = 0
    cbxTipoSaldo.ListIndex = 0
    chkSinFechaFin.Value = 1
    
    txtProdCodiExclu.Text = ""
    
    cbxPlanFaci.ListIndex = 0
    cbxPlanFaciProgCent.ListIndex = 0
    txtPlanCantFaci.Text = "1"
    txtPlanFaciCodiExcl.Text = ""
    
    dtFechaFin.Enabled = False
    
    txtCodiPlan.Enabled = True
    
    fraFacilidades.Enabled = False
    fraProducto.Enabled = False
    
    cmdModificaPlan.Enabled = False
    
    Call suEnable_Ctl1(Me.Tag, cmdNuevoPlan)
    
    cmdCopiarPlan.Enabled = False
    
    grdPlanes.Row = 0
    
    grdFacilidades.Rows = 1
    
    ' incorpora campo Uso. Proyecto OMV. JPABLOS. 19-jun-2011.
    cbxUso.ListIndex = 0
    txtRut_empresa.Text = ""
    cbxAcumula.ListIndex = 0
    txPaqPortal.Text = ""
    txCantLineas.Text = ""
    cbx4g.ListIndex = 0
    cbxCargoBoleta.ListIndex = 0
    
End Sub

Private Sub suLlenaFacilidades()
    Dim i As Long
    
    cbxPlanFaci.Clear
    cbxPlanFaci.AddItem "SELECCIONE"
    For i = 0 To UBound(arrFacilidades)
        cbxPlanFaci.AddItem arrFacilidades(i)
    Next i
    cbxPlanFaci.ListIndex = 0
    
    cbxFacilidad.Clear
    cbxFacilidad.AddItem "SELECCIONE"
    For i = 0 To UBound(arrFacilidades)
        cbxFacilidad.AddItem arrFacilidades(i)
    Next i
    cbxFacilidad.ListIndex = 0
End Sub

Private Sub suLlenaRentas()
    
    Dim i As Long
   
    cbxRenta.Clear
    cbxRenta.AddItem "SELECCIONE"
    For i = 0 To UBound(arrRentas)
        cbxRenta.AddItem arrRentas(i)
    Next i
    cbxRenta.ListIndex = 0
    
End Sub

Private Sub suLlenaPlanes()
    Dim sCodiPlan As String, sDescPlan As String, sCreacion As String
    Dim sFin As String, sCodiFacilida  As String
    Dim sDesFacilida As String, sFlagControl As String
    Dim sCodiRenta  As String, sDescRenta As String
    Dim sNombCentral As String, sPropCargaIni As String
    Dim lValorPlan  As Long, sTipoSaldo As String
    Dim lCargaIni As Long, sCodiInterno As String
    Dim i As Long, iPlanOK As Integer, j As Integer
    Dim ifiltro As Integer, iGTD As Integer
    Dim sUso As String
    Dim sRutEmpresa As String
    Dim sFlag_Acumula As String
    Dim sRut_Tercero As String, sFlag_Portal As String
    Dim sCodi_Rentasms As String, sDesc_Rentasms As String, sCodi_Internosms As String
    Dim sCodi_Rentadatos As String, sDesc_Rentadatos As String, sCodi_Internodatos As String
    Dim iNumResults As Integer, sAgrupador As String
    Dim sPaqPortal As String, iCantLineas As Integer, sFlag_4g As String, sFlag_CargoBoleta As String
                                       
    grdPlanes.Redraw = False
    
    If Me.chkPlanesGTDMovil.Value = 1 Then
        iGTD = 1
    End If
    
    If Me.chkPlanesVigentes.Value = 1 And Me.chkPlanesNoVigentes.Value = 1 Then
        ifiltro = 0
    End If
    
    If Me.chkPlanesVigentes.Value = 1 And Me.chkPlanesNoVigentes.Value = 0 Then
        ifiltro = 1
    End If
    
    If Me.chkPlanesVigentes.Value = 0 And Me.chkPlanesNoVigentes.Value = 1 Then
        ifiltro = -1
    End If
    
    If Me.chkPlanesVigentes.Value = 0 And Me.chkPlanesNoVigentes.Value = 0 Then
        ifiltro = -2
    End If
    
    'If Me.chkPlanesVigentes.Value = 1
    
    grdPlanes.Rows = 1
    
     If iGTD = 1 Then
        iNumResults = GetPlanesMovil(ifiltro)
     Else
        iNumResults = GetPlanes(ifiltro)
     End If
    If iNumResults > 0 Then
        With grdPlanes
            i = 1
              ' se incorpora campo Uso. JPABLOS. Proyecto OMV. 17-jun-2011
            Do While GetPlanesRead(sCodiPlan, sDescPlan, sCreacion, sFin, _
                                   sCodiFacilida, sDesFacilida, sFlagControl, _
                                   sCodiRenta, sDescRenta, sNombCentral, sPropCargaIni, _
                                   lValorPlan, sTipoSaldo, lCargaIni, _
                                   sCodiInterno, iPlanOK, sUso, sFlag_Acumula, _
                                   sFlag_Portal, sRutEmpresa, _
                                   sCodi_Rentasms, sDesc_Rentasms, sCodi_Internosms, _
                                   sCodi_Rentadatos, sDesc_Rentadatos, sCodi_Internodatos, _
                                   sRut_Tercero, sAgrupador, sPaqPortal, iCantLineas, sFlag_4g, sFlag_CargoBoleta)
                                   
                .Rows = .Rows + 1
                .TextMatrix(i, 0) = sAgrupador
                .TextMatrix(i, 1) = sCodiPlan
                .TextMatrix(i, 2) = sDescPlan
                .TextMatrix(i, 3) = sCreacion
                .TextMatrix(i, 4) = sFin
                .TextMatrix(i, 5) = sCodiFacilida
                .TextMatrix(i, 6) = " " & sCodiFacilida & " - " & sDesFacilida
                .TextMatrix(i, 7) = sFlagControl
                .TextMatrix(i, 8) = sCodiRenta
                .TextMatrix(i, 9) = " " & sCodiInterno & " - " & sDescRenta
                .TextMatrix(i, 10) = sNombCentral
                .TextMatrix(i, 11) = sPropCargaIni
                .TextMatrix(i, 12) = lValorPlan
                .TextMatrix(i, 13) = sTipoSaldo
                .TextMatrix(i, 14) = lCargaIni
                .TextMatrix(i, 15) = sUso
                .TextMatrix(i, 16) = sFlag_Acumula
                .TextMatrix(i, 17) = sFlag_Portal
                If sRutEmpresa = "" And Len(sPaqPortal) > 3 Then
                    .TextMatrix(i, 18) = "0"
                Else
                    .TextMatrix(i, 18) = sRutEmpresa
                End If
                .TextMatrix(i, 19) = sCodi_Rentasms
                .TextMatrix(i, 20) = " " & sCodi_Internosms & " - " & sDesc_Rentasms
                .TextMatrix(i, 21) = sCodi_Rentadatos
                .TextMatrix(i, 22) = " " & sCodi_Internodatos & " - " & sDesc_Rentadatos
                .TextMatrix(i, 23) = sRut_Tercero
                .TextMatrix(i, 24) = sPaqPortal
                .TextMatrix(i, 25) = iCantLineas
                .TextMatrix(i, 26) = sFlag_4g
                .TextMatrix(i, 27) = sFlag_CargoBoleta
                
                .Row = i
                If iPlanOK = 1 Then
                    For j = 0 To .Cols - 1
                        .Col = j
                        .CellBackColor = vbWhite
                        .CellForeColor = vbBlack
                    Next j
                Else
                    For j = 0 To .Cols - 1
                        .Col = j
                        .CellBackColor = vbRed
                        .CellForeColor = vbWhite
                    Next j
                End If
                i = i + 1
            Loop
        End With
    End If

    
    grdPlanes.Redraw = True
    
    Call cmdLimpiar_Click
End Sub

Private Sub suInicializaForm()
    
      ' se incorpora campo Uso. Proyecto OMV. JPABLOS. 17-jun-2011
    With grdPlanes
        .Cols = 28
        .Rows = 2
        
        .TextMatrix(0, 0) = "Agrupador"
        .ColWidth(0) = 1300
        .TextMatrix(0, 1) = "C�digo"
        .ColWidth(1) = 1300
        .TextMatrix(0, 2) = "Descripci�n"
        .ColWidth(2) = 4000
        .TextMatrix(0, 3) = "Inicio"
        .ColWidth(3) = 1000
        .TextMatrix(0, 4) = "Fin"
        .ColWidth(4) = 1000
        .TextMatrix(0, 5) = "Codi_Facilida"
        .ColWidth(5) = 0
        .TextMatrix(0, 6) = "Facilidad"
        .ColWidth(6) = 4000
        .TextMatrix(0, 7) = "Control"
        .ColWidth(7) = 600
        .TextMatrix(0, 8) = "Codi_Renta"
        .ColWidth(8) = 0
        .TextMatrix(0, 9) = "Renta"
        .ColWidth(9) = 4000
        .TextMatrix(0, 10) = "Central"
        .ColWidth(10) = 1300
        .TextMatrix(0, 11) = "Propor."
        .ColWidth(11) = 600
        .TextMatrix(0, 12) = "Valor Plan"
        .ColWidth(12) = 1000
        .TextMatrix(0, 13) = "Tipo Saldo"
        .ColWidth(13) = 1000
        .TextMatrix(0, 14) = "Carga Inicial"
        .ColWidth(14) = 1000
        .TextMatrix(0, 15) = "Uso"
        .ColWidth(15) = 1000
        .TextMatrix(0, 16) = "Acumula Mins"
        .ColWidth(16) = 1000
        .TextMatrix(0, 17) = "Portal"
        .ColWidth(17) = 1000
        .TextMatrix(0, 18) = "Rut Empresa"
        .ColWidth(18) = 1100
        .TextMatrix(0, 19) = "codi sms"
        .ColWidth(19) = 0
        .TextMatrix(0, 20) = "Renta SMS"
        .ColWidth(20) = 0
        .TextMatrix(0, 21) = "codi datos"
        .ColWidth(21) = 0
        .TextMatrix(0, 22) = "Renta Datos"
        .ColWidth(22) = 0
        .TextMatrix(0, 23) = "Empresa Tercera"
        .ColWidth(23) = 0
        .TextMatrix(0, 24) = "Paquete Portal"
        .ColWidth(24) = 1000
        .TextMatrix(0, 25) = "Lineas"
        .ColWidth(25) = 1000
        .TextMatrix(0, 26) = "4G"
        .ColWidth(26) = 500
        .TextMatrix(0, 27) = "Cargo Boleta"
        .ColWidth(27) = 1000
        
        .FixedCols = 0
        .FixedRows = 1
    End With
    
    With grdFacilidades
        .Rows = 2
        .Cols = 6
        
        .TextMatrix(0, 0) = "Facilidad"
        .TextMatrix(0, 1) = "Cant."
        .TextMatrix(0, 2) = "Prog. Central"
        .TextMatrix(0, 3) = "Codi. Exclusion"
        .TextMatrix(0, 4) = "Codi_Facilida"
        .TextMatrix(0, 5) = "Marcado"
        
        .ColWidth(0) = 3500
        .ColWidth(1) = 500
        .ColWidth(2) = 1000
        .ColWidth(3) = 1000
        .ColWidth(4) = 0
        .ColWidth(5) = 0
        
        .FixedCols = 0
        .FixedRows = 1
    End With
    
    Call suLlenaFacilidades
    Call suLlenaRentas
    Call gsuLlenaComboSiNo(cbxCargaIniProp)
    Call gsuLlenaComboSiNo(cbxComputosCentral)
    Call gsuLlenaComboSiNo(cbxPlanFaciProgCent)
    Call gsuLlenaComboSiNo(cbxProdObligatorio)
    Call suLlenaProductos
    
    Call suLlenaTipoSaldo
    
    ' Valores posibles para cbxUso. Proyecto OMV. JPABLOS. 17-jun-2011.
    Call suLlenaUso
    Call suLlenaAcumula
    Call suLlenaPortal
  '  chkPlanesNoVigentes.Value = 1
  '  chkPlanesVigentes.Value = 1
    Call suLlenaAgrupador
    Call suLlena4g
    Call suLlenaCargoBoleta
    
End Sub

Private Sub suLlenaProductos()
    Dim i As Long
    
    cbxProductos.Clear
    cbxProductos.AddItem "SELECCIONE"
    For i = 0 To UBound(arrProductos)
        cbxProductos.AddItem arrProductos(i)
    Next i
    
    cbxProductos.ListIndex = 0
End Sub

Private Sub suLlenaTipoSaldo()
    With cbxTipoSaldo
        .Clear
        .AddItem "SELECCIONE"
        .AddItem "PESOS"
        .AddItem "MINUTOS"
        .ListIndex = 0
    End With
    
End Sub
Private Sub suLlenaControlada()
   ' With cbxControlada
   '     .Clear
   '     .AddItem "SELECCIONE"
   '     .AddItem "SI"
   '     .AddItem "NO"
   '     .ListIndex = 0
   ' End With
    
End Sub
Private Sub suLlenaAcumula()
    With cbxAcumula
        .Clear
        .AddItem "SELECCIONE"
        .AddItem "SI"
        .AddItem "NO"
        .ListIndex = 0
    End With
    
End Sub
Private Sub suLlena4g()
    With cbx4g
        .Clear
        .AddItem "SELECCIONE"
        .AddItem "SI"
        .AddItem "NO"
        .ListIndex = 0
    End With
    
End Sub
Private Sub suLlenaCargoBoleta()
    With cbxCargoBoleta
        .Clear
        .AddItem "SELECCIONE"
        .AddItem "SI"
        .AddItem "NO"
        .ListIndex = 0
    End With
    
End Sub

Private Sub suLlenaUso()
    ' Valores posibles para cbxUso. Proyecto OMV. JPABLOS. 17-jun-2011.
    With cbxUso
        .Clear
        .AddItem "SELECCIONE"
        .AddItem "PREPAGO"
        .AddItem "POSPAGO"
        .ListIndex = 0
    End With
    
End Sub
Private Sub suLlenaPortal()
    ' Valores posibles para cbxUso. Proyecto OMV. JPABLOS. 17-jun-2011.
    With cbxPortal
        .Clear
        .AddItem "SELECCIONE"
        .AddItem "SI"
        .AddItem "NO"
        .ListIndex = 0
    End With
    
End Sub
Private Sub suLlenaAgrupador()
    
    Dim Codi_Agrupa As String
    Dim nro_agrupa As Integer, i As Integer
    
    
    cbxAgrupador.AddItem "SELECCIONE"
        
    nro_agrupa = GetPlanesAgrupa
    ReDim arrAgrupador(nro_agrupa) As String
    cbxAgrupador.Clear
        
    i = 0
    Do While GetPlanesAgrupaRead(Codi_Agrupa)
        arrAgrupador(i) = Codi_Agrupa
        cbxAgrupador.AddItem Codi_Agrupa
        i = i + 1
    Loop
        
    cbxAgrupador.ListIndex = 0
End Sub

Private Sub Form_Resize()
  If frmPlanes.Width > 1500 Then
'        Frame1.Width = fingrend.Width - 150
        grdPlanes.Width = frmPlanes.Width - 500
        fraPlanes.Width = frmPlanes.Width - 300
   End If
   
   If frmPlanes.Height > 8500 Then
  '      Frame1.Height = fingrend.Height - 150
        grdPlanes.Height = frmPlanes.Height - 7500
        fraPlanes.Height = frmPlanes.Height - 4300
        fraFacilidades.Top = frmPlanes.Height - 4200
        fraProducto.Top = frmPlanes.Height - 4200
        fraContainer.Top = frmPlanes.Height - 6900
        
   End If
End Sub

Private Sub grdFacilidades_Click()
    Dim i As Long
    
    
    If grdFacilidades.Row > 0 Then
        With grdFacilidades
            
            
            For i = 0 To UBound(arrCodiFacilidades)
                If arrCodiFacilidades(i) = .TextMatrix(.Row, 4) Then
                    Me.cbxPlanFaci.ListIndex = i + 1
                    Exit For
                End If
            Next i
            
            txtPlanCantFaci.Text = .TextMatrix(.Row, 1)
            txtPlanFaciCodiExcl.Text = .TextMatrix(.Row, 3)
            
            cbxPlanFaciProgCent.ListIndex = 0
            If .TextMatrix(.Row, 2) = "SI" Then
                cbxPlanFaciProgCent.ListIndex = 1
            Else
                cbxPlanFaciProgCent.ListIndex = 2
            End If
            
        End With
        
        cbxPlanFaci.Enabled = False
        cmdAgregarFacilidad.Caption = "Modificar"
    End If
End Sub

Private Sub grdPlanes_Click()
    Dim i As Long
    Dim intcount As Integer
    cmdNuevoPlan.Enabled = False
    
    Call suEnable_Ctl1(Me.Tag, cmdModificaPlan)
    Call suEnable_Ctl1(Me.Tag, cmdCopiarPlan)
    
    fraFacilidades.Enabled = True
    fraProducto.Enabled = True
    
    txtCodiPlan.Enabled = False
    
    With grdPlanes
        txtCodiPlan.Text = .TextMatrix(.Row, 1)
        Me.txtDescPlan.Text = .TextMatrix(.Row, 2)
        If .TextMatrix(.Row, 4) <> "" Then
            dtFechaFin.Value = .TextMatrix(.Row, 4)
            chkSinFechaFin.Value = 0
        Else
            chkSinFechaFin.Value = 1
        End If
        Call chkSinFechaFin_Click
        
        For i = 0 To UBound(arrFacilidades)
            If arrCodiFacilidades(i) = .TextMatrix(.Row, 5) Then
                cbxFacilidad.ListIndex = i + 1
                Exit For
            End If
        Next i
        
        Call gsuAsignaComboSiNo(cbxComputosCentral, .TextMatrix(.Row, 7))
        
        For i = 0 To UBound(arrRentas)
            If arrCodiRentas(i) = .TextMatrix(.Row, 8) Then
                cbxRenta.ListIndex = i + 1
                Exit For
            End If
        Next i

        txtCentral.Text = .TextMatrix(.Row, 10)
        
        Call gsuAsignaComboSiNo(cbxCargaIniProp, .TextMatrix(.Row, 11))

        txtValorPlan.Text = .TextMatrix(.Row, 12)

        If .TextMatrix(.Row, 13) = "PESOS" Then
            cbxTipoSaldo.ListIndex = 1
        Else
            cbxTipoSaldo.ListIndex = 2
        End If

        txtValorCargaInicial.Text = .TextMatrix(.Row, 14)
                
        ' se icnorpora campo Uso. JPABLOS. Proyecto OMV. 17-jun-2011
        If .TextMatrix(.Row, 15) = "PREPAGO" Then
            cbxUso.ListIndex = 1
        ElseIf .TextMatrix(.Row, 15) = "POSPAGO" Then
            cbxUso.ListIndex = 2
        Else
            cbxUso.ListIndex = 0
        End If
                        
        Call gsuAsignaComboSiNo(cbxAcumula, .TextMatrix(.Row, 16)) 'acumula minutos
        Call gsuAsignaComboSiNo(cbxPortal, .TextMatrix(.Row, 17)) 'portal
        txtRut_empresa.Text = .TextMatrix(.Row, 18) 'rut empresa
        
        txCodiSMS.Text = ""
        txDescRentaSms.Text = ""
        
        For i = 0 To UBound(arrRentas)
            If arrCodiRentas(i) = .TextMatrix(.Row, 19) Then
                txCodiSMS.Text = arrCodiConcepto(i)
                txDescRentaSms.Text = arrDescConcepto(i)
                Exit For
            End If
        Next i
        
        txCodiDatos.Text = ""
        txDescRentaDatos.Text = ""
        
        For i = 0 To UBound(arrRentas)
            If arrCodiRentas(i) = .TextMatrix(.Row, 21) Then
                txCodiDatos.Text = arrCodiConcepto(i)
                txDescRentaDatos.Text = arrDescConcepto(i)
                Exit For
            End If
        Next i
        
        txRutTercera.Text = .TextMatrix(.Row, 23) 'Empresa Tercera
        
        For i = 0 To UBound(arrAgrupador)
            If arrAgrupador(i) = .TextMatrix(.Row, 0) Then
                cbxAgrupador.ListIndex = i
                If arrAgrupador(i) = "MOV" Then
                    lb_multimedia.Caption = "�Con Corte?"
                Else
                    lb_multimedia.Caption = "�Computos en Central?"
                End If
                Exit For
            End If
        Next i
        txPaqPortal.Text = .TextMatrix(.Row, 24)
        txCantLineas.Text = .TextMatrix(.Row, 25)
        Call gsuAsignaComboSiNo(cbx4g, .TextMatrix(.Row, 26))
        Call gsuAsignaComboSiNo(cbxCargoBoleta, .TextMatrix(.Row, 27))
        
    End With
    
    Call suTraeDatosPlan
End Sub

Private Sub suTraeDatosPlan()
    Dim iCant As Long
    Dim sCodi As String, sDesc As String
    Dim lCant As Long, sCodiEx As String, sProgCen As String
    Dim sCodiProducto As String
    Dim sFlagObligatorio As String, sCodiExclusion As String
    
    iCant = GetPlanFaci(txtCodiPlan.Text)
    grdFacilidades.Rows = 1
    If iCant > 0 Then
        With grdFacilidades
            Do While GetPlanFaciRead(sCodi, sDesc, lCant, sCodiEx, sProgCen)
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, 0) = sDesc
                .TextMatrix(.Rows - 1, 1) = lCant
                .TextMatrix(.Rows - 1, 2) = sProgCen
                .TextMatrix(.Rows - 1, 3) = sCodiEx
                .TextMatrix(.Rows - 1, 4) = sCodi
                .TextMatrix(.Rows - 1, 5) = "0"
            Loop
        End With
    End If
    
    iCant = GetPlanProd(txtCodiPlan.Text)
    
    cbxProductos.ListIndex = 0
    txtProdCodiExclu.Text = ""
    cbxProdObligatorio.ListIndex = 0
    If iCant > 0 Then
        Call GetPlanProdRead(sCodiProducto, sFlagObligatorio, sCodiExclusion)
        
        For iCant = 0 To UBound(arrCodiProductos)
            If arrCodiProductos(iCant) = sCodiProducto Then
                cbxProductos.ListIndex = iCant + 1
                Exit For
            End If
        Next iCant
        
        cbxProdObligatorio.ListIndex = 0
        If sFlagObligatorio = "SI" Then
            cbxProdObligatorio.ListIndex = 1
        Else
            cbxProdObligatorio.ListIndex = 2
        End If
        
        txtProdCodiExclu.Text = sCodiExclusion
    End If
End Sub

Private Sub txCodiDatos_LostFocus()
Dim intcount As Integer
    For intcount = 0 To UBound(arrRentas)

       If arrCodiConcepto(intcount) = txCodiDatos.Text Then
            Exit For
       End If
    Next intcount
    
       If intcount > UBound(arrRentas) Then
          txDescRentaDatos.Text = "CODIGO RENTA NO EXISTE" 'arrRentas(intcount)
       Else
          txDescRentaDatos.Text = arrDescConcepto(intcount) '.Desc '"NO"
          txCptoDatos.Text = arrCodiRentas(intcount) 'arrRentas(intcount)
       End If
 

End Sub

Private Sub txCodiSMS_LostFocus()
Dim intcount As Integer
    For intcount = 0 To UBound(arrRentas)

       If arrCodiConcepto(intcount) = txCodiSMS.Text Then
            Exit For
       End If
    Next intcount
       If intcount > UBound(arrRentas) Then
          txDescRentaSms.Text = "CODIGO RENTA NO EXISTE" 'arrRentas(intcount)
       Else
          txDescRentaSms.Text = arrDescConcepto(intcount) '.Desc '"NO"
          txCptoSMS.Text = arrCodiRentas(intcount)
       End If
 
    
End Sub

Private Sub txtCentral_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))
End Sub

Private Sub txtCodiPlan_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))
End Sub

Private Sub txtDescPlan_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))
End Sub

Private Sub txtPlanCantFaci_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtPlanFaciCodiExcl_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))

End Sub

Private Sub txtProdCodiExclu_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))
End Sub

Private Sub txtValorCargaInicial_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtValorPlan_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub
