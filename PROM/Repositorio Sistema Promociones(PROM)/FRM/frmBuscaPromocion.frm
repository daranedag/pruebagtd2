VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmBuscaPromocion 
   Caption         =   "Buscador de Promociones"
   ClientHeight    =   6675
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11445
   LinkTopic       =   "Form1"
   ScaleHeight     =   6675
   ScaleWidth      =   11445
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   975
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   11175
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "Filtrar"
         Height          =   495
         Left            =   9480
         TabIndex        =   3
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox txtNombre 
         Height          =   405
         Left            =   2160
         TabIndex        =   2
         Text            =   "Text1"
         Top             =   240
         Width           =   3135
      End
      Begin MSFlexGridLib.MSFlexGrid grdOrig 
         Height          =   375
         Left            =   7200
         TabIndex        =   4
         Top             =   480
         Visible         =   0   'False
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         _Version        =   393216
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre Promocion"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   1935
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdPromociones 
      Height          =   5415
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   9551
      _Version        =   393216
   End
End
Attribute VB_Name = "frmBuscaPromocion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdFiltrar_Click()
    Dim i As Long, j As Long
    Dim iFila As Long
    
    MousePointer = vbHourglass
    
    With grdPromociones
        .Visible = False
        .Rows = 1
        For i = 1 To grdOrig.Rows - 1
            .Rows = .Rows + 1
            For j = 0 To grdOrig.Cols - 1
                .TextMatrix(i, j) = grdOrig.TextMatrix(i, j)
            Next j
        Next i
        .Visible = True
    End With
    iFila = 1
    If txtNombre.Text <> "" Then
        With grdPromociones
            .Visible = False
            .Rows = 1
            Me.Refresh
            ' filtrando por el texto ingresado
            If txtNombre <> "" Then
                For i = 1 To grdOrig.Rows - 1
                    If InStr(1, grdOrig.TextMatrix(i, 1), txtNombre.Text, 1) > 0 Then
                        .Rows = .Rows + 1
                        For j = 0 To .Cols - 1
                            .TextMatrix(.Rows - 1, j) = grdOrig.TextMatrix(i, j)
                        Next j
                    End If
                Next i
            End If
            .Visible = True
        End With
    End If
    MousePointer = vbDefault
End Sub

Private Sub Form_Activate()
    txtNombre.Text = ""
End Sub

Private Sub Form_Load()
    Call suInicializaForm
End Sub

Private Sub suInicializaForm()
        
    grdOrig.Cols = 6
    grdOrig.Rows = 1
    
    With grdPromociones
        .Cols = 6
        .Rows = 2
        
        
        .TextMatrix(0, 0) = "C�digo"
        .TextMatrix(0, 1) = "Descripci�n"
        .TextMatrix(0, 2) = "Fecha Creaci�n"
        .TextMatrix(0, 3) = "Fecha Inicio"
        .TextMatrix(0, 4) = "Fecha T�rmino"
        .TextMatrix(0, 5) = "Fila"
        
        .ColWidth(0) = 1000
        .ColWidth(1) = 7500
        .ColWidth(2) = 1300
        .ColWidth(3) = 1300
        .ColWidth(4) = 1300
        .ColWidth(5) = 0
        
        .FixedCols = 0
        .FixedRows = 1
    End With

End Sub

Private Sub grdPromociones_DblClick()
    If grdPromociones.Row > 0 Then
        giCualPromocionSeleccionada = grdPromociones.TextMatrix(grdPromociones.Row, 5)
        Unload Me
    End If
End Sub
