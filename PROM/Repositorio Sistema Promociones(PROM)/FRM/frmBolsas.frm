VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Begin VB.Form frmBolsas 
   Caption         =   "Mantenedor de Bolsas"
   ClientHeight    =   9000
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13365
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9000
   ScaleWidth      =   13365
   Tag             =   "FRMBOLSAS"
   WindowState     =   2  'Maximized
   Begin VB.Frame fraBolsas 
      Caption         =   "Bolsas Existentes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6015
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   13335
      Begin VB.ComboBox cbxCargaBoleta 
         Height          =   315
         Left            =   7200
         Style           =   2  'Dropdown List
         TabIndex        =   52
         Top             =   4920
         Width           =   1095
      End
      Begin VB.CommandButton cmdEliminarBolsa 
         Caption         =   "Eliminar Bolsa"
         Height          =   375
         Left            =   8280
         TabIndex        =   25
         Tag             =   "BO_ELIMINAR"
         Top             =   5400
         Width           =   1575
      End
      Begin VB.TextBox txtCodPaquete 
         Height          =   285
         Left            =   1800
         MaxLength       =   12
         TabIndex        =   19
         Text            =   "Text1"
         ToolTipText     =   "C�digo de paquete con el que se aplicar� la bolsa desde sistemas externos (IVR, Web, etc.)."
         Top             =   4920
         Width           =   2055
      End
      Begin VB.ComboBox cbxCargaAutoIniMes 
         Height          =   315
         ItemData        =   "frmBolsas.frx":0000
         Left            =   4200
         List            =   "frmBolsas.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   20
         ToolTipText     =   "Indica si la bolsa se carga autom�ticamente al principio de cada mes hasta que el cliente la d� de baja."
         Top             =   4920
         Width           =   1095
      End
      Begin VB.ComboBox cbxVisibleWeb 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   4920
         Width           =   1095
      End
      Begin VB.ComboBox cbxVisibleIVR 
         Height          =   315
         Left            =   11520
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   4320
         Width           =   1095
      End
      Begin VB.TextBox txtNombreRadius 
         Height          =   285
         Left            =   9120
         MaxLength       =   50
         TabIndex        =   16
         Text            =   "12345678901234567890"
         Top             =   4320
         Width           =   2055
      End
      Begin VB.TextBox txtMBDatosRoaming 
         Height          =   285
         Left            =   7200
         MaxLength       =   10
         TabIndex        =   15
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de MB para datos en Roaming. -1 = ilimitado."
         Top             =   4320
         Width           =   1215
      End
      Begin VB.TextBox txtMBDatos 
         Height          =   285
         Left            =   5640
         MaxLength       =   10
         TabIndex        =   14
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de MB para datos. -1 = ilimitado."
         Top             =   4320
         Width           =   1215
      End
      Begin VB.TextBox txtDinero 
         Height          =   285
         Left            =   1680
         MaxLength       =   10
         TabIndex        =   12
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de dinero en pesos que incluye la bolsa."
         Top             =   4320
         Width           =   1215
      End
      Begin VB.TextBox txtNombreCentral 
         Height          =   285
         Left            =   3240
         MaxLength       =   50
         TabIndex        =   13
         Text            =   "12345678901234567890"
         Top             =   4320
         Width           =   2055
      End
      Begin VB.TextBox txtCantidadSms 
         Height          =   285
         Left            =   120
         MaxLength       =   10
         TabIndex        =   11
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de SMS. -1 = ilimitado."
         Top             =   4320
         Width           =   1215
      End
      Begin VB.TextBox txtMinutosRoaming 
         Height          =   285
         Left            =   10440
         MaxLength       =   10
         TabIndex        =   10
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de Minutos en Roaming. -1 = ilimitado."
         Top             =   3720
         Width           =   1215
      End
      Begin VB.TextBox txtMinutosIslaPascua 
         Height          =   285
         Left            =   8520
         MaxLength       =   10
         TabIndex        =   9
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de Minutos a Isla de Pascua. -1 = ilimitado."
         Top             =   3720
         Width           =   1215
      End
      Begin VB.TextBox txtMinutosRural 
         Height          =   285
         Left            =   6960
         MaxLength       =   10
         TabIndex        =   8
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de Minutos a Rurales. -1 = ilimitado."
         Top             =   3720
         Width           =   1215
      End
      Begin VB.TextBox txtMinutosTodoDestino 
         Height          =   285
         Left            =   4920
         MaxLength       =   10
         TabIndex        =   7
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de Minutos a Todo Destino. -1 = ilimitado."
         Top             =   3720
         Width           =   1215
      End
      Begin VB.TextBox txtMinutosOffNet 
         Height          =   285
         Left            =   3360
         MaxLength       =   10
         TabIndex        =   6
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de Minutos Off Net. -1 = ilimitado."
         Top             =   3720
         Width           =   1215
      End
      Begin VB.TextBox txtMinutosOnNet 
         Height          =   285
         Left            =   1800
         MaxLength       =   10
         TabIndex        =   5
         Text            =   "Text1"
         ToolTipText     =   "Cantidad de Minutos On Net. -1 = ilimitado."
         Top             =   3720
         Width           =   1215
      End
      Begin VB.TextBox txtDiasVigencia 
         Height          =   285
         Left            =   120
         MaxLength       =   10
         TabIndex        =   4
         Text            =   "Text1"
         ToolTipText     =   "D�as que dura la bolsa. -1 = sin l�mite, -2 = hasta fin de mes."
         Top             =   3720
         Width           =   1215
      End
      Begin VB.TextBox txtValCargo 
         Height          =   285
         Left            =   6360
         MaxLength       =   12
         TabIndex        =   2
         Text            =   "0"
         ToolTipText     =   "Valor que se descontar� en la Central del saldo de recargas al aplicar la bolsa."
         Top             =   3120
         Width           =   2055
      End
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "Limpiar"
         Height          =   375
         Left            =   120
         TabIndex        =   21
         Top             =   5400
         Width           =   1815
      End
      Begin VB.CommandButton cmdNuevaBolsa 
         Caption         =   "Nueva Bolsa"
         Height          =   375
         Left            =   2160
         TabIndex        =   22
         Tag             =   "BO_NUEVA"
         Top             =   5400
         Width           =   1815
      End
      Begin VB.CommandButton cmdModificaBolsa 
         Caption         =   "Modificar Bolsa"
         Height          =   375
         Left            =   4200
         TabIndex        =   23
         Tag             =   "BO_MODIFICAR"
         Top             =   5400
         Width           =   1815
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "Volver"
         Height          =   375
         Left            =   10080
         TabIndex        =   26
         Top             =   5400
         Width           =   1815
      End
      Begin VB.TextBox txtIdBolsa 
         Height          =   285
         Left            =   120
         MaxLength       =   10
         TabIndex        =   0
         Text            =   "Text1"
         Top             =   3120
         Width           =   1215
      End
      Begin VB.TextBox txtDescBolsa 
         Height          =   285
         Left            =   1800
         MaxLength       =   100
         TabIndex        =   1
         Text            =   "Text1"
         Top             =   3120
         Width           =   4215
      End
      Begin VB.ComboBox cbxFacilidad 
         Height          =   315
         Left            =   9240
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   3120
         Width           =   3975
      End
      Begin VB.CommandButton cmdCopiarBolsa 
         Caption         =   "Copiar Bolsa"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6240
         TabIndex        =   24
         Tag             =   "BO_COPIAR"
         Top             =   5400
         Width           =   1815
      End
      Begin MSFlexGridLib.MSFlexGrid grdBolsas 
         Height          =   2415
         Left            =   120
         TabIndex        =   49
         Top             =   240
         Width           =   13095
         _ExtentX        =   23098
         _ExtentY        =   4260
         _Version        =   393216
         SelectionMode   =   1
      End
      Begin VB.Label Label21 
         Caption         =   "Carga Boleta"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   7200
         TabIndex        =   51
         Top             =   4680
         Width           =   2535
      End
      Begin VB.Label Label19 
         Caption         =   "Carga Autom�tica Inicio Mes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   0
         TabIndex        =   50
         Top             =   0
         Width           =   2535
      End
      Begin VB.Label lblCodPaquete 
         Caption         =   "C�digo paquete"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1800
         TabIndex        =   48
         Top             =   4680
         Width           =   1455
      End
      Begin VB.Label Label20 
         Caption         =   "Carga Autom�tica Inicio Mes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   4200
         TabIndex        =   47
         Top             =   4680
         Width           =   2535
      End
      Begin VB.Label Label18 
         Caption         =   "�Visible en Web?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   46
         Top             =   4680
         Width           =   1575
      End
      Begin VB.Label Label17 
         Caption         =   "�Visible en IVR?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   11520
         TabIndex        =   45
         Top             =   4080
         Width           =   1455
      End
      Begin VB.Label Label16 
         Caption         =   "Nombre en Radius"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   9120
         TabIndex        =   44
         Top             =   4080
         Width           =   2055
      End
      Begin VB.Label Label15 
         Caption         =   "MB Datos Roaming"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   7200
         TabIndex        =   43
         Top             =   4080
         Width           =   1695
      End
      Begin VB.Label Label14 
         Caption         =   "MB Datos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   5640
         TabIndex        =   42
         Top             =   4080
         Width           =   1215
      End
      Begin VB.Label Label13 
         Caption         =   "Dinero"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1680
         TabIndex        =   41
         Top             =   4080
         Width           =   1215
      End
      Begin VB.Label Label12 
         Caption         =   "Nombre en Central"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   3240
         TabIndex        =   40
         Top             =   4080
         Width           =   2055
      End
      Begin VB.Label Label11 
         Caption         =   "Cantidad SMS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   4080
         Width           =   1575
      End
      Begin VB.Label Label10 
         Caption         =   "Minutos Roaming"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   10440
         TabIndex        =   38
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label Label9 
         Caption         =   "Minutos Isla Pascua"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   8520
         TabIndex        =   37
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label Label8 
         Caption         =   "Minutos Rural"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   6960
         TabIndex        =   36
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label Label6 
         Caption         =   "Minutos Todo Destino"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   4920
         TabIndex        =   35
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label Label3 
         Caption         =   "Minutos Off Net"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   3360
         TabIndex        =   34
         Top             =   3480
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "Minutos On Net"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1800
         TabIndex        =   33
         Top             =   3480
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "D�as de Vigencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   3480
         Width           =   1575
      End
      Begin VB.Label lblValCentral 
         Caption         =   "Valor en Central"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   6360
         TabIndex        =   31
         Top             =   2880
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "ID de Bolsa"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   2880
         Width           =   1455
      End
      Begin VB.Label Label5 
         Caption         =   "Descripci�n de Bolsa"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1800
         TabIndex        =   29
         Top             =   2880
         Width           =   1935
      End
      Begin VB.Label Label7 
         Caption         =   "Facilidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   9240
         TabIndex        =   28
         Top             =   2880
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmBolsas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCopiarBolsa_Click()
    Dim sNuevoId As String, sNuevaFacilidad As String, sResult As String
    Dim i As Long
    
    If MsgBox("�Est� seguro de copiar la bolsa " & Me.txtIdBolsa.Text & ": " & Me.txtDescBolsa.Text & "?", vbYesNo) = vbYes Then
        
        sNuevoId = InputBox("Ingrese el ID de la nueva bolsa.", "Ingreso de Datos")
        
        If sNuevoId = "" Then
            MsgBox "Debe ingresar el ID de la nueva bolsa"
            Exit Sub
        ElseIf Not IsNumeric(sNuevoId) Then
            MsgBox "El ID de la nueva bolsa debe ser num�rico"
            Exit Sub
        End If
        
        sNuevaFacilidad = InputBox("Ingrese el c�digo de la nueva facilidad para la bolsa.", "Ingreso de Datos")
        
        If Len(sNuevaFacilidad) > 4 Then
            MsgBox "El c�digo de la nueva facilidad no puede tener m�s de 4 caracteres"
            Exit Sub
        End If

        If CopiaBolsa(txtIdBolsa.Text, _
                      Val(sNuevoId), _
                      sNuevaFacilidad, _
                      sResult) Then
            If Mid(sResult, 1, 2) <> "00" Then
                MsgBox Mid(sResult, 4), vbCritical, "REVISE"
                Exit Sub
            Else
                Call suInicializaForm
                Call suInicializaBolsas
                Call suLlenaBolsas
            End If
        End If
    End If
End Sub

Private Sub cmdEliminarBolsa_Click()
    Dim sResult As String

    If MsgBox("�Est� seguro de eliminar la bolsa " & txtIdBolsa.Text & ": " & txtDescBolsa.Text & "?" & vbCrLf & "Esto no podr� ser deshecho.", vbYesNo) = vbYes Then
        
        If DelBolsa(CLng(txtIdBolsa.Text), sResult) Then
            If Mid(sResult, 1, 2) <> "00" Then
                MsgBox Mid(sResult, 4), vbCritical, "REVISE"
                Exit Sub
            Else
                Call suInicializaBolsas
                Call suLlenaBolsas
            End If
        End If
    
    End If
End Sub

Private Sub cmdLimpiar_Click()
    Call suInicializaBolsas
End Sub

Private Sub cmdModificaBolsa_Click()
    Dim sResult As String
    
    If fnValidaDatos Then
    
        If ModBolsa(Val(Me.txtIdBolsa.Text), _
                    Me.txtDescBolsa.Text, _
                    arrCodiFacilidades(Me.cbxFacilidad.ListIndex - 1), _
                    Val(Me.txtValCargo.Text), _
                    Val(Me.txtDiasVigencia.Text), _
                    Val(Me.txtMinutosOnNet.Text), _
                    Val(Me.txtMinutosOffNet.Text), _
                    Val(Me.txtMinutosTodoDestino.Text), _
                    Val(Me.txtMinutosRural.Text), _
                    Val(Me.txtMinutosIslaPascua.Text), _
                    Val(Me.txtMinutosRoaming.Text), _
                    Val(Me.txtCantidadSms.Text), _
                    Trim(Me.txtNombreCentral.Text), _
                    Val(Me.txtMBDatos.Text), _
                    Val(Me.txtMBDatosRoaming.Text), _
                    Trim(Me.txtNombreRadius.Text), _
                    Left(Me.cbxVisibleIVR.Text, 1), _
                    Left(Me.cbxVisibleWeb.Text, 1), _
                    IIf(Me.txtCodPaquete.Text = "", Null, Val(Me.txtCodPaquete.Text)), _
                    Val(Me.txtDinero.Text), _
                    Left(Me.cbxCargaAutoIniMes.Text, 1), _
                    Left(Me.cbxCargaBoleta.Text, 1), _
                    sResult) Then
            If Mid(sResult, 1, 2) <> "00" Then
                MsgBox Mid(sResult, 4), vbCritical, "REVISE"
                Exit Sub
            Else
                Call suInicializaForm
                Call suInicializaBolsas
            
                Call suLlenaBolsas
            End If
        End If
    End If
End Sub

Private Sub cmdNuevaBolsa_Click()
    Dim sResult As String
    
    If fnValidaDatos Then
    
        If InsBolsa(Val(Me.txtIdBolsa.Text), _
                    Me.txtDescBolsa.Text, _
                    arrCodiFacilidades(Me.cbxFacilidad.ListIndex - 1), _
                    Val(Me.txtValCargo.Text), _
                    Val(Me.txtDiasVigencia.Text), _
                    Val(Me.txtMinutosOnNet.Text), _
                    Val(Me.txtMinutosOffNet.Text), _
                    Val(Me.txtMinutosTodoDestino.Text), _
                    Val(Me.txtMinutosRural.Text), _
                    Val(Me.txtMinutosIslaPascua.Text), _
                    Val(Me.txtMinutosRoaming.Text), _
                    Val(Me.txtCantidadSms.Text), _
                    Trim(Me.txtNombreCentral.Text), _
                    Val(Me.txtMBDatos.Text), _
                    Val(Me.txtMBDatosRoaming.Text), _
                    Trim(Me.txtNombreRadius.Text), _
                    Left(Me.cbxVisibleIVR.Text, 1), _
                    Left(Me.cbxVisibleWeb.Text, 1), _
                    IIf(Me.txtCodPaquete.Text = "", Null, Val(Me.txtCodPaquete.Text)), _
                    Val(Me.txtDinero.Text), _
                    Left(Me.cbxCargaAutoIniMes.Text, 1), _
                    Left(Me.cbxCargaBoleta.Text, 1), _
                    sResult) Then
            If Mid(sResult, 1, 2) <> "00" Then
                MsgBox Mid(sResult, 4), vbCritical, "REVISE"
                Exit Sub
            Else
                Call suInicializaForm
                Call suInicializaBolsas
            
                Call suLlenaBolsas
            End If
        End If
    End If
End Sub

Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call suInicializaForm
    'Call suInicializaBolsas
    
    Call suLlenaBolsas
    
    Dim controlesForm As Control
    For Each controlesForm In Controls
      If (TypeOf controlesForm Is CommandButton) Then
          If controlesForm.Tag <> "" Then
              Call suEnable_Ctl1(Me.Tag, controlesForm)
          End If
      End If
    Next
    
End Sub

Private Sub suInicializaForm()
    
    With grdBolsas
        .Cols = 22
        .Rows = 2
        
        .TextMatrix(0, 0) = "ID"
        .ColWidth(0) = 750
        .TextMatrix(0, 1) = "Descripci�n"
        .ColWidth(1) = 4000
        .TextMatrix(0, 2) = "C�d. Facilidad"
        .ColWidth(2) = 750
        .TextMatrix(0, 3) = "Valor en Central"
        .ColWidth(3) = 1500
        .TextMatrix(0, 4) = "D�as Vigencia"
        .ColWidth(4) = 1500
        .TextMatrix(0, 5) = "Minutos On Net"
        .ColWidth(5) = 1500
        .TextMatrix(0, 6) = "Minutos Off Net"
        .ColWidth(6) = 1500
        .TextMatrix(0, 7) = "Minutos Todo Destino"
        .ColWidth(7) = 1500
        .TextMatrix(0, 8) = "Minutos Rural"
        .ColWidth(8) = 1500
        .TextMatrix(0, 9) = "Minutos Isla Pascua"
        .ColWidth(9) = 1500
        .TextMatrix(0, 10) = "Minutos Roaming"
        .ColWidth(10) = 1500
        .TextMatrix(0, 11) = "Cant. SMS"
        .ColWidth(11) = 1500
        .TextMatrix(0, 12) = "Dinero"
        .ColWidth(12) = 1500
        .TextMatrix(0, 13) = "Nombre en Central"
        .ColWidth(13) = 1500
        .TextMatrix(0, 14) = "MB Datos"
        .ColWidth(13) = 1500
        .TextMatrix(0, 15) = "MB Datos Roaming"
        .ColWidth(15) = 1500
        .TextMatrix(0, 16) = "Nombre en Radius"
        .ColWidth(16) = 1500
        .TextMatrix(0, 17) = "Visible IVR"
        .ColWidth(17) = 1500
        .TextMatrix(0, 18) = "Visible Web"
        .ColWidth(18) = 1500
        .TextMatrix(0, 19) = "C�d. Paquete"
        .ColWidth(19) = 1500
        .TextMatrix(0, 20) = "Carga Auto. Ini. Mes"
        .ColWidth(20) = 1500
        .TextMatrix(0, 21) = "Cargo Boleta"
        .ColWidth(20) = 1500
               
        .FixedCols = 0
        .FixedRows = 1
    End With
    
    Call suLlenaFacilidades
    Call gsuLlenaComboSiNo(cbxVisibleIVR)
    Call gsuLlenaComboSiNo(cbxVisibleWeb)
    Call gsuLlenaComboSiNo(cbxCargaAutoIniMes)
    Call gsuLlenaComboSiNo(cbxCargaBoleta)

End Sub

Private Sub suLlenaFacilidades()
    Dim i As Long
    
    cbxFacilidad.Clear
    cbxFacilidad.AddItem "SELECCIONE"
    For i = 0 To UBound(arrFacilidades)
        cbxFacilidad.AddItem arrFacilidades(i)
    Next i
    cbxFacilidad.ListIndex = 0
End Sub



Private Sub suInicializaBolsas()
    
    ' valores de textbox
    txtCantidadSms.Text = ""
    txtCodPaquete.Text = ""
    txtDescBolsa.Text = ""
    txtDiasVigencia.Text = ""
    txtDinero.Text = ""
    txtIdBolsa.Text = "": txtIdBolsa.Locked = False: txtIdBolsa.BackColor = vbWindowBackground
    txtMBDatos.Text = ""
    txtMBDatosRoaming.Text = ""
    txtMinutosIslaPascua.Text = ""
    txtMinutosOffNet.Text = ""
    txtMinutosOnNet.Text = ""
    txtMinutosRoaming.Text = ""
    txtMinutosRural.Text = ""
    txtMinutosTodoDestino.Text = ""
    txtNombreCentral.Text = ""
    txtNombreRadius.Text = ""
    txtValCargo.Text = ""
    
    ' valores de combobox
    cbxCargaAutoIniMes.ListIndex = 0
    cbxFacilidad.ListIndex = 0
    cbxVisibleIVR.ListIndex = 0
    cbxVisibleWeb.ListIndex = 0
    cbxCargaBoleta.ListIndex = 0
    
    ' valores de botones
    cmdModificaBolsa.Enabled = False
    Call suEnable_Ctl1(Me.Tag, cmdNuevaBolsa)
    cmdCopiarBolsa.Enabled = False
    cmdEliminarBolsa.Enabled = False
        
    ' valores de grillas
    grdBolsas.Row = 0
    
End Sub

Private Sub suLlenaBolsas()
    Dim lIdBolsa As Long, sDescBolsa As String, sCodiFacilida As String, lPrecio As Long
    Dim lDiasVigencia As Long, lMinutosOnNet As Long, lMinutosOffNet As Long
    Dim lMinutosTodoDestino As Long, lMinutosRural As Long, lMinutosIslaPascua As Long
    Dim lMinutosRoaming As Long, lQSms As Long, sNombreMswitch As String, lQMBDatos As Long
    Dim lQMBDatosRoaming As Long, sNombreRadius As String, sVisibleIvr As String
    Dim sVisibleWeb As String, sCodiPaquete As String, lDinero As Long, sCargaAutoIniMes As String, sFlag_CargaBoleta As String
    Dim i As Long
    
    grdBolsas.Rows = 1
    
    If GetBolsas() > 0 Then
        With grdBolsas
            i = 1
            Do While GetBolsasRead(lIdBolsa, sDescBolsa, sCodiFacilida, lPrecio, _
                                   lDiasVigencia, lMinutosOnNet, lMinutosOffNet, _
                                   lMinutosTodoDestino, lMinutosRural, lMinutosIslaPascua, _
                                   lMinutosRoaming, lQSms, sNombreMswitch, lQMBDatos, _
                                   lQMBDatosRoaming, sNombreRadius, sVisibleIvr, _
                                   sVisibleWeb, sCodiPaquete, lDinero, sCargaAutoIniMes, sFlag_CargaBoleta)
                .Rows = .Rows + 1
                .TextMatrix(i, 0) = lIdBolsa
                .TextMatrix(i, 1) = sDescBolsa
                .TextMatrix(i, 2) = sCodiFacilida
                .TextMatrix(i, 3) = lPrecio
                .TextMatrix(i, 4) = lDiasVigencia
                .TextMatrix(i, 5) = lMinutosOnNet
                .TextMatrix(i, 6) = lMinutosOffNet
                .TextMatrix(i, 7) = lMinutosTodoDestino
                .TextMatrix(i, 8) = lMinutosRural
                .TextMatrix(i, 9) = lMinutosIslaPascua
                .TextMatrix(i, 10) = lMinutosRoaming
                .TextMatrix(i, 11) = lQSms
                .TextMatrix(i, 12) = lDinero
                .TextMatrix(i, 13) = sNombreMswitch
                .TextMatrix(i, 14) = lQMBDatos
                .TextMatrix(i, 15) = lQMBDatosRoaming
                .TextMatrix(i, 16) = sNombreRadius
                .TextMatrix(i, 17) = sVisibleIvr
                .TextMatrix(i, 18) = sVisibleWeb
                .TextMatrix(i, 19) = sCodiPaquete
                .TextMatrix(i, 20) = sCargaAutoIniMes
                .TextMatrix(i, 21) = sFlag_CargaBoleta
                .Row = i
                i = i + 1
            Loop
        End With
        
        Call GetBolsasClose
    End If
    Call cmdLimpiar_Click
End Sub

Private Sub grdBolsas_Click()
    Dim i As Long

    cmdNuevaBolsa.Enabled = False
    
    Call suEnable_Ctl1(Me.Tag, cmdModificaBolsa)
    Call suEnable_Ctl1(Me.Tag, cmdCopiarBolsa)
    Call suEnable_Ctl1(Me.Tag, cmdEliminarBolsa)

    
    txtIdBolsa.Locked = True
    txtIdBolsa.BackColor = vbButtonFace
    
    With grdBolsas
        txtIdBolsa.Text = .TextMatrix(.Row, 0)
        txtDescBolsa.Text = .TextMatrix(.Row, 1)

        cbxFacilidad.ListIndex = 0
        For i = 0 To UBound(arrFacilidades)
            If arrCodiFacilidades(i) = .TextMatrix(.Row, 2) Then
                cbxFacilidad.ListIndex = i + 1
                Exit For
            End If
        Next i
        
        txtValCargo.Text = .TextMatrix(.Row, 3)
        txtDiasVigencia.Text = .TextMatrix(.Row, 4)
        txtMinutosOnNet.Text = .TextMatrix(.Row, 5)
        txtMinutosOffNet.Text = .TextMatrix(.Row, 6)
        txtMinutosTodoDestino.Text = .TextMatrix(.Row, 7)
        txtMinutosRural.Text = .TextMatrix(.Row, 8)
        txtMinutosIslaPascua.Text = .TextMatrix(.Row, 9)
        txtMinutosRoaming.Text = .TextMatrix(.Row, 10)
        txtCantidadSms.Text = .TextMatrix(.Row, 11)
        txtDinero.Text = .TextMatrix(.Row, 12)
        txtNombreCentral.Text = .TextMatrix(.Row, 13)
        txtMBDatos.Text = .TextMatrix(.Row, 14)
        txtMBDatosRoaming.Text = .TextMatrix(.Row, 15)
        txtNombreRadius.Text = .TextMatrix(.Row, 16)
        Call gsuAsignaComboSiNo(cbxVisibleIVR, .TextMatrix(.Row, 17))
        Call gsuAsignaComboSiNo(cbxVisibleWeb, .TextMatrix(.Row, 18))
        txtCodPaquete.Text = .TextMatrix(.Row, 19)
        Call gsuAsignaComboSiNo(cbxCargaAutoIniMes, .TextMatrix(.Row, 20))
        Call gsuAsignaComboSiNo(cbxCargaBoleta, .TextMatrix(.Row, 21))
    End With
End Sub
Private Sub suLlenaCargaBoleta()
    With cbxCargaBoleta
        .Clear
        .AddItem "SELECCIONE"
        .AddItem "SI"
        .AddItem "NO"
        .ListIndex = 0
    End With
    
End Sub

Private Function fnValidaDatos() As Boolean
    fnValidaDatos = True
    
    If txtIdBolsa.Text = "" Then
        MsgBox "Debe ingresar el ID de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtIdBolsa.Text) Then
        MsgBox "El ID de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtDescBolsa.Text = "" Then
        MsgBox "Debe ingresar la descripci�n de la bolsa"
        fnValidaDatos = False
        Exit Function
    End If
    
    If cbxFacilidad.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar una facilidad para la bolsa"
        fnValidaDatos = False
        Exit Function
    End If

    If txtValCargo.Text = "" Then
        MsgBox "Debe ingresar el Valor en la Central de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtValCargo.Text) Then
        MsgBox "El Valor en la Central de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtDiasVigencia.Text = "" Then
        MsgBox "Debe ingresar los D�as de Vigencia de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtDiasVigencia.Text) Then
        MsgBox "Los D�as de Vigencia de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtMinutosOnNet.Text = "" Then
        MsgBox "Debe ingresar los Minutos On Net de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtMinutosOnNet.Text) Then
        MsgBox "Los Minutos On Net de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtMinutosOffNet.Text = "" Then
        MsgBox "Debe ingresar los Minutos Off Net de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtMinutosOffNet.Text) Then
        MsgBox "Los Minutos Off Net de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtMinutosTodoDestino.Text = "" Then
        MsgBox "Debe ingresar los Minutos Todo Destino de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtMinutosTodoDestino.Text) Then
        MsgBox "Los Minutos Todo Destino de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtMinutosRural.Text = "" Then
        MsgBox "Debe ingresar los Minutos a Rural de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtMinutosRural.Text) Then
        MsgBox "Los Minutos a Rural de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtMinutosIslaPascua.Text = "" Then
        MsgBox "Debe ingresar los Minutos a Isla de Pascua de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtMinutosIslaPascua.Text) Then
        MsgBox "Los Minutos a Isla de Pascua de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtMinutosRoaming.Text = "" Then
        MsgBox "Debe ingresar los Minutos en Roaming de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtMinutosRoaming.Text) Then
        MsgBox "Los Minutos en Roaming de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtCantidadSms.Text = "" Then
        MsgBox "Debe ingresar la Cantidad de SMS de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtCantidadSms.Text) Then
        MsgBox "La Cantidad de SMS de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If

    If txtDinero.Text = "" Then
        MsgBox "Debe ingresar el Dinero de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtDinero.Text) Then
        MsgBox "El Dinero de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If

    If txtMBDatos.Text = "" Then
        MsgBox "Debe ingresar los MB de Datos de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtMBDatos.Text) Then
        MsgBox "Los MB de Datos de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtMBDatosRoaming.Text = "" Then
        MsgBox "Debe ingresar los MB de Datos en Roaming de la bolsa"
        fnValidaDatos = False
        Exit Function
    ElseIf Not IsNumeric(txtMBDatosRoaming.Text) Then
        MsgBox "Los MB de Datos en Roaming de la bolsa debe ser num�rico"
        fnValidaDatos = False
        Exit Function
    End If
        
    If cbxVisibleIVR.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si la bolsa es Visible en IVR"
        fnValidaDatos = False
        Exit Function
    End If
    
    If cbxVisibleWeb.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si la bolsa es Visible en la Web"
        fnValidaDatos = False
        Exit Function
    End If
        
    If cbxCargaAutoIniMes.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si la bolsa se Carga Autom�ticamente al Inicio del Mes"
        fnValidaDatos = False
        Exit Function
    End If
    
    If cbxCargaBoleta.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar si acepta carga en boleta"
        fnValidaDatos = False
        Exit Function
    End If
    
End Function
