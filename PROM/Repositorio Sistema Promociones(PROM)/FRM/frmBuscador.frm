VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmBuscador 
   Caption         =   "Buscador"
   ClientHeight    =   3825
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6795
   LinkTopic       =   "Form1"
   ScaleHeight     =   3825
   ScaleWidth      =   6795
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   2655
      Left            =   120
      TabIndex        =   5
      Top             =   840
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   4683
      _Version        =   393216
   End
   Begin VB.TextBox txtCodigo 
      Height          =   375
      Left            =   5880
      TabIndex        =   4
      Text            =   "Text2"
      Top             =   720
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox txtFiltro 
      Height          =   375
      Left            =   6000
      TabIndex        =   3
      Text            =   "Text2"
      Top             =   240
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "Buscar"
      Height          =   375
      Left            =   3720
      TabIndex        =   2
      Top             =   240
      Width           =   2055
   End
   Begin VB.TextBox txtBuscar 
      Height          =   285
      Left            =   120
      MaxLength       =   100
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   360
      Width           =   3375
   End
   Begin VB.Label Label5 
      Caption         =   "Texto o C�digo a Buscar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   2535
   End
End
Attribute VB_Name = "frmBuscador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdBuscar_Click()
    Dim sCodi As String, sDesc As String

    Select Case txtFiltro.Text
        Case "R"
            Call BusRentas(txtBuscar.Text)
            With grdDatos
                .Rows = 1
                Do While BusRentasRead(sCodi, sDesc)
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, 0) = " " & sDesc
                    .TextMatrix(.Rows - 1, 1) = sCodi
                Loop
            End With
        Case "F"
            Call BusFaci(txtBuscar.Text)
            With grdDatos
                .Rows = 1
                Do While BusFaciRead(sCodi, sDesc)
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, 0) = " " & sDesc
                    .TextMatrix(.Rows - 1, 1) = sCodi
                Loop
            End With
        Case "L"
            Call BusLocalidad(txtBuscar.Text)
            With grdDatos
                .Rows = 1
                Do While BusLocalidadRead(sCodi, sDesc)
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, 0) = " " & sDesc
                    .TextMatrix(.Rows - 1, 1) = sCodi
                Loop
            End With
    End Select
End Sub

Private Sub Form_Activate()
    Dim sTitulo As String
    grdDatos.Cols = 2
    grdDatos.Rows = 2
    
    Select Case txtFiltro.Text
        Case "R"
            sTitulo = "Renta"
        Case "F"
            sTitulo = "Facilidad"
        Case "L"
            sTitulo = "Localidad"
    End Select
    
    grdDatos.TextMatrix(0, 0) = sTitulo
    grdDatos.TextMatrix(0, 1) = "Codigo"
    
    grdDatos.ColWidth(0) = 6000
    grdDatos.ColWidth(1) = 500
    
    grdDatos.FixedCols = 0
    grdDatos.FixedRows = 1
    grdDatos.Rows = 1
    
    txtCodigo.Text = ""
    txtBuscar.Text = ""
    
    txtBuscar.SetFocus
End Sub

Private Sub grdDatos_DblClick()
    If grdDatos.Row > 0 Then
        txtCodigo.Text = grdDatos.TextMatrix(grdDatos.Row, 1)
        Me.Hide
    End If
End Sub

Private Sub txtBuscar_KeyPress(KEYASCII As Integer)
    If KEYASCII = 13 Then
        Call cmdBuscar_Click
    End If
End Sub
