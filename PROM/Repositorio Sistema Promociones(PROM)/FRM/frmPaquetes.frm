VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmPaquetes 
   Appearance      =   0  'Flat
   Caption         =   "INGRESO Y MODIFICACCION DE PAQUETES"
   ClientHeight    =   10305
   ClientLeft      =   225
   ClientTop       =   570
   ClientWidth     =   16845
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   10305
   ScaleWidth      =   16845
   Tag             =   "FRMPAQUETES"
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab SSTab1 
      Height          =   9135
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   17655
      _ExtentX        =   31141
      _ExtentY        =   16113
      _Version        =   393216
      Style           =   1
      Tabs            =   6
      Tab             =   4
      TabsPerRow      =   6
      TabHeight       =   520
      BackColor       =   -2147483643
      TabCaption(0)   =   "Paquetes"
      TabPicture(0)   =   "frmPaquetes.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cb_alta_inmediata_sva"
      Tab(0).Control(1)=   "cb_es_promo"
      Tab(0).Control(2)=   "cb_habilitado"
      Tab(0).Control(3)=   "li_paquetes"
      Tab(0).Control(4)=   "cb_prioritario"
      Tab(0).Control(5)=   "cb_tipo_segmento"
      Tab(0).Control(6)=   "cb_tipo_producto"
      Tab(0).Control(7)=   "cmdBorrarseleccion"
      Tab(0).Control(8)=   "cmd_listado"
      Tab(0).Control(9)=   "dtFechaFin"
      Tab(0).Control(10)=   "dtFechaInicio"
      Tab(0).Control(11)=   "CommonDialog1"
      Tab(0).Control(12)=   "co_salir"
      Tab(0).Control(13)=   "co_clonar"
      Tab(0).Control(14)=   "cmd_exportar"
      Tab(0).Control(15)=   "cmd_importar"
      Tab(0).Control(16)=   "cb_agrupador"
      Tab(0).Control(17)=   "tx_busca"
      Tab(0).Control(18)=   "co_elimpaq"
      Tab(0).Control(19)=   "co_actupaq"
      Tab(0).Control(20)=   "co_insepaq"
      Tab(0).Control(21)=   "tx_max_venta"
      Tab(0).Control(22)=   "tx_max_equipo"
      Tab(0).Control(23)=   "tx_max_servicio"
      Tab(0).Control(24)=   "tx_max_renta"
      Tab(0).Control(25)=   "tx_desc_paquete"
      Tab(0).Control(26)=   "chk_paq_novigentes"
      Tab(0).Control(27)=   "chk_paq_vigentes"
      Tab(0).Control(28)=   "altaPaquete"
      Tab(0).Control(29)=   "Label38"
      Tab(0).Control(30)=   "Label35"
      Tab(0).Control(31)=   "Label36"
      Tab(0).Control(32)=   "Label34"
      Tab(0).Control(33)=   "Label33"
      Tab(0).Control(34)=   "Label32"
      Tab(0).Control(35)=   "Label9"
      Tab(0).Control(36)=   "Label18"
      Tab(0).Control(37)=   "Label8"
      Tab(0).Control(38)=   "Label7"
      Tab(0).Control(39)=   "Label6"
      Tab(0).Control(40)=   "Label5"
      Tab(0).Control(41)=   "Label4"
      Tab(0).Control(42)=   "Label3"
      Tab(0).Control(43)=   "Label2"
      Tab(0).Control(44)=   "lb_codi_paquete"
      Tab(0).Control(45)=   "Label1"
      Tab(0).ControlCount=   46
      TabCaption(1)   =   "Detalle"
      TabPicture(1)   =   "frmPaquetes.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "dp_fech_termino"
      Tab(1).Control(1)=   "dp_fech_inicio"
      Tab(1).Control(2)=   "tx_desc_elemento"
      Tab(1).Control(3)=   "tx_dias_duracion"
      Tab(1).Control(4)=   "ch_adelanta_dura_vigen"
      Tab(1).Control(5)=   "ch_adelanta_peri_gracia"
      Tab(1).Control(6)=   "cb_area"
      Tab(1).Control(7)=   "cb_flag_obligatorio"
      Tab(1).Control(8)=   "cb_flag_competencia"
      Tab(1).Control(9)=   "tx_interes"
      Tab(1).Control(10)=   "cb_prop_elemento"
      Tab(1).Control(11)=   "co_inseelem"
      Tab(1).Control(12)=   "co_actuelem"
      Tab(1).Control(13)=   "co_elimelem"
      Tab(1).Control(14)=   "tx_peri_cobro"
      Tab(1).Control(15)=   "tx_dura_vigen"
      Tab(1).Control(16)=   "tx_peri_gracia"
      Tab(1).Control(17)=   "cb_tipo_elemento"
      Tab(1).Control(18)=   "tx_codi_elemento"
      Tab(1).Control(19)=   "cb_tipo_elem_rel"
      Tab(1).Control(20)=   "cb_tecnologia"
      Tab(1).Control(21)=   "cb_codi_elem_rel"
      Tab(1).Control(22)=   "cb_tipo_peticion"
      Tab(1).Control(23)=   "li_detalle_paquetes"
      Tab(1).Control(24)=   "Label28"
      Tab(1).Control(25)=   "Label27"
      Tab(1).Control(26)=   "Label26"
      Tab(1).Control(27)=   "Label25"
      Tab(1).Control(28)=   "Label24"
      Tab(1).Control(29)=   "Label23"
      Tab(1).Control(30)=   "Label22"
      Tab(1).Control(31)=   "Label21"
      Tab(1).Control(32)=   "Label17"
      Tab(1).Control(33)=   "Label16"
      Tab(1).Control(34)=   "Label15"
      Tab(1).Control(35)=   "Label14"
      Tab(1).Control(36)=   "Label13"
      Tab(1).Control(37)=   "Label12"
      Tab(1).Control(38)=   "Label11"
      Tab(1).Control(39)=   "lbl_tecnologia"
      Tab(1).Control(40)=   "lbl_tipo_peticion"
      Tab(1).ControlCount=   41
      TabCaption(2)   =   "Localidades"
      TabPicture(2)   =   "frmPaquetes.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmd_desel_localidad_todos"
      Tab(2).Control(1)=   "cmd_sel_localidad_todos"
      Tab(2).Control(2)=   "cmd_act_fecha"
      Tab(2).Control(3)=   "cmd_sel_localidad"
      Tab(2).Control(4)=   "cmd_desel_localidad"
      Tab(2).Control(5)=   "txtBuscarLocalidades"
      Tab(2).Control(6)=   "li_localidades"
      Tab(2).Control(7)=   "dtpFechaFLoc"
      Tab(2).Control(8)=   "dtpFechaILoc"
      Tab(2).Control(9)=   "li_localidades_seleccionadas"
      Tab(2).Control(10)=   "Label29"
      Tab(2).Control(11)=   "lblFechaF"
      Tab(2).Control(12)=   "lblFechaI"
      Tab(2).ControlCount=   13
      TabCaption(3)   =   "Canales de Venta"
      TabPicture(3)   =   "frmPaquetes.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "cmd_sel_canal"
      Tab(3).Control(1)=   "cmd_desel_canal"
      Tab(3).Control(2)=   "txtBuscarCanales"
      Tab(3).Control(3)=   "cmd_sel_canal_todos"
      Tab(3).Control(4)=   "cmd_desel_canal_todos"
      Tab(3).Control(5)=   "li_canales"
      Tab(3).Control(6)=   "li_canales_seleccionados"
      Tab(3).Control(7)=   "Label20"
      Tab(3).ControlCount=   8
      TabCaption(4)   =   "Agrupador"
      TabPicture(4)   =   "frmPaquetes.frx":0070
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "Label31"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "Label30"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "gr_agrupador"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "fr_conceptos_excluyentes"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).Control(4)=   "co_agregar"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).Control(5)=   "co_actualizar"
      Tab(4).Control(5).Enabled=   0   'False
      Tab(4).Control(6)=   "co_eliminar"
      Tab(4).Control(6).Enabled=   0   'False
      Tab(4).Control(7)=   "tx_desc_agrupador"
      Tab(4).Control(7).Enabled=   0   'False
      Tab(4).Control(8)=   "tx_codi_agrupador"
      Tab(4).Control(8).Enabled=   0   'False
      Tab(4).Control(9)=   "Frame1"
      Tab(4).Control(9).Enabled=   0   'False
      Tab(4).ControlCount=   10
      TabCaption(5)   =   "Id Proyecto"
      TabPicture(5)   =   "frmPaquetes.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Frame2"
      Tab(5).ControlCount=   1
      Begin VB.Frame Frame2 
         Caption         =   "Concepto Asociado al Proyecto"
         Height          =   3135
         Left            =   -74760
         TabIndex        =   133
         Top             =   720
         Width           =   9375
         Begin VB.TextBox concepto_proy 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            Height          =   288
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   139
            Top             =   720
            Width           =   1035
         End
         Begin VB.CommandButton actualiza_proy 
            Caption         =   "Actualizar"
            Height          =   375
            Left            =   7920
            TabIndex        =   138
            Tag             =   "AG_MODIFICAR"
            Top             =   1560
            Width           =   1215
         End
         Begin VB.CommandButton elimina_proy 
            Caption         =   "Eliminar"
            Height          =   375
            Left            =   7920
            TabIndex        =   137
            Tag             =   "AG_ELIMINAR"
            Top             =   2040
            Width           =   1215
         End
         Begin VB.CommandButton agrega_proy 
            Caption         =   "Agregar"
            Height          =   375
            Left            =   7920
            TabIndex        =   136
            Tag             =   "AG_AGREGAR"
            Top             =   1080
            Width           =   1215
         End
         Begin VB.TextBox id_proyecto 
            Appearance      =   0  'Flat
            Height          =   288
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   135
            Top             =   360
            Width           =   1035
         End
         Begin VB.TextBox desc_proyecto 
            Appearance      =   0  'Flat
            Height          =   288
            Left            =   3480
            TabIndex        =   134
            Top             =   360
            Width           =   3555
         End
         Begin MSFlexGridLib.MSFlexGrid gr_proyecto 
            Height          =   1935
            Left            =   240
            TabIndex        =   140
            Top             =   1080
            Width           =   7455
            _ExtentX        =   13150
            _ExtentY        =   3413
            _Version        =   393216
            FocusRect       =   0
            SelectionMode   =   1
            AllowUserResizing=   1
            FormatString    =   " ID Proyecto | Descripci�n                                                                 "
         End
         Begin VB.Label Label39 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Concepto"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   240
            TabIndex        =   142
            Top             =   720
            Width           =   825
         End
         Begin VB.Label Label37 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "ID del Proyecto"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   240
            TabIndex        =   141
            Top             =   360
            Width           =   1335
         End
      End
      Begin VB.ComboBox cb_alta_inmediata_sva 
         Height          =   315
         Left            =   -66360
         TabIndex        =   132
         Text            =   "cb_alta_inmediata_sva"
         Top             =   600
         Width           =   975
      End
      Begin VB.ComboBox cb_es_promo 
         Height          =   315
         Left            =   -63000
         TabIndex        =   130
         Text            =   "cb_es_promo"
         Top             =   795
         Width           =   3595
      End
      Begin VB.ComboBox cb_habilitado 
         Height          =   315
         Left            =   -63000
         TabIndex        =   128
         Text            =   "cb_habilitado"
         Top             =   2495
         Width           =   3585
      End
      Begin MSComctlLib.ListView li_paquetes 
         Height          =   5895
         Left            =   -74880
         TabIndex        =   17
         Top             =   3120
         Width           =   15375
         _ExtentX        =   27120
         _ExtentY        =   10398
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   15
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "Number"
            Text            =   "Codigo"
            Object.Width           =   1412
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "Text"
            Text            =   "Descripci�n"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "Date"
            Text            =   "Inicio"
            Object.Width           =   2030
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "Date"
            Text            =   "Fin"
            Object.Width           =   2030
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "Number"
            Text            =   "Max.Venta"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "Number"
            Text            =   "Max.Renta"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "Number"
            Text            =   "Max.Equip"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "Number"
            Text            =   "Max.Serv"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "Combo"
            Text            =   "Agrupa"
            Object.Width           =   1588
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "Combo"
            Text            =   "Tipo Producto"
            Object.Width           =   1588
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "Combo"
            Text            =   "Segmento"
            Object.Width           =   1588
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "Combo"
            Text            =   "Prioridad"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "Combo"
            Text            =   "Habilitado"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "Combo"
            Text            =   "Promo"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Object.Tag             =   "Combo"
            Text            =   "Alta Inmediata"
            Object.Width           =   1940
         EndProperty
      End
      Begin VB.ComboBox cb_prioritario 
         Height          =   315
         Left            =   -63000
         TabIndex        =   126
         Text            =   "cb_prioritario"
         Top             =   1995
         Width           =   3585
      End
      Begin VB.ComboBox cb_tipo_segmento 
         Height          =   315
         Left            =   -63000
         TabIndex        =   123
         Text            =   "cb_tipo_segmento"
         Top             =   1200
         Width           =   3585
      End
      Begin VB.CommandButton cmd_desel_localidad_todos 
         Caption         =   "<<"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   121
         Tag             =   "LO_ELIMINAR"
         Top             =   5160
         Width           =   615
      End
      Begin VB.CommandButton cmd_sel_localidad_todos 
         Caption         =   ">>"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   120
         Tag             =   "LO_AGREGAR"
         Top             =   4320
         Width           =   615
      End
      Begin VB.ComboBox cb_tipo_producto 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -63000
         Style           =   2  'Dropdown List
         TabIndex        =   118
         Top             =   1605
         Width           =   3585
      End
      Begin VB.CommandButton cmd_sel_canal 
         Caption         =   ">"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   115
         Tag             =   "LO_AGREGAR"
         Top             =   3120
         Width           =   615
      End
      Begin VB.CommandButton cmd_desel_canal 
         Caption         =   "<"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   114
         Tag             =   "LO_ELIMINAR"
         Top             =   3840
         Width           =   615
      End
      Begin VB.TextBox txtBuscarCanales 
         Height          =   285
         Left            =   -74760
         TabIndex        =   113
         Top             =   840
         Width           =   5055
      End
      Begin VB.CommandButton cmd_sel_canal_todos 
         Caption         =   ">>"
         Height          =   255
         Left            =   -67320
         TabIndex        =   111
         Tag             =   "SE_AGREGAR_TODO"
         Top             =   3480
         Width           =   615
      End
      Begin VB.CommandButton cmd_desel_canal_todos 
         Caption         =   "<<"
         Height          =   255
         Left            =   -67320
         TabIndex        =   110
         Tag             =   "SE_ELIMINAR_TODO"
         Top             =   4200
         Width           =   615
      End
      Begin VB.Frame Frame1 
         Caption         =   "Clase de Peticiones para Agrupador:"
         Height          =   5175
         Left            =   240
         TabIndex        =   100
         Top             =   3720
         Width           =   17175
         Begin VB.CommandButton cmd_selecciona 
            Caption         =   ">"
            Enabled         =   0   'False
            Height          =   255
            Left            =   7560
            TabIndex        =   104
            Tag             =   "SE_AGREGAR"
            Top             =   1320
            Width           =   615
         End
         Begin VB.CommandButton cmd_selecciona_todos 
            Caption         =   ">>"
            Height          =   255
            Left            =   7560
            TabIndex        =   103
            Tag             =   "SE_AGREGAR_TODO"
            Top             =   1560
            Width           =   615
         End
         Begin VB.CommandButton cmd_deselecciona 
            Caption         =   "<"
            Enabled         =   0   'False
            Height          =   255
            Left            =   7560
            TabIndex        =   102
            Tag             =   "SE_ELIMINAR"
            Top             =   1800
            Width           =   615
         End
         Begin VB.CommandButton cmd_deselecciona_todos 
            Caption         =   "<<"
            Height          =   255
            Left            =   7560
            TabIndex        =   101
            Tag             =   "SE_ELIMINAR_TODO"
            Top             =   2040
            Width           =   615
         End
         Begin MSComctlLib.ListView li_seleccionados 
            Height          =   4695
            Left            =   8520
            TabIndex        =   105
            Top             =   360
            Width           =   8415
            _ExtentX        =   14843
            _ExtentY        =   8281
            View            =   3
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Tipo"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Clas"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Subclase"
               Object.Width           =   4304
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Clase"
               Object.Width           =   4304
            EndProperty
         End
         Begin MSComctlLib.ListView li_disponibles 
            Height          =   4695
            Left            =   120
            TabIndex        =   106
            Top             =   360
            Width           =   7095
            _ExtentX        =   12515
            _ExtentY        =   8281
            View            =   3
            Arrange         =   1
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Tipo"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Clas"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Subclase"
               Object.Width           =   4304
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Clase"
               Object.Width           =   4304
            EndProperty
         End
      End
      Begin VB.TextBox tx_codi_agrupador 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1440
         TabIndex        =   99
         Top             =   660
         Width           =   3255
      End
      Begin VB.TextBox tx_desc_agrupador 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1440
         TabIndex        =   98
         Top             =   1020
         Width           =   3255
      End
      Begin VB.CommandButton co_eliminar 
         Caption         =   "Eliminar"
         Height          =   375
         Left            =   6360
         TabIndex        =   97
         Tag             =   "AG_ELIMINAR"
         Top             =   2340
         Width           =   1215
      End
      Begin VB.CommandButton co_actualizar 
         Caption         =   "Actualizar"
         Height          =   375
         Left            =   6360
         TabIndex        =   96
         Tag             =   "AG_MODIFICAR"
         Top             =   1860
         Width           =   1215
      End
      Begin VB.CommandButton co_agregar 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   6360
         TabIndex        =   95
         Tag             =   "AG_AGREGAR"
         Top             =   1380
         Width           =   1215
      End
      Begin VB.Frame fr_conceptos_excluyentes 
         Caption         =   "Conceptos Mutuamente Excluyentes"
         Height          =   3135
         Left            =   7920
         TabIndex        =   85
         Top             =   480
         Width           =   9375
         Begin VB.TextBox tx_desc_concepto 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            Height          =   288
            Left            =   3480
            Locked          =   -1  'True
            TabIndex        =   91
            Top             =   360
            Width           =   3555
         End
         Begin VB.TextBox tx_codi_concepto 
            Appearance      =   0  'Flat
            Height          =   288
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   90
            Top             =   360
            Width           =   1035
         End
         Begin VB.CommandButton co_agregar_concepto 
            Caption         =   "Agregar"
            Height          =   375
            Left            =   7920
            TabIndex        =   89
            Tag             =   "AG_AGREGAR"
            Top             =   1080
            Width           =   1215
         End
         Begin VB.CommandButton co_eliminar_concepto 
            Caption         =   "Eliminar"
            Height          =   375
            Left            =   7920
            TabIndex        =   88
            Tag             =   "AG_ELIMINAR"
            Top             =   2040
            Width           =   1215
         End
         Begin VB.CommandButton co_actualizar_concepto 
            Caption         =   "Actualizar"
            Height          =   375
            Left            =   7920
            TabIndex        =   87
            Tag             =   "AG_MODIFICAR"
            Top             =   1560
            Width           =   1215
         End
         Begin VB.TextBox tx_prioridad 
            Appearance      =   0  'Flat
            Height          =   288
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   86
            Top             =   720
            Width           =   1035
         End
         Begin MSFlexGridLib.MSFlexGrid gr_conceptos 
            Height          =   1935
            Left            =   240
            TabIndex        =   92
            Top             =   1080
            Width           =   7455
            _ExtentX        =   13150
            _ExtentY        =   3413
            _Version        =   393216
            Cols            =   4
            FocusRect       =   0
            SelectionMode   =   1
            AllowUserResizing=   1
            FormatString    =   "Item | Codigo | Descripcion | Prioridad"
         End
         Begin VB.Label Label10 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "C�digo Concepto (F5):"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   240
            TabIndex        =   94
            Top             =   360
            Width           =   1920
         End
         Begin VB.Label Label19 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Prioridad:"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   240
            TabIndex        =   93
            Top             =   720
            Width           =   825
         End
      End
      Begin VB.CommandButton cmdBorrarseleccion 
         Caption         =   "Borrar Seleccion"
         Height          =   495
         Left            =   -59160
         TabIndex        =   84
         Top             =   7380
         Width           =   1245
      End
      Begin VB.CommandButton cmd_act_fecha 
         Caption         =   "Actualiza Fecha"
         Enabled         =   0   'False
         Height          =   375
         Left            =   -61080
         TabIndex        =   83
         Tag             =   "LO_ACTUALIZA"
         Top             =   1740
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker dp_fech_termino 
         Height          =   375
         Left            =   -65280
         TabIndex        =   82
         Top             =   2220
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   111411201
         CurrentDate     =   42338
      End
      Begin MSComCtl2.DTPicker dp_fech_inicio 
         Height          =   375
         Left            =   -65280
         TabIndex        =   81
         Top             =   1740
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   111411201
         CurrentDate     =   42338
      End
      Begin VB.TextBox tx_desc_elemento 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   288
         Left            =   -71160
         Locked          =   -1  'True
         TabIndex        =   80
         Top             =   1380
         Width           =   3555
      End
      Begin VB.CommandButton cmd_listado 
         Caption         =   "Listado"
         Height          =   375
         Left            =   -59160
         TabIndex        =   79
         Tag             =   "PA_LISTADO"
         Top             =   6660
         Width           =   1245
      End
      Begin VB.CommandButton cmd_sel_localidad 
         Caption         =   ">"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   78
         Tag             =   "LO_AGREGAR"
         Top             =   4020
         Width           =   615
      End
      Begin VB.CommandButton cmd_desel_localidad 
         Caption         =   "<"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   77
         Tag             =   "LO_ELIMINAR"
         Top             =   4740
         Width           =   615
      End
      Begin MSComCtl2.DTPicker dtFechaFin 
         Height          =   375
         Left            =   -73560
         TabIndex        =   75
         Top             =   2220
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   111411201
         CurrentDate     =   42332
      End
      Begin MSComCtl2.DTPicker dtFechaInicio 
         Height          =   375
         Left            =   -73560
         TabIndex        =   74
         Top             =   1740
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         Format          =   111411201
         CurrentDate     =   42332
      End
      Begin VB.TextBox txtBuscarLocalidades 
         Height          =   285
         Left            =   -74760
         TabIndex        =   3
         Top             =   1740
         Width           =   5055
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   -58320
         Top             =   8220
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.TextBox tx_dias_duracion 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -65280
         MaxLength       =   4
         TabIndex        =   54
         Top             =   900
         Width           =   960
      End
      Begin VB.CheckBox ch_adelanta_dura_vigen 
         Appearance      =   0  'Flat
         Caption         =   "Adelantar"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -71280
         TabIndex        =   53
         ToolTipText     =   "Se adelanta la fecha en la que se determina que se debe terminar de cobrar el concepto, al �ltimo d�a del mes anterior a ese mes."
         Top             =   3300
         Width           =   1335
      End
      Begin VB.CheckBox ch_adelanta_peri_gracia 
         Appearance      =   0  'Flat
         Caption         =   "Adelantar"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -71280
         TabIndex        =   52
         ToolTipText     =   "Se adelanta la fecha en la que se determina que se debe comenzar a cobrar el concepto, al primer d�a de ese mes."
         Top             =   2820
         Width           =   1335
      End
      Begin VB.ComboBox cb_area 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -61680
         Style           =   2  'Dropdown List
         TabIndex        =   51
         Top             =   2340
         Width           =   2475
      End
      Begin VB.ComboBox cb_flag_obligatorio 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -61680
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   1380
         Width           =   1425
      End
      Begin VB.ComboBox cb_flag_competencia 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -61680
         Style           =   2  'Dropdown List
         TabIndex        =   49
         Top             =   900
         Width           =   1425
      End
      Begin VB.TextBox tx_interes 
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   -65280
         MaxLength       =   10
         TabIndex        =   48
         Top             =   2700
         Width           =   1155
      End
      Begin VB.ComboBox cb_prop_elemento 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -61680
         Style           =   2  'Dropdown List
         TabIndex        =   47
         Top             =   1860
         Width           =   2475
      End
      Begin VB.CommandButton co_inseelem 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Agregar"
         Height          =   375
         Left            =   -58920
         TabIndex        =   46
         Tag             =   "DPA_AGREGAR"
         Top             =   3780
         Width           =   1245
      End
      Begin VB.CommandButton co_actuelem 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Modificar"
         Height          =   375
         Left            =   -58920
         TabIndex        =   45
         Tag             =   "DPA_MODIFICAR"
         Top             =   4380
         Width           =   1245
      End
      Begin VB.CommandButton co_elimelem 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Eliminar"
         Height          =   375
         Left            =   -58920
         TabIndex        =   44
         Tag             =   "DPA_ELIMINAR"
         Top             =   4980
         Width           =   1245
      End
      Begin VB.TextBox tx_peri_cobro 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -65280
         MaxLength       =   8
         TabIndex        =   43
         Top             =   1380
         Width           =   960
      End
      Begin VB.TextBox tx_dura_vigen 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -72240
         MaxLength       =   14
         TabIndex        =   42
         Top             =   3300
         Width           =   840
      End
      Begin VB.TextBox tx_peri_gracia 
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   -72240
         MaxLength       =   10
         TabIndex        =   41
         Top             =   2820
         Width           =   840
      End
      Begin VB.ComboBox cb_tipo_elemento 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -72240
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   900
         Width           =   2568
      End
      Begin VB.TextBox tx_codi_elemento 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -72240
         MaxLength       =   10
         TabIndex        =   39
         Top             =   1380
         Width           =   1035
      End
      Begin VB.ComboBox cb_tipo_elem_rel 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -72240
         Style           =   2  'Dropdown List
         TabIndex        =   38
         Top             =   1860
         Width           =   2568
      End
      Begin VB.ComboBox cb_tecnologia 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -61680
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   2820
         Width           =   2475
      End
      Begin VB.ComboBox cb_codi_elem_rel 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -72240
         Style           =   2  'Dropdown List
         TabIndex        =   36
         Top             =   2340
         Width           =   2568
      End
      Begin VB.ComboBox cb_tipo_peticion 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "frmPaquetes.frx":00A8
         Left            =   -61680
         List            =   "frmPaquetes.frx":00AA
         TabIndex        =   35
         Top             =   3300
         Width           =   2475
      End
      Begin VB.CommandButton co_salir 
         Caption         =   "Salir"
         Height          =   375
         Left            =   -59160
         TabIndex        =   34
         Top             =   1080
         Width           =   1245
      End
      Begin VB.CommandButton co_clonar 
         Caption         =   "Copiar"
         Height          =   375
         Left            =   -59160
         TabIndex        =   33
         Tag             =   "PA_COPIAR"
         Top             =   4800
         Width           =   1245
      End
      Begin VB.CommandButton cmd_exportar 
         Caption         =   "Exportar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   -59160
         TabIndex        =   32
         Tag             =   "PA_EXPORTAR"
         Top             =   5400
         Width           =   1245
      End
      Begin VB.CommandButton cmd_importar 
         Caption         =   "Importar"
         Height          =   375
         Left            =   -59160
         TabIndex        =   31
         Tag             =   "PA_IMPORTAR"
         Top             =   6000
         Width           =   1245
      End
      Begin VB.ComboBox cb_agrupador 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -68400
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   2640
         Width           =   3585
      End
      Begin VB.TextBox tx_busca 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -73560
         MaxLength       =   100
         TabIndex        =   15
         Top             =   2640
         Width           =   3225
      End
      Begin VB.CommandButton co_elimpaq 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Eliminar"
         Height          =   375
         Left            =   -59175
         TabIndex        =   14
         Tag             =   "PA_ELIMINAR"
         Top             =   4200
         Width           =   1245
      End
      Begin VB.CommandButton co_actupaq 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Modificar"
         Height          =   375
         Left            =   -59175
         TabIndex        =   13
         Tag             =   "PA_MODIFICAR"
         Top             =   3600
         Width           =   1245
      End
      Begin VB.CommandButton co_insepaq 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Agregar"
         Height          =   375
         Left            =   -59160
         TabIndex        =   12
         Tag             =   "PA_AGREGAR"
         Top             =   3000
         Width           =   1245
      End
      Begin VB.TextBox tx_max_venta 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -65940
         MaxLength       =   4
         TabIndex        =   11
         Top             =   1545
         Width           =   510
      End
      Begin VB.TextBox tx_max_equipo 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -65940
         MaxLength       =   4
         TabIndex        =   10
         Top             =   1830
         Width           =   510
      End
      Begin VB.TextBox tx_max_servicio 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -65940
         MaxLength       =   4
         TabIndex        =   9
         Top             =   2130
         Width           =   510
      End
      Begin VB.TextBox tx_max_renta 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -65940
         MaxLength       =   4
         TabIndex        =   8
         Top             =   1260
         Width           =   510
      End
      Begin VB.TextBox tx_desc_paquete 
         Appearance      =   0  'Flat
         Height          =   288
         Left            =   -73575
         MaxLength       =   100
         TabIndex        =   7
         Top             =   1380
         Width           =   4260
      End
      Begin VB.CheckBox chk_paq_novigentes 
         Caption         =   "No Vigentes"
         Height          =   255
         Left            =   -70680
         TabIndex        =   6
         Top             =   960
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin VB.CheckBox chk_paq_vigentes 
         Caption         =   "Vigentes"
         Height          =   255
         Left            =   -72000
         TabIndex        =   5
         Top             =   960
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin MSComctlLib.ListView li_localidades 
         Height          =   6735
         Left            =   -74760
         TabIndex        =   4
         Top             =   2160
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   11880
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Localidad"
            Object.Width           =   4763
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Fecha Inicio"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha Fin"
            Object.Width           =   2646
         EndProperty
      End
      Begin MSComCtl2.DTPicker dtpFechaFLoc 
         Height          =   375
         Left            =   -72960
         TabIndex        =   2
         Top             =   1020
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   111411201
         CurrentDate     =   38915
      End
      Begin MSComCtl2.DTPicker dtpFechaILoc 
         Height          =   375
         Left            =   -74760
         TabIndex        =   1
         Top             =   1020
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   111411201
         CurrentDate     =   38915
      End
      Begin MSComctlLib.ListView li_detalle_paquetes 
         Height          =   5175
         Left            =   -74880
         TabIndex        =   72
         Top             =   3780
         Width           =   15735
         _ExtentX        =   27755
         _ExtentY        =   9128
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   22
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Codigo"
            Object.Width           =   1412
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tipo"
            Object.Width           =   1060
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Descripci�n"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Gracia"
            Object.Width           =   1524
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Duraci�n (m)"
            Object.Width           =   2028
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Periodicidad"
            Object.Width           =   1651
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Propiedad"
            Object.Width           =   1651
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Interes"
            Object.Width           =   1651
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Competencia"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Obligatorio"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Fech Gracia"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "Area"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "Ad.Gracia"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "Ad.Duraci�n"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Text            =   "Duraci�n (d)"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Text            =   "Fecha Inicio"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Text            =   "Fecha Termino"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Text            =   "Tipo Elem Relacionado"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   18
            Text            =   "Elemento relacionado"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Text            =   "Tecnolog�a"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   20
            Text            =   "Tipo Peticion"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   21
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.ListView li_localidades_seleccionadas 
         Height          =   6735
         Left            =   -66480
         TabIndex        =   76
         Top             =   2220
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   11880
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Localidad"
            Object.Width           =   4763
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Fecha Inicio"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha Fin"
            Object.Width           =   2646
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid gr_agrupador 
         Height          =   2175
         Left            =   240
         TabIndex        =   107
         Top             =   1380
         Width           =   6015
         _ExtentX        =   10610
         _ExtentY        =   3836
         _Version        =   393216
         FocusRect       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         FormatString    =   "Codigo | Agrupador"
      End
      Begin MSComctlLib.ListView li_canales 
         Height          =   6735
         Left            =   -74760
         TabIndex        =   112
         Top             =   1320
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   11880
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Canal de Venta"
            Object.Width           =   4763
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1764
         EndProperty
      End
      Begin MSComctlLib.ListView li_canales_seleccionados 
         Height          =   6735
         Left            =   -66480
         TabIndex        =   116
         Top             =   1320
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   11880
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Canal de Venta"
            Object.Width           =   4763
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1764
         EndProperty
      End
      Begin VB.Label altaPaquete 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Alta Inmediata SVA:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -68400
         TabIndex        =   131
         Top             =   600
         Width           =   1710
      End
      Begin VB.Label Label38 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Promoci�n:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   -64440
         TabIndex        =   129
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label35 
         Caption         =   "Habilitado Oferta:"
         ForeColor       =   &H00C00000&
         Height          =   495
         Left            =   -64425
         TabIndex        =   127
         Top             =   2450
         Width           =   1215
      End
      Begin VB.Label Label36 
         Caption         =   "Prioritario:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   -64425
         TabIndex        =   125
         Top             =   2060
         Width           =   1215
      End
      Begin VB.Label Label34 
         Caption         =   "(Solo para SVA)"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -59280
         TabIndex        =   124
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label Label33 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo Segmento:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -64440
         TabIndex        =   122
         Top             =   1280
         Width           =   1350
      End
      Begin VB.Label Label32 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo Producto:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -64440
         TabIndex        =   119
         Top             =   1680
         Width           =   1275
      End
      Begin VB.Label Label20 
         Caption         =   "Buscar"
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   -74760
         TabIndex        =   117
         Top             =   600
         Width           =   1815
      End
      Begin VB.Label Label30 
         Caption         =   "Descripci�n"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   240
         TabIndex        =   109
         Top             =   1020
         Width           =   1095
      End
      Begin VB.Label Label31 
         Caption         =   "Codigo:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   240
         TabIndex        =   108
         Top             =   660
         Width           =   1095
      End
      Begin VB.Label Label29 
         Caption         =   "Buscar"
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   -74760
         TabIndex        =   73
         Top             =   1500
         Width           =   1815
      End
      Begin VB.Label Label28 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha Termino :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -67320
         TabIndex        =   71
         Top             =   2340
         Width           =   1395
      End
      Begin VB.Label Label27 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha Inicio :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -67320
         TabIndex        =   70
         Top             =   1800
         Width           =   1185
      End
      Begin VB.Label Label26 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Duraci�n (d�as) :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -67320
         TabIndex        =   69
         Top             =   1020
         Width           =   1455
      End
      Begin VB.Label Label25 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Area Tarifaria :"
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   -63120
         TabIndex        =   68
         Top             =   2340
         Width           =   1335
      End
      Begin VB.Label Label24 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Obligatorio :"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   -63120
         TabIndex        =   67
         Top             =   1380
         Width           =   1050
      End
      Begin VB.Label Label23 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Competencia :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -63120
         TabIndex        =   66
         Top             =   1020
         Width           =   1230
      End
      Begin VB.Label Label22 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Interes :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -67320
         TabIndex        =   65
         Top             =   2820
         Width           =   705
      End
      Begin VB.Label Label21 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Propiedad :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -63120
         TabIndex        =   64
         Top             =   1860
         Width           =   990
      End
      Begin VB.Label Label17 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Periodicidad de Cobro :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -67320
         TabIndex        =   63
         Top             =   1440
         Width           =   1995
      End
      Begin VB.Label Label16 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Periodo Gracia :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74880
         TabIndex        =   62
         Top             =   2820
         Width           =   1365
      End
      Begin VB.Label Label15 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Duraci�n (meses) :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74880
         TabIndex        =   61
         Top             =   3300
         Width           =   1605
      End
      Begin VB.Label Label14 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo Elemento :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74880
         TabIndex        =   60
         Top             =   1020
         Width           =   1320
      End
      Begin VB.Label Label13 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo Elemento (F5):"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74880
         TabIndex        =   59
         Top             =   1380
         Width           =   1980
      End
      Begin VB.Label Label12 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo Elemento Relacionado:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74880
         TabIndex        =   58
         Top             =   1860
         Width           =   2490
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo Elemento Relacionado:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74880
         TabIndex        =   57
         Top             =   2340
         Width           =   2535
      End
      Begin VB.Label lbl_tecnologia 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tecnolog�a:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -63120
         TabIndex        =   56
         Top             =   2820
         Width           =   1095
      End
      Begin VB.Label lbl_tipo_peticion 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo Petici�n:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -63120
         TabIndex        =   55
         Top             =   3300
         Width           =   1215
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Agrupador:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -69480
         TabIndex        =   30
         Top             =   2640
         Width           =   945
      End
      Begin VB.Label Label18 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Buscar:"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   -74400
         TabIndex        =   29
         Top             =   2640
         Width           =   660
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nmro M�ximo de Cargos :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -68355
         TabIndex        =   28
         Top             =   1610
         Width           =   2025
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nmro M�ximo de Equipos :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -68355
         TabIndex        =   27
         Top             =   1910
         Width           =   2115
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nmro M�ximo de Servicios :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -68355
         TabIndex        =   26
         Top             =   2210
         Width           =   2205
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nmro M�ximo de Rentas :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -68355
         TabIndex        =   25
         Top             =   1310
         Width           =   2250
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha Fin :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74535
         TabIndex        =   24
         Top             =   2280
         Width           =   930
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha Inicio :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74760
         TabIndex        =   23
         Top             =   1860
         Width           =   1110
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74730
         TabIndex        =   22
         Top             =   1500
         Width           =   1110
      End
      Begin VB.Label lb_codi_paquete 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   -73575
         TabIndex        =   21
         Top             =   1020
         Width           =   1230
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo :"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -74325
         TabIndex        =   20
         Top             =   1140
         Width           =   705
      End
      Begin VB.Label lblFechaF 
         Caption         =   "Fecha Fin"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -72960
         TabIndex        =   19
         Top             =   780
         Width           =   975
      End
      Begin VB.Label lblFechaI 
         Caption         =   "Fecha Inicio"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -74760
         TabIndex        =   18
         Top             =   780
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmPaquetes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub actualiza_proy_Click()

Dim iFila As Integer, sRespuesta As String
Dim resp As String

resp = MsgBox("�Esta seguro de actulaizar el proyecto?", vbYesNo)
    
If resp = vbYes Then

    iFila = gr_proyecto.Row
    If desc_proyecto.Text = "" Then
     MsgBox ("Debe ingresar descripci�n de Proyecto")
     Exit Sub
     
    End If
    

    Call ModificaProyecto(Val(lb_codi_paquete.Caption), id_proyecto.Text, desc_proyecto.Text, concepto_proy.Text, sRespuesta)
    Call suLlenaGrillaProyectos(gr_proyecto)
    id_proyecto.Text = ""
    desc_proyecto.Text = ""
   ' concepto_proy.Text = ""
    id_proyecto.Enabled = True
    agrega_proy.Enabled = True
Else
End If

End Sub


Private Sub elimina_proy_Click()
    Dim iFila As Integer, sRespuesta As String
    Dim resp As String

resp = MsgBox("�Esta seguro de eliminar el proyecto?", vbYesNo)
    
If resp = vbYes Then
    iFila = gr_proyecto.Row

    Call EliminaProyecto(Val(lb_codi_paquete.Caption), gr_proyecto.TextMatrix(gr_proyecto.Row, 0), sRespuesta)

    gr_proyecto.Row = iFila
    gr_proyecto.RowSel = iFila
    gr_proyecto.TopRow = iFila
    Call suLlenaGrillaProyectos(gr_proyecto)
    id_proyecto.Text = ""
    desc_proyecto.Text = ""
   ' concepto_proy.Text = ""
    agrega_proy.Enabled = True
    id_proyecto.Enabled = True
Else
End If
End Sub

Private Sub agrega_proy_Click()
    Dim sRespuesta As String
    Dim Ret As String
    Dim resp As String

id_proyecto.Enabled = True
resp = MsgBox("�Esta seguro de agregar nuevo proyecto?", vbYesNo)
    
If resp = vbYes Then

If id_proyecto.Text = "" Then
    MsgBox ("Debe ingresar Id de Proyecto")
    Exit Sub

End If
             
If desc_proyecto.Text = "" Then
     MsgBox ("Debe ingresar descripci�n de Proyecto")
     Exit Sub
     
End If



    Ret = IngresaProyecto(Val(lb_codi_paquete.Caption), id_proyecto.Text, desc_proyecto.Text, _
                                      concepto_proy.Text, sRespuesta)

    Call suLlenaGrillaProyectos(gr_proyecto)
    id_proyecto.Text = ""
    desc_proyecto.Text = ""
   ' concepto_proy.Text = ""

Else
End If
End Sub

Private Sub cmd_desel_localidad_todos_Click()
    Dim i As Long, sFecha As String, iCant As Long
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_localidades_seleccionadas.ListItems.Count
        Call EliminaLocaPaquete(Val(lb_codi_paquete.Caption), li_localidades_seleccionadas.ListItems(i).SubItems(1))
    Next

    Call MostrarLocalidades(Val(lb_codi_paquete.Caption))
End Sub

Private Sub cmd_importar_Click()

On Error GoTo ErrHandler
    Dim xlApp As Object
    Dim xlWB As Object
    Dim xlWS1 As Object
    Dim xlWS2 As Object
    Dim xlWS3 As Object
    Dim xlWS4 As Object
    Dim Range As Object
    Dim i As Long, j As Long, id_elemento As Long
    Dim sRespuesta As String
    Dim CodiPaquete As Long
    
    Dim Ret As Integer
    
    If gfnPedirNombreArchivo(CommonDialog1) Then
        Set xlApp = CreateObject("Excel.Application")
        xlApp.Visible = False
        Set xlWB = xlApp.Workbooks.Open(CommonDialog1.FileName)
        Set xlWS1 = xlWB.Worksheets("Paquetes")
        Set xlWS2 = xlWB.Worksheets("Detalle de Paquetes")
        Set xlWS3 = xlWB.Worksheets("Localidades")
        Set xlWS4 = xlWB.Worksheets("Canales de Venta")
        
        Set Range = xlWS1.UsedRange
        
        GPBase.BeginTrans
        
        For i = 2 To Range.Rows.Count
             If Range.Cells(i, 1) = "" Then
                 Exit For
             End If
            
             Ret = ActualizaPaquete(Val(Range.Cells(i, 1)), _
                                      Trim(Range.Cells(i, 2)), _
                                      Format(Trim(Range.Cells(i, 3)), "dd-mm-yyyy"), _
                                      Format(Trim(Range.Cells(i, 4)), "dd-mm-yyyy"), _
                                      Val(Range.Cells(i, 5)), _
                                      Val(Range.Cells(i, 6)), _
                                      Val(Range.Cells(i, 7)), _
                                      Val(Range.Cells(i, 8)), _
                                      Trim(Range.Cells(i, 9)), _
                                      Val(Trim(Range.Cells(i, 10))), _
                                      Val(Trim(Range.Cells(i, 11))), _
                                      Trim(Range.Cells(i, 12)), _
                                      Trim(Range.Cells(i, 13)), _
                                      Trim(Range.Cells(i, 14)), _
                                      Trim(Range.Cells(i, 15)), _
                                      sRespuesta)
                                      
            If sRespuesta <> "OK" Then
                MsgBox "En registro " + Str(i) + ":" + sRespuesta + " (Paquete:" + Trim(Range.Cells(i, 1)) + ")", vbCritical, "Error en Detalle de Paquetes"
                xlApp.Workbooks.Close
                GPBase.RollbackTrans
                Exit Sub
            End If
            
        Next
        
        Set Range = xlWS2.UsedRange
        CodiPaquete = 0
        For i = 2 To Range.Rows.Count
             If Range.Cells(i, 1) = "" Then
                 Exit For
             End If
             
             If Val(Range.Cells(i, 1)) <> CodiPaquete Then
                 Ret = Srv_Siad_DetaPaq01(Range.Cells(i, 1), "", "")
                 CodiPaquete = Val(Range.Cells(i, 1))
             End If
             
             Ret = IngresaDetallePaquete(Val(Range.Cells(i, 1)), _
                                      Trim(Range.Cells(i, 3)), _
                                      Trim(Range.Cells(i, 2)), _
                                      Val(Range.Cells(i, 5)), _
                                      Val(Range.Cells(i, 6)), _
                                      Val(Range.Cells(i, 7)), _
                                      Range.Cells(i, 8), _
                                      Val(Range.Cells(i, 9)), _
                                      Trim(Range.Cells(i, 10)), _
                                      Range.Cells(i, 11), _
                                      "", _
                                      Trim(Range.Cells(i, 13)), _
                                      Trim(Range.Cells(i, 14)), _
                                      Trim(Range.Cells(i, 15)), _
                                      Val(Range.Cells(i, 16)), _
                                      Format(Trim(Range.Cells(i, 17)), "dd-mm-yyyy"), _
                                      Format(Trim(Range.Cells(i, 18)), "dd-mm-yyyy"), _
                                      Trim(Range.Cells(i, 19)), _
                                      Trim(Range.Cells(i, 20)), _
                                      Trim(Range.Cells(i, 21)), _
                                      Trim(Range.Cells(i, 22)), _
                                      id_elemento, _
                                      sRespuesta)
                                      
            If sRespuesta <> "OK" Then
                MsgBox "En registro " + Str(i) + ":" + sRespuesta + " (Paquete:" + Trim(Range.Cells(i, 1)) + ")", vbCritical, "Error en Detalle de Paquetes"
                xlApp.Workbooks.Close
                GPBase.RollbackTrans
                Exit Sub
            End If
            
        Next
        
        Set Range = xlWS3.UsedRange
        CodiPaquete = 0
        For i = 2 To Range.Rows.Count
             
             If Range.Cells(i, 1) = "" Then
                 Exit For
             End If
             
             If Val(Range.Cells(i, 1)) <> CodiPaquete Then
                 CodiPaquete = Val(Range.Cells(i, 1))
                 
                 Ret = EliminaLocaPaquete(CodiPaquete, "")

             End If
        
             Ret = IngresaLocaPaquete(CodiPaquete, _
                                      Trim(Range.Cells(i, 3)), _
                                      "", _
                                      Format(Trim(Range.Cells(i, 4)), "dd-mm-yyyy"), _
                                      Format(Trim(Range.Cells(i, 5)), "dd-mm-yyyy"), _
                                      sRespuesta)
         
             If sRespuesta <> "OK" Then
                MsgBox "En registro " + Str(i) + ":" + sRespuesta + " (Paquete:" + Trim(Range.Cells(i, 1)) + ")", vbCritical, "Error en Localidades"
                xlApp.Workbooks.Close
                GPBase.RollbackTrans
                Exit Sub
             End If
        Next
       
        Set Range = xlWS4.UsedRange
        CodiPaquete = 0
        For i = 2 To Range.Rows.Count
             
             If Range.Cells(i, 1) = "" Then
                 Exit For
             End If
             
             If Val(Range.Cells(i, 1)) <> CodiPaquete Then
                 CodiPaquete = Val(Range.Cells(i, 1))
                 Ret = EliminaCanalPaquete(CodiPaquete, "")
             End If
        
             Ret = IngresaCanalPaquete(CodiPaquete, _
                                      Trim(Range.Cells(i, 3)), _
                                      sRespuesta)
         
             If sRespuesta <> "OK" Then
                MsgBox "En registro " + Str(i) + ":" + sRespuesta + " (Paquete:" + Trim(Range.Cells(i, 1)) + ")", vbCritical, "Error en Canles de Venta"
                xlApp.Workbooks.Close
                GPBase.RollbackTrans
                Exit Sub
             End If
        Next
       
    End If
    
    xlApp.Workbooks.Close
    GPBase.CommitTrans
    
    MsgBox "Proceso de Import Exitoso", vbInformation, "Informacion"
    
    Call suLlenaPaquetes

    Exit Sub
    
ErrHandler:
    xlApp.Workbooks.Close
    GPBase.RollbackTrans
    MsgBox Err.Description
    MsgBox "Revise el formato de la planilla Excel!", vbCritical, "Error!"

End Sub

Private Sub cmd_listado_Click()
    Dim vsSeparador As String
    
    Dim iFila As Long ' counter
    Dim iColumna As Long ' counter
    Dim xl As Object
    Dim wbk As Object
    
    Dim ws1 As Object
    
    Dim i As Long
    Dim j As Long
    Dim df As Long, dc As Long
    Dim k As Long, l As Long
    
    Dim ldf As Long, ldc As Long
    Dim lk As Long, ll As Long
    
    Dim Total As Long

    vsSeparador = fnSeparadorListas
    
    Set xl = CreateObject("Excel.Application")

    xl.Visible = False
    Set wbk = xl.Workbooks.Add
    Set ws1 = wbk.Worksheets(1)
    ws1.Name = "Paquetes"
    

    iFila = 1
            
    For j = 1 To li_paquetes.ColumnHeaders.Count
        ws1.Cells(iFila, j).Font.Bold = True
        ws1.Cells(iFila, j).Font.Color = vbBlack
        ws1.Cells(iFila, j).Interior.Color = &HDFBF91
        ws1.Cells(iFila, j).Value = li_paquetes.ColumnHeaders(j).Text
    Next j
    
    Screen.MousePointer = vbHourglass
    
    k = 1
    lk = 1
    For i = 1 To li_paquetes.ListItems.Count
        iFila = iFila + 1
        For iColumna = 1 To li_paquetes.ColumnHeaders.Count
            If li_paquetes.ColumnHeaders.Item(iColumna).Width > 1 Then
                ws1.Cells(iFila, iColumna).Font.Color = vbBlack
                ws1.Cells(iFila, iColumna).Interior.Color = vbWhite
                ws1.Cells(iFila, iColumna).Borders.LineStyle = 1
                If iColumna = 1 Then
                    ws1.Cells(iFila, iColumna).Value = " " & li_paquetes.ListItems(i)
                Else
                    ws1.Cells(iFila, iColumna).Value = " " & li_paquetes.ListItems(i).SubItems(iColumna - 1)
                End If
            End If
        Next
    Next
    
    For j = 1 To li_paquetes.ColumnHeaders.Count
        xl.Columns(j).EntireColumn.AutoFit
    Next j
    
    xl.Visible = True
    Screen.MousePointer = vbDefault

End Sub


Private Sub cmd_sel_localidad_Click()
    Dim i As Long, sFechaF As String, iCant As Long, sRespuesta As String
    
    For i = 1 To li_localidades.ListItems.Count
        If li_localidades.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
     
    If MsgBox("�Esta seguro de agregar las " & Trim(Str(iCant)) & " localidades seleccionadas?", vbYesNo) = vbYes Then
        For i = 1 To li_localidades.ListItems.Count
            If li_localidades.ListItems(i).Checked Then
                If IsNull(dtpFechaFLoc.Value) Then
                    sFechaF = ""
                Else
                    sFechaF = Format(dtpFechaFLoc.Value, "dd-mm-yyyy")
                End If
                If Not IngresaLocaPaquete(Val(lb_codi_paquete.Caption), li_localidades.ListItems(i).SubItems(1), "", Format(dtpFechaILoc.Value, "dd-mm-yyyy"), sFechaF, sRespuesta) Then
                    MsgBox sRespuesta, vbCritical, "Error"
                End If
            End If
        Next
        Call MostrarLocalidades(Val(lb_codi_paquete.Caption))
    End If
    
End Sub

Private Sub cmd_desel_localidad_Click()
    Dim i As Long, sFecha As String, iCant As Long
    
    For i = 1 To li_localidades_seleccionadas.ListItems.Count
        If li_localidades_seleccionadas.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
     
    If MsgBox("�Esta seguro de eliminar las " & Trim(Str(iCant)) & " localidades seleccionadas?", vbYesNo) = vbYes Then
        For i = 1 To li_localidades_seleccionadas.ListItems.Count
            If li_localidades_seleccionadas.ListItems(i).Checked Then
                Call EliminaLocaPaquete(Val(lb_codi_paquete.Caption), li_localidades_seleccionadas.ListItems(i).SubItems(1))
            End If
        Next
    
        Call MostrarLocalidades(Val(lb_codi_paquete.Caption))
    End If
End Sub

Private Sub cmd_sel_canal_Click()
    Dim i As Long, iCant As Long, sRespuesta As String
    
    For i = 1 To li_canales.ListItems.Count
        If li_canales.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
     
    If MsgBox("�Esta seguro de agregar los " & Trim(Str(iCant)) & " canales seleccionados?", vbYesNo) = vbYes Then
        For i = 1 To li_canales.ListItems.Count
            If li_canales.ListItems(i).Checked Then
                If Not IngresaCanalPaquete(Val(lb_codi_paquete.Caption), li_canales.ListItems(i).SubItems(1), sRespuesta) Then
                    MsgBox sRespuesta, vbCritical, "Error"
                End If
            End If
        Next
        Call MostrarCanalesVenta(Val(lb_codi_paquete.Caption))
        Call ChequeaBotonesCanales
    End If
    
End Sub

Private Sub cmd_desel_canal_Click()
    Dim i As Long, sFecha As String, iCant As Long
    
    For i = 1 To li_canales_seleccionados.ListItems.Count
        If li_canales_seleccionados.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next

    If MsgBox("�Esta seguro de eliminar los " & Trim(Str(iCant)) & " canales seleccionados?", vbYesNo) = vbYes Then
        For i = 1 To li_canales_seleccionados.ListItems.Count
            If li_canales_seleccionados.ListItems(i).Checked Then
                Call EliminaCanalPaquete(Val(lb_codi_paquete.Caption), li_canales_seleccionados.ListItems(i).SubItems(1))
            End If
        Next
        Call MostrarCanalesVenta(Val(lb_codi_paquete.Caption))
        Call ChequeaBotonesCanales
    End If
End Sub

Private Sub cmd_sel_canal_todos_Click()
    Dim i As Integer
    Dim sRespuesta As String
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_canales.ListItems.Count
        If Not IngresaCanalPaquete(Val(lb_codi_paquete.Caption), li_canales.ListItems(i).SubItems(1), sRespuesta) Then
            MsgBox sRespuesta, vbCritical, "Error"
        End If
    Next i
    Call MostrarCanalesVenta(Val(lb_codi_paquete.Caption))
    
    Call ChequeaBotonesCanales
    
End Sub

Private Sub cmd_desel_canal_todos_Click()
    Dim i As Integer
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_canales_seleccionados.ListItems.Count
        Call EliminaCanalPaquete(Val(lb_codi_paquete.Caption), li_canales_seleccionados.ListItems(i).SubItems(1))
    Next i
    Call MostrarCanalesVenta(Val(lb_codi_paquete.Caption))
    
    Call ChequeaBotonesCanales
End Sub

Private Sub ChequeaBotonesCanales()
    If li_canales.ListItems.Count = 0 Then
        cmd_sel_canal_todos.Enabled = False
        Call suEnable_Ctl1(Me.Tag, cmd_desel_canal_todos)
    Else
        Call suEnable_Ctl1(Me.Tag, cmd_sel_canal_todos)
    End If
    
    If li_canales_seleccionados.ListItems.Count = 0 Then
        cmd_desel_canal_todos.Enabled = False
        Call suEnable_Ctl1(Me.Tag, cmd_sel_canal_todos)
    Else
        Call suEnable_Ctl1(Me.Tag, cmd_desel_canal_todos)
    End If
End Sub

Private Sub cmd_act_fecha_Click()
    Dim sFechaF As String
    Dim i As Long, iCant As Long
    
    iCant = 0
    For i = 1 To li_localidades_seleccionadas.ListItems.Count
        If li_localidades_seleccionadas.ListItems(i).Checked Then
            If IsNull(dtpFechaFLoc.Value) Then
                sFechaF = ""
            Else
                If dtpFechaFLoc.Value < dtpFechaILoc.Value Then
                    dtpFechaFLoc.Value = dtpFechaILoc.Value
                    Exit Sub
                End If
                sFechaF = Format(dtpFechaFLoc.Value, "dd-mm-yyyy")
            End If
            Call ActualizaLocaPaquete(Val(lb_codi_paquete.Caption), li_localidades_seleccionadas.ListItems(i).SubItems(1), "", Format(dtpFechaILoc.Value, "dd-mm-yyyy"), sFechaF)
            iCant = iCant + 1
        End If
    Next

    If iCant > 0 Then
        Call MostrarLocalidades(Val(lb_codi_paquete.Caption))
    End If
    
End Sub

Private Sub cmd_sel_localidad_todos_Click()
    Dim i As Long, sFechaF As String, iCant As Long, sRespuesta As String
    For i = 1 To li_localidades.ListItems.Count
        If IsNull(dtpFechaFLoc.Value) Then
            sFechaF = ""
        Else
            sFechaF = Format(dtpFechaFLoc.Value, "dd-mm-yyyy")
        End If
        If Not IngresaLocaPaquete(Val(lb_codi_paquete.Caption), li_localidades.ListItems(i).SubItems(1), "", Format(dtpFechaILoc.Value, "dd-mm-yyyy"), sFechaF, sRespuesta) Then
            MsgBox sRespuesta, vbCritical, "Error"
        End If
    Next
    Call MostrarLocalidades(Val(lb_codi_paquete.Caption))
End Sub

Private Sub cmdBorrarseleccion_Click()
    Call suBorraSeleccionLista(li_paquetes)
    cmdBorrarseleccion.Enabled = False
    cmd_exportar.Enabled = False
End Sub



Private Sub co_actuelem_Click()
    
    If Fn_Valida_Datos Then
        If ActualizaDetallePaquete(li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index).SubItems(21), _
                                    Fn_Valor_Combo(cb_tipo_elemento), _
                                    tx_codi_elemento.Text, _
                                    Val(tx_peri_gracia.Text), _
                                    Val(tx_dura_vigen.Text), _
                                    Val(tx_peri_cobro.Text), _
                                    Fn_Valor_Combo(cb_prop_elemento), _
                                    Val(tx_interes.Text), _
                                    Fn_Valor_Combo(cb_flag_competencia), _
                                    Fn_Valor_Combo(cb_flag_obligatorio), _
                                    "", _
                                    Fn_Valor_Combo(cb_area), _
                                    IIf(ch_adelanta_peri_gracia = 1, "S", "N"), _
                                    IIf(ch_adelanta_dura_vigen = 1, "S", "N"), _
                                    Val(tx_dias_duracion.Text), _
                                    IIf(IsNull(dp_fech_inicio.Value), "", Format(dp_fech_inicio.Value, "dd-mm-yyyy")), _
                                    IIf(IsNull(dp_fech_termino.Value), "", Format(dp_fech_termino.Value, "dd-mm-yyyy")), _
                                    Fn_Valor_Combo(cb_tipo_elem_rel), _
                                    cb_codi_elem_rel.Text, _
                                    Fn_Valor_Combo(cb_tecnologia), _
                                    Fn_Valor_Combo(cb_tipo_peticion)) Then
            
            Dim xValores(21) As String
            xValores(0) = tx_codi_elemento.Text
            xValores(1) = Fn_Valor_Combo(cb_tipo_elemento)
            xValores(2) = tx_desc_elemento.Text
            xValores(3) = tx_peri_gracia.Text
            xValores(4) = tx_dura_vigen.Text
            xValores(5) = tx_peri_cobro.Text
            xValores(6) = Fn_Valor_Combo(cb_prop_elemento)
            xValores(7) = tx_interes.Text
            xValores(8) = Fn_Valor_Combo(cb_flag_competencia)
            xValores(9) = Fn_Valor_Combo(cb_flag_obligatorio)
            xValores(10) = ""
            xValores(11) = Fn_Valor_Combo(cb_area)
            xValores(12) = IIf(ch_adelanta_peri_gracia = 1, "S", "N")
            xValores(13) = IIf(ch_adelanta_dura_vigen = 1, "S", "N")
            xValores(14) = tx_dias_duracion.Text
            xValores(15) = IIf(IsNull(dp_fech_inicio.Value), "", Format(dp_fech_inicio.Value, "dd-mm-yyyy"))
            xValores(16) = IIf(IsNull(dp_fech_termino.Value), "", Format(dp_fech_termino.Value, "dd-mm-yyyy"))
            xValores(17) = Fn_Valor_Combo(cb_tipo_elem_rel)
            xValores(18) = cb_codi_elem_rel
            xValores(19) = Fn_Valor_Combo(cb_tecnologia)
            xValores(20) = Fn_Valor_Combo(cb_tipo_peticion)
            xValores(21) = li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index).SubItems(21)
            
            Call suModificaToLista(li_detalle_paquetes, xValores, li_detalle_paquetes.SelectedItem.Index)
            Call li_detalle_paquetes_ItemClick(li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index))
            
        Else
            MsgBox "Problemas al actualizar elemento, no se pudo actualizar"
        End If
    End If

End Sub

Private Sub cmd_deselecciona_Click()
    Dim i As Integer
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_seleccionados.ListItems.Count
        If li_seleccionados.ListItems(i).Selected Then
            Call Srv_parp_relacion_elimina(11, li_seleccionados.ListItems(i).SubItems(1), "102", gr_agrupador.TextMatrix(gr_agrupador.Row, 0), "")
        End If
    Next i
    gr_agrupador_Click
End Sub

Private Sub cmd_deselecciona_todos_Click()
    Dim i As Integer
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_seleccionados.ListItems.Count
            Call Srv_parp_relacion_elimina(11, li_seleccionados.ListItems(i).SubItems(1), "102", gr_agrupador.TextMatrix(gr_agrupador.Row, 0), "")
    Next i
    gr_agrupador_Click
End Sub

Private Sub cmd_selecciona_Click()
    Dim i As Integer
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_disponibles.ListItems.Count
        If li_disponibles.ListItems(i).Selected Then
            Call Srv_parp_relacion_ingresa(11, li_disponibles.ListItems(i).SubItems(1), "102", gr_agrupador.TextMatrix(gr_agrupador.Row, 0), "")
        End If
    Next i
    gr_agrupador_Click
End Sub

Private Sub cmd_selecciona_todos_Click()
    Dim i As Integer
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_disponibles.ListItems.Count
        Call Srv_parp_relacion_ingresa(11, li_disponibles.ListItems(i).SubItems(1), "102", gr_agrupador.TextMatrix(gr_agrupador.Row, 0), "")
    Next i
    gr_agrupador_Click
End Sub

Private Sub co_agregar_Click()
    Dim iFila As Integer
    iFila = gr_agrupador.Row
    If tx_desc_agrupador.Text <> "" Then
        Call Srv_parn_parame07(102, tx_codi_agrupador.Text, tx_desc_agrupador.Text)
    End If
    Call suLlenaAgrupador(cb_agrupador, 102)
    Call suLlenaGrillaAgrupador(gr_agrupador, 102)
    gr_agrupador.Row = iFila
    gr_agrupador.RowSel = iFila
    gr_agrupador.TopRow = iFila
    Call gr_agrupador_Click
End Sub

Private Sub co_actualizar_Click()
    Dim iFila As Integer
    
    iFila = gr_agrupador.Row
    If tx_desc_agrupador.Text <> "" Then
        Call Srv_paru_parame06(102, tx_codi_agrupador.Text, tx_desc_agrupador.Text)
    End If
    Call suLlenaAgrupador(cb_agrupador, 102)
    Call suLlenaGrillaAgrupador(gr_agrupador, 102)
    gr_agrupador.Row = iFila
    gr_agrupador.RowSel = iFila
    gr_agrupador.TopRow = iFila
    Call gr_agrupador_Click
End Sub

Private Sub co_eliminar_Click()
    Dim iFila As Integer
    iFila = gr_agrupador.Row
    If tx_codi_agrupador.Text <> "" Then
        Call Srv_pard_parame07(102, tx_codi_agrupador.Text, 11)
    End If
    Call suLlenaAgrupador(cb_agrupador, 102)
    Call suLlenaGrillaAgrupador(gr_agrupador, 102)
    gr_agrupador.Row = iFila
    gr_agrupador.RowSel = iFila
    gr_agrupador.TopRow = iFila
    Call gr_agrupador_Click
End Sub

Private Sub co_agregar_concepto_Click()
    Dim iFila As Integer
    iFila = gr_conceptos.Row
    
    If fnValidaExcluyentes Then
        Call Srv_parn_parame07(346, fnDevuelveCodiElem(346), tx_desc_concepto.Text, Val(tx_codi_concepto.Text), tx_prioridad.Text, , "V")
    End If

    'Call gr_agrupador_Click
        
    Call suLlenaGrillaConceptos(gr_conceptos)
    gr_conceptos.Row = iFila
    gr_conceptos.RowSel = iFila
    gr_conceptos.TopRow = iFila
    Call gr_conceptos_Click
End Sub

Private Sub co_actualizar_concepto_Click()
    Dim iFila As Integer
    iFila = gr_conceptos.Row
    If fnValidaExcluyentes Then
        Call Srv_paru_parame06(346, Trim(gr_conceptos.TextMatrix(gr_conceptos.Row, 0)), tx_desc_concepto.Text, Val(tx_codi_concepto.Text), Trim(tx_prioridad.Text), , "V")
    End If
    
    'Call gr_agrupador_Click
    
    Call suLlenaGrillaConceptos(gr_conceptos)
    gr_conceptos.Row = iFila
    gr_conceptos.RowSel = iFila
    gr_conceptos.TopRow = iFila
    Call gr_conceptos_Click
End Sub

Private Sub co_eliminar_concepto_Click()
    Dim iFila As Integer
    iFila = gr_conceptos.Row
    If tx_codi_agrupador.Text <> "" Then
        Call Srv_pard_parame07(346, Trim(gr_conceptos.TextMatrix(gr_conceptos.Row, 0)))
    End If

    'Call gr_agrupador_Click
    
    Call suLlenaGrillaConceptos(gr_conceptos)
    gr_conceptos.Row = iFila
    gr_conceptos.RowSel = iFila
    gr_conceptos.TopRow = iFila
    Call gr_conceptos_Click
End Sub


Private Sub co_elimelem_Click()
    If MsgBox("Desea eliminar elemento codigo: " + li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index) + " del paquete N� " + li_paquetes.ListItems(li_paquetes.SelectedItem.Index), vbYesNo) = vbYes Then
        
        If EliminaDetallePaquete(li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index).SubItems(21)) Then
            Call suEliminaFromLista(li_detalle_paquetes, li_detalle_paquetes.SelectedItem.Index)
            Call li_detalle_paquetes_ItemClick(li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index))
        Else
            MsgBox "Problemas al eliminar elemento"
        End If
    End If
End Sub

Private Sub co_inseelem_Click()
    Dim Linea As String, Ret As Integer, id_elemento As Long, sRespuesta As String
    
    If Fn_Valida_Datos Then
        If IngresaDetallePaquete(Val(lb_codi_paquete.Caption), _
                              Fn_Valor_Combo(cb_tipo_elemento), _
                              Trim(tx_codi_elemento.Text), _
                              Val(tx_peri_gracia.Text), _
                              Val(tx_dura_vigen.Text), _
                              Val(tx_peri_cobro.Text), _
                              Fn_Valor_Combo(cb_prop_elemento), _
                              Val(tx_interes.Text), _
                              Fn_Valor_Combo(cb_flag_competencia), _
                              Fn_Valor_Combo(cb_flag_obligatorio), _
                              "", _
                              Fn_Valor_Combo(cb_area), _
                              IIf(ch_adelanta_peri_gracia = 1, "S", "N"), _
                              IIf(ch_adelanta_dura_vigen = 1, "S", "N"), _
                              Val(tx_dias_duracion.Text), _
                              IIf(IsNull(dp_fech_inicio.Value), "", Format(dp_fech_inicio.Value, "dd-mm-yyyy")), _
                              IIf(IsNull(dp_fech_termino.Value), "", Format(dp_fech_termino.Value, "dd-mm-yyyy")), _
                              Fn_Valor_Combo(cb_tipo_elem_rel), _
                              cb_codi_elem_rel, _
                              Fn_Valor_Combo(cb_tecnologia), _
                              Fn_Valor_Combo(cb_tipo_peticion), _
                              id_elemento, sRespuesta) Then
            
            If sRespuesta = "OK" Then
                Dim xValores(21) As String
                xValores(0) = tx_codi_elemento.Text
                xValores(1) = Fn_Valor_Combo(cb_tipo_elemento)
                xValores(2) = tx_desc_elemento.Text
                xValores(3) = tx_peri_gracia.Text
                xValores(4) = tx_dura_vigen.Text
                xValores(5) = tx_peri_cobro.Text
                xValores(6) = Fn_Valor_Combo(cb_prop_elemento)
                xValores(7) = tx_interes.Text
                xValores(8) = Fn_Valor_Combo(cb_flag_competencia)
                xValores(9) = Fn_Valor_Combo(cb_flag_obligatorio)
                xValores(10) = ""
                xValores(11) = Fn_Valor_Combo(cb_area)
                xValores(12) = IIf(ch_adelanta_peri_gracia = 1, "S", "N")
                xValores(13) = IIf(ch_adelanta_dura_vigen = 1, "S", "N")
                xValores(14) = tx_dias_duracion.Text
                xValores(15) = IIf(IsNull(dp_fech_inicio.Value), "", Format(dp_fech_inicio.Value, "dd-mm-yyyy"))
                xValores(16) = IIf(IsNull(dp_fech_termino.Value), "", Format(dp_fech_termino.Value, "dd-mm-yyyy"))
                xValores(17) = Fn_Valor_Combo(cb_tipo_elem_rel)
                xValores(18) = cb_codi_elem_rel
                xValores(19) = Fn_Valor_Combo(cb_tecnologia)
                xValores(20) = Fn_Valor_Combo(cb_tipo_peticion)
                xValores(21) = id_elemento
                
                Call suAgregaToLista(li_detalle_paquetes, xValores)
                Call li_detalle_paquetes_ItemClick(li_detalle_paquetes.ListItems(li_detalle_paquetes.ListItems.Count))
            Else
                MsgBox "Problemas al insertar elemento, no se pudo ingresar: " + sRespuesta
            End If

        Else
            MsgBox "Problemas al insertar elemento, no se pudo ingresar: " + sRespuesta
        End If
    End If
End Sub

Private Function fnValidaExcluyentes() As Boolean
    fnValidaExcluyentes = True
    
    If tx_codi_concepto.Text = "" Then
        MsgBox "Debe ingresar un valor en C�digo de Concepto"
        fnValidaExcluyentes = False
        Exit Function
    End If
    
    If tx_prioridad.Text = "" Then
        MsgBox "Debe ingresar la prioridad del C�digo de Concepto"
        fnValidaExcluyentes = False
        Exit Function
    End If
    
    If tx_desc_concepto.Text = "" Then
        MsgBox "Debe ingresar la descripci�n del C�digo de Concepto"
        fnValidaExcluyentes = False
        Exit Function
    End If
    
End Function

Private Function Fn_Valida_Datos()
    Fn_Valida_Datos = True
    If Fn_Valor_Combo(cb_tipo_elemento) = "" Then
        MsgBox "Debe seleccionar un Tipo de Elemento"
        Fn_Valida_Datos = False
        Exit Function
    End If
    
    If tx_codi_elemento.Text = "" Then
        MsgBox "Debe ingresar un valor en Codigo de Elemento"
        Fn_Valida_Datos = False
        Exit Function
    End If
    
    If Fn_Valor_Combo(cb_flag_competencia) = "" Then
        MsgBox "Debe seleccionar Competencia"
        Fn_Valida_Datos = False
        Exit Function
    End If
    
    If Fn_Valor_Combo(cb_flag_obligatorio) = "" Then
        MsgBox "Debe seleccionar Obligatorio"
        Fn_Valida_Datos = False
        Exit Function
    End If
    
    If Fn_Valor_Combo(cb_prop_elemento) = "" Then
        MsgBox "Debe seleccionar Propiedad"
        Fn_Valida_Datos = False
        Exit Function
    End If

End Function

Private Sub cb_tipo_elem_rel_Click()
    Dim Tipo_Elemento As String, Codi_Elemento As String, Peri_Gracia As Integer, Dura_Vigen As Integer, Peri_Cobro As Integer, Prop_Elemento As String, interes As Double, Flag_Competencia As String, flag_obligatorio As String, Fech_Gracia As String, rowid As String, desc_elemento As String, codi_area As String
    Dim Adelanta_Peri_Gracia As String, Adelanta_Dura_Vigen As String ' SSIN#11087. JPABLOS. 17-ago-2009.
    Dim Dias_Duracion As Integer ' Proyecto OMV. JPABLOS. 22-jun-2011.
    Dim Fech_Inicio As String, Fech_Termino As String ' Se agrega fecha de inicio y fin. SI14323 MJPEREZ 14-mar-2012
    Dim Tipo_Elem_Rel As String, Codi_Elem_Rel As String ' SI 15664 JMARTINE
    Dim Ret As Integer, i As Integer
    

    If cb_tipo_elem_rel.ListIndex <> -1 Then
        If Fn_Valor_Combo(cb_tipo_elem_rel) <> li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index).SubItems(17) Then
            cb_codi_elem_rel.ListIndex = -1
        End If
        
        If li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index).SubItems(1) = "R" Or li_detalle_paquetes.ListItems(li_detalle_paquetes.SelectedItem.Index).SubItems(1) = "V" Then
            cb_codi_elem_rel.Clear
'            If cb_tipo_elem_rel.ListIndex = 0 Then
'                cb_codi_elem_rel.AddItem "  "
'            End If
            Ret = Srv_ElementosRel_Open(Val(frmPaquetes.lb_codi_paquete.Caption), Fn_Valor_Combo(cb_tipo_elem_rel))
            If Ret <> 0 Then
                For i = 1 To Ret
                    Ret = Srv_ElementosRel_Read(Tipo_Elemento, Codi_Elemento, Tipo_Elem_Rel, Codi_Elem_Rel)
                    cb_codi_elem_rel.AddItem Codi_Elemento + "  "
                Next i
            End If
            Ret = Srv_ElementosRel_Close()
        End If
    End If
End Sub

Private Sub cb_tipo_elemento_click()
    Gp_Ayuda.Sino = False
    Select Case Fn_Valor_Combo(cb_tipo_elemento)
        Case C_PAQ_RENTA, C_PAQ_VENTA
            Gp_Ayuda.Tipo_Ayuda = C_A_CONCEPTOS
            Gp_Ayuda.Sino = True
        Case C_PAQ_EQUIPO
            Gp_Ayuda.Tipo_Ayuda = C_A_EQUIPOS
            Gp_Ayuda.Sino = True
        Case C_PAQ_SERVICIO
            Gp_Ayuda.Tipo_Ayuda = C_A_FACILIDADES
            Gp_Ayuda.Sino = True
    End Select
End Sub

Private Sub chk_paq_novigentes_Click()
    Call suLlenaPaquetes
End Sub

Private Sub chk_paq_vigentes_Click()
    Call suLlenaPaquetes
End Sub

Private Sub cmd_exportar_Click()
    Dim vsSeparador As String
    
    Dim iFila As Long ' counter
    Dim iColumna As Long ' counter
    
    Dim iCol As Long ' counter
    Dim iColDet As Long ' counter
    
    Dim xl As Object
    Dim wbk As Object
    
    Dim ws1 As Object
    Dim ws2 As Object
    Dim ws3 As Object
    Dim ws4 As Object
    
    Dim i As Long
    Dim j As Long
    Dim df As Long, dc As Long
    Dim k As Long, l As Long
    
    Dim ldf As Long, ldc As Long
    Dim lk As Long, ll As Long
    Dim lk2 As Long
    
    Dim Total As Long

    vsSeparador = fnSeparadorListas
    
    Set xl = CreateObject("Excel.Application")

    xl.Visible = False
    Set wbk = xl.Workbooks.Add
    
    Set ws1 = wbk.Worksheets(1)
    ws1.Name = "Paquetes"
    
    Set ws2 = wbk.Worksheets.Add
    ws2.Name = "Detalle de Paquetes"

    Set ws3 = wbk.Worksheets.Add
    ws3.Name = "Localidades"
    
    Set ws4 = wbk.Worksheets.Add
    ws4.Name = "Canales de Venta"
  
    iFila = 1
    k = 0
            
    For j = 1 To li_paquetes.ColumnHeaders.Count
        If li_paquetes.ColumnHeaders.Item(j).Width > 0 Then
            k = k + 1
            ws1.Cells(1, k).Font.Bold = True
            ws1.Cells(1, k).Font.Color = vbBlack
            ws1.Cells(1, k).Interior.Color = &HDFBF91
            ws1.Cells(1, k).Value = li_paquetes.ColumnHeaders(j).Text
        End If
    Next j
            
    k = 1
    For j = 0 To li_detalle_paquetes.ColumnHeaders.Count
        If j = 0 Then
            ws2.Cells(iFila, k).Font.Bold = True
            ws2.Cells(iFila, k).Font.Color = vbBlack
            ws2.Cells(iFila, k).Interior.Color = &HDFBF91
            ws2.Cells(iFila, k).Value = "Paquete"
        Else
            If li_detalle_paquetes.ColumnHeaders.Item(j).Width > 0 Then
                k = k + 1
                ws2.Cells(iFila, k).Font.Bold = True
                ws2.Cells(iFila, k).Font.Color = vbBlack
                ws2.Cells(iFila, k).Interior.Color = &HDFBF91
                ws2.Cells(iFila, k).Value = li_detalle_paquetes.ColumnHeaders(j).Text
            End If
        End If
    Next j
    
    For j = 0 To li_localidades.ColumnHeaders.Count
        ws3.Cells(iFila, j + 1).Font.Bold = True
        ws3.Cells(iFila, j + 1).Font.Color = vbBlack
        ws3.Cells(iFila, j + 1).Interior.Color = &HDFBF91
        If j = 0 Then
            ws3.Cells(iFila, j + 1).Value = "Paquete"
        Else
            ws3.Cells(iFila, j + 1).Value = li_localidades.ColumnHeaders(j).Text
        End If
    Next j
    
    For j = 0 To li_canales.ColumnHeaders.Count
        ws4.Cells(iFila, j + 1).Font.Bold = True
        ws4.Cells(iFila, j + 1).Font.Color = vbBlack
        ws4.Cells(iFila, j + 1).Interior.Color = &HDFBF91
        If j = 0 Then
            ws4.Cells(iFila, j + 1).Value = "Paquete"
        Else
            ws4.Cells(iFila, j + 1).Value = li_canales.ColumnHeaders(j).Text
        End If
    Next j
    
    Screen.MousePointer = vbHourglass
    k = 1
    lk = 1
    lk2 = 1
    iColumna = 0
    For i = 1 To li_paquetes.ListItems.Count
        If li_paquetes.ListItems(i).Checked Then
            iFila = iFila + 1
            iColumna = 0
            For iCol = 1 To li_paquetes.ColumnHeaders.Count
                If li_paquetes.ColumnHeaders.Item(iCol).Width > 1 Then
                    iColumna = iColumna + 1
                    ws1.Cells(iFila, iColumna).Font.Color = vbBlack
                    ws1.Cells(iFila, iColumna).Interior.Color = vbWhite
                    ws1.Cells(iFila, iColumna).Borders.LineStyle = 1
                    If iCol = 1 Then
                        ws1.Cells(iFila, iColumna).Value = " " & li_paquetes.ListItems(i)
                        Call li_paquetes_ItemClick(li_paquetes.ListItems(i))
                        For df = 1 To li_detalle_paquetes.ListItems.Count - 1
                            k = k + 1
                            iColDet = 1
                            For dc = 0 To li_detalle_paquetes.ColumnHeaders.Count
                                If dc = 0 Then
                                    ws2.Cells(k, 1).Font.Color = vbBlack
                                    ws2.Cells(k, 1).Interior.Color = vbWhite
                                    ws2.Cells(k, 1).Borders.LineStyle = 1
                                    ws2.Cells(k, 1).Value = " " & li_paquetes.ListItems(i)
                                Else
                                    If li_detalle_paquetes.ColumnHeaders.Item(dc).Width > 1 Then
                                        iColDet = iColDet + 1
                                        ws2.Cells(k, iColDet).Font.Color = vbBlack
                                        ws2.Cells(k, iColDet).Interior.Color = vbWhite
                                        ws2.Cells(k, iColDet).Borders.LineStyle = 1
                                        If dc = 1 Then
                                            If li_detalle_paquetes.ColumnHeaders.Item(dc).Tag <> "" Then
                                                ws2.Cells(k, iColDet).Value = " " & li_detalle_paquetes.ListItems(df).SubItems(li_detalle_paquetes.ColumnHeaders.Item(dc).Tag)
                                            Else
                                                ws2.Cells(k, iColDet).Value = " " & li_detalle_paquetes.ListItems(df)
                                            End If
                                        Else
                                            If li_detalle_paquetes.ColumnHeaders.Item(dc).Tag <> "" Then
                                                ws2.Cells(k, iColDet).Value = " " & li_detalle_paquetes.ListItems(df).SubItems(li_detalle_paquetes.ColumnHeaders.Item(dc).Tag)
                                            Else
                                                ws2.Cells(k, iColDet).Value = " " & li_detalle_paquetes.ListItems(df).SubItems(dc - 1)
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        Next
                        
                        For ldf = 1 To li_localidades_seleccionadas.ListItems.Count
                            lk = lk + 1
                            For ldc = 0 To li_localidades_seleccionadas.ColumnHeaders.Count
                                If ldc = 0 Then
                                    ws3.Cells(lk, ldc + 1).Font.Color = vbBlack
                                    ws3.Cells(lk, ldc + 1).Interior.Color = vbWhite
                                    ws3.Cells(lk, ldc + 1).Borders.LineStyle = 1
                                    ws3.Cells(lk, ldc + 1).Value = " " & li_paquetes.ListItems(i)
                                Else
                                    If li_localidades_seleccionadas.ColumnHeaders.Item(ldc).Width > 1 Then
                                        ws3.Cells(lk, ldc + 1).Font.Color = vbBlack
                                        ws3.Cells(lk, ldc + 1).Interior.Color = vbWhite
                                        ws3.Cells(lk, ldc + 1).Borders.LineStyle = 1
                                        If ldc = 1 Then
                                            ws3.Cells(lk, ldc + 1).Value = " " & li_localidades_seleccionadas.ListItems(ldf)
                                        Else
                                            ws3.Cells(lk, ldc + 1).Value = " " & li_localidades_seleccionadas.ListItems(ldf).SubItems(ldc - 1)
                                        End If
                                    End If
                                End If
                            Next
                        Next
                        
                        For ldf = 1 To li_canales_seleccionados.ListItems.Count
                            lk2 = lk2 + 1
                            For ldc = 0 To li_canales_seleccionados.ColumnHeaders.Count
                                If ldc = 0 Then
                                    ws4.Cells(lk2, ldc + 1).Font.Color = vbBlack
                                    ws4.Cells(lk2, ldc + 1).Interior.Color = vbWhite
                                    ws4.Cells(lk2, ldc + 1).Borders.LineStyle = 1
                                    ws4.Cells(lk2, ldc + 1).Value = " " & li_paquetes.ListItems(i)
                                Else
                                    If li_canales_seleccionados.ColumnHeaders.Item(ldc).Width > 1 Then
                                        ws4.Cells(lk2, ldc + 1).Font.Color = vbBlack
                                        ws4.Cells(lk2, ldc + 1).Interior.Color = vbWhite
                                        ws4.Cells(lk2, ldc + 1).Borders.LineStyle = 1
                                        If ldc = 1 Then
                                            ws4.Cells(lk2, ldc + 1).Value = " " & li_canales_seleccionados.ListItems(ldf)
                                        Else
                                            ws4.Cells(lk2, ldc + 1).Value = " " & li_canales_seleccionados.ListItems(ldf).SubItems(ldc - 1)
                                        End If
                                    End If
                                End If
                            Next
                        Next
                        
                    Else
                        If li_paquetes.ColumnHeaders.Item(iCol).Tag <> "" And IsNumeric(li_paquetes.ColumnHeaders.Item(iCol).Tag) Then
                            ws1.Cells(iFila, iColumna).Value = " " & li_paquetes.ListItems(i).SubItems(li_paquetes.ColumnHeaders.Item(iCol).Tag)
                        Else
                            ws1.Cells(iFila, iColumna).Value = " " & li_paquetes.ListItems(i).SubItems(iCol - 1)
                        End If
                    End If
                End If
            Next
        End If
    Next
    
    For j = 1 To li_paquetes.ColumnHeaders.Count
        xl.Columns(j).EntireColumn.AutoFit
    Next j
    
    xl.Visible = True
    Screen.MousePointer = vbDefault
    Call li_paquetes_ItemClick(li_paquetes.SelectedItem)
End Sub


Private Sub suListaToExcel(ByRef xl As Object, ByVal NombreHoja As String, ByVal Hoja As Integer, ByVal iFila As Long, Lista As ListView)
    Dim iColumna As Long ' counter
    Dim wbk As Object
    
    Dim ws1 As Object
    
    Dim i As Long
    Dim j As Long
    
    'Set xl = CreateObject("Excel.Application")

    xl.Visible = False
    Set wbk = xl.Workbooks.Add
    Set ws1 = wbk.Worksheets(1)
    ws1.Name = NombreHoja
    

    For j = 1 To li_paquetes.ColumnHeaders.Count
        ws1.Cells(1, j).Font.Bold = True
        ws1.Cells(1, j).Font.Color = vbBlack
        ws1.Cells(1, j).Interior.Color = &HDFBF91
        ws1.Cells(1, j).Value = li_paquetes.ColumnHeaders(j).Text
    Next j
    
    Screen.MousePointer = vbHourglass

    For i = 1 To Lista.ListItems.Count
        If Lista.Checkboxes = True Then
            If Lista.ListItems(i).Checked Then
                iFila = iFila + 1
                For iColumna = 1 To Lista.ColumnHeaders.Count
                    If Lista.ColumnHeaders.Item(iColumna).Width > 1 Then
                        ws1.Cells(iFila, iColumna).Font.Color = vbBlack
                        ws1.Cells(iFila, iColumna).Interior.Color = vbWhite
                        ws1.Cells(iFila, iColumna).Borders.LineStyle = 1
                        If iColumna = 1 Then
                            ws1.Cells(iFila, iColumna).Value = " " & Lista.ListItems(i)
                        Else
                            ws1.Cells(iFila, iColumna).Value = " " & Lista.ListItems(i).SubItems(iColumna - 1)
                        End If
                    End If
                Next
            End If
        Else
            iFila = iFila + 1
            For iColumna = 1 To Lista.ColumnHeaders.Count
                If Lista.ColumnHeaders.Item(iColumna).Width > 1 Then
                    ws1.Cells(iFila, iColumna).Font.Color = vbBlack
                    ws1.Cells(iFila, iColumna).Interior.Color = vbWhite
                    ws1.Cells(iFila, iColumna).Borders.LineStyle = 1
                    If iColumna = 1 Then
                        ws1.Cells(iFila, iColumna).Value = " " & Lista.ListItems(i)
                    Else
                        ws1.Cells(iFila, iColumna).Value = " " & Lista.ListItems(i).SubItems(iColumna - 1)
                    End If
                End If
            Next
        End If
    Next
    
    For j = 1 To Lista.ColumnHeaders.Count
        xl.Columns(j).EntireColumn.AutoFit
    Next j
    
    xl.Visible = True
    Screen.MousePointer = vbDefault
    
End Sub

Private Sub co_actupaq_Click()
    Dim xValores() As String, sRespuesta As String ', habilitado As String
    'If (Check_habilitado.Value = 0) Then
     '   habilitado = "N"
    'Else
     '   habilitado = "S"
    'End If
    If ActualizaPaquete(Val(lb_codi_paquete.Caption), _
                             tx_desc_paquete.Text, _
                             Format(dtFechaInicio.Value, "dd-mm-yyyy"), _
                             IIf(IsNull(dtFechaFin.Value), "", Format(dtFechaFin.Value, "dd-mm-yyyy")), _
                             Val(tx_max_venta.Text), _
                             Val(tx_max_renta.Text), _
                             Val(tx_max_equipo.Text), _
                             Val(tx_max_servicio.Text), _
                             Fn_Valor_Combo(cb_agrupador), _
                             Fn_Valor_Combo(cb_tipo_producto), _
                             Fn_Valor_Combo(cb_tipo_segmento), _
                             Fn_Valor_Combo(cb_prioritario), _
                             Trim$(cb_habilitado), _
                             Trim$(cb_es_promo), _
                             Trim$(cb_alta_inmediata_sva), _
                             sRespuesta) Then
        ReDim xValores(15)
        xValores(0) = lb_codi_paquete.Caption
        xValores(1) = tx_desc_paquete.Text
        xValores(2) = Format(dtFechaInicio.Value, "dd-mm-yyyy")
        xValores(3) = IIf(IsNull(dtFechaFin.Value), "", Format(dtFechaFin.Value, "dd-mm-yyyy"))
        xValores(4) = tx_max_venta.Text
        xValores(5) = tx_max_renta.Text
        xValores(6) = tx_max_equipo.Text
        xValores(7) = tx_max_servicio.Text
        xValores(8) = Fn_Valor_Combo(cb_agrupador)
        xValores(9) = Fn_Valor_Combo(cb_tipo_producto)
        xValores(10) = Fn_Valor_Combo(cb_tipo_segmento)
        xValores(11) = Fn_Valor_Combo(cb_prioritario)
        xValores(12) = Trim$(cb_habilitado)
        xValores(13) = Trim$(cb_es_promo)
        xValores(14) = Trim$(cb_alta_inmediata_sva)
        
        Call suModificaToLista(li_paquetes, xValores, li_paquetes.SelectedItem.Index)
        Call li_paquetes_ItemClick(li_paquetes.ListItems(li_paquetes.SelectedItem.Index))
    Else
        If sRespuesta <> "OK" Then
            MsgBox sRespuesta, vbCritical, "ERROR"
        End If
    End If

End Sub

Private Sub co_clonar_Click()
    Dim xValores() As String, CodiPaquete As Long

    If CopiaPaquete(li_paquetes.ListItems(li_paquetes.SelectedItem.Index), CodiPaquete) Then
        ReDim xValores(14)
        xValores(0) = CodiPaquete
        xValores(1) = "!! COPIA " + li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(1)
        xValores(2) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(2)
        xValores(3) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(3)
        xValores(4) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(4)
        xValores(5) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(5)
        xValores(6) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(6)
        xValores(7) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(7)
        xValores(8) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(8)
        xValores(9) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(9)
        xValores(10) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(10)
        xValores(11) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(11)
        xValores(12) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(12)
        xValores(13) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(13)
        xValores(14) = li_paquetes.ListItems(li_paquetes.SelectedItem.Index).SubItems(14)
        
        
        Call suAgregaToLista(li_paquetes, xValores)
        Call li_paquetes_ItemClick(li_paquetes.ListItems(li_paquetes.ListItems.Count - 1))
    End If

End Sub

Private Sub co_elimpaq_Click()
    If MsgBox("Desea eliminar paquete N� :" + lb_codi_paquete.Caption + ", se eliminar�n tambi�n todos sus elementos", vbYesNo) = vbYes Then
        If EliminaPaquete(li_paquetes.ListItems(li_paquetes.SelectedItem.Index)) Then
            Call suEliminaFromLista(li_paquetes, li_paquetes.SelectedItem.Index)
            Call li_paquetes_ItemClick(li_paquetes.ListItems(li_paquetes.SelectedItem.Index))
        Else
            MsgBox "Problemas al eliminar"
        End If
    End If
End Sub

Private Sub co_insepaq_Click()
    Dim xValores() As String, sRespuesta As String ', habilitado As String
    'If (Check_habilitado.Value = 0) Then
     '   habilitado = "N"
    'Else
     '   habilitado = "S"
    'End If
                                                                                 
    If IngresaPaquete(Val(lb_codi_paquete.Caption), _
                             tx_desc_paquete.Text, _
                             Format(dtFechaInicio.Value, "dd-mm-yyyy"), _
                             IIf(IsNull(dtFechaFin.Value), "", Format(dtFechaFin.Value, "dd-mm-yyyy")), _
                             Val(tx_max_venta.Text), _
                             Val(tx_max_renta.Text), _
                             Val(tx_max_equipo.Text), _
                             Val(tx_max_servicio.Text), _
                             Trim$(cb_habilitado), _
                             Fn_Valor_Combo(cb_agrupador), _
                             Fn_Valor_Combo(cb_tipo_producto), _
                             Fn_Valor_Combo(cb_tipo_segmento), _
                             Fn_Valor_Combo(cb_prioritario), _
                             Trim$(cb_es_promo), _
                             Trim$(cb_alta_inmediata_sva), _
                             sRespuesta) Then
         
         
        ReDim xValores(15)
        xValores(0) = lb_codi_paquete.Caption
        xValores(1) = tx_desc_paquete.Text
        xValores(2) = Format(dtFechaInicio.Value, "dd-mm-yyyy")
        xValores(3) = IIf(IsNull(dtFechaFin.Value), "", Format(dtFechaFin.Value, "dd-mm-yyyy"))
        xValores(4) = tx_max_venta.Text
        xValores(5) = tx_max_renta.Text
        xValores(6) = tx_max_equipo.Text
        xValores(7) = tx_max_servicio.Text
        xValores(8) = Fn_Valor_Combo(cb_agrupador)
        xValores(9) = Fn_Valor_Combo(cb_tipo_producto)
        xValores(10) = Fn_Valor_Combo(cb_tipo_segmento)
        xValores(11) = Fn_Valor_Combo(cb_prioritario)
        xValores(12) = Trim$(cb_habilitado)
        xValores(13) = Trim$(cb_es_promo)
        xValores(14) = Trim$(cb_alta_inmediata_sva)
        
        Call suAgregaToLista(li_paquetes, xValores)
        Call li_paquetes_ItemClick(li_paquetes.ListItems(li_paquetes.ListItems.Count))
            
    Else
        If sRespuesta <> "OK" Then
            MsgBox sRespuesta, vbCritical, "ERROR"
        End If
    End If
End Sub

Private Sub co3_salir_Click()
    Unload Me
End Sub


Private Sub co_salir_Click()
    'Unload fdetalle
    Unload Me
End Sub

Private Sub suLlenaPaquetes()
    Dim Total As Integer, i As Integer, Ret As Integer, Linea As String
    Dim codi_paquete As Integer, Desc_Paquete As String, fech_inivigen As String, fech_finvigen As String, Max_Venta As Integer, Max_Renta As Integer, Max_Equipo As Integer, Max_Servicio As Integer, Codi_Agrupa As String, Codi_Elemp As String, Tipo_Producto As Integer, Tipo_Segmento As Integer, prioritario As String, habilitado As String, promocion As String, alta_sva As String
    Dim dFecha As Date
    Dim lpq As ListItem
    
    dFecha = Format(Now(), "dd-mm-yyyy")
    
    Screen.MousePointer = vbHourglass
    
    li_paquetes.Visible = False
    
    li_paquetes.ListItems.Clear
    
    Total = Srv_Paquetes_Open(chk_paq_vigentes.Value, chk_paq_novigentes.Value)
    
    For i = 1 To Total
        Ret = Srv_Paquetes_Read(codi_paquete, Desc_Paquete, fech_inivigen, fech_finvigen, Max_Venta, Max_Renta, Max_Equipo, Max_Servicio, Codi_Agrupa, Tipo_Producto, Tipo_Segmento, prioritario, habilitado, promocion, alta_sva)
        Dim xValores(15) As String
        xValores(0) = Trim$(codi_paquete)
        xValores(1) = Trim$(Desc_Paquete)
        xValores(2) = Trim$(fech_inivigen)
        xValores(3) = Trim$(fech_finvigen)
        xValores(4) = Trim$(Max_Venta)
        xValores(5) = Trim$(Max_Renta)
        xValores(6) = Trim$(Max_Equipo)
        xValores(7) = Trim$(Max_Servicio)
        xValores(8) = Trim$(Codi_Agrupa)
        xValores(9) = Trim$(Tipo_Producto)
        xValores(10) = Trim$(Tipo_Segmento)
        xValores(11) = Trim$(prioritario)
        xValores(12) = Trim$(habilitado)
        xValores(13) = Trim$(promocion)
        xValores(14) = Trim$(alta_sva)
        Call suAgregaToLista(li_paquetes, xValores)
        
    Next i
    Ret = Srv_Paquetes_Close()
    
    li_paquetes.Visible = True
    
    
    Screen.MousePointer = vbDefault
    
End Sub




Private Sub Form_Load()
    
    gr_agrupador.ColWidth(0) = 748
    gr_agrupador.ColWidth(1) = 5212
    
    gr_conceptos.ColWidth(0) = 1
    gr_conceptos.ColWidth(2) = 5000
    
    SSTab1.Tab = 0
    
    Call suLlenaPaquetes
    Call Su_Llenar_combos
   
    dtpFechaILoc.Value = Now
    dtpFechaFLoc.Value = Now
    dtpFechaFLoc.Value = Null
    
    Call suLlenaAgrupador(cb_agrupador, 102)
    Call suLlenaAgrupador(cb_tipo_producto, 302)
    Call suLlenaAgrupador(cb_tipo_segmento, 303)
    Call suLlenaAgrupador(cb_prioritario, 265)
    Call llenaHabilitado
    Call llenaPromo
    Call llenaAltaInmediata
    Call suLlenaGrillaAgrupador(gr_agrupador, 102)
    
    Dim controlesForm As Control
    For Each controlesForm In Controls
      If (TypeOf controlesForm Is CommandButton) Then
          If controlesForm.Tag <> "" Then
              Call suEnable_Ctl1(Me.Tag, controlesForm)
          End If
      End If
    Next
    
    cmd_exportar.Enabled = False
    cmdBorrarseleccion.Enabled = False
    
    Call li_paquetes_ItemClick(li_paquetes.ListItems(li_paquetes.ListItems.Count))
    Call suLlenaGrillaConceptos(gr_conceptos)
    
    Call gr_agrupador_Click
    
End Sub


Private Sub Su_Llenar_combos()
    Dim i As Integer, Ret As Integer, Elem As String, desc As String
    
    cb_tipo_elemento.Clear
    cb_tipo_elemento.AddItem "R - Renta"
    cb_tipo_elemento.AddItem "V - Venta"
    cb_tipo_elemento.AddItem "E - Equipo"
    cb_tipo_elemento.AddItem "S - Servicio Adicional"
    cb_tipo_elemento.ListIndex = 0
    
    cb_tipo_elem_rel.Clear
    cb_codi_elem_rel.Clear
    
    Ret = Srv_Parame_Open(8)
    If Ret <> 0 Then
        For i = 1 To Ret
            Ret = Srv_Parame_Read(Elem, desc)
            cb_prop_elemento.AddItem Elem + " - " + desc
        Next i
        cb_prop_elemento.ListIndex = 0
    End If
    Ret = Srv_Parame_Close()

    cb_area.AddItem ""
    Ret = Srv_Parame_Open(350)
    If Ret <> 0 Then
        For i = 1 To Ret
            Ret = Srv_Parame_Read(Elem, desc)
            cb_area.AddItem Elem + " - " + desc
        Next i
        cb_area.ListIndex = 0
    End If
    Ret = Srv_Parame_Close()

    cb_tecnologia.AddItem ""
    Ret = Srv_Parame_Open(341)
    If Ret <> 0 Then
        For i = 1 To Ret
            Ret = Srv_Parame_Read(Elem, desc)
            cb_tecnologia.AddItem Elem + " - " + desc
        Next i
        cb_tecnologia.ListIndex = 0
    End If
    Ret = Srv_Parame_Close()
    
    cb_tipo_peticion.AddItem ""
    Ret = Srv_Parame_Open(344)
    If Ret <> 0 Then
        For i = 1 To Ret
            Ret = Srv_Parame_Read(Elem, desc)
            cb_tipo_peticion.AddItem desc
        Next i
        cb_tipo_peticion.ListIndex = 0
    End If
    Ret = Srv_Parame_Close()
    
    
    cb_flag_competencia.Clear
    cb_flag_competencia.AddItem "A - AMBAS"
    cb_flag_competencia.AddItem "S - SI"
    cb_flag_competencia.AddItem "N - NO"
    cb_flag_competencia.ListIndex = 0

    cb_flag_obligatorio.AddItem "N - NO"
    cb_flag_obligatorio.AddItem "S - SI"
    cb_flag_obligatorio.ListIndex = 0
    
End Sub

Private Sub gr_agrupador_Click()
    Dim Total As Integer, i As Integer, Ret As Integer
    Dim lid As ListItem
    Dim lis As ListItem
    
    Dim Tipo_Peticion As String, clase_peticion As String, subclase As String, clase As String

   
    li_disponibles.ListItems.Clear

    Total = Srv_Disponibles_Open(Val(gr_agrupador.TextMatrix(gr_agrupador.Row, 0)))
    For i = 1 To Total
        Total = Srv_Disponibles_Read(Tipo_Peticion, clase_peticion, subclase, clase)
        Set lid = li_disponibles.ListItems.Add(, , Right$(Space$(4) & Trim$(Tipo_Peticion), 4))
        lid.SubItems(1) = Trim$(clase_peticion)
        lid.SubItems(2) = Trim$(subclase)
        lid.SubItems(3) = Trim$(clase)
    Next i
    Ret = Srv_Disponibles_Close()
    
    li_seleccionados.ListItems.Clear

    Total = Srv_Seleccionados_Open(Val(gr_agrupador.TextMatrix(gr_agrupador.Row, 0)))
    For i = 1 To Total
        Total = Srv_Seleccionados_Read(Tipo_Peticion, clase_peticion, subclase, clase)
        Set lis = li_seleccionados.ListItems.Add(, , Right$(Space$(4) & Trim$(Tipo_Peticion), 4))
        lis.SubItems(1) = Trim$(clase_peticion)
        lis.SubItems(2) = Trim$(subclase)
        lis.SubItems(3) = Trim$(clase)
    Next i
    Ret = Srv_Seleccionados_Close()
    
    'fr_conceptos_excluyentes.Enabled = True
    
    
    If gr_agrupador.Row > 0 And gr_agrupador.Row < gr_agrupador.Rows - 1 Then

        tx_codi_agrupador.Text = gr_agrupador.TextMatrix(gr_agrupador.Row, 0)
        tx_desc_agrupador.Text = gr_agrupador.TextMatrix(gr_agrupador.Row, 1)
        co_agregar.Enabled = False
        Call suEnable_Ctl1(Me.Tag, co_eliminar)
        Call suEnable_Ctl1(Me.Tag, co_actualizar)
        
   Else
        Call suEnable_Ctl1(Me.Tag, co_agregar)
        co_eliminar.Enabled = False
        co_actualizar.Enabled = False
        'fr_conceptos_excluyentes.Enabled = False
   End If
   
   cmd_selecciona.Enabled = False
   cmd_deselecciona.Enabled = False

   
   If gr_agrupador.Row = gr_agrupador.Rows - 1 Then
       tx_codi_agrupador.Text = gr_agrupador.TextMatrix(gr_agrupador.Rows - 2, 0) + 1
       tx_desc_agrupador.Text = ""
       tx_desc_agrupador.SetFocus
   End If

   '
   Call gr_conceptos_Click
End Sub

Private Sub gr_conceptos_Click()
    Dim Total As Integer, i As Integer, Ret As Integer
        
    If gr_conceptos.Row > 0 And gr_conceptos.Row < gr_conceptos.Rows - 1 Then

        tx_codi_concepto.Text = gr_conceptos.TextMatrix(gr_conceptos.Row, 1)
        tx_desc_concepto.Text = gr_conceptos.TextMatrix(gr_conceptos.Row, 2)
        tx_prioridad.Text = gr_conceptos.TextMatrix(gr_conceptos.Row, 3)

        co_agregar_concepto.Enabled = False
        Call suEnable_Ctl1(Me.Tag, co_eliminar_concepto)
        Call suEnable_Ctl1(Me.Tag, co_actualizar_concepto)
   Else
        Call suEnable_Ctl1(Me.Tag, co_agregar_concepto)
        co_eliminar_concepto.Enabled = False
        co_actualizar_concepto.Enabled = False
   End If
   
   If gr_conceptos.Row = gr_conceptos.Rows - 1 Then
        tx_codi_concepto.Text = ""
        tx_desc_concepto.Text = ""
        tx_prioridad.Text = ""
   End If
End Sub



Private Sub gr_proyecto_Click()
Dim Total As Integer, i As Integer, Ret As Integer

        
    If gr_proyecto.Row > 0 And gr_proyecto.Row < gr_proyecto.Rows - 1 Then

        id_proyecto.Text = gr_proyecto.TextMatrix(gr_proyecto.Row, 0)
        desc_proyecto.Text = gr_proyecto.TextMatrix(gr_proyecto.Row, 1)
     '   concepto_proy.Text = gr_proyecto.TextMatrix(gr_proyecto.Row, 2)
        id_proyecto.Enabled = False
        agrega_proy.Enabled = False
       ' co_agregar_concepto.Enabled = False
        'Call suEnable_Ctl1(Me.Tag, co_eliminar_concepto)
        'Call suEnable_Ctl1(Me.Tag, co_actualizar_concepto)
   Else
        If gr_proyecto.Row = gr_proyecto.Rows - 1 Then
            agrega_proy.Enabled = True
            id_proyecto.Enabled = True
        End If
        'Call suEnable_Ctl1(Me.Tag, co_agregar_concepto)
        'co_eliminar_concepto.Enabled = False
        'co_actualizar_concepto.Enabled = False
   End If
   
   If gr_proyecto.Row = gr_proyecto.Rows - 1 Then
        id_proyecto.Text = ""
        desc_proyecto.Text = ""
       ' concepto_proy.Text = ""
   End If
End Sub

Private Sub li_disponibles_Click()
    Call suEnable_Ctl1(Me.Tag, cmd_selecciona)
    cmd_deselecciona.Enabled = False
End Sub

Private Sub li_localidades_seleccionadas_ItemClick(ByVal Item As MSComctlLib.ListItem)
    If Item.Index > 0 And Item.Index <= li_localidades_seleccionadas.ListItems.Count Then
        dtpFechaILoc.Value = Now
        dtpFechaILoc.Value = IIf(li_localidades_seleccionadas.ListItems(Item.Index).SubItems(2) = "", Null, li_localidades_seleccionadas.ListItems(Item.Index).SubItems(2))
        If li_localidades_seleccionadas.ListItems(Item.Index).SubItems(3) <> "" Then
            dtpFechaFLoc.Value = li_localidades_seleccionadas.ListItems(Item.Index).SubItems(3)
        Else
            dtpFechaFLoc.Value = Now
            dtpFechaFLoc.Value = Null
        End If
    End If
End Sub

Private Sub li_localidades_seleccionadas_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Long, iCant As Long
    
    For i = 1 To li_localidades_seleccionadas.ListItems.Count
        If li_localidades_seleccionadas.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
    
    If iCant > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_desel_localidad)
        Call suEnable_Ctl1(Me.Tag, cmd_act_fecha)
    Else
        cmd_desel_localidad.Enabled = False
        cmd_act_fecha.Enabled = False
    End If
End Sub

Private Sub li_localidades_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Long, iCant As Long
    
    For i = 1 To li_localidades.ListItems.Count
        If li_localidades.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
    
    If iCant > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_sel_localidad)
    Else
        cmd_sel_localidad.Enabled = False
    End If
End Sub

Private Sub li_canales_seleccionados_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Long, iCant As Long
    
    For i = 1 To li_canales_seleccionados.ListItems.Count
        If li_canales_seleccionados.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
    
    If iCant > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_desel_canal)
    Else
        cmd_desel_canal.Enabled = False
    End If

End Sub

Private Sub li_canales_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Long, iCant As Long
    
    For i = 1 To li_canales.ListItems.Count
        If li_canales.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
    
    If iCant > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_sel_canal)
    Else
        cmd_sel_canal.Enabled = False
    End If
    
    If li_canales.ListItems.Count > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_sel_canal_todos)
    Else
        cmd_sel_canal_todos.Enabled = False
    End If
    
End Sub

Private Sub li_paquetes_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Dim frm As New frmFiltro
    Dim i As Integer

    If Not IsNumeric(li_paquetes.ColumnHeaders(ColumnHeader.Index).Tag) Then
        frm.TipoFiltro = li_paquetes.ColumnHeaders(ColumnHeader.Index).Tag
        frm.tx_campo.Text = ColumnHeader.Text
        
        For i = 1 To li_paquetes.ColumnHeaders.Count
           frm.cmdBorrar.Enabled = False
           If InStr(1, li_paquetes.ColumnHeaders.Item(i).Text, "[F] ", vbTextCompare) > 0 Then
              frm.cmdBorrar.Enabled = True
              Exit For
           End If
        Next i
        
        If li_paquetes.ColumnHeaders(ColumnHeader.Index).Tag = "Combo" Then
            frm.cb_filtro.Clear
            Select Case ColumnHeader.Index
            Case 9
                For i = 0 To cb_agrupador.ListCount - 1
                   frm.cb_filtro.AddItem (cb_agrupador.List(i))
                Next i
                frm.Separador = " - "
            Case 10
                For i = 0 To cb_tipo_producto.ListCount - 1
                   frm.cb_filtro.AddItem (cb_tipo_producto.List(i))
                Next i
                frm.Separador = " - "
            Case 11
                For i = 0 To cb_tipo_segmento.ListCount - 1
                   frm.cb_filtro.AddItem (cb_tipo_segmento.List(i))
                Next i
                frm.Separador = " - "
            Case 12
                For i = 0 To cb_prioritario.ListCount - 1
                    frm.cb_filtro.AddItem (cb_prioritario.List(i))
                Next i
                frm.Separador = " - "
            Case 13
                For i = 0 To cb_habilitado.ListCount - 1
                    frm.cb_filtro.AddItem (cb_habilitado.List(i))
                Next i
                frm.Separador = ""
            Case 14
                For i = 0 To cb_es_promo.ListCount - 1
                    frm.cb_filtro.AddItem (cb_es_promo.List(i))
                Next i
                frm.Separador = ""
            Case 15
                For i = 0 To cb_alta_inmediata_sva.ListCount - 1
                    frm.cb_filtro.AddItem (cb_alta_inmediata_sva.List(i))
                Next i
                frm.Separador = ""
            End Select
            frm.cb_filtro.ListIndex = 0
        End If
        
                
        frm.Show vbModal
        
        Select Case frm.Valor
        Case "Aplicar"
           Call suFiltro(li_paquetes, ColumnHeader.Index - 1, frm.Operador, frm.Filtro)
           
           ColumnHeader.Text = IIf(InStr(1, ColumnHeader.Text, "[F] ", vbTextCompare) > 0, Mid(ColumnHeader.Text, InStr(1, ColumnHeader.Text, "[F] ", vbTextCompare) + 4), ColumnHeader.Text)
           ColumnHeader.Text = "[F] " + ColumnHeader.Text
           
        Case "Borrar"
           Call suLlenaPaquetes
           
           For i = 1 To li_paquetes.ColumnHeaders.Count
              If InStr(1, li_paquetes.ColumnHeaders.Item(i).Text, "[F] ", vbTextCompare) > 0 Then
                 li_paquetes.ColumnHeaders.Item(i).Text = Mid(li_paquetes.ColumnHeaders.Item(i).Text, 5)
              End If
           Next i
        
        End Select
    End If
    
    Set frm = Nothing

End Sub

Private Sub li_seleccionados_Click()
    Call suEnable_Ctl1(Me.Tag, cmd_deselecciona)
    cmd_selecciona.Enabled = False
End Sub

Private Sub li_detalle_paquetes_ItemClick(ByVal Item As MSComctlLib.ListItem)
        
    Call Su_Asigna_Combo(cb_tipo_elemento, li_detalle_paquetes.ListItems(Item.Index).SubItems(1))
    
    tx_codi_elemento.Text = li_detalle_paquetes.ListItems(Item.Index)
    tx_desc_elemento.Text = Trim(li_detalle_paquetes.ListItems(Item.Index).SubItems(2))
    
    tx_peri_gracia.Text = Trim(Str(Val(li_detalle_paquetes.ListItems(Item.Index).SubItems(3))))
    tx_dura_vigen.Text = Trim(Str(Val(li_detalle_paquetes.ListItems(Item.Index).SubItems(4))))
    tx_peri_cobro.Text = Trim(Str(Val(li_detalle_paquetes.ListItems(Item.Index).SubItems(5))))
    
    Call Su_Asigna_Combo(cb_prop_elemento, li_detalle_paquetes.ListItems(Item.Index).SubItems(6), True)
    
    tx_interes.Text = Trim(Str(Val(li_detalle_paquetes.ListItems(Item.Index).SubItems(7))))

    Call Su_Asigna_Combo(cb_flag_competencia, li_detalle_paquetes.ListItems(Item.Index).SubItems(8), True)
    
    Call Su_Asigna_Combo(cb_flag_obligatorio, li_detalle_paquetes.ListItems(Item.Index).SubItems(9), True)
    
    Call Su_Asigna_Combo(cb_area, li_detalle_paquetes.ListItems(Item.Index).SubItems(11), True)
    
    ch_adelanta_peri_gracia.Value = IIf(li_detalle_paquetes.ListItems(Item.Index).SubItems(12) = "S", 1, 0)
    ch_adelanta_dura_vigen.Value = IIf(li_detalle_paquetes.ListItems(Item.Index).SubItems(13) = "S", 1, 0)
    tx_dias_duracion.Text = Trim(Str(Val(li_detalle_paquetes.ListItems(Item.Index).SubItems(14))))

    ' Se agrega fecha de inicio y fin. SI14323 MJPEREZ 14-mar-2012
    dp_fech_inicio.Value = IIf(li_detalle_paquetes.ListItems(Item.Index).SubItems(15) = "", "", li_detalle_paquetes.ListItems(Item.Index).SubItems(15))
    dp_fech_termino.Value = IIf(li_detalle_paquetes.ListItems(Item.Index).SubItems(16) = "", "", li_detalle_paquetes.ListItems(Item.Index).SubItems(16))
    
    
    cb_tipo_elem_rel.Clear
    cb_codi_elem_rel.Clear
    
    If li_detalle_paquetes.ListItems(Item.Index).SubItems(1) = "R" Or li_detalle_paquetes.ListItems(Item.Index).SubItems(1) = "V" Then
        cb_tipo_elem_rel.AddItem " "
        cb_tipo_elem_rel.AddItem "E - Equipo"
        cb_tipo_elem_rel.AddItem "S - Servicio Adicional"
        cb_tipo_elem_rel.ListIndex = -1
    End If
    
    Call Su_Asigna_Combo(cb_tipo_elem_rel, li_detalle_paquetes.ListItems(Item.Index).SubItems(17))
    Call Su_Asigna_Combo(cb_codi_elem_rel, li_detalle_paquetes.ListItems(Item.Index).SubItems(18))
    Call Su_Asigna_Combo(cb_tecnologia, li_detalle_paquetes.ListItems(Item.Index).SubItems(19))
    Call Su_Asigna_Combo(cb_tipo_peticion, li_detalle_paquetes.ListItems(Item.Index).SubItems(20))
    
    If li_detalle_paquetes.ListItems(Item.Index).SubItems(1) = "E" Then
        lbl_tecnologia.Visible = True
        cb_tecnologia.Visible = True
    Else
        lbl_tecnologia.Visible = False
        cb_tecnologia.Visible = False
    End If
    
    If li_detalle_paquetes.ListItems(Item.Index).SubItems(1) = "V" Then
        lbl_tipo_peticion.Visible = True
        cb_tipo_peticion.Visible = True
    Else
        lbl_tipo_peticion.Visible = False
        cb_tipo_peticion.Visible = False
    End If
    
    If Item.Index > 0 And Item.Index < li_detalle_paquetes.ListItems.Count Then
        Call suEnable_Ctl1(Me.Tag, co_inseelem)
        Call suEnable_Ctl1(Me.Tag, co_actuelem)
        Call suEnable_Ctl1(Me.Tag, co_elimelem)
    Else
        Call suEnable_Ctl1(Me.Tag, co_inseelem)
        co_actuelem.Enabled = False
        co_elimelem.Enabled = False
        
    End If

    li_detalle_paquetes.ListItems(Item.Index).Selected = True
    
End Sub


Private Sub li_paquetes_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Integer, bln As Boolean
    
    If Item.Index > 0 And Item.Index < li_paquetes.ListItems.Count Then
        If li_paquetes.ListItems(Item.Index).Checked Then
            Call suEnable_Ctl1(Me.Tag, cmd_exportar)
            cmdBorrarseleccion.Enabled = True
        Else
            bln = False
            For i = 1 To li_paquetes.ListItems.Count
                 If li_paquetes.ListItems(i).Checked Then
                     bln = True
                     Exit For
                 End If
            Next i
            If bln = False Then
                 cmd_exportar.Enabled = False
                 cmdBorrarseleccion.Enabled = False
            End If
        End If
    End If
    
End Sub

Private Sub li_paquetes_ItemClick(ByVal Item As MSComctlLib.ListItem)

    Dim codi_paquete As Long, Desc_Paquete As String, fech_inivigen As String, fech_finvigen As String, Max_Venta As Integer, Max_Renta As Integer, Max_Equipo As Integer, Max_Servicio As Integer, Codi_Agrupa As String, Ret As Integer
        
    lb_codi_paquete.Caption = li_paquetes.ListItems(Item.Index)
    tx_desc_paquete.Text = li_paquetes.ListItems(Item.Index).SubItems(1)
    
    frmPaquetes.Caption = "INGRESO Y MODIFICACCION DE PAQUETES " & "[" & lb_codi_paquete.Caption & " - " & tx_desc_paquete.Text & "]"
    
    'tx_fech_inivigen.Text = li_paquetes.ListItems(Item.index).SubItems(2)
    'tx_fech_finvigen.Text = li_paquetes.ListItems(Item.index).SubItems(3)
    If li_paquetes.ListItems(Item.Index).SubItems(3) = "" Then
        dtFechaFin.Value = Now
        dtFechaFin.Value = ""
    End If

    dtFechaInicio.Value = IIf(li_paquetes.ListItems(Item.Index).SubItems(2) = "", Now, li_paquetes.ListItems(Item.Index).SubItems(2))
    dtFechaFin.Value = IIf(li_paquetes.ListItems(Item.Index).SubItems(3) = "", "", li_paquetes.ListItems(Item.Index).SubItems(3))
    
    tx_max_renta.Text = Val(li_paquetes.ListItems(Item.Index).SubItems(4))
    tx_max_venta.Text = Val(li_paquetes.ListItems(Item.Index).SubItems(5))
    tx_max_equipo.Text = Val(li_paquetes.ListItems(Item.Index).SubItems(6))
    tx_max_servicio.Text = Val(li_paquetes.ListItems(Item.Index).SubItems(7))
    
    Call Su_Asigna_Combo(cb_agrupador, li_paquetes.ListItems(Item.Index).SubItems(8), True)
    Call Su_Asigna_Combo(cb_tipo_producto, li_paquetes.ListItems(Item.Index).SubItems(9), True)
    Call Su_Asigna_Combo(cb_tipo_segmento, li_paquetes.ListItems(Item.Index).SubItems(10), True)
    'comboBox prioritario
    Call Su_Asigna_Combo(cb_prioritario, li_paquetes.ListItems(Item.Index).SubItems(11), True)
    

    cb_habilitado = IIf(li_paquetes.ListItems(Item.Index).SubItems(12) = "", "S", li_paquetes.ListItems(Item.Index).SubItems(12))
    cb_es_promo = IIf(li_paquetes.ListItems(Item.Index).SubItems(13) = "", "S", li_paquetes.ListItems(Item.Index).SubItems(13))
    cb_alta_inmediata_sva = IIf(li_paquetes.ListItems(Item.Index).SubItems(14) = "", "S", li_paquetes.ListItems(Item.Index).SubItems(14))
    
    If Item.Index > 0 And Item.Index < li_paquetes.ListItems.Count Then
        co_insepaq.Enabled = False
        
        Call suEnable_Ctl1(Me.Tag, co_actupaq)
        Call suEnable_Ctl1(Me.Tag, co_elimpaq)
        Call suEnable_Ctl1(Me.Tag, co_clonar)
        
        SSTab1.TabEnabled(1) = True
        SSTab1.TabEnabled(2) = True
        SSTab1.TabEnabled(3) = True
        SSTab1.TabEnabled(5) = True
        
        Call MostrarDetallePaquete(Val(li_paquetes.ListItems(Item.Index)), 1)
        Call MostrarLocalidades(Val(lb_codi_paquete.Caption))
        Call MostrarCanalesVenta(Val(lb_codi_paquete.Caption))
    Else
        
        Call suEnable_Ctl1(Me.Tag, co_insepaq)
        co_actupaq.Enabled = False
        co_elimpaq.Enabled = False
        co_clonar.Enabled = False
        SSTab1.TabEnabled(1) = False
        SSTab1.TabEnabled(2) = False
        SSTab1.TabEnabled(3) = False
        SSTab1.TabEnabled(5) = False
        
        Ret = Srv_Siaq_Paquete_Open()
        Ret = Srv_Siaq_Paquete_Read(codi_paquete)
        Ret = Srv_Siaq_Paquete_Close()
    
        lb_codi_paquete.Caption = Trim(Str(codi_paquete))
        
        li_paquetes.ListItems(Item.Index).EnsureVisible
        
    End If
    
    li_paquetes.ListItems(Item.Index).Selected = True
    
    Call Su_Limpiar_Grilla(gr_proyecto)
    concepto_proy.Text = ""
    If li_detalle_paquetes.ListItems.Count > 0 Then
        If Trim(Str(Val(li_paquetes.ListItems(Item.Index).SubItems(8)))) = 40 Then
          concepto_proy.Text = li_detalle_paquetes.ListItems(1).Text
          Call li_detalle_paquetes_ItemClick(li_detalle_paquetes.ListItems(li_detalle_paquetes.ListItems.Count))
        
        Call suLlenaGrillaProyectos(gr_proyecto)
        Else
          frmPaquetes.concepto_proy.Text = ""
        End If
        
      End If
End Sub

Private Sub MostrarLocalidades(codi_paquete As Integer)
    Dim Total As Integer, i As Integer, Ret As Integer
    Dim lid As ListItem
    Dim lis As ListItem
    Dim cont As Long
    
    Dim codi_localida As String, desc_localida As String, fech_ini As String, fech_fin As String, seleccionada As Integer
    
    On Error Resume Next
    

    li_localidades.Visible = False
    li_localidades.ListItems.Clear

    li_localidades_seleccionadas.Visible = False
    li_localidades_seleccionadas.ListItems.Clear
    
    Screen.MousePointer = vbHourglass
    cont = 0
    Total = Srv_Loca_Seleccionados_Open(codi_paquete)
    For i = 1 To Total
        Total = Srv_Loca_Seleccionados_Read(codi_localida, desc_localida, fech_ini, fech_fin, seleccionada)
        If seleccionada = 1 Then
            Set lid = li_localidades_seleccionadas.ListItems.Add(, , Trim$(desc_localida))
            lid.SubItems(1) = Trim$(codi_localida)
            lid.SubItems(2) = Trim$(fech_ini)
            lid.SubItems(3) = Trim$(fech_fin)
        Else
            Set lid = li_localidades.ListItems.Add(, , Trim$(desc_localida))
            lid.SubItems(1) = Trim$(codi_localida)
            lid.SubItems(2) = Trim$(fech_ini)
            lid.SubItems(3) = Trim$(fech_fin)
        End If
    Next i
    Ret = Srv_Loca_Seleccionados_Close()
    
    Screen.MousePointer = vbDefault
    
    li_localidades.Visible = True
    li_localidades_seleccionadas.Visible = True
    cmd_desel_localidad.Enabled = False
    cmd_sel_localidad.Enabled = False
    cmd_act_fecha.Enabled = False

End Sub

Private Sub MostrarCanalesVenta(codi_paquete As Integer)
    Dim Total As Integer, i As Integer, Ret As Integer
    Dim lid As ListItem
    Dim lis As ListItem
    Dim cont As Long
    
    Dim codi_canal_vta As Integer, desc_canal_vta As String, seleccionado As Integer
    
    On Error Resume Next
    

    li_canales.Visible = False
    li_canales.ListItems.Clear

    li_canales_seleccionados.Visible = False
    li_canales_seleccionados.ListItems.Clear
    
    Screen.MousePointer = vbHourglass
    cont = 0

    Total = Srv_Canal_Seleccionados_Open(codi_paquete)
    For i = 1 To Total
        Total = Srv_Canal_Seleccionados_Read(codi_canal_vta, desc_canal_vta, seleccionado)
        If seleccionado = 1 Then
            Set lid = li_canales_seleccionados.ListItems.Add(, , Trim$(desc_canal_vta))
            lid.SubItems(1) = Trim$(codi_canal_vta)
        Else
            Set lid = li_canales.ListItems.Add(, , Trim$(desc_canal_vta))
            lid.SubItems(1) = Trim$(codi_canal_vta)
        End If
    Next i
    Ret = Srv_Canal_Seleccionados_Close()
    
    Screen.MousePointer = vbDefault
    
    li_canales.Visible = True
    li_canales_seleccionados.Visible = True
    cmd_desel_canal.Enabled = False
    cmd_sel_canal.Enabled = False
    
End Sub

Private Sub MostrarDetallePaquete(ByVal codi_paquete As Integer, ByVal Index As Integer)
  
    Dim Total As Integer, i As Integer, Ret As Integer
    Dim Tipo_Elemento As String, Codi_Elemento As String, Peri_Gracia As Integer, Dura_Vigen As Integer, Peri_Cobro As Integer, Prop_Elemento As String, interes As Double, Flag_Competencia As String, flag_obligatorio As String, Fech_Gracia As String, id_elemento As Long, desc_elemento As String, codi_area As String
    Dim Adelanta_Peri_Gracia As String, Adelanta_Dura_Vigen As String ' SSIN#11087. JPABLOS. 17-ago-2009.
    Dim Dias_Duracion As Integer ' Proyecto OMV. JPABLOS. 22-jun-2011.
    Dim Fech_Inicio As String, Fech_Termino As String ' Se agrega fecha de inicio y fin. SI14323 MJPEREZ 14-mar-2012
    Dim Tipo_Elem_Rel As String, Codi_Elem_Rel As String, Tecnologia As String  ' SI 15664 JMARTINE
    Dim Tipo_Peticion As String
    
    li_detalle_paquetes.Visible = False
    
    li_detalle_paquetes.ListItems.Clear

    Screen.MousePointer = vbHourglass
    
    Total = Srv_Elementos_Open(codi_paquete)
    For i = 1 To Total
        Ret = Srv_Elementos_Read(Tipo_Elemento, _
                                    Codi_Elemento, _
                                    Peri_Gracia, _
                                    Dura_Vigen, _
                                    Peri_Cobro, _
                                    Prop_Elemento, _
                                    interes, _
                                    Flag_Competencia, _
                                    flag_obligatorio, _
                                    Fech_Gracia, _
                                    id_elemento, _
                                    desc_elemento, _
                                    codi_area, _
                                    Adelanta_Peri_Gracia, _
                                    Adelanta_Dura_Vigen, _
                                    Dias_Duracion, _
                                    Fech_Inicio, _
                                    Fech_Termino, _
                                    Tipo_Elem_Rel, _
                                    Codi_Elem_Rel, _
                                    Tecnologia, _
                                    Tipo_Peticion)
        
        Dim xValores(22) As String
        xValores(0) = Trim$(Codi_Elemento)
        xValores(1) = Trim$(Tipo_Elemento)
        xValores(2) = Trim$(desc_elemento)
        xValores(3) = Trim$(Peri_Gracia)
        xValores(4) = Trim$(Dura_Vigen)
        xValores(5) = Trim$(Peri_Cobro)
        xValores(6) = Trim$(Prop_Elemento)
        xValores(7) = Trim$(interes)
        xValores(8) = Trim$(Flag_Competencia)
        xValores(9) = Trim$(flag_obligatorio)
        xValores(10) = Trim$(Fech_Gracia)
        xValores(11) = Trim$(codi_area)
        xValores(12) = Trim$(Adelanta_Peri_Gracia)
        xValores(13) = Trim$(Adelanta_Dura_Vigen)
        xValores(14) = Trim$(Dias_Duracion)
        xValores(15) = Trim$(Fech_Inicio)
        xValores(16) = Trim$(Fech_Termino)
        xValores(17) = Trim$(Tipo_Elem_Rel)
        xValores(18) = Trim$(Codi_Elem_Rel)
        xValores(19) = Trim$(Tecnologia)
        xValores(20) = Trim$(Tipo_Peticion)
        xValores(21) = Trim$(Str(id_elemento))
        Call suAgregaToLista(li_detalle_paquetes, xValores)
    Next i
    
    Ret = Srv_Elementos_Close()
    Screen.MousePointer = vbDefault
    li_detalle_paquetes.Visible = True
    
End Sub




Private Sub tx_busca_Change()
    SendKeys "{END}"
    Call gsuScanStringLW(li_paquetes, tx_busca, 1, 22)
End Sub

Private Sub tx_busca_KeyUp(KeyCode As Integer, Shift As Integer)
    Call gsuScanStringLW(li_paquetes, tx_busca, 1, 22)
End Sub

Private Sub tx_codi_elemento_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then
        fayuda.Show vbModal
        If Gp_Ayuda.Sino Then
            tx_codi_elemento.Text = Gp_Ayuda.Codi_Vuelta
            tx_desc_elemento.Text = Gp_Ayuda.Desc_Codi_Vuelta
        End If
        Gp_Ayuda.Sino = False
    End If
End Sub

Private Sub tx_codi_elemento_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
        KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 Then
        KEYASCII = 0
    End If
End Sub

Private Sub tx_codi_elemento_LostFocus()
    If Trim$(tx_codi_elemento) <> "" Then
        If Not Fn_Validacion(Fn_Valor_Combo(cb_tipo_elemento), tx_codi_elemento.Text, tx_desc_elemento) Then
            MsgBox "C�digo no v�lido"
            tx_codi_elemento.Text = ""
            tx_codi_elemento.SetFocus
        End If
    End If
End Sub

Private Sub tx_codi_concepto_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then
        fayuda.Show vbModal
        If Gp_Ayuda.Sino Then
            tx_codi_concepto.Text = Gp_Ayuda.Codi_Vuelta
            tx_desc_concepto.Text = Gp_Ayuda.Desc_Codi_Vuelta
        End If
        Gp_Ayuda.Sino = False
    End If
End Sub

Private Sub tx_codi_concepto_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
        KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 Then
        KEYASCII = 0
    End If
End Sub

Private Sub tx_codi_concepto_LostFocus()
    If Trim$(tx_codi_concepto) <> "" Then
        If Not Fn_Validacion("V", tx_codi_concepto.Text, tx_desc_concepto) Then
            MsgBox "C�digo no v�lido"
            tx_codi_concepto.Text = ""
            tx_codi_concepto.SetFocus
        End If
    End If
End Sub

Private Function Fn_Validacion(ByVal TipoElemento As String, ByVal CodigoElemento As String, ByRef Texto As TextBox)

    Dim Ret As Integer
    Dim desc_facilida As String, zona As Integer, cent As Integer, desc_fonofacilida As String, tipo_validacion As String
    Dim desc_concepto As String, vlor_concepto As Double, Flag_Afectiva As String, Tasa_Interes As Double, codi_interno As String, unid_monetar As String, inst_renta As String, codi_hijo As Long, flag_equipo As String
    Dim desc_equipo As String, pide_cantidad As String, flag_estandar As String, Codi_Tipo As Integer


    Fn_Validacion = True

    Select Case UCase(TipoElemento)
        Case "S":
            Ret = Srv_FacilidaDesc_Open(Trim$(CodigoElemento))
            If Ret = 0 Then
                Fn_Validacion = False
            Else
                Ret = Srv_FacilidaDesc_Read(desc_facilida, desc_fonofacilida, tipo_validacion)
                Texto.Text = desc_facilida
            End If
            Ret = Srv_FacilidaDesc_Close()
        Case "E":
            Ret = SrvEquipoDescOpen(Trim$(CodigoElemento))
            If Ret = 0 Then
                Fn_Validacion = False
            Else
                Ret = SrvEquipoDescRead(desc_equipo, pide_cantidad, flag_estandar, Codi_Tipo)
                Texto.Text = desc_equipo
            End If
            Ret = SrvEquipoDescClose()
        Case "R", "V":
            Ret = Srv_DescConcepto_Open(Val(CodigoElemento), Gp_VAplicacion.codi_empresa)
            If Ret = -1 Then
                Fn_Validacion = False
            Else
                Ret = Srv_DescConcepto_Read(desc_concepto, vlor_concepto, Flag_Afectiva, Tasa_Interes, codi_interno, unid_monetar, inst_renta, codi_hijo, flag_equipo)
                Texto.Text = desc_concepto
            End If
            Ret = Srv_DescConcepto_Close()
    End Select

End Function


Private Sub tx_max_equipo_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
        KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 Then
        KEYASCII = 0
    End If
End Sub

Private Sub tx_max_renta_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
        KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 Then
        KEYASCII = 0
    End If
End Sub

Private Sub tx_max_servicio_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
        KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 Then
        KEYASCII = 0
    End If
End Sub

Private Sub tx_max_venta_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
        KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 Then
        KEYASCII = 0
    End If
End Sub



Private Sub tx_dias_duracion_Change()
  Su_Set_Vigencia_Cpto tx_dias_duracion, True
End Sub

Private Sub tx_dura_vigen_Change()
  Su_Set_Vigencia_Cpto tx_dura_vigen, True
End Sub

Private Sub tx_dura_vigen_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
      KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 And KEYASCII <> Asc(".") Then
       KEYASCII = 0
    End If
End Sub


Private Sub Su_Set_Vigencia_Cpto(tb As TextBox, ingreso_relativo As Boolean)
    If ingreso_relativo = False And tb.Text <> "" Then
        tx_dias_duracion.ForeColor = vbGrayText
        tx_dura_vigen.ForeColor = vbGrayText
        tx_peri_gracia.ForeColor = vbGrayText
        tx_dias_duracion.Text = "0"
        tx_dura_vigen.Text = "0"
        tx_peri_gracia.Text = "0"
    ElseIf ingreso_relativo = True And tb.Text <> "" And tb.Text <> "0" Then
        tx_dias_duracion.ForeColor = vbBlack
        tx_dura_vigen.ForeColor = vbBlack
        tx_peri_gracia.ForeColor = vbBlack
        dp_fech_inicio.Value = ""
        dp_fech_termino.Value = ""
    End If
End Sub

Private Sub tx_peri_cobro_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
        KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 And KEYASCII <> Asc(".") Then
        KEYASCII = 0
    End If
End Sub

Private Sub tx_peri_gracia_Change()
  Su_Set_Vigencia_Cpto tx_peri_gracia, True
End Sub

Private Sub tx_peri_gracia_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
      KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 And KEYASCII <> Asc(".") Then
       KEYASCII = 0
    End If
End Sub

Private Sub tx_prioridad_KeyPress(KEYASCII As Integer)
    If Chr(KEYASCII) = "'" Then
        KEYASCII = 0
    End If
    If (KEYASCII < Asc("0") Or KEYASCII > Asc("9")) And KEYASCII <> 8 Then
        KEYASCII = 0
    End If
End Sub

Private Sub txtBuscarCanales_Change()
    SendKeys "{END}"
    Call gsuScanStringLW(li_canales, txtBuscarCanales, 0, 24)
End Sub

Private Sub txtBuscarCanales_KeyUp(KeyCode As Integer, Shift As Integer)
    Call gsuScanStringLW(li_canales, txtBuscarCanales, 0, 24)
End Sub

Private Sub txtBuscarLocalidades_Change()
    SendKeys "{END}"
    Call gsuScanStringLW(li_localidades, txtBuscarLocalidades, 0, 24)
End Sub

Private Sub txtBuscarLocalidades_KeyUp(KeyCode As Integer, Shift As Integer)
    Call gsuScanStringLW(li_localidades, txtBuscarLocalidades, 0, 24)
End Sub


Private Sub llenaHabilitado()
    With cb_habilitado
        .Clear
        .AddItem "S"
        .AddItem "N"
        .ListIndex = 0
    End With
End Sub

Private Sub llenaPromo()
    With cb_es_promo
        .Clear
        .AddItem "S"
        .AddItem "N"
        .ListIndex = 0
    End With
End Sub

Private Sub llenaAltaInmediata()
    With cb_alta_inmediata_sva
        .Clear
        .AddItem "S"
        .AddItem "N"
        .ListIndex = 0
    End With
End Sub

