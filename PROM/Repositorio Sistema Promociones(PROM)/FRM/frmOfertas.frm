VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmOfertas 
   Caption         =   "Mantenedor de Ofertas"
   ClientHeight    =   9195
   ClientLeft      =   120
   ClientTop       =   420
   ClientWidth     =   17685
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9195
   ScaleWidth      =   17685
   Tag             =   "FRMOFERTAS"
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab SSTab1 
      Height          =   8895
      Left            =   120
      TabIndex        =   0
      Tag             =   "DOF_AGREGAR"
      Top             =   120
      Width           =   17415
      _ExtentX        =   30718
      _ExtentY        =   15690
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      Tab             =   1
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Ofertas Existentes"
      TabPicture(0)   =   "frmOfertas.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cmdBorrarseleccion"
      Tab(0).Control(1)=   "cmd_listado"
      Tab(0).Control(2)=   "cb_agrupador"
      Tab(0).Control(3)=   "CommonDialog1"
      Tab(0).Control(4)=   "cmd_importar"
      Tab(0).Control(5)=   "cmd_exportar"
      Tab(0).Control(6)=   "li_ofertas"
      Tab(0).Control(7)=   "txtBuscar"
      Tab(0).Control(8)=   "cmdEliminarOferta"
      Tab(0).Control(9)=   "cmdCopiaOferta"
      Tab(0).Control(10)=   "cmdNuevaOferta"
      Tab(0).Control(11)=   "cmdModificarOferta"
      Tab(0).Control(12)=   "txtCodiOferta"
      Tab(0).Control(13)=   "cmdVolver"
      Tab(0).Control(14)=   "chkOfertaNoVig"
      Tab(0).Control(15)=   "chkOfertaVig"
      Tab(0).Control(16)=   "chkPackCompartido"
      Tab(0).Control(17)=   "txtDescOferta"
      Tab(0).Control(18)=   "txtObsOferta"
      Tab(0).Control(19)=   "txtValOferta"
      Tab(0).Control(20)=   "txtValPromocion"
      Tab(0).Control(21)=   "txtCodDesc"
      Tab(0).Control(22)=   "txtCodCargo"
      Tab(0).Control(23)=   "txtUnidMonetar"
      Tab(0).Control(24)=   "dtFechaFin"
      Tab(0).Control(25)=   "dtFechaInicio"
      Tab(0).Control(26)=   "Label10"
      Tab(0).Control(27)=   "Label4"
      Tab(0).Control(28)=   "lblFechaIni"
      Tab(0).Control(29)=   "lblFechaFin"
      Tab(0).Control(30)=   "lblDescOferta"
      Tab(0).Control(31)=   "lblObsOferta"
      Tab(0).Control(32)=   "lblValOferta"
      Tab(0).Control(33)=   "lvlValPromocion"
      Tab(0).Control(34)=   "Label2"
      Tab(0).Control(35)=   "Label3"
      Tab(0).Control(36)=   "Label9"
      Tab(0).ControlCount=   37
      TabCaption(1)   =   "Detalle Oferta"
      TabPicture(1)   =   "frmOfertas.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lblTipoPaquete"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lblExclusion"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lblCodPaquete"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lblArea"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "lblCargo"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Label1"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "lblValCargo"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "lblValDesc"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "lblDescProm"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "lblValDescProm"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "lblMesesProm"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "lblClasPaquete"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "lblPorcDesc"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "lblValTotal"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "Label5"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "Label7"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).Control(16)=   "Label8"
      Tab(1).Control(16).Enabled=   0   'False
      Tab(1).Control(17)=   "lblObligatorio"
      Tab(1).Control(17).Enabled=   0   'False
      Tab(1).Control(18)=   "txtPaqAdic"
      Tab(1).Control(18).Enabled=   0   'False
      Tab(1).Control(19)=   "cmdAgregarDetO"
      Tab(1).Control(19).Enabled=   0   'False
      Tab(1).Control(20)=   "cmdEliminarDetO"
      Tab(1).Control(20).Enabled=   0   'False
      Tab(1).Control(21)=   "cbxTipoPaquete"
      Tab(1).Control(21).Enabled=   0   'False
      Tab(1).Control(22)=   "cbxExclusion"
      Tab(1).Control(22).Enabled=   0   'False
      Tab(1).Control(23)=   "txtCodPaquete"
      Tab(1).Control(23).Enabled=   0   'False
      Tab(1).Control(24)=   "cmdModificarDetO"
      Tab(1).Control(24).Enabled=   0   'False
      Tab(1).Control(25)=   "cbxArea"
      Tab(1).Control(25).Enabled=   0   'False
      Tab(1).Control(26)=   "txtCargo"
      Tab(1).Control(26).Enabled=   0   'False
      Tab(1).Control(27)=   "txtDescuento"
      Tab(1).Control(27).Enabled=   0   'False
      Tab(1).Control(28)=   "txtValCargo"
      Tab(1).Control(28).Enabled=   0   'False
      Tab(1).Control(29)=   "txtValDesc"
      Tab(1).Control(29).Enabled=   0   'False
      Tab(1).Control(30)=   "txtValDescProm"
      Tab(1).Control(30).Enabled=   0   'False
      Tab(1).Control(31)=   "txtMesesProm"
      Tab(1).Control(31).Enabled=   0   'False
      Tab(1).Control(32)=   "txtDescProm"
      Tab(1).Control(32).Enabled=   0   'False
      Tab(1).Control(33)=   "cbxClasPaquete"
      Tab(1).Control(33).Enabled=   0   'False
      Tab(1).Control(34)=   "txtPorcDesc"
      Tab(1).Control(34).Enabled=   0   'False
      Tab(1).Control(35)=   "txtValTotal"
      Tab(1).Control(35).Enabled=   0   'False
      Tab(1).Control(36)=   "txtValDescPC"
      Tab(1).Control(36).Enabled=   0   'False
      Tab(1).Control(37)=   "txtDescPC"
      Tab(1).Control(37).Enabled=   0   'False
      Tab(1).Control(38)=   "txtRowid"
      Tab(1).Control(38).Enabled=   0   'False
      Tab(1).Control(39)=   "chkIniciaCobro"
      Tab(1).Control(39).Enabled=   0   'False
      Tab(1).Control(40)=   "li_detalle_ofertas"
      Tab(1).Control(40).Enabled=   0   'False
      Tab(1).Control(41)=   "cbxObligatorio"
      Tab(1).Control(41).Enabled=   0   'False
      Tab(1).ControlCount=   42
      TabCaption(2)   =   "Localidades"
      TabPicture(2)   =   "frmOfertas.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lblFechaF"
      Tab(2).Control(1)=   "lblFechaI"
      Tab(2).Control(2)=   "Label6"
      Tab(2).Control(3)=   "li_localidades_seleccionadas"
      Tab(2).Control(4)=   "dtpFechaILoc"
      Tab(2).Control(5)=   "dtpFechaFLoc"
      Tab(2).Control(6)=   "li_localidades"
      Tab(2).Control(7)=   "txtBuscarLocalidades"
      Tab(2).Control(8)=   "cmd_sel_localidad"
      Tab(2).Control(9)=   "cmd_desel_localidad"
      Tab(2).Control(10)=   "cmd_act_fecha"
      Tab(2).Control(11)=   "cmd_sel_localidad_todos"
      Tab(2).Control(12)=   "cmd_desel_localidad_todos"
      Tab(2).ControlCount=   13
      TabCaption(3)   =   "Agrupador"
      TabPicture(3)   =   "frmOfertas.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "tx_codi_agrupador"
      Tab(3).Control(1)=   "tx_desc_agrupador"
      Tab(3).Control(2)=   "co_eliminar"
      Tab(3).Control(3)=   "co_actualizar"
      Tab(3).Control(4)=   "co_agregar"
      Tab(3).Control(5)=   "gr_agrupador"
      Tab(3).Control(6)=   "Label30"
      Tab(3).Control(7)=   "Label31"
      Tab(3).ControlCount=   8
      TabCaption(4)   =   "Canales de Venta"
      TabPicture(4)   =   "frmOfertas.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "cmd_desel_canal_todos"
      Tab(4).Control(1)=   "cmd_sel_canal_todos"
      Tab(4).Control(2)=   "cmd_desel_canal"
      Tab(4).Control(3)=   "cmd_sel_canal"
      Tab(4).Control(4)=   "li_canales"
      Tab(4).Control(5)=   "li_canales_seleccionados"
      Tab(4).ControlCount=   6
      Begin VB.ComboBox cbxObligatorio 
         Height          =   315
         ItemData        =   "frmOfertas.frx":008C
         Left            =   8520
         List            =   "frmOfertas.frx":008E
         Style           =   2  'Dropdown List
         TabIndex        =   105
         Top             =   2280
         Width           =   2055
      End
      Begin MSComctlLib.ListView li_detalle_ofertas 
         Height          =   6075
         Left            =   240
         TabIndex        =   73
         Top             =   2880
         Width           =   15495
         _ExtentX        =   27331
         _ExtentY        =   10716
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   26
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "20"
            Text            =   "Area"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "21"
            Text            =   "Tipo paquete"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Cod. Paquete"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Cod. Cargo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Cod. Desc."
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Valor total"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Porc. Desc."
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Valor Cargo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Valor Descuento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Desc. Prom."
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Valor desc. Prom."
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "Meses Prom."
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "22"
            Text            =   "Exclusi�n"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Object.Tag             =   "23"
            Text            =   "Clase paquete"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Text            =   "Marca"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Text            =   "Paq. Adicional"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Text            =   "Cod desc PC"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Text            =   "Valor desc PC"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   18
            Text            =   "Inicia Cobro"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Text            =   "rowid"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   20
            Text            =   "null"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   21
            Text            =   "null"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   22
            Text            =   "null"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   23
            Text            =   "null"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   24
            Text            =   "Alta Obligatoria"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   25
            Object.Width           =   0
         EndProperty
      End
      Begin VB.CheckBox chkIniciaCobro 
         Caption         =   "Inicia Cobro"
         Height          =   255
         Left            =   6960
         TabIndex        =   103
         Top             =   2280
         Width           =   1335
      End
      Begin VB.CommandButton cmd_desel_localidad_todos 
         Caption         =   "<<"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   102
         Tag             =   "LO_ELIMINAR"
         Top             =   5160
         Width           =   615
      End
      Begin VB.CommandButton cmd_sel_localidad_todos 
         Caption         =   ">>"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   101
         Tag             =   "LO_AGREGAR"
         Top             =   4440
         Width           =   615
      End
      Begin VB.CommandButton cmd_desel_canal_todos 
         Caption         =   "<<"
         Height          =   255
         Left            =   -67440
         TabIndex        =   98
         Tag             =   "SE_ELIMINAR_TODO"
         Top             =   4080
         Width           =   615
      End
      Begin VB.CommandButton cmd_sel_canal_todos 
         Caption         =   ">>"
         Height          =   255
         Left            =   -67440
         TabIndex        =   97
         Tag             =   "SE_AGREGAR_TODO"
         Top             =   3360
         Width           =   615
      End
      Begin VB.CommandButton cmd_desel_canal 
         Caption         =   "<"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67440
         TabIndex        =   96
         Tag             =   "LO_ELIMINAR"
         Top             =   3720
         Width           =   615
      End
      Begin VB.CommandButton cmd_sel_canal 
         Caption         =   ">"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67440
         TabIndex        =   95
         Tag             =   "LO_AGREGAR"
         Top             =   3000
         Width           =   615
      End
      Begin VB.CommandButton cmdBorrarseleccion 
         Caption         =   "Borrar Seleccion"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   -59400
         TabIndex        =   94
         Top             =   7140
         Width           =   1245
      End
      Begin VB.CommandButton cmd_act_fecha 
         Caption         =   "Actualiza Fecha"
         Enabled         =   0   'False
         Height          =   375
         Left            =   -60960
         TabIndex        =   93
         Tag             =   "LO_ACTUALIZA"
         Top             =   1620
         Width           =   1695
      End
      Begin VB.CommandButton cmd_listado 
         Caption         =   "Listado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -59400
         TabIndex        =   92
         Top             =   6420
         Width           =   1245
      End
      Begin VB.CommandButton cmd_desel_localidad 
         Caption         =   "<"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   91
         Tag             =   "LO_ELIMINAR"
         Top             =   4860
         Width           =   615
      End
      Begin VB.CommandButton cmd_sel_localidad 
         Caption         =   ">"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67320
         TabIndex        =   90
         Tag             =   "LO_AGREGAR"
         Top             =   4140
         Width           =   615
      End
      Begin VB.TextBox tx_codi_agrupador 
         Enabled         =   0   'False
         Height          =   285
         Left            =   -73560
         TabIndex        =   85
         Top             =   780
         Width           =   3255
      End
      Begin VB.TextBox tx_desc_agrupador 
         Height          =   285
         Left            =   -73560
         TabIndex        =   84
         Top             =   1140
         Width           =   3255
      End
      Begin VB.CommandButton co_eliminar 
         Caption         =   "Eliminar"
         Height          =   375
         Left            =   -62040
         TabIndex        =   83
         Tag             =   "AG_ELIMINAR"
         Top             =   1860
         Width           =   1215
      End
      Begin VB.CommandButton co_actualizar 
         Caption         =   "Modificar"
         Height          =   375
         Left            =   -62040
         TabIndex        =   82
         Tag             =   "AG_MODIFICAR"
         Top             =   1380
         Width           =   1215
      End
      Begin VB.CommandButton co_agregar 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   -62040
         TabIndex        =   81
         Tag             =   "AG_AGREGAR"
         Top             =   900
         Width           =   1215
      End
      Begin VB.ComboBox cb_agrupador 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -64200
         Style           =   2  'Dropdown List
         TabIndex        =   79
         Top             =   2400
         Width           =   3705
      End
      Begin VB.TextBox txtRowid 
         Height          =   285
         Left            =   12960
         TabIndex        =   78
         Top             =   2400
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox txtBuscarLocalidades 
         Height          =   285
         Left            =   -74760
         TabIndex        =   76
         Top             =   1740
         Width           =   5055
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   -59280
         Top             =   8280
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton cmd_importar 
         Caption         =   "Importar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -59400
         TabIndex        =   75
         Tag             =   "OF_IMPORTAR"
         Top             =   5760
         Width           =   1245
      End
      Begin VB.CommandButton cmd_exportar 
         Caption         =   "Exportar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -59400
         TabIndex        =   74
         Tag             =   "OF_EXPORTAR"
         Top             =   5160
         Width           =   1245
      End
      Begin MSComctlLib.ListView li_ofertas 
         Height          =   6015
         Left            =   -74760
         TabIndex        =   72
         Top             =   2940
         Width           =   15135
         _ExtentX        =   26696
         _ExtentY        =   10610
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "Number"
            Text            =   "C�digo"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "Number"
            Text            =   "Cargo"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "Number"
            Text            =   "Dcto"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "Text"
            Text            =   "Descripci�n"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "Text"
            Text            =   "Observacion"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "Number"
            Text            =   "Valor oferta"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "Number"
            Text            =   "Valor promocion"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "Date"
            Text            =   "Fecha creaci�n"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "Date"
            Text            =   "Fecha Inicio"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Object.Tag             =   "Date"
            Text            =   "Fecha T�rmino"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "Combo"
            Text            =   "Pack Compartido"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Tag             =   "Number"
            Text            =   "Und. Monetaria"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "Combo"
            Text            =   "Agrupador"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Left            =   -74760
         TabIndex        =   70
         Top             =   2400
         Width           =   5055
      End
      Begin VB.CommandButton cmdEliminarOferta 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -59400
         TabIndex        =   64
         Tag             =   "OF_ELIMINAR"
         Top             =   3960
         Width           =   1215
      End
      Begin VB.CommandButton cmdCopiaOferta 
         Caption         =   "Copiar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -59400
         TabIndex        =   63
         Tag             =   "OF_COPIAR"
         Top             =   4440
         Width           =   1215
      End
      Begin VB.CommandButton cmdNuevaOferta 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -59400
         TabIndex        =   62
         Tag             =   "OF_AGREGAR"
         Top             =   3000
         Width           =   1215
      End
      Begin VB.CommandButton cmdModificarOferta 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -59400
         TabIndex        =   61
         Tag             =   "OF_MODIFICAR"
         Top             =   3480
         Width           =   1215
      End
      Begin VB.TextBox txtCodiOferta 
         Enabled         =   0   'False
         Height          =   285
         Left            =   -59400
         TabIndex        =   60
         Text            =   "Text1"
         Top             =   7860
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -59400
         TabIndex        =   59
         Top             =   900
         Width           =   1215
      End
      Begin VB.CheckBox chkOfertaNoVig 
         Caption         =   "Ofertas No Vigentes"
         Height          =   255
         Left            =   -66240
         TabIndex        =   58
         Top             =   2400
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin VB.CheckBox chkOfertaVig 
         Caption         =   "Ofertas Vigentes"
         Height          =   255
         Left            =   -68040
         TabIndex        =   57
         Top             =   2400
         Value           =   1  'Checked
         Width           =   1575
      End
      Begin VB.CheckBox chkPackCompartido 
         Caption         =   "Pack Compartido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -64200
         TabIndex        =   56
         ToolTipText     =   "Es aplicable a ofertas de Pack Compartido"
         Top             =   1200
         Width           =   1815
      End
      Begin VB.TextBox txtDescOferta 
         Height          =   285
         Left            =   -74760
         MaxLength       =   200
         TabIndex        =   44
         Top             =   1200
         Width           =   6495
      End
      Begin VB.TextBox txtObsOferta 
         Height          =   285
         Left            =   -74760
         MaxLength       =   2000
         TabIndex        =   43
         Top             =   1800
         Width           =   6495
      End
      Begin VB.TextBox txtValOferta 
         Height          =   285
         Left            =   -64200
         MaxLength       =   12
         TabIndex        =   42
         Text            =   "0"
         Top             =   1800
         Width           =   1815
      End
      Begin VB.TextBox txtValPromocion 
         Height          =   285
         Left            =   -62280
         MaxLength       =   12
         TabIndex        =   41
         Text            =   "0"
         Top             =   1800
         Width           =   1815
      End
      Begin VB.TextBox txtCodDesc 
         Height          =   285
         Left            =   -66120
         MaxLength       =   12
         TabIndex        =   40
         Text            =   "Text1"
         Top             =   1800
         Width           =   1815
      End
      Begin VB.TextBox txtCodCargo 
         Height          =   285
         Left            =   -68040
         MaxLength       =   12
         TabIndex        =   39
         Text            =   "Text1"
         Top             =   1800
         Width           =   1815
      End
      Begin VB.TextBox txtUnidMonetar 
         Enabled         =   0   'False
         Height          =   285
         Left            =   -60360
         TabIndex        =   38
         Top             =   1800
         Width           =   2175
      End
      Begin VB.TextBox txtDescPC 
         Height          =   285
         Left            =   8520
         MaxLength       =   12
         TabIndex        =   20
         Top             =   1080
         Width           =   1800
      End
      Begin VB.TextBox txtValDescPC 
         Height          =   285
         Left            =   4560
         MaxLength       =   12
         TabIndex        =   19
         Text            =   "0"
         ToolTipText     =   "Valor de descuento aplicable al titular del Pack Compartido"
         Top             =   1680
         Width           =   1800
      End
      Begin VB.TextBox txtValTotal 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2400
         MaxLength       =   12
         TabIndex        =   18
         Text            =   "Text1"
         Top             =   1680
         Width           =   1800
      End
      Begin VB.TextBox txtPorcDesc 
         Height          =   285
         Left            =   240
         MaxLength       =   5
         TabIndex        =   17
         Text            =   "0"
         Top             =   1680
         Width           =   1800
      End
      Begin VB.ComboBox cbxClasPaquete 
         Height          =   315
         Left            =   2400
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1080
         Width           =   2055
      End
      Begin VB.TextBox txtDescProm 
         Height          =   285
         Left            =   6480
         MaxLength       =   12
         TabIndex        =   15
         Text            =   "Text1"
         Top             =   1680
         Width           =   2000
      End
      Begin VB.TextBox txtMesesProm 
         Height          =   285
         Left            =   10680
         MaxLength       =   12
         TabIndex        =   14
         Text            =   "0"
         Top             =   1680
         Width           =   1575
      End
      Begin VB.TextBox txtValDescProm 
         Height          =   285
         Left            =   8520
         MaxLength       =   12
         TabIndex        =   13
         Text            =   "0"
         Top             =   1680
         Width           =   2000
      End
      Begin VB.TextBox txtValDesc 
         Height          =   285
         Left            =   12840
         MaxLength       =   12
         TabIndex        =   12
         Text            =   "0"
         Top             =   1080
         Width           =   2000
      End
      Begin VB.TextBox txtValCargo 
         Height          =   285
         Left            =   6480
         MaxLength       =   12
         TabIndex        =   11
         Text            =   "0"
         Top             =   1080
         Width           =   1920
      End
      Begin VB.TextBox txtDescuento 
         Height          =   285
         Left            =   10680
         MaxLength       =   12
         TabIndex        =   10
         Text            =   "Text1"
         Top             =   1080
         Width           =   2000
      End
      Begin VB.TextBox txtCargo 
         Height          =   285
         Left            =   4560
         MaxLength       =   12
         TabIndex        =   9
         Text            =   "Text1"
         Top             =   1080
         Width           =   1800
      End
      Begin VB.ComboBox cbxArea 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1080
         Width           =   2055
      End
      Begin VB.CommandButton cmdModificarDetO 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   15960
         TabIndex        =   7
         Tag             =   "DOF_MODIFICAR"
         Top             =   3300
         Width           =   1215
      End
      Begin VB.TextBox txtCodPaquete 
         Height          =   285
         Left            =   2400
         MaxLength       =   12
         TabIndex        =   6
         Text            =   "Text1"
         Top             =   2280
         Width           =   2055
      End
      Begin VB.ComboBox cbxExclusion 
         Height          =   315
         ItemData        =   "frmOfertas.frx":0090
         Left            =   4560
         List            =   "frmOfertas.frx":0092
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   2280
         Width           =   2175
      End
      Begin VB.ComboBox cbxTipoPaquete 
         Height          =   315
         ItemData        =   "frmOfertas.frx":0094
         Left            =   240
         List            =   "frmOfertas.frx":00A1
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   2280
         Width           =   2055
      End
      Begin VB.CommandButton cmdEliminarDetO 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   15960
         TabIndex        =   3
         Tag             =   "DOF_ELIMINAR"
         Top             =   3780
         Width           =   1215
      End
      Begin VB.CommandButton cmdAgregarDetO 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   15960
         TabIndex        =   2
         Tag             =   "DOF_AGREGAR"
         Top             =   2820
         Width           =   1215
      End
      Begin VB.TextBox txtPaqAdic 
         Height          =   285
         Left            =   12840
         MaxLength       =   12
         TabIndex        =   1
         Top             =   1680
         Width           =   1575
      End
      Begin MSComCtl2.DTPicker dtFechaFin 
         Height          =   300
         Left            =   -66120
         TabIndex        =   52
         Top             =   1200
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   49479681
         CurrentDate     =   38915
      End
      Begin MSComCtl2.DTPicker dtFechaInicio 
         Height          =   300
         Left            =   -68040
         TabIndex        =   53
         Top             =   1200
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         _Version        =   393216
         Format          =   49479681
         CurrentDate     =   38915
      End
      Begin MSComctlLib.ListView li_localidades 
         Height          =   6855
         Left            =   -74760
         TabIndex        =   65
         Top             =   2100
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   12091
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Localidad"
            Object.Width           =   4763
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Fecha Inicio"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha Fin"
            Object.Width           =   2646
         EndProperty
      End
      Begin MSComCtl2.DTPicker dtpFechaFLoc 
         Height          =   375
         Left            =   -72840
         TabIndex        =   66
         Top             =   1080
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   49479681
         CurrentDate     =   38915
      End
      Begin MSComCtl2.DTPicker dtpFechaILoc 
         Height          =   375
         Left            =   -74760
         TabIndex        =   67
         Top             =   1080
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   49479681
         CurrentDate     =   38915
      End
      Begin MSFlexGridLib.MSFlexGrid gr_agrupador 
         Height          =   7455
         Left            =   -74760
         TabIndex        =   86
         Top             =   1500
         Width           =   8175
         _ExtentX        =   14420
         _ExtentY        =   13150
         _Version        =   393216
         FocusRect       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         FormatString    =   "Codigo | Agrupador"
      End
      Begin MSComctlLib.ListView li_localidades_seleccionadas 
         Height          =   6855
         Left            =   -66360
         TabIndex        =   89
         Top             =   2100
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   12091
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Localidad"
            Object.Width           =   4763
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Fecha Inicio"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha Fin"
            Object.Width           =   2646
         EndProperty
      End
      Begin MSComctlLib.ListView li_canales 
         Height          =   6735
         Left            =   -74880
         TabIndex        =   99
         Top             =   1200
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   11880
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Canal de Venta"
            Object.Width           =   4763
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1764
         EndProperty
      End
      Begin MSComctlLib.ListView li_canales_seleccionados 
         Height          =   6735
         Left            =   -66600
         TabIndex        =   100
         Top             =   1200
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   11880
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Canal de Venta"
            Object.Width           =   4763
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Codigo"
            Object.Width           =   1764
         EndProperty
      End
      Begin VB.Label lblObligatorio 
         Caption         =   "Alta Obligatoria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   8520
         TabIndex        =   104
         Top             =   2040
         Width           =   1455
      End
      Begin VB.Label Label30 
         Caption         =   "Descripci�n"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   -74760
         TabIndex        =   88
         Top             =   1140
         Width           =   1095
      End
      Begin VB.Label Label31 
         Caption         =   "Codigo:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   -74760
         TabIndex        =   87
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label Label10 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Agrupador:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -64200
         TabIndex        =   80
         Top             =   2160
         Width           =   825
      End
      Begin VB.Label Label6 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   -74760
         TabIndex        =   77
         Top             =   1500
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   -74760
         TabIndex        =   71
         Top             =   2160
         Width           =   1815
      End
      Begin VB.Label lblFechaI 
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -74760
         TabIndex        =   69
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label lblFechaF 
         Caption         =   "Fecha Fin"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -72840
         TabIndex        =   68
         Top             =   840
         Width           =   975
      End
      Begin VB.Label lblFechaIni 
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -68040
         TabIndex        =   55
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label lblFechaFin 
         Caption         =   "Fecha Fin"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -66120
         TabIndex        =   54
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label lblDescOferta 
         Caption         =   "Descripci�n de la Oferta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -74760
         TabIndex        =   51
         Top             =   960
         Width           =   3135
      End
      Begin VB.Label lblObsOferta 
         Caption         =   "Observaci�n de la Oferta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -74760
         TabIndex        =   50
         Top             =   1560
         Width           =   3135
      End
      Begin VB.Label lblValOferta 
         Caption         =   "Valor Oferta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -64200
         TabIndex        =   49
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label lvlValPromocion 
         Caption         =   "Valor Promoci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -62280
         TabIndex        =   48
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "C�digo descuento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -66120
         TabIndex        =   47
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo cargo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -68040
         TabIndex        =   46
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label Label9 
         Caption         =   "Unidad Monetaria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -60360
         TabIndex        =   45
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "Cod. desc. Pack Comp"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   8520
         TabIndex        =   37
         ToolTipText     =   "C�digo de descuento aplicable al titular del Pack Compartido"
         Top             =   840
         Width           =   2040
      End
      Begin VB.Label Label7 
         Caption         =   "Vlor desc Pack Comp"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   4560
         TabIndex        =   36
         Top             =   1440
         Width           =   1935
      End
      Begin VB.Label Label5 
         Caption         =   "Paq Adicional"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   12840
         TabIndex        =   35
         Top             =   1440
         Width           =   1335
      End
      Begin VB.Label lblValTotal 
         Caption         =   "Valor total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2400
         TabIndex        =   34
         Top             =   1440
         Width           =   1335
      End
      Begin VB.Label lblPorcDesc 
         Caption         =   "Porcentaje descuento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   1440
         Width           =   1935
      End
      Begin VB.Label lblClasPaquete 
         Caption         =   "Clase paquete"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2400
         TabIndex        =   32
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lblMesesProm 
         Caption         =   "Meses promoci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   10680
         TabIndex        =   31
         Top             =   1440
         Width           =   1575
      End
      Begin VB.Label lblValDescProm 
         Caption         =   "Valor desc. promoci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   8520
         TabIndex        =   30
         Top             =   1440
         Width           =   2055
      End
      Begin VB.Label lblDescProm 
         Caption         =   "Cod. Desc. promoci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   6480
         TabIndex        =   29
         Top             =   1440
         Width           =   1935
      End
      Begin VB.Label lblValDesc 
         Caption         =   "Vlor descto NEGATIVO"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   12840
         TabIndex        =   28
         Top             =   840
         Width           =   2175
      End
      Begin VB.Label lblValCargo 
         Caption         =   "Valor cargo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   6480
         TabIndex        =   27
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo descuento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   10680
         TabIndex        =   26
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label lblCargo 
         Caption         =   "C�digo cargo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   4560
         TabIndex        =   25
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label lblArea 
         Caption         =   "Area"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lblCodPaquete 
         Caption         =   "C�digo paquete"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2400
         TabIndex        =   23
         Top             =   2280
         Width           =   1455
      End
      Begin VB.Label lblExclusion 
         Caption         =   "Exclusi�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   4560
         TabIndex        =   22
         Top             =   2040
         Width           =   1455
      End
      Begin VB.Label lblTipoPaquete 
         Caption         =   "Tipo paquete"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   2040
         Width           =   1455
      End
   End
End
Attribute VB_Name = "frmOfertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim mbAgregar As Boolean
Dim Cod_Paquete As String

Function fnValidaCodigo(txCodi As TextBox) As Integer
    
    Dim i As Integer, iOk As Integer, sCodi As String

    sCodi = IIf(Trim$(txCodi.Text) = "", "0", Trim$(txCodi.Text))
    
    If sCodi <> "0" Then
        iOk = -1
        For i = 0 To UBound(arrRentas)
            If Trim$(sCodi) = arrCodiConcepto(i) Then
                iOk = i
                Exit For
            End If
        Next i
        
        If iOk < 0 Then
            MsgBox "C�digo [" & sCodi & "] NO v�lido", vbCritical, "ERROR"
            txCodi.Text = "0"
        Else
            If Trim$(txtUnidMonetar.Text) <> Trim(arrUnidMonetar(i)) Then
                MsgBox "Unidad Monetaria de Codigo [" & sCodi & "] NO v�lido"
                txCodi.Text = "0"
                iOk = -1
            End If
        End If

    End If

    fnValidaCodigo = iOk
    
End Function

Private Function fnValidaDetalleOferta() As Boolean
    Dim i As Long
    Dim lValCargo As Double, lValDesc As Double
    Dim lValDescProm As Double, lValTotalO As Double
    Dim sExcluRev As String, sExcluIng As String
    
    Dim lCodPaquete As Long
    Dim lpaqadic As Long

    fnValidaDetalleOferta = True
    If cbxClasPaquete.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar UNA clase de paquete"
        fnValidaDetalleOferta = False
        Exit Function
    End If

    If cbxClasPaquete.Text <> "M  -  Paquete Opcional" Then
        If txtCargo.Text = "" Then
            MsgBox "Debe ingresar un c�digo cargo"
            fnValidaDetalleOferta = False
            Exit Function
        End If
    End If

    If txtDescuento.Text <> "" And txtDescuento.Text <> "0" Then
        If txtValDesc.Text = "" Or txtValDesc.Text = "0" Then
            MsgBox "Debe ingresar un valor descuento"
            fnValidaDetalleOferta = False
            Exit Function
        End If
    End If
    
    If txtDescPC.Text <> "" And txtDescPC.Text <> "0" Then
        If txtValDescPC.Text = "" Or txtValDescPC.Text = "0" Then
            MsgBox "Debe ingresar un valor descuento Pack Compartido"
            fnValidaDetalleOferta = False
            Exit Function
        End If
    End If
    

    If cbxArea.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar un �rea"
        fnValidaDetalleOferta = False
        Exit Function
    End If

    If (txtValCargo.Text = "" Or txtValCargo.Text = "0") And cbxClasPaquete.Text <> "M  -  Paquete Opcional" Then
        MsgBox "Debe ingresar un valor cargo"
        fnValidaDetalleOferta = False
        Exit Function
    End If

    If txtCodPaquete.Text = "" Then
        MsgBox "Debe ingresar el c�digo paquete correspondiente"
        fnValidaDetalleOferta = False
        Exit Function
    End If
    
    If (txtValTotal.Text = "" Or txtValTotal.Text = "0") And cbxClasPaquete.Text <> "M  -  Paquete Opcional" And cbxArea.Text <> "SVA  -  Servicios de Valor Agregado" Then
        MsgBox "Debe ingresar un valor total"
        fnValidaDetalleOferta = False
        Exit Function
    End If
    
    If txtValPromocion.Text <> "" And txtValPromocion.Text <> "0" Then
        If txtDescProm.Text = "" Then
            MsgBox "Debe ingresar un Cod. Desc. promoci�n"
            fnValidaDetalleOferta = False
            Exit Function
        End If
        
        If txtValDescProm.Text = "" Then
            MsgBox "Debe ingresar un valor Desc. promoci�n"
            fnValidaDetalleOferta = False
            Exit Function
        End If
        
        If txtMesesProm.Text = "" Then
            MsgBox "Debe ingresar los meses de la promoci�n"
            fnValidaDetalleOferta = False
            Exit Function
        End If
    End If
    
    If txtPorcDesc.Text = "" Then
        MsgBox "Debe ingresar un porcentaje de descuento"
        fnValidaDetalleOferta = False
        Exit Function
    End If
    
    If cbxTipoPaquete.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar un tipo de paquete"
        fnValidaDetalleOferta = False
        Exit Function
    End If

    If txtValPromocion <> "0" Then
        If txtDescProm.Text = "" Then
            MsgBox "Debe ingresar el c�digo descuento de la promoci�n"
            fnValidaDetalleOferta = False
            Exit Function
        End If
        
        If txtValDescProm.Text = "" Then
            MsgBox "Debe ingresar el valor descuento de la promoci�n"
            fnValidaDetalleOferta = False
            Exit Function
        End If
        
        If txtMesesProm.Text = "" Then
            MsgBox "Debe ingresar los meses de duraci�n de la promoci�n"
            fnValidaDetalleOferta = False
            Exit Function
        End If
    Else
        txtDescProm.Text = ""
        txtValDescProm.Text = "0"
        txtMesesProm.Text = "0"
    End If
    lCodPaquete = txtCodPaquete.Text
    
    'hacer la validacion
    If txtPaqAdic.Text <> "" And txtPaqAdic.Text <> "0" Then
        lpaqadic = txtPaqAdic.Text
    Else
        lpaqadic = 0
    End If
        
    lValCargo = txtValCargo.Text
    lValDesc = txtValDesc.Text
    lValDescProm = txtValDescProm.Text 'ticket 69581
    
    If txtValDesc.Text = "" Then
        lValDesc = 0
    End If
    
    If txtValDescProm.Text = "" Then
        lValDescProm = 0
    End If
    
    If cbxExclusion.Text <> "NADA" Then
        ' �rea de Negocios puede ocupar menos de 4 caracteres.
        ' SSOT#23617. JPABLOS. 29-may-2008.
        'sExcluIng = Mid(arrAreas(cbxExclusion.ListIndex - 1), 10, 100)
        sExcluIng = Mid(arrAreas(cbxExclusion.ListIndex - 1), InStr(1, arrAreas(cbxExclusion.ListIndex - 1), "-") + 3, 100)
    End If

    If txtValDescProm.Text = "" Or txtValDescProm.Text = "0" Then
        lValTotalO = lValCargo + lValDesc
    Else
        lValTotalO = lValCargo + lValDesc + lValDescProm
    End If
    
    

    sExcluRev = "|"
  
    
    For i = 1 To li_detalle_ofertas.ListItems.Count - 1
        
        If (sExcluIng <> li_detalle_ofertas.ListItems(i).SubItems(12) Or sExcluIng = "") And _
        ((li_detalle_ofertas.ListItems(i).SubItems(12) = "") Or (InStr(sExcluRev, li_detalle_ofertas.ListItems(i).SubItems(12)) < 1)) _
        Then
            If mbAgregar Then
                lValTotalO = lValTotalO + li_detalle_ofertas.ListItems(i).SubItems(5)
            Else
                If i <> li_detalle_ofertas.SelectedItem.Index Then
                    lValTotalO = lValTotalO + li_detalle_ofertas.ListItems(i).SubItems(5)
                End If
            End If
            sExcluRev = sExcluRev & li_detalle_ofertas.ListItems(i).SubItems(12) & "|"
        End If
    Next i
'RAALTAMI TI400050: Se modifica el tipo de conversi�n por ofertas en UF.
    If lValTotalO > CSng(txtValOferta.Text) Then
        MsgBox "La suma de los detalles de la oferta no debe sobrepasar el valor oferta Verifique"
        fnValidaDetalleOferta = False
        Exit Function
    End If

End Function



Private Sub chkPaquetesNoVigentes_Click()
    Call suTraeDatosOferta(Val(txtCodiOferta.Text), 1)
End Sub

Private Sub chkPaquetesVigentes_Click()
    Call suTraeDatosOferta(Val(txtCodiOferta.Text), 1)
End Sub

Private Sub cmd_act_fecha_Click()
    Dim sFechaF As String
    Dim i As Long, iCant As Long
    
    iCant = 0
    For i = 1 To li_localidades_seleccionadas.ListItems.Count
        If li_localidades_seleccionadas.ListItems(i).Checked Then
            If IsNull(dtpFechaFLoc.Value) Then
                sFechaF = ""
            Else
                If dtpFechaFLoc.Value < dtpFechaILoc.Value Then
                    dtpFechaFLoc.Value = dtpFechaILoc.Value
                    Exit Sub
                End If
                sFechaF = Format(dtpFechaFLoc.Value, "dd-mm-yyyy")
            End If
            Call UpdOferLoca(CLng(txtCodiOferta.Text), li_localidades_seleccionadas.ListItems(i).SubItems(1), sFechaF)
            iCant = iCant + 1
        End If
    Next

    If iCant > 0 Then
        Call suTraerLocalidades(CLng(txtCodiOferta.Text))
    End If
    
End Sub

Private Sub cmd_desel_localidad_todos_Click()
    Dim i As Long, sFecha As String, iCant As Long
     
    For i = 1 To li_localidades_seleccionadas.ListItems.Count
        Call DelOferLoca(CLng(txtCodiOferta.Text), li_localidades_seleccionadas.ListItems(i).SubItems(1))
    Next

    Call suTraerLocalidades(Val(txtCodiOferta.Text))
End Sub

Private Sub cmd_exportar_Click()
    Dim vsSeparador As String
    
    Dim iFila As Long ' counter
    Dim iColumna As Long ' counter
    
    Dim iCol As Long ' counter
    Dim iColDet As Long ' counter
    
    Dim xl As Object
    Dim wbk As Object

    Dim ws1 As Object
    Dim ws2 As Object
    Dim ws3 As Object
    Dim ws4 As Object
    
    Dim i As Long
    Dim j As Long
    Dim df As Long, dc As Long
    Dim k As Long, l As Long
    
    Dim ldf As Long, ldc As Long
    Dim lk As Long, ll As Long, lk1 As Long
    Dim lk2 As Long
    
    Dim Total As Long

    vsSeparador = fnSeparadorListas
    
    Set xl = CreateObject("Excel.Application")

    xl.Visible = False
    Set wbk = xl.Workbooks.Add
    
    Set ws1 = wbk.Worksheets(1)
    ws1.Name = "Ofertas"
    
    Set ws2 = wbk.Worksheets.Add
    ws2.Name = "Detalle de Ofertas"

    Set ws3 = wbk.Worksheets.Add
    ws3.Name = "Localidades"
    
    Set ws4 = wbk.Worksheets.Add
    ws4.Name = "Canales de Venta"


    iFila = 1
    k = 0
            
    For j = 1 To li_ofertas.ColumnHeaders.Count
        If li_ofertas.ColumnHeaders.Item(j).Width > 0 Then
            k = k + 1
            ws1.Cells(1, k).Font.Bold = True
            ws1.Cells(1, k).Font.Color = vbBlack
            ws1.Cells(1, k).Interior.Color = &HDFBF91
            ws1.Cells(1, k).Value = li_ofertas.ColumnHeaders(j).Text
        End If
    Next j
            
    k = 1

    For j = 0 To li_detalle_ofertas.ColumnHeaders.Count
        If j = 0 Then
            ws2.Cells(1, k).Font.Bold = True
            ws2.Cells(1, k).Font.Color = vbBlack
            ws2.Cells(1, k).Interior.Color = &HDFBF91
            ws2.Cells(1, k).Value = "Oferta"
        Else
            If li_detalle_ofertas.ColumnHeaders.Item(j).Width > 0 Then
                k = k + 1
                ws2.Cells(1, k).Font.Bold = True
                ws2.Cells(1, k).Font.Color = vbBlack
                ws2.Cells(1, k).Interior.Color = &HDFBF91
                ws2.Cells(1, k).Value = li_detalle_ofertas.ColumnHeaders(j).Text
            End If
        End If
    Next j
    

    For j = 0 To li_localidades.ColumnHeaders.Count
        ws3.Cells(1, j + 1).Font.Bold = True
        ws3.Cells(1, j + 1).Font.Color = vbBlack
        ws3.Cells(1, j + 1).Interior.Color = &HDFBF91
        If j = 0 Then
            ws3.Cells(1, j + 1).Value = "Oferta"
        Else
            ws3.Cells(1, j + 1).Value = li_localidades.ColumnHeaders(j).Text
        End If
    Next j
    
    For j = 0 To li_canales.ColumnHeaders.Count
        ws4.Cells(1, j + 1).Font.Bold = True
        ws4.Cells(1, j + 1).Font.Color = vbBlack
        ws4.Cells(1, j + 1).Interior.Color = &HDFBF91
        If j = 0 Then
            ws4.Cells(1, j + 1).Value = "Oferta"
        Else
            ws4.Cells(1, j + 1).Value = li_canales.ColumnHeaders(j).Text
        End If
    Next j
    
    Screen.MousePointer = vbHourglass
    k = 1
    lk = 1
    lk2 = 1
    iColumna = 0
    For i = 1 To li_ofertas.ListItems.Count
        If li_ofertas.ListItems(i).Checked Then
            iFila = iFila + 1
            iColumna = 0
            For iCol = 1 To li_ofertas.ColumnHeaders.Count
                If li_ofertas.ColumnHeaders.Item(iCol).Width > 1 Then
                    iColumna = iColumna + 1
                    ws1.Cells(iFila, iColumna).Font.Color = vbBlack
                    ws1.Cells(iFila, iColumna).Interior.Color = vbWhite
                    ws1.Cells(iFila, iColumna).Borders.LineStyle = 1
                    If iCol = 1 Then
                        ws1.Cells(iFila, iColumna).Value = " " & li_ofertas.ListItems(i)
                        Call li_ofertas_ItemClick(li_ofertas.ListItems(i))
                        For df = 1 To li_detalle_ofertas.ListItems.Count - 1
                            If li_detalle_ofertas.ListItems(df).SubItems(13) = "Paquete Opcional" Then
                                k = k + 1
                                iColDet = 1
                                For dc = 0 To li_detalle_ofertas.ColumnHeaders.Count
                                    If dc = 0 Then
                                        ws2.Cells(k, 1).Font.Color = vbBlack
                                        ws2.Cells(k, 1).Interior.Color = vbWhite
                                        ws2.Cells(k, 1).Borders.LineStyle = 1
                                        ws2.Cells(k, 1).Value = " " & li_ofertas.ListItems(i)
                                    Else
                                         If li_detalle_ofertas.ColumnHeaders.Item(dc).Width > 1 Then
                                            iColDet = iColDet + 1
                                            ws2.Cells(k, iColDet).Font.Color = vbBlack
                                            ws2.Cells(k, iColDet).Interior.Color = vbWhite
                                            ws2.Cells(k, iColDet).Borders.LineStyle = 1
                                            If dc = 1 Then
                                                If li_detalle_ofertas.ColumnHeaders.Item(dc).Tag <> "" Then
                                                    ws2.Cells(k, iColDet).Value = " " & li_detalle_ofertas.ListItems(df).SubItems(li_detalle_ofertas.ColumnHeaders.Item(dc).Tag)
                                                Else
                                                    ws2.Cells(k, iColDet).Value = " " & li_detalle_ofertas.ListItems(df)
                                                End If
                                            Else
                                                If li_detalle_ofertas.ColumnHeaders.Item(dc).Tag <> "" Then
                                                    ws2.Cells(k, iColDet).Value = " " & li_detalle_ofertas.ListItems(df).SubItems(li_detalle_ofertas.ColumnHeaders.Item(dc).Tag)
                                                Else
                                                    ws2.Cells(k, iColDet).Value = " " & li_detalle_ofertas.ListItems(df).SubItems(dc - 1)
                                                End If
                                            End If
                                        End If
                                    End If
                                Next
                            End If
                        Next
                        
                        For ldf = 1 To li_localidades_seleccionadas.ListItems.Count
                            lk = lk + 1
                            For ldc = 0 To li_localidades_seleccionadas.ColumnHeaders.Count
                                If ldc = 0 Then
                                    ws3.Cells(lk, ldc + 1).Font.Color = vbBlack
                                    ws3.Cells(lk, ldc + 1).Interior.Color = vbWhite
                                    ws3.Cells(lk, ldc + 1).Borders.LineStyle = 1
                                    ws3.Cells(lk, ldc + 1).Value = " " & li_ofertas.ListItems(i)
                                Else
                                    If li_localidades_seleccionadas.ColumnHeaders.Item(ldc).Width > 1 Then
                                        ws3.Cells(lk, ldc + 1).Font.Color = vbBlack
                                        ws3.Cells(lk, ldc + 1).Interior.Color = vbWhite
                                        ws3.Cells(lk, ldc + 1).Borders.LineStyle = 1
                                        If ldc = 1 Then
                                            ws3.Cells(lk, ldc + 1).Value = " " & li_localidades_seleccionadas.ListItems(ldf)
                                        Else
                                            ws3.Cells(lk, ldc + 1).Value = " " & li_localidades_seleccionadas.ListItems(ldf).SubItems(ldc - 1)
                                        End If
                                    End If
                                End If
                            Next
                        Next
                        
                        For ldf = 1 To li_canales_seleccionados.ListItems.Count
                            lk2 = lk2 + 1
                            For ldc = 0 To li_canales_seleccionados.ColumnHeaders.Count
                                If ldc = 0 Then
                                    ws4.Cells(lk2, ldc + 1).Font.Color = vbBlack
                                    ws4.Cells(lk2, ldc + 1).Interior.Color = vbWhite
                                    ws4.Cells(lk2, ldc + 1).Borders.LineStyle = 1
                                    ws4.Cells(lk2, ldc + 1).Value = " " & li_ofertas.ListItems(i)
                                Else
                                    If li_canales_seleccionados.ColumnHeaders.Item(ldc).Width > 1 Then
                                        ws4.Cells(lk2, ldc + 1).Font.Color = vbBlack
                                        ws4.Cells(lk2, ldc + 1).Interior.Color = vbWhite
                                        ws4.Cells(lk2, ldc + 1).Borders.LineStyle = 1
                                        If ldc = 1 Then
                                            ws4.Cells(lk2, ldc + 1).Value = " " & li_canales_seleccionados.ListItems(ldf)
                                        Else
                                            ws4.Cells(lk2, ldc + 1).Value = " " & li_canales_seleccionados.ListItems(ldf).SubItems(ldc - 1)
                                        End If
                                    End If
                                End If
                            Next
                        Next
                    Else
                        If li_ofertas.ColumnHeaders.Item(iCol).Tag <> "" And IsNumeric(li_ofertas.ColumnHeaders.Item(iCol).Tag) Then
                            ws1.Cells(iFila, iColumna).Value = " " & li_ofertas.ListItems(i).SubItems(li_ofertas.ColumnHeaders.Item(iCol).Tag)
                        Else
                            ws1.Cells(iFila, iColumna).Value = " " & li_ofertas.ListItems(i).SubItems(iCol - 1)
                        End If
                    End If
                End If
            Next
        End If
    Next
    
    For j = 1 To li_ofertas.ColumnHeaders.Count
        xl.Columns(j).EntireColumn.AutoFit
    Next j
    
    xl.Visible = True
    Screen.MousePointer = vbDefault
    Call li_ofertas_ItemClick(li_ofertas.SelectedItem)

End Sub

Private Sub cmd_importar_Click()

On Error GoTo ErrHandler
    Dim xlApp As Object
    Dim xlWB As Object
    Dim xlWS1 As Object
    Dim xlWS2 As Object
    Dim xlWS3 As Object
    Dim xlWS4 As Object
    Dim Range As Object
    Dim i As Long, j As Long
    Dim sRespuesta As String
    Dim CodiOferta As Long
    
    Dim Ret As Integer
    GPBase.BeginTrans
    
    If gfnPedirNombreArchivo(CommonDialog1) Then
        Set xlApp = CreateObject("Excel.Application")
        xlApp.Visible = False
        Set xlWB = xlApp.Workbooks.Open(CommonDialog1.FileName)
        Set xlWS1 = xlWB.Worksheets("Ofertas")
        Set xlWS2 = xlWB.Worksheets("Detalle de Ofertas")
        Set xlWS3 = xlWB.Worksheets("Localidades")
        Set xlWS4 = xlWB.Worksheets("Canales de Venta")
        
        Set Range = xlWS1.UsedRange
        
        
        
        For i = 2 To Range.Rows.Count
             If Range.Cells(i, 1) = "" Then
                 Exit For
             End If

             Ret = UpdOferta(Val(Range.Cells(i, 1)), _
                             Range.Cells(i, 4), _
                             Range.Cells(i, 2), _
                             Range.Cells(i, 3), _
                             Format(Trim(Range.Cells(i, 9)), "dd-mm-yyyy"), _
                             IIf(IsNull(Range.Cells(i, 10)), "NADA", Format(Trim(Range.Cells(i, 10)), "dd-mm-yyyy")), _
                             Range.Cells(i, 5), _
                             Range.Cells(i, 6), _
                             Range.Cells(i, 7), _
                             Trim(Range.Cells(i, 11)), _
                             Range.Cells(i, 13), _
                             sRespuesta)
                             
            If sRespuesta <> "OK" Then
                MsgBox "En registro " + Str(i) + ":" + sRespuesta + " (Oferta:" + Trim(Range.Cells(i, 1)) + ")", vbCritical, "Error en Ofertas"
                xlApp.Workbooks.Close
                GPBase.RollbackTrans
                Exit Sub
            End If
                     
        Next
        
        Set Range = xlWS2.UsedRange
        CodiOferta = 0
        For i = 2 To Range.Rows.Count
        
             If Range.Cells(i, 1) = "" Then
                 Exit For
             End If
             
             If Val(Range.Cells(i, 1)) <> CodiOferta Then
                 Ret = DelOferDet(Val(Range.Cells(i, 1)), "", "", "M")
                 If Not Ret Then
                    xlApp.Workbooks.Close
                    GPBase.RollbackTrans
                    Exit Sub
                 End If
                 CodiOferta = Val(Range.Cells(i, 1))
             End If

             Ret = InsOferDet(Val(Range.Cells(i, 1)), _
                              Range.Cells(i, 2), _
                              Range.Cells(i, 3), _
                              Range.Cells(i, 5), _
                              Range.Cells(i, 6), _
                              Val(Range.Cells(i, 7)), _
                              Val(Range.Cells(i, 8)), _
                              Val(Range.Cells(i, 9)), _
                              Val(Range.Cells(i, 10)), _
                              Range.Cells(i, 11), _
                              Val(Range.Cells(i, 12)), _
                              Val(Range.Cells(i, 13)), _
                              Range.Cells(i, 14), _
                              Range.Cells(i, 15), _
                              Val(Range.Cells(i, 4)), _
                              Val(Range.Cells(i, 16)), _
                              Range.Cells(i, 17), _
                              Val(Range.Cells(i, 18)), _
                              Range.Cells(i, 19), _
                              Range.Cells(i, 20), _
                              sRespuesta)
                              
            If sRespuesta <> "OK" Then
                MsgBox "En registro " + Str(i) + ":" + sRespuesta + " (Oferta:" + Trim(Range.Cells(i, 1)) + ")", vbCritical, "Error en Detalle de Oferta"
                xlApp.Workbooks.Close
                GPBase.RollbackTrans
                Exit Sub
            End If
        Next
        
        Set Range = xlWS3.UsedRange
        
        CodiOferta = 0
        For i = 2 To Range.Rows.Count
             
            If Range.Cells(i, 1) = "" Then
                Exit For
            End If
            
            If Val(Range.Cells(i, 1)) <> CodiOferta Then
                Ret = DelOferLoca(CLng(Range.Cells(i, 1)), "")
                CodiOferta = Val(Range.Cells(i, 1))
            End If
            
            Ret = InsOferLoca(Range.Cells(i, 1), _
                              Trim(Range.Cells(i, 3)), _
                              Format(Trim(Range.Cells(i, 4)), "dd-mm-yyyy"), _
                              Format(Trim(Range.Cells(i, 5)), "dd-mm-yyyy"), _
                              sRespuesta)
            
            If sRespuesta <> "OK" Then
               MsgBox "En registro " + Str(i) + ":" + sRespuesta + " (Oferta:" + Trim(Range.Cells(i, 1)) + ")", vbCritical, "Error en Detalle de Oferta"
               xlApp.Workbooks.Close
               GPBase.RollbackTrans
               Exit Sub
            End If
        Next
        
        Set Range = xlWS4.UsedRange
        CodiOferta = 0
        For i = 2 To Range.Rows.Count
             
             If Range.Cells(i, 1) = "" Then
                 Exit For
             End If
             
             If Val(Range.Cells(i, 1)) <> CodiOferta Then
                 CodiOferta = Val(Range.Cells(i, 1))
                 Ret = EliminaCanalOferta(CodiOferta, "")
             End If
        
             Ret = IngresaCanalOferta(CodiOferta, _
                                      Trim(Range.Cells(i, 3)), _
                                      sRespuesta)
         
             If sRespuesta <> "OK" Then
                MsgBox "En registro " + Str(i) + ":" + sRespuesta + " (Paquete:" + Trim(Range.Cells(i, 1)) + ")", vbCritical, "Error en Canles de Venta"
                xlApp.Workbooks.Close
                GPBase.RollbackTrans
                Exit Sub
             End If
        Next
        
    Else
       GPBase.RollbackTrans
       Exit Sub
    End If
    
    xlApp.Workbooks.Close
    GPBase.CommitTrans

    MsgBox "Proceso de Import Exitoso", vbInformation, "Informacion"
    
    Call suLlenaOfertas

    Exit Sub
    
ErrHandler:
    If Not xlApp Is Nothing Then
        xlApp.Workbooks.Close
    End If
    GPBase.RollbackTrans
    MsgBox "Revise el formato de la planilla Excel!", vbCritical, "Error!"
End Sub

Private Sub cmd_listado_Click()
    Dim vsSeparador As String
    
    Dim iFila As Long ' counter
    Dim iColumna As Long ' counter
    Dim xl As Object
    Dim wbk As Object
    
    Dim ws1 As Object
    
    Dim i As Long
    Dim j As Long
    Dim df As Long, dc As Long
    Dim k As Long, l As Long
    
    Dim ldf As Long, ldc As Long
    Dim lk As Long, ll As Long, lk1 As Long
    
    Dim Total As Long

    vsSeparador = fnSeparadorListas
    
    Set xl = CreateObject("Excel.Application")

    xl.Visible = False
    Set wbk = xl.Workbooks.Add
    
    Set ws1 = wbk.Worksheets(1)
    ws1.Name = "Ofertas"

    iFila = 1
            
    For j = 1 To li_ofertas.ColumnHeaders.Count
        ws1.Cells(iFila, j).Font.Bold = True
        ws1.Cells(iFila, j).Font.Color = vbBlack
        ws1.Cells(iFila, j).Interior.Color = &HDFBF91
        ws1.Cells(iFila, j).Value = li_ofertas.ColumnHeaders(j).Text
    Next j
    
    Screen.MousePointer = vbHourglass
    k = 1
    lk = 1
    For i = 1 To li_ofertas.ListItems.Count
        iFila = iFila + 1
        For iColumna = 1 To li_ofertas.ColumnHeaders.Count
            If li_ofertas.ColumnHeaders.Item(iColumna).Width > 1 Then
                ws1.Cells(iFila, iColumna).Font.Color = vbBlack
                ws1.Cells(iFila, iColumna).Interior.Color = vbWhite
                ws1.Cells(iFila, iColumna).Borders.LineStyle = 1
                If iColumna = 1 Then
                    ws1.Cells(iFila, iColumna).Value = " " & li_ofertas.ListItems(i)
                Else
                    ws1.Cells(iFila, iColumna).Value = " " & li_ofertas.ListItems(i).SubItems(iColumna - 1)
                End If
            End If
        Next
    Next
    
    For j = 1 To li_ofertas.ColumnHeaders.Count
        xl.Columns(j).EntireColumn.AutoFit
    Next j
    
    xl.Visible = True
    Screen.MousePointer = vbDefault

End Sub

'
Private Sub MostrarCanalesVenta(codi_oferta As Integer)
    Dim Total As Integer, i As Integer, Ret As Integer
    Dim lid As ListItem
    Dim lis As ListItem
    Dim cont As Long
    
    Dim codi_canal_vta As Integer, desc_canal_vta As String, seleccionado As Integer
    
    On Error Resume Next
    

    li_canales.Visible = False
    li_canales.ListItems.Clear

    li_canales_seleccionados.Visible = False
    li_canales_seleccionados.ListItems.Clear
    
    Screen.MousePointer = vbHourglass
    cont = 0

    Total = Srv_CanalOferta_Seleccionados_Open(codi_oferta)
    For i = 1 To Total
        Total = Srv_CanalOferta_Seleccionados_Read(codi_canal_vta, desc_canal_vta, seleccionado)
        If seleccionado = 1 Then
            Set lid = li_canales_seleccionados.ListItems.Add(, , Trim$(desc_canal_vta))
            lid.SubItems(1) = Trim$(codi_canal_vta)
        Else
            Set lid = li_canales.ListItems.Add(, , Trim$(desc_canal_vta))
            lid.SubItems(1) = Trim$(codi_canal_vta)
        End If
    Next i
    Ret = Srv_CanalOferta_Seleccionados_Close()
    
    Screen.MousePointer = vbDefault
    
    li_canales.Visible = True
    li_canales_seleccionados.Visible = True
    cmd_desel_canal.Enabled = False
    cmd_sel_canal.Enabled = False
    
End Sub

Private Sub cmd_sel_canal_Click()
    Dim i As Long, iCant As Long, sRespuesta As String
    
    For i = 1 To li_canales.ListItems.Count
        If li_canales.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
     
    If MsgBox("�Esta seguro de agregar los " & Trim(Str(iCant)) & " canales seleccionados?", vbYesNo) = vbYes Then
        For i = 1 To li_canales.ListItems.Count
            If li_canales.ListItems(i).Checked Then
                If Not IngresaCanalOferta(Val(txtCodiOferta.Text), li_canales.ListItems(i).SubItems(1), sRespuesta) Then
                    MsgBox sRespuesta, vbCritical, "Error"
                End If
            End If
        Next
        Call MostrarCanalesVenta(Val(txtCodiOferta.Text))
        Call ChequeaBotonesCanales
    End If
    
End Sub

Private Sub cmd_desel_canal_Click()
    Dim i As Long, sFecha As String, iCant As Long
    
    For i = 1 To li_canales_seleccionados.ListItems.Count
        If li_canales_seleccionados.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next

    If MsgBox("�Esta seguro de eliminar los " & Trim(Str(iCant)) & " canales seleccionados?", vbYesNo) = vbYes Then
        For i = 1 To li_canales_seleccionados.ListItems.Count
            If li_canales_seleccionados.ListItems(i).Checked Then
                Call EliminaCanalOferta(Val(txtCodiOferta.Text), li_canales_seleccionados.ListItems(i).SubItems(1))
            End If
        Next
        Call MostrarCanalesVenta(Val(txtCodiOferta.Text))
        Call ChequeaBotonesCanales
    End If
End Sub

Private Sub cmd_sel_canal_todos_Click()
    Dim i As Integer
    Dim sRespuesta As String
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_canales.ListItems.Count
        If Not IngresaCanalOferta(Val(txtCodiOferta.Text), li_canales.ListItems(i).SubItems(1), sRespuesta) Then
            MsgBox sRespuesta, vbCritical, "Error"
        End If
    Next i
    Call MostrarCanalesVenta(Val(txtCodiOferta.Text))
    
    Call ChequeaBotonesCanales
    
End Sub

Private Sub cmd_desel_canal_todos_Click()
    Dim i As Integer
    'Recorre todos los items y para sumar los SubItems
    For i = 1 To li_canales_seleccionados.ListItems.Count
        Call EliminaCanalOferta(Val(txtCodiOferta.Text), li_canales_seleccionados.ListItems(i).SubItems(1))
    Next i
    Call MostrarCanalesVenta(Val(txtCodiOferta.Text))
    
    Call ChequeaBotonesCanales
End Sub

Private Sub ChequeaBotonesCanales()
    If li_canales.ListItems.Count = 0 Then
        cmd_sel_canal_todos.Enabled = False
        Call suEnable_Ctl1(Me.Tag, cmd_desel_canal_todos)
    Else
        Call suEnable_Ctl1(Me.Tag, cmd_sel_canal_todos)
    End If
    
    If li_canales_seleccionados.ListItems.Count = 0 Then
        cmd_desel_canal_todos.Enabled = False
        Call suEnable_Ctl1(Me.Tag, cmd_sel_canal_todos)
    Else
        Call suEnable_Ctl1(Me.Tag, cmd_desel_canal_todos)
    End If
End Sub
'

Private Sub cmd_sel_localidad_Click()
    Dim i As Long, sFechaF As String, iCant As Long, sRespuesta As String
    
    For i = 1 To li_localidades.ListItems.Count
        If li_localidades.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
     
    If MsgBox("�Esta seguro de agregar las " & Trim(Str(iCant)) & " localidades seleccionadas?", vbYesNo) = vbYes Then
        For i = 1 To li_localidades.ListItems.Count
            If li_localidades.ListItems(i).Checked Then
                If IsNull(dtpFechaFLoc.Value) Then
                    sFechaF = ""
                Else
                    sFechaF = Format(dtpFechaFLoc.Value, "dd-mm-yyyy")
                End If
                
                If Not InsOferLoca(CLng(txtCodiOferta.Text), li_localidades.ListItems(i).SubItems(1), Format(dtpFechaILoc.Value, "dd-mm-yyyy"), sFechaF, sRespuesta) Then
                    MsgBox sRespuesta, vbCritical, "Error"
                End If

            End If
        Next
        Call suTraerLocalidades(Val(txtCodiOferta.Text))
    End If
    
End Sub

Private Sub cmd_desel_localidad_Click()
    Dim i As Long, sFecha As String, iCant As Long
    
    For i = 1 To li_localidades_seleccionadas.ListItems.Count
        If li_localidades_seleccionadas.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
     
    If MsgBox("�Esta seguro de eliminar las " & Trim(Str(iCant)) & " localidades seleccionadas?", vbYesNo) = vbYes Then
        For i = 1 To li_localidades_seleccionadas.ListItems.Count
            If li_localidades_seleccionadas.ListItems(i).Checked Then
                Call DelOferLoca(CLng(txtCodiOferta.Text), li_localidades_seleccionadas.ListItems(i).SubItems(1))
            End If
        Next
    
        Call suTraerLocalidades(Val(txtCodiOferta.Text))
    End If
End Sub


Private Sub cmd_sel_localidad_todos_Click()
    Dim i As Long, sFechaF As String, iCant As Long, sRespuesta As String
     
    For i = 1 To li_localidades.ListItems.Count
        If IsNull(dtpFechaFLoc.Value) Then
            sFechaF = ""
        Else
            sFechaF = Format(dtpFechaFLoc.Value, "dd-mm-yyyy")
        End If
        
        If Not InsOferLoca(CLng(txtCodiOferta.Text), li_localidades.ListItems(i).SubItems(1), Format(dtpFechaILoc.Value, "dd-mm-yyyy"), sFechaF, sRespuesta) Then
            MsgBox sRespuesta, vbCritical, "Error"
        End If
    Next
    Call suTraerLocalidades(Val(txtCodiOferta.Text))
End Sub

Private Sub cmdAgregarDetO_Click()
    mbAgregar = True
    If Not fnValidaDetalleOferta Then
        Exit Sub
    End If

    Call suAgregaDetalleOferta
End Sub

Private Sub suAgregaDetalleOferta()
    Dim lCodigo As Long, lValTotal As Double, lPorcDesc As Long, lValCargo As Double, lValDesc As Double
    Dim lValDescProm As Double, sRespuesta As String, sExclusion As String
    Dim lMesesProm As Long, lCodPaquete As Long, lpaqadic As Long
    Dim lCodDescPc As String, lValdDescPC As Double, lIniciaCobro, lObligatorio As String
    If txtValPromocion = "0" Then
        txtDescProm.Text = ""
        txtValDescProm.Text = "0"
        txtMesesProm.Text = "0"
    End If
    
    lCodPaquete = txtCodPaquete.Text
    If txtPaqAdic.Text <> "" And txtPaqAdic.Text <> "0" Then
        lpaqadic = txtPaqAdic.Text
    Else
        lpaqadic = 0
    End If
    
    If txtValDescPC.Text = "" Then txtValDescPC.Text = 0
    
    lCodigo = txtCodiOferta.Text
    lValTotal = txtValTotal.Text
    lPorcDesc = txtPorcDesc.Text
    lValCargo = txtValCargo.Text
    lValDesc = txtValDesc.Text
    lValDescProm = txtValDescProm.Text 'ticket 69581
    ' mjperez Pack Compartido
    lCodDescPc = txtDescPC.Text
    lValdDescPC = txtValDescPC.Text
    'SI 17318 Se valida paquetes obligatorio solo en Alta
    lObligatorio = cbxObligatorio.Text
    
    If txtMesesProm.Text <> "" And txtMesesProm.Text <> "0" Then
        lMesesProm = txtMesesProm.Text
    Else
        lMesesProm = 0
    End If
    
    If cbxExclusion.Text = "NADA" Then
        sExclusion = ""
    Else
        sExclusion = arrCodiAreas(cbxExclusion.ListIndex - 1)
    End If
    
    If chkIniciaCobro.Value = "1" Then lIniciaCobro = "S" Else lIniciaCobro = "N"

    If InsOferDet(lCodigo, arrCodiAreas(cbxArea.ListIndex - 1), Mid(cbxTipoPaquete.Text, 1, 5), txtCargo.Text, _
                  txtDescuento.Text, lValTotal, lPorcDesc, lValCargo, lValDesc, txtDescProm.Text, _
                  lValDescProm, lMesesProm, sExclusion, arrCodiClasepaque(cbxClasPaquete.ListIndex - 1), _
                  lCodPaquete, lpaqadic, lCodDescPc, lValdDescPC, lIniciaCobro, lObligatorio, sRespuesta) Then
                  
        If sRespuesta = "OK" Then
            Call suTraeDatosOferta(Val(txtCodiOferta.Text), li_detalle_ofertas.ListItems.Count + 1)
        Else
            MsgBox sRespuesta, vbCritical, "ERROR"
        End If
    End If
End Sub

Private Sub cmdBorrarseleccion_Click()
    Call suBorraSeleccionLista(li_ofertas)
    cmdBorrarseleccion.Enabled = False
    cmd_exportar.Enabled = False
End Sub

Private Sub cmdEliminarDetO_Click()
    Dim i As Long, iCant As Long
    Dim sCodiArea As String, sCodiClasPaquete As String ' SSIN#15788. JPABLOS. 05-may-2015.

    If MsgBox("�Esta seguro de ELIMINAR el registro seleccionado?", vbInformation + vbYesNo, "Confirmaci�n") = vbNo Then
        Exit Sub
    End If
   
    Call DelOferDetRowid(txtRowid.Text)

    Call suTraeDatosOferta(CLng(txtCodiOferta.Text), li_detalle_ofertas.SelectedItem.Index)

End Sub


Private Sub cmdModificarDetO_Click()
    'Mejoras por SI 17318 ASALAZAR
    Dim mCodiArea As String, mCodiPaquete As Long, mCodiPaqueteAnt As Long, mTipoPaquete As String, mCodiCargo As String, mCodDesc As String, mCodDescPC As String
    Dim mCodigo As Long, mValTotal As Double, mPorcDesc As Long, mValCargo As Double, mValDesc As Double, mDescProm As String, mValDescProm As Double
    Dim mValDescPC As Double, mCodiClasPaquete As String, mMesesProm As Long, mPaqAdic As Long, mExclusion As String, mIniciaCobro As String
    Dim mObligatorio As String, mRespuesta As String
    
    If MsgBox("�Esta Seguro de MODIFICAR el registro seleccionado?", vbInformation + vbYesNo, "Confirmaci�n") = vbNo Then
        Exit Sub
    End If
    
    If txtValPromocion = "0" Then
        txtDescProm.Text = ""
        txtValDescProm.Text = "0"
        txtMesesProm.Text = "0"
    End If
    
    If txtValDescPC.Text = "" Then txtValDescPC.Text = 0
    
    mCodigo = txtCodiOferta.Text
    mCodiArea = arrCodiAreas(cbxArea.ListIndex - 1)
    mCodiPaquete = txtCodPaquete.Text
    mCodiPaqueteAnt = Cod_Paquete
    mTipoPaquete = Mid(cbxTipoPaquete.Text, 1, 5)
    mCodiCargo = txtCargo.Text
    mCodDesc = txtDescuento.Text
    mCodDescPC = txtDescPC.Text
    mValTotal = txtValTotal.Text
    mPorcDesc = txtPorcDesc.Text
    mValCargo = txtValCargo.Text
    mValDesc = txtValDesc.Text
    mDescProm = txtDescProm.Text
    mValDescProm = txtValDescProm.Text
    mValDescPC = txtValDescPC.Text
    mCodiClasPaquete = arrCodiClasepaque(cbxClasPaquete.ListIndex - 1)
    If txtMesesProm.Text <> "" And txtMesesProm.Text <> "0" Then
        mMesesProm = txtMesesProm.Text
    Else
        mMesesProm = 0
    End If
    If txtPaqAdic.Text <> "" And txtPaqAdic.Text <> "0" Then
        mPaqAdic = txtPaqAdic.Text
    Else
        mPaqAdic = 0
    End If
    If cbxExclusion.Text = "NADA" Then
        mExclusion = ""
    Else
        mExclusion = arrCodiAreas(cbxExclusion.ListIndex - 1)
    End If
    If chkIniciaCobro.Value = "1" Then mIniciaCobro = "S" Else mIniciaCobro = "N"
    mObligatorio = cbxObligatorio.Text
    
    mbAgregar = False
    If fnValidaDetalleOferta Then
        If UdpDetOferta(mCodigo, _
                        mCodiPaquete, _
                        mCodiPaqueteAnt, _
                        mCodiArea, _
                        mTipoPaquete, _
                        mCodiCargo, _
                        mCodDesc, _
                        mValTotal, _
                        mPorcDesc, _
                        mValCargo, _
                        mValDesc, _
                        mDescProm, _
                        mValDescProm, _
                        mMesesProm, _
                        mExclusion, _
                        mCodiClasPaquete, _
                        mPaqAdic, _
                        mCodDescPC, _
                        mValDescPC, _
                        mIniciaCobro, _
                        mObligatorio, _
                        mRespuesta) Then
                        
                        If mRespuesta = "OK" Then
                            Call suTraeDatosOferta(Val(txtCodiOferta.Text), li_detalle_ofertas.ListItems.Count + 1)
                        Else
                            MsgBox mRespuesta, vbCritical, "ERROR" '"No se pudo actualizar la oferta"
                        End If
        Else
            If mRespuesta <> "OK" Then
               MsgBox mRespuesta, vbCritical, "ERROR"
            End If
        End If
    End If
    
    'Forma antigua
    'Dim i As Long, sCodiArea As String, sCodiClasPaquete As String ' SSIN#15788. JPABLOS. 05-may-2015.
    'If MsgBox("�Esta Seguro de MODIFICAR el registro seleccionado?", vbInformation + vbYesNo, "Confirmaci�n") = vbNo Then
    '    Exit Sub
    'End If
    
    'mbAgregar = False
    'If Not fnValidaDetalleOferta Then
    '    Exit Sub
    'End If
        
    'If True = DelOferDetRowid(txtRowid.Text) Then
    '    Call suAgregaDetalleOferta
    'End If
    
End Sub


Private Sub cmdNuevaOferta_Click()
    Dim lCodOferta As Long
    Dim sRespuesta As String
    Dim pack_comp As String
    Dim xValores() As String
    
    If chkPackCompartido.Value = "1" Then pack_comp = "S" Else pack_comp = "N"
    
    If fnValidaDatos Then
        If InsOferta(UCase(txtDescOferta.Text), _
                           txtCodCargo.Text, _
                           txtCodDesc.Text, _
                           Format(dtFechaInicio.Value, "dd-mm-yyyy"), _
                           IIf(IsNull(dtFechaFin.Value), "NADA", Format(dtFechaFin.Value, "dd-mm-yyyy")), _
                           "XXXX", _
                           UCase(txtObsOferta.Text), _
                           Val(txtValOferta.Text), _
                           Val(txtValPromocion.Text), _
                           pack_comp, _
                           Fn_Valor_Combo(cb_agrupador), _
                           lCodOferta, _
                           sRespuesta) Then
                           
            If sRespuesta = "OK" Then
                ReDim xValores(13)
                xValores(0) = lCodOferta
                xValores(1) = txtCodCargo.Text
                xValores(2) = txtCodDesc.Text
                xValores(3) = UCase(txtDescOferta.Text)
                xValores(4) = UCase(txtObsOferta.Text)
                xValores(5) = txtValOferta.Text
                xValores(6) = txtValPromocion.Text
                xValores(7) = Now
                xValores(8) = Format(dtFechaInicio.Value, "dd-mm-yyyy")
                xValores(9) = IIf(IsNull(dtFechaFin.Value), "", Format(dtFechaFin.Value, "dd-mm-yyyy"))
                xValores(10) = pack_comp
                xValores(11) = txtUnidMonetar.Text
                xValores(12) = Fn_Valor_Combo(cb_agrupador)
                Call suAgregaToLista(li_ofertas, xValores)
                Call li_ofertas_ItemClick(li_ofertas.ListItems(li_ofertas.ListItems.Count))
            Else
                MsgBox sRespuesta, vbCritical, "ERROR"
            End If
        Else
            If sRespuesta <> "OK" Then
                MsgBox sRespuesta, vbCritical, "ERROR"
            End If
        End If
    End If
End Sub

Private Sub cmdModificarOferta_Click()
    Dim sRespuesta As String
    Dim pack_comp As String
    Dim xValores() As String

    If chkPackCompartido.Value = "1" Then pack_comp = "S" Else pack_comp = "N"
    
    If fnValidaDatos Then
        If UpdOferta(txtCodiOferta.Text, _
                     txtDescOferta.Text, _
                     txtCodCargo.Text, _
                     txtCodDesc.Text, _
                     Format(dtFechaInicio.Value, "dd-mm-yyyy"), _
                     IIf(IsNull(dtFechaFin.Value), _
                     "NADA", _
                     Format(dtFechaFin.Value, "dd-mm-yyyy")), _
                     txtObsOferta.Text, _
                     txtValOferta.Text, _
                     txtValPromocion.Text, _
                     pack_comp, _
                     Fn_Valor_Combo(cb_agrupador), _
                     sRespuesta) Then
                     
            If sRespuesta = "OK" Then
                ReDim xValores(13)
                xValores(0) = txtCodiOferta.Text
                xValores(1) = txtCodCargo.Text
                xValores(2) = txtCodDesc.Text
                xValores(3) = UCase(txtDescOferta.Text)
                xValores(4) = UCase(txtObsOferta.Text)
                xValores(5) = txtValOferta.Text
                xValores(6) = txtValPromocion.Text
                xValores(7) = li_ofertas.ListItems(li_ofertas.SelectedItem.Index).SubItems(7)
                xValores(8) = Format(dtFechaInicio.Value, "dd-mm-yyyy")
                xValores(9) = IIf(IsNull(dtFechaFin.Value), "", Format(dtFechaFin.Value, "dd-mm-yyyy"))
                xValores(10) = pack_comp
                xValores(11) = txtUnidMonetar.Text
                xValores(12) = Fn_Valor_Combo(cb_agrupador)
                Call suModificaToLista(li_ofertas, xValores, li_ofertas.SelectedItem.Index)
                Call li_ofertas_ItemClick(li_ofertas.ListItems(li_ofertas.SelectedItem.Index))
            Else
                MsgBox "No se pudo actualizar la oferta", vbCritical, "ERROR"
            End If
        Else
            If sRespuesta <> "OK" Then
                MsgBox "No se pudo actualizar la oferta", vbCritical, "ERROR"
            End If
        End If
    End If
End Sub

Private Sub cmdCopiaOferta_Click()
    Dim xValores() As String
    Dim lCodOferta As Long
    
    If MsgBox("�Esta seguro de copiar la oferta: " & Me.txtDescOferta.Text & "?", vbYesNo) = vbYes Then
        If CopiaOferta(CLng(Me.txtCodiOferta.Text), "XXXX", lCodOferta) Then
            
            ReDim xValores(13)
            xValores(0) = lCodOferta
            xValores(1) = txtCodCargo.Text
            xValores(2) = txtCodDesc.Text
            xValores(3) = "!! COPIA " + UCase(txtDescOferta.Text)
            xValores(4) = UCase(txtObsOferta.Text)
            xValores(5) = txtValOferta.Text
            xValores(6) = txtValPromocion.Text
            xValores(7) = Now
            xValores(8) = Format(dtFechaInicio.Value, "dd-mm-yyyy")
            xValores(9) = IIf(IsNull(dtFechaFin.Value), "", Format(dtFechaFin.Value, "dd-mm-yyyy"))
            xValores(10) = li_ofertas.ListItems(li_ofertas.SelectedItem.Index).SubItems(10)
            xValores(11) = txtUnidMonetar.Text
            xValores(12) = Fn_Valor_Combo(cb_agrupador)
            
            Call suAgregaToLista(li_ofertas, xValores)
            Call li_ofertas_ItemClick(li_ofertas.ListItems(li_ofertas.ListItems.Count - 1))
            
        End If
    End If
End Sub

Private Sub cmdEliminarOferta_Click()
    If MsgBox("� Esta seguro de eliminar la oferta: " & txtDescOferta.Text & "?" & vbCrLf & "Esto no podr� ser deshecho", vbYesNo) = vbYes Then
        If DelOferta(CLng(txtCodiOferta.Text)) Then
            Call suEliminaFromLista(li_ofertas, li_ofertas.SelectedItem.Index)
            Call li_ofertas_ItemClick(li_ofertas.ListItems(li_ofertas.SelectedItem.Index))
        End If
    End If
End Sub

Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub chkOfertaNoVig_Click()
    Call suLlenaOfertas
End Sub

Private Sub chkOfertaVig_Click()
    Call suLlenaOfertas
End Sub

Private Sub co_agregar_Click()
    Dim iFila As Integer
    iFila = gr_agrupador.Row
    If tx_desc_agrupador.Text <> "" Then
        Call Srv_parn_parame07(202, tx_codi_agrupador.Text, tx_desc_agrupador.Text)
    End If
    Call suLlenaAgrupador(cb_agrupador, 202)
    Call suLlenaGrillaAgrupador(gr_agrupador, 202)
    gr_agrupador.Row = iFila
    gr_agrupador.RowSel = iFila
    gr_agrupador.TopRow = iFila
    Call gr_agrupador_Click
End Sub

Private Sub co_actualizar_Click()
    Dim iFila As Integer
    
    iFila = gr_agrupador.Row
    If tx_desc_agrupador.Text <> "" Then
        Call Srv_paru_parame06(202, tx_codi_agrupador.Text, tx_desc_agrupador.Text)
    End If
    Call suLlenaAgrupador(cb_agrupador, 202)
    Call suLlenaGrillaAgrupador(gr_agrupador, 202)
    gr_agrupador.Row = iFila
    gr_agrupador.RowSel = iFila
    gr_agrupador.TopRow = iFila
    Call gr_agrupador_Click
End Sub

Private Sub co_eliminar_Click()
    Dim iFila As Integer
    iFila = gr_agrupador.Row
    If tx_codi_agrupador.Text <> "" Then
        Call Srv_pard_parame07(202, tx_codi_agrupador.Text, 11)
    End If
    Call suLlenaAgrupador(cb_agrupador, 202)
    Call suLlenaGrillaAgrupador(gr_agrupador, 202)
    gr_agrupador.Row = iFila
    gr_agrupador.RowSel = iFila
    gr_agrupador.TopRow = iFila
    Call gr_agrupador_Click
End Sub

Private Sub Form_Load()
    Call suInicializaForm
    Call suLlenaOfertas
    Call li_ofertas_ItemClick(li_ofertas.ListItems(li_ofertas.ListItems.Count))
    Call gr_agrupador_Click
    
    cmd_exportar.Enabled = False
    cmdBorrarseleccion.Enabled = False
    
End Sub


Private Sub suInicializaForm()

    gr_agrupador.ColWidth(0) = 748
    gr_agrupador.ColWidth(1) = 5212
    SSTab1.Tab = 0
    
    Call suLlenaAreas
    Call suLlenaClasePaque
    Call suLlenaExclusiones
    Call suLlenaAgrupador(cb_agrupador, 202)
    Call suLlenaGrillaAgrupador(gr_agrupador, 202)
    Call suLlenaObligatorio
    
    Dim controlesForm As Control
    For Each controlesForm In Controls
      If (TypeOf controlesForm Is CommandButton) Then
          If controlesForm.Tag <> "" Then
              Call suEnable_Ctl1(Me.Tag, controlesForm)
          End If
      End If
    Next
    
End Sub
Private Sub suLlenaObligatorio()
    With cbxObligatorio
        .Clear
        .AddItem "S"
        .AddItem "N"
        .ListIndex = 0
    End With
End Sub



Private Sub suLlenaAreas()
    Dim i As Integer

    cbxArea.Clear
    cbxArea.AddItem "SELECCIONE"
    For i = 0 To UBound(arrAreas)
        cbxArea.AddItem arrAreas(i)
    Next i
    
    cbxArea.ListIndex = 0
End Sub

Private Sub suLlenaClasePaque()
    Dim i As Integer

    cbxClasPaquete.Clear
    cbxClasPaquete.AddItem "SELECCIONE"
    For i = 0 To UBound(arrClasePaque)
        cbxClasPaquete.AddItem arrClasePaque(i)
    Next i
    
    cbxClasPaquete.ListIndex = 0
End Sub

Private Sub suLlenaExclusiones()
    Dim i As Integer

    cbxExclusion.Clear
    cbxExclusion.AddItem "NADA"
    For i = 0 To UBound(arrAreas)
        cbxExclusion.AddItem arrAreas(i)
    Next i
    
    cbxExclusion.ListIndex = 0
End Sub

Private Function fnValidaDatos() As Boolean
    fnValidaDatos = True
    
'    If txtCodiOferta.Text = "" Then
'        MsgBox "Debe ingresar el codigo de la oferta"
'        fnValidaDatos = False
'        Exit Function
'    End If
    
    If txtDescOferta.Text = "" Then
        MsgBox "Debe ingresar la descripci�n de la oferta"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtObsOferta.Text = "" Then
        MsgBox "Debe ingresar la observaci�n de la oferta"
        fnValidaDatos = False
        Exit Function
    End If

    If txtValOferta.Text = "" Then
        MsgBox "Debe ingresar el valor de la oferta"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtCodCargo.Text = "" Then
        MsgBox "Debe ingresar el c�digo correspondiente al cargo"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtCodDesc.Text = "" Then
        MsgBox "Debe ingresar el c�digo correspondiente al descuento"
        fnValidaDatos = False
        Exit Function
    End If
    
    If Not IsNull(dtFechaFin.Value) Then
        If dtFechaFin.Value < dtFechaInicio.Value Then
            MsgBox "No puede poner la fecha de fin antes que la fecha de inicio"
            fnValidaDatos = False
            Exit Function
        End If
    End If
End Function

Private Sub suLlenaOfertas()
    Dim lCodiOferta As Long, sDescOferta As String
    Dim sFechaCreacion As String, dFechaI As Date, dFechaF As Date
    Dim sObservacion As String, lValOferta As Double, lValPromo As Double
    Dim bEntra As Boolean
    Dim dFecha As Date, sFechaInicio As String, sFechaFin As String
    Dim sCargo As String, sDcto As String, sCodiCargo As String, sCodiDcto As String, sPackComp As String, sUnidMonetar As String, sCodiAgrupa As String
    
    bEntra = False
    dFecha = Format(Now(), "dd-mm-yyyy")
    
    Dim Total As Integer, i As Integer, Ret As Integer, Linea As String
    'Dim dFecha As Date
    Dim lof As ListItem
    
    dFecha = Format(Now(), "dd-mm-yyyy")
    
    Screen.MousePointer = vbHourglass
    
    li_ofertas.ListItems.Clear
    li_ofertas.Visible = False
    
    Total = GetOfertas(Me.chkOfertaVig.Value, Me.chkOfertaNoVig.Value)
    
    For i = 1 To Total
        Ret = GetOfertasRead(lCodiOferta, sDescOferta, sObservacion, lValOferta, lValPromo, sFechaCreacion, sFechaInicio, sFechaFin, sCargo, sCodiCargo, sDcto, sCodiDcto, sPackComp, sUnidMonetar, sCodiAgrupa)
        
        Dim xValores(15) As String
        xValores(0) = Trim$(lCodiOferta)
        xValores(1) = Trim$(sCargo)
        'xValores(2) = Trim$(sCodiCargo)
        xValores(2) = Trim$(sDcto)
        'xValores(4) = Trim$(sCodiDcto)
        xValores(3) = Trim$(sDescOferta)
        xValores(4) = Trim$(sObservacion)
        xValores(5) = Trim$(lValOferta)
        xValores(6) = Trim$(lValPromo)
        xValores(7) = Trim$(sFechaCreacion)
        xValores(8) = Trim$(sFechaInicio)
        xValores(9) = Trim$(sFechaFin)
        xValores(10) = Trim$(sPackComp)
        xValores(11) = Trim$(sUnidMonetar)
        xValores(12) = Trim$(sCodiAgrupa)

        Call suAgregaToLista(li_ofertas, xValores)
    Next i
    Ret = GetOfertasClose()

    li_ofertas.Visible = True
    
    Screen.MousePointer = vbDefault
    
End Sub


Private Sub gr_agrupador_Click()
    Dim Total As Integer, i As Integer, Ret As Integer
   
    If gr_agrupador.Row > 0 And gr_agrupador.Row < gr_agrupador.Rows - 1 Then
        tx_codi_agrupador.Text = gr_agrupador.TextMatrix(gr_agrupador.Row, 0)
        tx_desc_agrupador.Text = gr_agrupador.TextMatrix(gr_agrupador.Row, 1)
        co_agregar.Enabled = False
        Call suEnable_Ctl1(Me.Tag, co_eliminar)
        Call suEnable_Ctl1(Me.Tag, co_actualizar)
   Else
        Call suEnable_Ctl1(Me.Tag, co_agregar)
        co_eliminar.Enabled = False
        co_actualizar.Enabled = False
   End If
   
    If gr_agrupador.Row = gr_agrupador.Rows - 1 Then
        tx_codi_agrupador.Text = Val(gr_agrupador.TextMatrix(gr_agrupador.Rows - 2, 0)) + 1
        tx_desc_agrupador.Text = ""
        'tx_desc_agrupador.SetFocus
    End If
   
End Sub


Private Sub li_canales_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Long, iCant As Long
    
    For i = 1 To li_canales.ListItems.Count
        If li_canales.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
    
    If iCant > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_sel_canal)
    Else
        cmd_sel_canal.Enabled = False
    End If
    
    If li_canales.ListItems.Count > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_sel_canal_todos)
    Else
        cmd_sel_canal_todos.Enabled = False
    End If
    
End Sub

Private Sub li_canales_seleccionados_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Long, iCant As Long
    
    For i = 1 To li_canales_seleccionados.ListItems.Count
        If li_canales_seleccionados.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
    
    If iCant > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_desel_canal)
    Else
        cmd_desel_canal.Enabled = False
    End If

End Sub

Private Sub li_detalle_ofertas_ItemClick(ByVal Item As MSComctlLib.ListItem)

    txtCodPaquete.Text = li_detalle_ofertas.ListItems(Item.Index).SubItems(2)
    Cod_Paquete = txtCodPaquete.Text
    txtCargo.Text = li_detalle_ofertas.ListItems(Item.Index).SubItems(3)
    txtDescuento.Text = li_detalle_ofertas.ListItems(Item.Index).SubItems(4)
    txtValTotal.Text = Val(li_detalle_ofertas.ListItems(Item.Index).SubItems(5))
    txtPorcDesc.Text = Val(li_detalle_ofertas.ListItems(Item.Index).SubItems(6))
    txtValCargo.Text = Val(li_detalle_ofertas.ListItems(Item.Index).SubItems(7))
    txtValDesc.Text = Val(li_detalle_ofertas.ListItems(Item.Index).SubItems(8))
    txtDescProm.Text = Val(li_detalle_ofertas.ListItems(Item.Index).SubItems(9))
    txtValDescProm.Text = Val(li_detalle_ofertas.ListItems(Item.Index).SubItems(10))
    txtMesesProm.Text = Val(li_detalle_ofertas.ListItems(Item.Index).SubItems(11))
    txtPaqAdic.Text = li_detalle_ofertas.ListItems(Item.Index).SubItems(15)
    txtDescPC.Text = li_detalle_ofertas.ListItems(Item.Index).SubItems(16)
    txtValDescPC.Text = Val(li_detalle_ofertas.ListItems(Item.Index).SubItems(17))
    chkIniciaCobro.Value = IIf(li_detalle_ofertas.ListItems(Item.Index).SubItems(18) = "S", 1, 0)
    txtRowid.Text = li_detalle_ofertas.ListItems(Item.Index).SubItems(19)
    
    Call gsuAsignaCombo(cbxArea, li_detalle_ofertas.ListItems(Item.Index).SubItems(20))
    Call gsuAsignaCombo(cbxTipoPaquete, li_detalle_ofertas.ListItems(Item.Index).SubItems(21))
    Call gsuAsignaCombo(cbxExclusion, li_detalle_ofertas.ListItems(Item.Index).SubItems(22))
    Call gsuAsignaCombo(cbxClasPaquete, li_detalle_ofertas.ListItems(Item.Index).SubItems(23))
    cbxObligatorio = IIf(li_detalle_ofertas.ListItems(Item.Index).SubItems(24) = "", "S", li_detalle_ofertas.ListItems(Item.Index).SubItems(24))

    
    If Item.Index > 0 And Item.Index < li_detalle_ofertas.ListItems.Count Then
        Call suEnable_Ctl1(Me.Tag, cmdAgregarDetO)
        Call suEnable_Ctl1(Me.Tag, cmdModificarDetO)
        Call suEnable_Ctl1(Me.Tag, cmdEliminarDetO)
    Else
        Call suEnable_Ctl1(Me.Tag, cmdAgregarDetO)
        cmdModificarDetO.Enabled = False
        cmdEliminarDetO.Enabled = False
    End If
    
    li_detalle_ofertas.ListItems(Item.Index).Selected = True
End Sub

Private Sub li_localidades_seleccionadas_ItemClick(ByVal Item As MSComctlLib.ListItem)
    If Item.Index > 0 And Item.Index <= li_localidades_seleccionadas.ListItems.Count Then
        'dtpFechaILoc.Value = li_localidades_seleccionadas.ListItems(Item.Index).SubItems(2)
        dtpFechaILoc.Value = Now
        dtpFechaILoc.Value = IIf(li_localidades_seleccionadas.ListItems(Item.Index).SubItems(2) = "", Null, li_localidades_seleccionadas.ListItems(Item.Index).SubItems(2))
        
        If li_localidades_seleccionadas.ListItems(Item.Index).SubItems(3) <> "" Then
            dtpFechaFLoc.Value = li_localidades_seleccionadas.ListItems(Item.Index).SubItems(3)
        Else
            dtpFechaFLoc.Value = Now
            dtpFechaFLoc.Value = Null
        End If
    End If
End Sub

Private Sub li_localidades_seleccionadas_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Long, iCant As Long
    
    For i = 1 To li_localidades_seleccionadas.ListItems.Count
        If li_localidades_seleccionadas.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
    
    If iCant > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_desel_localidad)
        Call suEnable_Ctl1(Me.Tag, cmd_act_fecha)
    Else
        cmd_desel_localidad.Enabled = False
        cmd_act_fecha.Enabled = False
    End If
End Sub

Private Sub li_localidades_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Long, iCant As Long
    
    For i = 1 To li_localidades.ListItems.Count
        If li_localidades.ListItems(i).Checked Then
            iCant = iCant + 1
        End If
    Next
    
    If iCant > 0 Then
        Call suEnable_Ctl1(Me.Tag, cmd_sel_localidad)
    Else
        cmd_sel_localidad.Enabled = False
    End If
End Sub

Private Sub li_ofertas_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Dim frm As New frmFiltro
    Dim i As Integer

    If Not IsNumeric(li_ofertas.ColumnHeaders(ColumnHeader.Index).Tag) Then
        frm.TipoFiltro = li_ofertas.ColumnHeaders(ColumnHeader.Index).Tag
        frm.tx_campo.Text = ColumnHeader.Text
        
        For i = 1 To li_ofertas.ColumnHeaders.Count
           frm.cmdBorrar.Enabled = False
           If InStr(1, li_ofertas.ColumnHeaders.Item(i).Text, "[F] ", vbTextCompare) > 0 Then
              frm.cmdBorrar.Enabled = True
              Exit For
           End If
        Next i
        
        If li_ofertas.ColumnHeaders(ColumnHeader.Index).Tag = "Combo" Then
            frm.cb_filtro.Clear
            Select Case ColumnHeader.Index
            Case 11
                frm.cb_filtro.AddItem "S"
                frm.cb_filtro.AddItem "N"
                frm.Separador = ""
            Case 13
                For i = 0 To cb_agrupador.ListCount - 1
                   frm.cb_filtro.AddItem (cb_agrupador.List(i))
                Next i
                frm.Separador = " - "
            End Select
            frm.cb_filtro.ListIndex = 0
        End If
        
        frm.Show vbModal
        
        Select Case frm.Valor
        Case "Aplicar"
           Call suFiltro(li_ofertas, ColumnHeader.Index - 1, frm.Operador, frm.Filtro)
           
           ColumnHeader.Text = IIf(InStr(1, ColumnHeader.Text, "[F] ", vbTextCompare) > 0, Mid(ColumnHeader.Text, InStr(1, ColumnHeader.Text, "[F] ", vbTextCompare) + 4), ColumnHeader.Text)
           ColumnHeader.Text = "[F] " + ColumnHeader.Text
           
        Case "Borrar"
           Call suLlenaOfertas
           
           For i = 1 To li_ofertas.ColumnHeaders.Count
              If InStr(1, li_ofertas.ColumnHeaders.Item(i).Text, "[F] ", vbTextCompare) > 0 Then
                 li_ofertas.ColumnHeaders.Item(i).Text = Mid(li_ofertas.ColumnHeaders.Item(i).Text, 5)
              End If
           Next i
        
        End Select
    End If
    
    Set frm = Nothing
End Sub

Private Sub li_ofertas_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim i As Integer, bln As Boolean
    
    If Item.Index > 0 And Item.Index < li_ofertas.ListItems.Count Then
        If li_ofertas.ListItems(Item.Index).Checked Then
            Call suEnable_Ctl1(Me.Tag, cmd_exportar)
            cmdBorrarseleccion.Enabled = True
        Else
            bln = False
            For i = 1 To li_ofertas.ListItems.Count
                 If li_ofertas.ListItems(i).Checked Then
                     bln = True
                     Exit For
                 End If
            Next i
            If bln = False Then
                 cmd_exportar.Enabled = False
                 cmdBorrarseleccion.Enabled = False
            End If
        End If
    End If
End Sub

Private Sub li_ofertas_ItemClick(ByVal Item As MSComctlLib.ListItem)
        
    txtCodiOferta.Text = li_ofertas.ListItems(Item.Index)
    txtCodCargo.Text = li_ofertas.ListItems(Item.Index).SubItems(1)
    txtCodDesc.Text = li_ofertas.ListItems(Item.Index).SubItems(2)
    txtDescOferta.Text = li_ofertas.ListItems(Item.Index).SubItems(3)
    txtObsOferta.Text = li_ofertas.ListItems(Item.Index).SubItems(4)
    txtValOferta.Text = li_ofertas.ListItems(Item.Index).SubItems(5)
    txtValPromocion.Text = li_ofertas.ListItems(Item.Index).SubItems(6)
    
    dtFechaInicio.Value = IIf(li_ofertas.ListItems(Item.Index).SubItems(8) = "", Now, li_ofertas.ListItems(Item.Index).SubItems(8))
    
    If li_ofertas.ListItems(Item.Index).SubItems(9) = "" Then
        dtFechaFin.Value = Now
        dtFechaFin.Value = ""
    End If
    dtFechaFin.Value = IIf(li_ofertas.ListItems(Item.Index).SubItems(9) = "", "", li_ofertas.ListItems(Item.Index).SubItems(9))
    
    chkPackCompartido.Value = IIf(li_ofertas.ListItems(Item.Index).SubItems(10) = "S", 1, 0)
    
    txtUnidMonetar.Text = li_ofertas.ListItems(Item.Index).SubItems(11)
    
    Call gsuAsignaCombo(cb_agrupador, li_ofertas.ListItems(Item.Index).SubItems(12))
    
    frmOfertas.Caption = "Mantenedor de Ofertas " & "[" & txtCodiOferta.Text & " - " & txtDescOferta.Text & "]"
    
    If Item.Index > 0 And Item.Index < li_ofertas.ListItems.Count Then
        Call suEnable_Ctl1(Me.Tag, cmdModificarOferta)
        Call suEnable_Ctl1(Me.Tag, cmdEliminarOferta)
        Call suEnable_Ctl1(Me.Tag, cmdCopiaOferta)
        
        cmdNuevaOferta.Enabled = False
        
        SSTab1.TabEnabled(1) = True
        SSTab1.TabEnabled(2) = True
        
        Call suTraeDatosOferta(Val(txtCodiOferta.Text), 1)
        Call suTraerLocalidades(Val(txtCodiOferta.Text))
        Call MostrarCanalesVenta(Val(txtCodiOferta.Text))
    Else
        cmdModificarOferta.Enabled = False
        cmdEliminarOferta.Enabled = False
        cmdCopiaOferta.Enabled = False

        Call suEnable_Ctl1(Me.Tag, cmdNuevaOferta)
        
        
        SSTab1.TabEnabled(1) = False
        SSTab1.TabEnabled(2) = False
        
        li_ofertas.ListItems(Item.Index).EnsureVisible
        li_ofertas.ListItems(Item.Index).Selected = True
        
    End If
    
    If li_detalle_ofertas.ListItems.Count > 0 Then
        Call li_detalle_ofertas_ItemClick(li_detalle_ofertas.ListItems(li_detalle_ofertas.ListItems.Count))
    End If
    
End Sub

Private Sub txtBuscar_Change()
    SendKeys "{END}"
    Call gsuScanStringLW(li_ofertas, txtBuscar, 4, 24)
End Sub

Private Sub txtBuscar_KeyUp(KeyCode As Integer, Shift As Integer)
    Call gsuScanStringLW(li_ofertas, txtBuscar, 4, 24)
End Sub



Private Sub txtBuscarLocalidades_Change()
    SendKeys "{END}"
    Call gsuScanStringLW(li_localidades, txtBuscarLocalidades, 0, 24)
End Sub

Private Sub txtBuscarLocalidades_KeyUp(KeyCode As Integer, Shift As Integer)
    Call gsuScanStringLW(li_localidades, txtBuscarLocalidades, 0, 24)
End Sub

Private Sub txtCargo_KeyPress(KEYASCII As Integer)
    KEYASCII = SoloNumeros(KEYASCII)
End Sub

Private Sub txtCargo_LostFocus()
    Dim i As Integer
    
    i = fnValidaCodigo(txtCargo)
    
End Sub


Private Sub txtCodCargo_KeyPress(KEYASCII As Integer)
    KEYASCII = SoloNumeros(KEYASCII)
End Sub

Private Sub txtCodCargo_LostFocus()
    Dim i As Integer, iOk As Integer, sCodi As String

    sCodi = Trim$(txtCodCargo.Text)
    
    If sCodi <> "" Then
        iOk = -1
        For i = 0 To UBound(arrRentas)
            If sCodi = arrCodiConcepto(i) Then
                iOk = i
                txtUnidMonetar.Text = arrUnidMonetar(i)
                Exit For
            End If
        Next i
        
        If iOk < 0 Then
            MsgBox "Codigo de Cargo NO v�lido"
            txtCodCargo.Text = ""
        End If

    End If

End Sub


Private Sub txtCodDesc_KeyPress(KEYASCII As Integer)
    KEYASCII = SoloNumeros(KEYASCII)
End Sub




Private Sub txtCodDesc_LostFocus()
    Dim i As Integer
    i = fnValidaCodigo(txtCodDesc)
End Sub


Private Sub txtDescPC_KeyPress(KEYASCII As Integer)
    KEYASCII = SoloNumeros(KEYASCII)
End Sub

Private Sub txtDescPC_LostFocus()
    Dim i As Integer
    i = fnValidaCodigo(txtDescPC)
End Sub


Private Sub txtDescProm_KeyPress(KEYASCII As Integer)
    KEYASCII = SoloNumeros(KEYASCII)
End Sub

Private Sub txtDescProm_LostFocus()
    Dim i As Integer
    i = fnValidaCodigo(txtDescProm)
End Sub


Private Sub txtDescuento_KeyPress(KEYASCII As Integer)
    KEYASCII = SoloNumeros(KEYASCII)
End Sub

Private Sub txtDescuento_LostFocus()
    Dim i As Integer
    i = fnValidaCodigo(txtDescuento)
End Sub

Private Sub txtMesesProm_KeyPress(KEYASCII As Integer)
    KEYASCII = SoloNumeros(KEYASCII)
End Sub

Private Sub txtPorcDesc_KeyPress(KEYASCII As Integer)
    KEYASCII = SoloNumerosDecimales(KEYASCII, txtPorcDesc)
End Sub

Private Sub txtValCargo_Change()
    Dim dValCargo As Double, dValDesc As Double, dValDescProm As Double
    On Error Resume Next
    
    If txtValCargo.Text <> "" Then
        dValCargo = CDbl(txtValCargo.Text)
        dValDesc = CDbl(Trim(txtValDesc.Text))
        dValDescProm = CDbl(txtValDescProm.Text)
        
        txtValTotal.Text = dValCargo + dValDesc
    End If
End Sub

Private Sub txtValCargo_KeyPress(KEYASCII As Integer)

        If Trim$(Mid$(txtUnidMonetar.Text, 1, 4)) = "2" Then
           KEYASCII = SoloNumerosDecimales(KEYASCII, txtValCargo)
        Else
            KEYASCII = SoloNumeros(KEYASCII)
        End If
        
End Sub

Private Sub txtValDesc_Change()
    Dim dValCargo As Double, dValDesc As Double, dValDescProm As Double
    
    If txtValDesc.Text <> "" And txtValDesc.Text <> "-" Then
        dValCargo = Val(txtValCargo.Text)
        dValDesc = Val(txtValDesc.Text)
        dValDescProm = Val(txtValDescProm.Text)
        
        txtValTotal.Text = dValCargo + dValDesc
    End If
End Sub

Private Sub txtValDesc_KeyPress(KEYASCII As Integer)
    If KEYASCII = 45 And Len(txtValDesc.Text) >= 1 Then
        KEYASCII = 0
        Exit Sub
    End If
    
    If KEYASCII <> 45 Then
        If Trim$(Mid$(txtUnidMonetar.Text, 1, 4)) = "2" Then
           KEYASCII = SoloNumerosDecimales(KEYASCII, txtValDesc)
        Else
            KEYASCII = SoloNumeros(KEYASCII)
        End If
    End If
End Sub

Private Sub txtValDescPC_KeyPress(KEYASCII As Integer)
        
        If Trim$(Mid$(txtUnidMonetar.Text, 1, 4)) = "2" Then
           KEYASCII = SoloNumerosDecimales(KEYASCII, txtValDescPC)
        Else
            KEYASCII = SoloNumeros(KEYASCII)
        End If

End Sub


Private Sub txtValDescProm_KeyPress(KEYASCII As Integer)
    
    If KEYASCII <> 45 Then
        If Trim$(Mid$(txtUnidMonetar.Text, 1, 4)) = "2" Then
           KEYASCII = SoloNumerosDecimales(KEYASCII, txtValDescProm)
        Else
            KEYASCII = SoloNumeros(KEYASCII)
        End If

    End If
End Sub

Private Sub txtValOferta_KeyPress(KEYASCII As Integer)

        If Trim$(Mid$(txtUnidMonetar.Text, 1, 4)) = "2" Then
           KEYASCII = SoloNumerosDecimales(KEYASCII, txtValOferta)
        Else
            KEYASCII = SoloNumeros(KEYASCII)
        End If
        
End Sub

Private Sub txtValPromocion_KeyPress(KEYASCII As Integer)
        
        If Trim$(Mid$(txtUnidMonetar.Text, 1, 4)) = "2" Then
           KEYASCII = SoloNumerosDecimales(KEYASCII, txtValPromocion)
        Else
            KEYASCII = SoloNumeros(KEYASCII)
        End If

End Sub

Private Sub txtValTotal_KeyPress(KEYASCII As Integer)
        
        If Trim$(Mid$(txtUnidMonetar.Text, 1, 4)) = "2" Then
           KEYASCII = SoloNumerosDecimales(KEYASCII, txtValTotal)
        Else
            KEYASCII = SoloNumeros(KEYASCII)
        End If

End Sub

Private Sub suTraerLocalidades(codi_oferta As Long)
    Dim Total As Integer, i As Integer, Ret As Integer
    Dim lid As ListItem
    Dim lis As ListItem
    Dim cont As Long
   
    Dim codi_localida As String, desc_localida As String, fech_ini As String, fech_fin As String, seleccionada As Long
    
    On Error Resume Next

    li_localidades.Visible = False
    li_localidades.ListItems.Clear
    li_localidades_seleccionadas.Visible = False
    li_localidades_seleccionadas.ListItems.Clear
    

    Screen.MousePointer = vbHourglass
    cont = 0
    Total = GetOferLoca(codi_oferta)
    For i = 1 To Total
        Total = GetOferLocaRead(codi_localida, desc_localida, fech_ini, fech_fin, seleccionada)

        If seleccionada = 1 Then
            Set lid = li_localidades_seleccionadas.ListItems.Add(, , Trim$(desc_localida))
            lid.SubItems(1) = Trim$(codi_localida)
            lid.SubItems(2) = Trim$(fech_ini)
            lid.SubItems(3) = Trim$(fech_fin)
        Else
            Set lid = li_localidades.ListItems.Add(, , Trim$(desc_localida))
            lid.SubItems(1) = Trim$(codi_localida)
            lid.SubItems(2) = Trim$(fech_ini)
            lid.SubItems(3) = Trim$(fech_fin)
        End If
    Next i
    Ret = GetOferLocaClose()
    
    Screen.MousePointer = vbDefault
    
    li_localidades.Visible = True
    li_localidades_seleccionadas.Visible = True
    cmd_desel_localidad.Enabled = False
    cmd_sel_localidad.Enabled = False
    cmd_act_fecha.Enabled = False

End Sub

Private Sub suTraeDatosOferta(codi_oferta As Integer, Index As Integer)
    Dim sCodiLocalidad As String, sDescLocalidad As String
    Dim sArea As String, sDescTipoPaquete As String, sCodCargo As String, sCodDesc As String
    Dim lValTotal As Double, dPorcDesc As Double, lValCargo As Double, lValDesc As Double
    Dim sCodDescProm As String
    Dim lValDescProm As Double, lMeses As Long
    Dim sExclusion As String, sDescClasePaquete As String
    Dim sFechaI As String, sFechaF As String, lCodPaque As Long, sPaqAdic As Long
    Dim sCodDescPC As String, lValDescPC As Double, sIniciaCobro As String, sRowid As String
    Dim sCodiArea As String, sTipoPaquete As String, sCodiExclusion As String, sClasPaquete As String, sObligatorio As String

    Dim Total As Integer, i As Integer, Ret As Integer, Linea As String
    'Dim dFecha As Date
    Dim ldof As ListItem
    

    
    Screen.MousePointer = vbHourglass
    
    li_detalle_ofertas.Visible = False
    
    li_detalle_ofertas.ListItems.Clear
    
    Total = GetDetOfer(codi_oferta) + 1
   
    For i = 1 To Total
    
        If i = Total Then
        
            Set ldof = li_detalle_ofertas.ListItems.Add(, , "")
                        
            ldof.SubItems(1) = ""
            ldof.SubItems(2) = ""
            ldof.SubItems(3) = ""
            ldof.SubItems(4) = ""
            ldof.SubItems(5) = ""
            ldof.SubItems(6) = ""
            ldof.SubItems(7) = ""
            ldof.SubItems(8) = ""
            ldof.SubItems(9) = ""
            ldof.SubItems(10) = ""
            ldof.SubItems(11) = ""
            ldof.SubItems(12) = ""
            ldof.SubItems(13) = ""
            ldof.SubItems(14) = ""
            ldof.SubItems(15) = ""
            ldof.SubItems(16) = ""
            ldof.SubItems(17) = ""
            ldof.SubItems(18) = ""
            ldof.SubItems(19) = ""
            ldof.SubItems(20) = ""
            ldof.SubItems(21) = ""
            ldof.SubItems(22) = ""
            ldof.SubItems(23) = ""
            ldof.SubItems(24) = ""
        
        
        Else
        
            Ret = GetDetOferRead(sArea, _
                                 sDescTipoPaquete, _
                                 sCodCargo, _
                                 sCodDesc, _
                                 lValTotal, _
                                 dPorcDesc, _
                                 lValCargo, _
                                 lValDesc, _
                                 sCodDescProm, _
                                 lValDescProm, _
                                 lMeses, _
                                 sExclusion, _
                                 sDescClasePaquete, _
                                 lCodPaque, _
                                 sPaqAdic, _
                                 sCodDescPC, _
                                 lValDescPC, _
                                 sIniciaCobro, _
                                 sRowid, _
                                 sCodiArea, _
                                 sTipoPaquete, _
                                 sCodiExclusion, _
                                 sClasPaquete, _
                                 sObligatorio)
            
            Set ldof = li_detalle_ofertas.ListItems.Add(, , Trim$(sArea))
                        
            ldof.SubItems(1) = Trim$(sDescTipoPaquete)
            ldof.SubItems(2) = Trim$(lCodPaque)
            ldof.SubItems(3) = Trim$(sCodCargo)
            ldof.SubItems(4) = Trim$(sCodDesc)
            ldof.SubItems(5) = Trim$(lValTotal)
            ldof.SubItems(6) = Trim$(dPorcDesc)
            ldof.SubItems(7) = Trim$(lValCargo)
            ldof.SubItems(8) = Trim$(lValDesc)
            ldof.SubItems(9) = Trim$(sCodDescProm)
            ldof.SubItems(10) = Trim$(lValDescProm)
            ldof.SubItems(11) = Trim$(lMeses)
            ldof.SubItems(12) = Trim$(sExclusion)
            ldof.SubItems(13) = Trim$(sDescClasePaquete)
            ldof.SubItems(14) = Trim$("")
            ldof.SubItems(15) = Trim$(sPaqAdic)
            ldof.SubItems(16) = Trim$(sCodDescPC)
            ldof.SubItems(17) = Trim$(lValDescPC)
            ldof.SubItems(18) = Trim$(sIniciaCobro)
            ldof.SubItems(19) = Trim$(sRowid)
            ldof.SubItems(20) = Trim$(sCodiArea)
            ldof.SubItems(21) = Trim$(sTipoPaquete)
            ldof.SubItems(22) = Trim$(sCodiExclusion)
            ldof.SubItems(23) = Trim$(sClasPaquete)
            ldof.SubItems(24) = Trim$(sObligatorio) ' Se agrega por SI 17318
            
        End If

        If Index = i Then
            li_detalle_ofertas.ListItems(i).EnsureVisible
            li_detalle_ofertas.ListItems(i).Selected = True
        End If

    Next i
    Ret = GetDetOferClose()
    
    li_detalle_ofertas.Visible = True
    
    Screen.MousePointer = vbDefault
        
End Sub
