VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmPromociones 
   Caption         =   "Mantenedor de Promociones"
   ClientHeight    =   9000
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   17880
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9000
   ScaleWidth      =   17880
   Tag             =   "FRMPROMOCIONES"
   WindowState     =   2  'Maximized
   Begin VB.Frame fraPlanes 
      Caption         =   "Planes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3375
      Left            =   0
      TabIndex        =   57
      Top             =   5520
      Width           =   5295
      Begin VB.CommandButton cmdPlanLimpiar 
         Caption         =   "Limpiar"
         Height          =   255
         Left            =   1320
         TabIndex        =   65
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtPlanCantPerm 
         Height          =   285
         Left            =   1440
         TabIndex        =   64
         Text            =   "Text1"
         Top             =   600
         Width           =   1575
      End
      Begin VB.CommandButton cmdPlanAgregar 
         Caption         =   "Agregar"
         Height          =   255
         Left            =   120
         TabIndex        =   60
         Tag             =   "PL_AGREGAR"
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton cmdPlanEliminar 
         Caption         =   "Eliminar"
         Height          =   255
         Left            =   2520
         TabIndex        =   59
         Tag             =   "PL_ELIMINAR"
         Top             =   960
         Width           =   975
      End
      Begin VB.ComboBox cbxPlanes 
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   58
         Top             =   240
         Width           =   4575
      End
      Begin MSFlexGridLib.MSFlexGrid grdPlanes 
         Height          =   1935
         Left            =   120
         TabIndex        =   61
         Top             =   1200
         Width           =   5055
         _ExtentX        =   8916
         _ExtentY        =   3413
         _Version        =   393216
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Label Label19 
         Caption         =   "Permanencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   63
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label18 
         Caption         =   "Plan"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   62
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame fraCobros 
      Caption         =   "Cobros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5775
      Left            =   11040
      TabIndex        =   22
      Top             =   3120
      Width           =   6735
      Begin VB.CommandButton cmdCobrosLimpiar 
         Caption         =   "Limpiar"
         Height          =   255
         Left            =   2040
         TabIndex        =   55
         Top             =   2640
         Width           =   1335
      End
      Begin VB.CommandButton cmdCobrosEliminar 
         Caption         =   "Eliminar"
         Height          =   255
         Left            =   3840
         TabIndex        =   54
         Tag             =   "CO_ELIMINAR"
         Top             =   2640
         Width           =   1335
      End
      Begin VB.CommandButton cmdCobrosAgregar 
         Caption         =   "Agregar"
         Height          =   255
         Left            =   120
         TabIndex        =   53
         Tag             =   "CO_AGREGAR"
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox txtCobrMaxCuotas 
         Height          =   285
         Left            =   1680
         TabIndex        =   52
         Text            =   "Text1"
         Top             =   2280
         Width           =   1335
      End
      Begin VB.TextBox txtCobrMinCuotas 
         Height          =   285
         Left            =   120
         TabIndex        =   51
         Text            =   "Text1"
         Top             =   2280
         Width           =   1335
      End
      Begin VB.TextBox txtCobrValorCobro 
         Height          =   285
         Left            =   3240
         TabIndex        =   48
         Text            =   "Text1"
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox txtCobrCantduracion 
         Height          =   285
         Left            =   1680
         TabIndex        =   47
         Text            =   "Text1"
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox txtCobrCantGracia 
         Height          =   285
         Left            =   120
         TabIndex        =   46
         Text            =   "Text1"
         Top             =   1680
         Width           =   1335
      End
      Begin VB.ComboBox cbxCobrCodigoOrigen 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   42
         Top             =   1080
         Width           =   4815
      End
      Begin VB.ComboBox cbxCobrTipoOrigen 
         Height          =   315
         Left            =   5040
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   1080
         Width           =   1455
      End
      Begin VB.ComboBox cbxCobrTipoCobro 
         Height          =   315
         Left            =   5040
         Style           =   2  'Dropdown List
         TabIndex        =   38
         Top             =   480
         Width           =   1455
      End
      Begin VB.ComboBox cbxCobros 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   35
         Top             =   480
         Width           =   4815
      End
      Begin MSFlexGridLib.MSFlexGrid grdPromCobros 
         Height          =   2655
         Left            =   120
         TabIndex        =   34
         Top             =   3000
         Width           =   6375
         _ExtentX        =   11245
         _ExtentY        =   4683
         _Version        =   393216
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Label Label17 
         Caption         =   "Max Cuotas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1680
         TabIndex        =   50
         Top             =   2040
         Width           =   1455
      End
      Begin VB.Label Label16 
         Caption         =   "Min. Cuotas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   2040
         Width           =   1455
      End
      Begin VB.Label Label15 
         Caption         =   "Valor Cobro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   3240
         TabIndex        =   45
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label Label14 
         Caption         =   "Cant Duraci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1680
         TabIndex        =   44
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label Label13 
         Caption         =   "Cant. Gracia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label Label12 
         Caption         =   "Codigo Origen"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label Label11 
         Caption         =   "Tipo Origen"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   5040
         TabIndex        =   39
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label Label10 
         Caption         =   "Tipo Cobro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   5040
         TabIndex        =   37
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label9 
         Caption         =   "Cobro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame fraProductos 
      Caption         =   "Productos y Equipos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5775
      Left            =   5400
      TabIndex        =   20
      Top             =   3120
      Width           =   5535
      Begin VB.CheckBox chkPromEquiSinFechaFin 
         Caption         =   "Sin Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1920
         TabIndex        =   72
         Top             =   3000
         Width           =   1575
      End
      Begin MSComCtl2.DTPicker dtPromEquiFechaIni 
         Height          =   255
         Left            =   120
         TabIndex        =   70
         Top             =   2640
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   450
         _Version        =   393216
         Format          =   90243073
         CurrentDate     =   39241
      End
      Begin VB.CommandButton cmdProdLimpiar 
         Caption         =   "Limpiar"
         Height          =   255
         Left            =   1440
         TabIndex        =   56
         Top             =   3285
         Width           =   1095
      End
      Begin VB.CommandButton cmdProdEliminar 
         Caption         =   "Eliminar"
         Height          =   255
         Left            =   2760
         TabIndex        =   33
         Tag             =   "PE_ELIMINAR"
         Top             =   3285
         Width           =   975
      End
      Begin VB.CommandButton cmdProdAgregar 
         Caption         =   "Agregar"
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Tag             =   "PE_AGREGAR"
         Top             =   3285
         Width           =   1095
      End
      Begin VB.TextBox txtProdCantPermanencia 
         Height          =   285
         Left            =   3000
         TabIndex        =   31
         Text            =   "Text1"
         Top             =   1920
         Width           =   2055
      End
      Begin VB.ComboBox cbxProdTipoRelacion 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   30
         Top             =   1920
         Width           =   2535
      End
      Begin MSFlexGridLib.MSFlexGrid grdPromEquipos 
         Height          =   2055
         Left            =   120
         TabIndex        =   27
         Top             =   3600
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   3625
         _Version        =   393216
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.CommandButton cmdProdDesasociar 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Desasociar Producto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1080
         Style           =   1  'Graphical
         TabIndex        =   26
         Tag             =   "PE_DESASOCIAR"
         Top             =   240
         Width           =   2655
      End
      Begin VB.ComboBox cbxEquipo 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   1320
         Width           =   4935
      End
      Begin VB.ComboBox cbxProductos 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   720
         Width           =   4935
      End
      Begin MSComCtl2.DTPicker dtPromEquiFechaFin 
         Height          =   255
         Left            =   1920
         TabIndex        =   71
         Top             =   2640
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   450
         _Version        =   393216
         Format          =   90243073
         CurrentDate     =   39241
      End
      Begin VB.Label Label21 
         Caption         =   "Fecha T�rmino"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1920
         TabIndex        =   69
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label Label20 
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   68
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label Label8 
         Caption         =   "Cant Permanencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   3000
         TabIndex        =   29
         Top             =   1680
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "Tipo Relaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label Label5 
         Caption         =   "Equipo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   480
         Width           =   1455
      End
   End
   Begin VB.Frame fraLocalidades 
      Caption         =   "Localidades en Venta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2295
      Left            =   0
      TabIndex        =   14
      Top             =   3120
      Width           =   5295
      Begin VB.ComboBox cbxLocalidad 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   240
         Width           =   3495
      End
      Begin VB.CommandButton cmdEliminaLocalidad 
         Caption         =   "Eliminar"
         Height          =   255
         Left            =   2280
         TabIndex        =   16
         Tag             =   "LO_ELIMINAR"
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdAgregaLocalidad 
         Caption         =   "Agregar"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Tag             =   "LO_AGREGAR"
         Top             =   600
         Width           =   1215
      End
      Begin MSFlexGridLib.MSFlexGrid grdPromLocalidades 
         Height          =   1215
         Left            =   120
         TabIndex        =   17
         Top             =   960
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   2143
         _Version        =   393216
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Label Label6 
         Caption         =   "Localidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame fraPromociones 
      Caption         =   "Promociones Existentes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3015
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   17535
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar Promoci�n"
         Height          =   375
         Left            =   10320
         TabIndex        =   75
         Top             =   2520
         Width           =   1695
      End
      Begin VB.CheckBox chkPromNoVigentes 
         Caption         =   "Promociones No Vigentes"
         Height          =   255
         Left            =   5400
         TabIndex        =   74
         Top             =   0
         Width           =   2295
      End
      Begin VB.CheckBox chkPromVigentes 
         Caption         =   "Promociones Vigentes"
         Height          =   255
         Left            =   2880
         TabIndex        =   73
         Top             =   0
         Width           =   2295
      End
      Begin VB.CommandButton cmdEliminarPromocion 
         Caption         =   "Eliminar Promoci�n"
         Height          =   375
         Left            =   8400
         TabIndex        =   67
         Tag             =   "PR_ELIMINAR"
         Top             =   2520
         Width           =   1695
      End
      Begin VB.CommandButton cmdCopiaPromocion 
         Caption         =   "Copiar Promoci�n"
         Height          =   375
         Left            =   6600
         TabIndex        =   66
         Tag             =   "PR_COPIAR"
         Top             =   2520
         Width           =   1575
      End
      Begin VB.CommandButton cmdNuevaPromocion 
         Caption         =   "Nueva Promoci�n"
         Height          =   375
         Left            =   2040
         TabIndex        =   7
         Tag             =   "PR_NUEVA"
         Top             =   2520
         Width           =   1935
      End
      Begin VB.CommandButton cmdModificarPromocion 
         Caption         =   "Modificar Promoci�n"
         Height          =   375
         Left            =   4320
         TabIndex        =   6
         Tag             =   "PR_MODIFICAR"
         Top             =   2520
         Width           =   1935
      End
      Begin VB.TextBox txtCodiPromocion 
         Enabled         =   0   'False
         Height          =   285
         Left            =   12480
         TabIndex        =   5
         Text            =   "Text1"
         Top             =   1920
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox txtDescPromocion 
         Height          =   285
         Left            =   120
         MaxLength       =   100
         TabIndex        =   4
         Text            =   "Text1"
         Top             =   2040
         Width           =   8175
      End
      Begin VB.CheckBox chkSinFechaFin 
         Caption         =   "Sin Fecha de T�rmino"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   10200
         TabIndex        =   3
         Top             =   2280
         Value           =   1  'Checked
         Width           =   2415
      End
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "Limpiar"
         Height          =   375
         Left            =   240
         TabIndex        =   2
         Top             =   2520
         Width           =   1560
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "Volver"
         Height          =   375
         Left            =   12240
         TabIndex        =   1
         Top             =   2520
         Width           =   975
      End
      Begin MSFlexGridLib.MSFlexGrid grdPromociones 
         Height          =   1455
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   14175
         _ExtentX        =   25003
         _ExtentY        =   2566
         _Version        =   393216
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin MSComCtl2.DTPicker dtFechaFin 
         Height          =   255
         Left            =   10200
         TabIndex        =   9
         Top             =   2040
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   450
         _Version        =   393216
         Format          =   90243073
         CurrentDate     =   38915
      End
      Begin MSComCtl2.DTPicker dtFechaInicio 
         Height          =   255
         Left            =   8400
         TabIndex        =   12
         Top             =   2040
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   450
         _Version        =   393216
         Format          =   90243073
         CurrentDate     =   38915
      End
      Begin VB.Label Label3 
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   8400
         TabIndex        =   13
         Top             =   1800
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "Descripci�n de la Promoci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1800
         Width           =   3135
      End
      Begin VB.Label Label4 
         Caption         =   "Fecha Fin"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   10200
         TabIndex        =   10
         Top             =   1800
         Width           =   1575
      End
   End
End
Attribute VB_Name = "frmPromociones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cbxCobros_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Long
        
    If KeyCode = vbKeyF5 Then
        frmBuscador.txtFiltro.Text = "R"
        frmBuscador.Show vbModal
        
        For i = 0 To UBound(arrCodiRentas)
            If arrCodiRentas(i) = frmBuscador.txtCodigo.Text Then
                cbxCobros.ListIndex = i + 1
             End If
        Next i
    End If
End Sub

Private Sub cbxCobrTipoOrigen_Click()
    Call suLlenaCodigoOrigen
End Sub

Private Sub cbxLocalidad_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Long
        
    If KeyCode = vbKeyF5 Then
        frmBuscador.txtFiltro.Text = "L"
        frmBuscador.Show vbModal
        
        For i = 0 To UBound(arrCodiLocalidades)
            If arrCodiLocalidades(i) = frmBuscador.txtCodigo.Text Then
                cbxLocalidad.ListIndex = i + 1
             End If
        Next i
    End If
End Sub

Private Sub chkPromEquiSinFechaFin_Click()
    If chkPromEquiSinFechaFin.Value = vbChecked Then
        dtPromEquiFechaFin.Enabled = False
    End If
    If chkPromEquiSinFechaFin.Value = vbUnchecked Then
        dtPromEquiFechaFin.Enabled = True
    End If
End Sub

Private Sub chkPromNoVigentes_Click()
    Call suLlenaPromociones
    Call cmdLimpiar_Click

End Sub

Private Sub chkPromVigentes_Click()
    Call suLlenaPromociones
    Call cmdLimpiar_Click
End Sub

Private Sub chkSinFechaFin_Click()
    If chkSinFechaFin.Value = vbChecked Then
        dtFechaFin.Enabled = False
    End If
    If chkSinFechaFin.Value = vbUnchecked Then
        dtFechaFin.Enabled = True
    End If
End Sub

Private Sub cmdAgregaLocalidad_Click()
    Dim i As Long
    
    If cbxLocalidad.ListIndex > 0 Then
        With grdPromLocalidades
            For i = 1 To .Rows - 1
                If arrCodiLocalidades(cbxLocalidad.ListIndex - 1) = .TextMatrix(i, 1) Then
                    MsgBox "Esta localidad ya esta ingresada"
                    Exit Sub
                End If
            Next i
        End With
        
        If InsPromLoca(CLng(txtCodiPromocion.Text), _
                       arrCodiLocalidades(cbxLocalidad.ListIndex - 1)) Then
            Call suTraeDatosPromocion
        End If
    Else
        MsgBox "Debe seleccionar una localidad"
    End If
End Sub

Private Sub cmdBuscar_Click()
    Dim i As Long, j As Long
    
    Load frmBuscaPromocion
    With frmBuscaPromocion
        For i = 1 To grdPromociones.Rows - 1
            For j = 0 To grdPromociones.Cols - 1
                .grdOrig.Rows = .grdOrig.Rows + 1
                .grdPromociones.Rows = .grdPromociones.Rows + 1
                .grdOrig.TextMatrix(i, j) = grdPromociones.TextMatrix(i, j)
                .grdPromociones.TextMatrix(i, j) = grdPromociones.TextMatrix(i, j)
            Next j
            .grdOrig.TextMatrix(i, 5) = i
            .grdPromociones.TextMatrix(i, 5) = i
        Next i
    End With
    
    giCualPlanSeleccionado = 0
    MousePointer = vbDefault
    frmBuscaPromocion.Show vbModal
    If giCualPromocionSeleccionada > 0 Then
        grdPromociones.TopRow = giCualPromocionSeleccionada
        grdPromociones.Row = giCualPromocionSeleccionada
        grdPromociones.RowSel = giCualPromocionSeleccionada
        grdPromociones.Col = 0
        grdPromociones.ColSel = grdPlanes.Cols - 1
        Call grdPromociones_Click
    End If
End Sub

Private Sub cmdCobrosAgregar_Click()
    Dim Texto As String, tmp As Variant
    Dim sCodiOrigen As String
    Dim i As Long
    
    If cbxCobros.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el cobro"
        Exit Sub
    End If

    If cbxCobrTipoCobro.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el Tipo de Cobro"
        Exit Sub
    End If

    If cbxCobrTipoOrigen.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el Tipo de Origen"
        Exit Sub
    End If
    
    If txtCobrCantGracia.Text <> "" Then
        If Not IsNumeric(txtCobrCantGracia.Text) Then
            MsgBox "La Gracia debe ser numerico o vacio"
            Exit Sub
        End If
    End If

    If txtCobrCantduracion.Text <> "" Then
        If Not IsNumeric(txtCobrCantduracion.Text) Then
            MsgBox "La Duracion debe ser numerico o vacio"
            Exit Sub
        End If
    End If

    If txtCobrValorCobro.Text <> "" Then
        If Not IsNumeric(txtCobrValorCobro.Text) Then
            MsgBox "el valor debe ser numerico o vacio"
            Exit Sub
        End If
    End If

    If txtCobrMinCuotas.Text <> "" Then
        If Not IsNumeric(txtCobrMinCuotas.Text) Then
            MsgBox "La Cantidad m�nima de cuotas debe ser numerico o vacio"
            Exit Sub
        End If
    End If
    If txtCobrMaxCuotas.Text <> "" Then
        If Not IsNumeric(txtCobrMaxCuotas.Text) Then
            MsgBox "La Cantidad m�nima de cuotas debe ser numerico o vacio"
            Exit Sub
        End If
    End If
    
    sCodiOrigen = ""
    If cbxCobrCodigoOrigen.Text <> "NADA" Then
        Select Case cbxCobrTipoOrigen.Text
            Case "EQUIPO"
                Texto = cbxCobrCodigoOrigen.Text
                tmp = Split(Texto, "(")
                sCodiOrigen = Replace(tmp(1), ")", "")
            Case "PLAN"
                Texto = cbxCobrCodigoOrigen.Text
                tmp = Split(Texto, " -- ")
                sCodiOrigen = Trim(tmp(0))
            Case "PRODUCTO"
                Texto = cbxCobrCodigoOrigen.Text
                tmp = Split(Texto, " -- ")
                sCodiOrigen = Trim(tmp(0))
        End Select
    End If

    If cmdCobrosAgregar.Caption = "Agregar" Then
        
        ' revisando que la renta seleccionada no este ingresada nuevamente
        With grdPromCobros
            For i = 1 To .Rows - 1
                If .TextMatrix(i, 9) = arrCodiRentas(cbxCobros.ListIndex - 1) Then
                    MsgBox "La renta seleccionada ya se encuentra asignada"
                    Exit Sub
                End If
            Next i
        End With
        
        If InsPromCobro(CLng(txtCodiPromocion.Text), _
                        arrCodiRentas(cbxCobros.ListIndex - 1), _
                        Left(cbxCobrTipoCobro.Text, 1), _
                        Left(cbxCobrTipoOrigen.Text, 4), _
                        IIf(cbxCobrCodigoOrigen.Text = "NADA", "", sCodiOrigen), _
                        IIf(Trim(txtCobrCantGracia.Text) = "", -1, Val(txtCobrCantGracia.Text)), _
                        IIf(Trim(txtCobrCantduracion.Text) = "", -1, Val(txtCobrCantduracion.Text)), _
                        IIf(Trim(txtCobrMinCuotas.Text) = "", -1, Val(txtCobrMinCuotas.Text)), _
                        IIf(Trim(txtCobrMaxCuotas.Text) = "", -1, Val(txtCobrMaxCuotas.Text)), _
                        IIf(Trim(txtCobrValorCobro.Text) = "", -1, Val(txtCobrValorCobro.Text))) Then
            Call suTraeDatosPromocion
        End If
    End If

    If cmdCobrosAgregar.Caption = "Modificar" Then
        If UpdPromCobro(CLng(txtCodiPromocion.Text), _
                    arrCodiRentas(cbxCobros.ListIndex - 1), _
                    Left(cbxCobrTipoCobro.Text, 1), _
                    Left(cbxCobrTipoOrigen.Text, 4), _
                    IIf(cbxCobrCodigoOrigen.Text = "NADA", "", sCodiOrigen), _
                    IIf(txtCobrCantGracia.Text = "", -1, Val(txtCobrCantGracia.Text)), _
                    IIf(txtCobrCantduracion.Text = "", -1, Val(txtCobrCantduracion.Text)), _
                    IIf(txtCobrMinCuotas.Text = "", -1, Val(txtCobrMinCuotas.Text)), _
                    IIf(txtCobrMaxCuotas.Text = "", -1, Val(txtCobrMaxCuotas.Text)), _
                    IIf(txtCobrValorCobro.Text = "", -1, Val(txtCobrValorCobro.Text))) Then
            Call suTraeDatosPromocion
        End If
    End If
    
    Call cmdCobrosLimpiar_Click
End Sub

Private Sub cmdCobrosEliminar_Click()
    If MsgBox("� Esta seguro de eliminar el cobro: " & cbxCobros.Text & "?", vbYesNo) = vbYes Then
        If DelPromCobro(CLng(Me.txtCodiPromocion.Text), _
                     arrCodiRentas(Me.cbxCobros.ListIndex - 1)) Then
            Call suTraeDatosPromocion
            Call cmdCobrosLimpiar_Click
        End If
    End If
End Sub

Private Sub cmdCobrosLimpiar_Click()
    cbxCobros.Enabled = True
    cbxCobros.ListIndex = 0
    
    cbxCobrTipoCobro.ListIndex = 0
    cbxCobrTipoOrigen.ListIndex = 0
    cbxCobrCodigoOrigen.ListIndex = 0
    txtCobrCantGracia.Text = ""
    txtCobrCantduracion.Text = ""
    txtCobrValorCobro.Text = ""
    txtCobrMinCuotas.Text = ""
    txtCobrMaxCuotas.Text = ""
    cmdCobrosAgregar.Caption = "Agregar"
    cmdCobrosEliminar.Enabled = False
    grdPromCobros.Row = 0
End Sub

Private Sub cmdcopiaPromocion_Click()
    
    If MsgBox("�Esta seguro de copiar la promoci�n: " & Me.txtDescPromocion.Text & "?", vbYesNo) = vbYes Then
        
        If CopiaPromo(CLng(Me.txtCodiPromocion.Text)) Then
            Call suLlenaPromociones
            Call cmdLimpiar_Click
            
        End If
    End If
End Sub

Private Sub cmdEliminaLocalidad_Click()
    Dim i As Long, iCant As Long

    With grdPromLocalidades
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 2) = "1" Then
                iCant = iCant + 1
            End If
        Next i
    End With

    With grdPromLocalidades
        If .Row > 0 Then
            If MsgBox("�Esta seguro de eliminar las " & Trim(Str(iCant)) & " localidades seleccionadas?", vbYesNo) = vbYes Then
                
                For i = 1 To .Rows - 1
                    If .TextMatrix(i, 2) = "1" Then
                        Call DelPromLoca(CLng(txtCodiPromocion.Text), _
                                       .TextMatrix(i, 1))
                    End If
                Next i
                Call suTraeDatosPromocion
            End If
        End If
    End With
End Sub

Private Sub cmdEliminarPromocion_Click()
    If MsgBox("� Esta seguro de eliminar la promocion: " & txtDescPromocion.Text & "?" & vbCrLf & "Esto no podr� ser deshecho", vbYesNo) = vbYes Then
        Call DelPromo(CLng(txtCodiPromocion.Text))
        Call suInicializaPromociones
        Call suLlenaPromociones
        Call suTraeDatosPromocion
        grdPromociones.Row = 0
    End If
End Sub

Private Sub cmdLimpiar_Click()
    Call suInicializaPromociones
    Call suTraeDatosPromocion
    
    'Me.txtDescPromocion.SetFocus
End Sub

Private Sub suTraeDatosPromocion()
    Dim sCodiLocalidad As String, sDescLocalidad As String
    Dim sCodiProducto As String
    Dim sDescModelo As String, sCodiEquipo As String
    Dim sTipoRelacion As String
    Dim sCodiCobro As String, sDescConcepto As String, sTipoCobro As String
    Dim sTipoOrigen As String, sCodiOrigen As String
    Dim sCantgracia As String, sCantDuracion As String, sMinCuotas As String
    Dim sMaxCuotas As String, sValorCobro As String
    Dim sCodiPlan As String, sDescPlan As String, sCantPermanencia As String
    Dim i As Long, sFechaInicio As String, sFechaTermino As String
    
    grdPromLocalidades.Rows = 1
    grdPromEquipos.Rows = 1
    grdPromCobros.Rows = 1
    grdPlanes.Rows = 1
    
    cbxCobros.Enabled = True
    cbxProductos.Enabled = True
    cbxEquipo.Enabled = True
    
    cmdProdAgregar.Enabled = False
    cmdProdLimpiar.Enabled = False
    cmdProdEliminar.Enabled = False
    
    If GetPromLoca(CLng(txtCodiPromocion.Text)) > 0 Then
        With grdPromLocalidades
            .Rows = 1
            Do While GetPromLocaRead(sCodiLocalidad, sDescLocalidad)
                .Rows = .Rows + 1
                
                .TextMatrix(.Rows - 1, 0) = sDescLocalidad
                .TextMatrix(.Rows - 1, 1) = sCodiLocalidad
                .TextMatrix(.Rows - 1, 2) = "0"
            Loop
        End With
    End If
    
    If GetPromProd(CLng(txtCodiPromocion.Text)) > 0 Then
        
        Call GetPromProdRead(sCodiProducto)
        
        For i = 0 To UBound(arrProductos)
            If arrCodiProductos(i) = sCodiProducto Then
                cbxProductos.ListIndex = i + 1
                Exit For
            End If
        Next i
            
        If GetProdEquip(sCodiProducto) > 0 Then
            cbxEquipo.Clear
            cbxEquipo.AddItem "SELECCIONE"
            Do While GetProdEquipRead(sDescModelo, vbNull, vbNull, sCodiEquipo)
                cbxEquipo.AddItem sDescModelo & " (" & sCodiEquipo & ")"
            Loop
            cbxEquipo.ListIndex = 0
        End If
        
        If GetPromEqui(CLng(txtCodiPromocion.Text)) > 0 Then
            With grdPromEquipos
                .Rows = 1
                Do While GetPromEquiRead(sCodiEquipo, sDescModelo, sTipoRelacion, sCantPermanencia, sFechaInicio, sFechaTermino)
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, 0) = sDescModelo
                    .TextMatrix(.Rows - 1, 1) = sTipoRelacion
                    .TextMatrix(.Rows - 1, 2) = sCantPermanencia
                    .TextMatrix(.Rows - 1, 3) = sCodiEquipo
                    .TextMatrix(.Rows - 1, 4) = sFechaInicio
                    .TextMatrix(.Rows - 1, 5) = sFechaTermino
                Loop
            End With
        End If

        cbxProductos.Enabled = False
        
        Call suEnable_Ctl1(Me.Tag, cmdProdAgregar)
        Call suEnable_Ctl1(Me.Tag, cmdProdLimpiar)
        Call suEnable_Ctl1(Me.Tag, cmdProdEliminar)
        
        cmdProdDesasociar.Caption = "Desasociar Producto"
    Else
        cbxProductos.Enabled = True
        cmdProdDesasociar.Caption = "Asociar Producto"
    End If
    
    If GetPromCobr(CLng(txtCodiPromocion.Text)) > 0 Then
        With grdPromCobros
            .Rows = 1
            Do While GetPromCobrRead(sCodiCobro, sDescConcepto, sTipoCobro, _
                                     sTipoOrigen, sCodiOrigen, sCantgracia, _
                                     sCantDuracion, sMinCuotas, sMaxCuotas, _
                                     sValorCobro)
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, 0) = sDescConcepto
                .TextMatrix(.Rows - 1, 1) = sTipoCobro
                .TextMatrix(.Rows - 1, 2) = sTipoOrigen
                .TextMatrix(.Rows - 1, 3) = sCodiOrigen
                .TextMatrix(.Rows - 1, 4) = sCantgracia
                .TextMatrix(.Rows - 1, 5) = sCantDuracion
                .TextMatrix(.Rows - 1, 6) = sMinCuotas
                .TextMatrix(.Rows - 1, 7) = sMaxCuotas
                .TextMatrix(.Rows - 1, 8) = sValorCobro
                .TextMatrix(.Rows - 1, 9) = sCodiCobro
            Loop
        End With
    End If
    
    If GetPromPlan(CLng(txtCodiPromocion.Text)) > 0 Then
        With grdPlanes
            .Rows = 1
            Do While GetPromPlanRead(sCodiPlan, sDescPlan, sCantPermanencia)
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, 0) = sDescPlan
                .TextMatrix(.Rows - 1, 1) = sCantPermanencia
                .TextMatrix(.Rows - 1, 2) = sCodiPlan
            Loop
        End With
    End If
    
End Sub

Private Sub cmdModificar_Click()

End Sub

Private Sub cmdModificarPromocion_Click()
    If fnValidaDatos Then
        If UpdPromocion(CLng(Me.txtCodiPromocion.Text), _
                        Me.txtDescPromocion.Text, _
                        Format(dtFechaInicio.Value, "dd-mm-yyyy"), _
                        IIf(Me.chkSinFechaFin.Value = 1, "NADA", Format(dtFechaFin.Value, "dd-mm-yyyy"))) Then
            Call suLlenaPromociones
            Call cmdLimpiar_Click
        Else
            MsgBox "No se ha podido modificar la promocion"
        End If
    End If
End Sub

Private Sub cmdNuevaPromocion_Click()
    If fnValidaDatos Then
        If InsPromocion(Me.txtDescPromocion.Text, _
                        Format(dtFechaInicio.Value, "dd-mm-yyyy"), _
                        IIf(Me.chkSinFechaFin.Value = 1, "NADA", Format(dtFechaFin.Value, "dd-mm-yyyy"))) Then
            Call suLlenaPromociones
        Else
            MsgBox "No se ha podido ingresar la promocion"
        End If
                        
    End If
End Sub

Private Function fnValidaDatos() As Boolean
    
    fnValidaDatos = True
    
    If txtCodiPromocion.Text = "" Then
        MsgBox "Debe ingresar el codigo de la promoci�n"
        fnValidaDatos = False
        Exit Function
    End If
    
    If txtDescPromocion.Text = "" Then
        MsgBox "Debe ingresar la descripci�n de la promoci�n"
        fnValidaDatos = False
        Exit Function
    End If
    
    If chkSinFechaFin.Value = 0 Then
        If dtFechaFin.Value < dtFechaInicio.Value Then
            MsgBox "No puede poner la fecha de fin antes que la fecha de inicio"
            fnValidaDatos = False
            Exit Function
        End If
    End If
End Function

Private Sub cmdPlanAgregar_Click()
    Dim i As Integer

    If cbxPlanes.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el plan"
        Exit Sub
    End If
    
    If txtPlanCantPerm.Text <> "" Then
        If Not IsNumeric(txtPlanCantPerm.Text) Then
            MsgBox "La cantidad de permanencia debe ser vacio o un numero"
        End If
    End If
    
    If cmdPlanAgregar.Caption = "Agregar" Then
    
        With grdPlanes
            For i = 1 To .Rows - 1
                If .TextMatrix(i, 2) = arrCodiPlanes(cbxPlanes.ListIndex - 1) Then
                    MsgBox "Este plan ya esta asociado"
                    Exit Sub
                End If
            Next i
        End With
    
        If InsPromPlan(CLng(txtCodiPromocion.Text), _
                       arrCodiPlanes(cbxPlanes.ListIndex - 1), _
                       IIf(txtPlanCantPerm.Text = "", -1, Val(txtPlanCantPerm.Text))) Then
            Call suTraeDatosPromocion
        End If
    End If

    If cmdPlanAgregar.Caption = "Modificar" Then
        If UpdPromPlan(CLng(txtCodiPromocion.Text), _
                       arrCodiPlanes(cbxPlanes.ListIndex - 1), _
                       IIf(txtPlanCantPerm.Text = "", -1, Val(txtPlanCantPerm.Text))) Then
            Call suTraeDatosPromocion
        End If
    End If

    Call cmdPlanLimpiar_Click
End Sub

Private Sub cmdPlanEliminar_Click()
    If MsgBox("�Esta seguro de eliminar el plan:" & cbxPlanes.Text & "?", vbYesNo) = vbYes Then
        If DelPromPlan(CLng(txtCodiPromocion.Text), _
                       grdPlanes.TextMatrix(grdPlanes.Row, 2)) Then
            Call cmdPlanLimpiar_Click
            Call suTraeDatosPromocion
        End If
    End If
End Sub

Private Sub cmdPlanLimpiar_Click()
    cbxPlanes.ListIndex = 0
    cbxPlanes.Enabled = True
    txtPlanCantPerm.Text = ""
    cmdPlanAgregar.Caption = "Agregar"
    grdPlanes.Row = 0
End Sub

Private Sub cmdProdAgregar_Click()
    Dim Texto As String, tmp As Variant
    Dim sCodiEqui As String
    Dim i As Long
    
    If cbxEquipo.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el equipo"
        Exit Sub
    End If
    
    If cbxProdTipoRelacion.Text = "SELECCIONE" Then
        MsgBox "Debe seleccionar el tipo de relacion"
        Exit Sub
    End If
    
    If txtProdCantPermanencia.Text <> "" Then
        If Not IsNumeric(txtProdCantPermanencia.Text) Then
            MsgBox "La cantidad de permanencia debe ser numerica o vacio"
            Exit Sub
        End If
    End If
    
    If chkPromEquiSinFechaFin.Value = 0 Then
        If dtPromEquiFechaFin.Value < dtPromEquiFechaIni.Value Then
            MsgBox "No puede poner la fecha de fin antes que la fecha de inicio"
            Exit Sub
        End If
    End If
    
    Texto = cbxEquipo.Text
    tmp = Split(Texto, "(")
    sCodiEqui = Replace(tmp(1), ")", "")
    
    If cmdProdAgregar.Caption = "Agregar" Then
    With grdPromEquipos
        For i = 1 To .Rows - 1
            If sCodiEqui = .TextMatrix(i, 3) Then
                MsgBox "Este equipo ya est asociado"
                Exit Sub
            End If
        Next i
    End With
    End If
    
    If cmdProdAgregar.Caption = "Agregar" Then
        If InsPromEqui(CLng(Me.txtCodiPromocion.Text), _
                       arrCodiProductos(Me.cbxProductos.ListIndex - 1), _
                       sCodiEqui, _
                       Left(Me.cbxProdTipoRelacion.Text, 4), _
                       IIf(txtProdCantPermanencia.Text = "", -1, Val(txtProdCantPermanencia.Text)), _
                       Format(dtPromEquiFechaIni.Value, "dd-mm-yyyy"), _
                       IIf(chkPromEquiSinFechaFin.Value = 1, "NADA", Format(dtPromEquiFechaFin.Value, "dd-mm-yyyy"))) Then
            Call suTraeDatosPromocion
        End If
    End If

    If cmdProdAgregar.Caption = "Modificar" Then
        If UpdPromEqui(CLng(Me.txtCodiPromocion.Text), _
                       arrCodiProductos(Me.cbxProductos.ListIndex - 1), _
                       sCodiEqui, _
                       Left(Me.cbxProdTipoRelacion.Text, 4), _
                       IIf(txtProdCantPermanencia.Text = "", -1, Val(txtProdCantPermanencia.Text)), _
                       Format(dtPromEquiFechaIni.Value, "dd-mm-yyyy"), _
                       IIf(chkPromEquiSinFechaFin.Value = 1, "NADA", Format(dtPromEquiFechaFin.Value, "dd-mm-yyyy"))) Then
            Call suTraeDatosPromocion
        End If
    End If

    Call cmdProdLimpiar_Click
End Sub

Private Sub cmdProdDesasociar_Click()
    Dim bRes As Boolean
    Dim sAgrega As String
    
    If cmdProdDesasociar.Caption = "Asociar Producto" Then
        bRes = True
        sAgrega = "S"
    Else
        If MsgBox("Esta operacion eliminar� las relaciones de la promocion con los equipos!!!" & vbCrLf & "�Esta seguro de Continuar?", vbExclamation + vbYesNo) = vbYes Then
            bRes = True
            sAgrega = "N"
        Else
            bRes = False
        End If
    End If
    If UpdPromProd(CLng(txtCodiPromocion.Text), _
                   arrCodiProductos(cbxProductos.ListIndex - 1), _
                   sAgrega) Then
        Call cmdProdLimpiar_Click
        Call suTraeDatosPromocion
    End If
    
End Sub

Private Sub cmdProdEliminar_Click()
    Dim Texto As String, tmp As Variant
    Dim sCodiEqui As String
    
    
    If MsgBox("� Esta seguro de eliminar el equipo: " & cbxEquipo.Text & "?", vbYesNo) = vbYes Then
        Texto = cbxEquipo.Text
        tmp = Split(Texto, "(")
        sCodiEqui = Replace(tmp(1), ")", "")
        If DelPromEqui(CLng(Me.txtCodiPromocion.Text), _
                       arrCodiProductos(Me.cbxProductos.ListIndex - 1), _
                       sCodiEqui) Then
            Call suTraeDatosPromocion
        End If
    End If
End Sub

Private Sub cmdProdLimpiar_Click()
    cbxEquipo.ListIndex = 0
    cbxProdTipoRelacion.ListIndex = 0
    txtProdCantPermanencia.Text = ""
    cbxEquipo.Enabled = True
    cmdProdAgregar.Caption = "Agregar"
    chkPromEquiSinFechaFin.Value = 1
    dtPromEquiFechaFin.Value = Date
    dtPromEquiFechaIni.Value = Date
    grdPromEquipos.Row = 0
End Sub

Private Sub cmdVolver_Click()
    Unload Me
End Sub

Private Sub suInicializaForm()
    
    With grdPromociones
        .Cols = 5
        .Rows = 2
        
        
        .TextMatrix(0, 0) = "C�digo"
        .TextMatrix(0, 1) = "Descripci�n"
        .TextMatrix(0, 2) = "Fecha Creaci�n"
        .TextMatrix(0, 3) = "Fecha Inicio"
        .TextMatrix(0, 4) = "Fecha T�rmino"
        
        .ColWidth(0) = 1000
        .ColWidth(1) = 7500
        .ColWidth(2) = 1300
        .ColWidth(3) = 1300
        .ColWidth(4) = 1300
        
        .FixedCols = 0
        .FixedRows = 1
    End With
    
    With grdPromCobros
        .Cols = 10
        .Rows = 2
        
        .TextMatrix(0, 0) = "Cobro"
        .TextMatrix(0, 1) = "Tipo Cobro"
        .TextMatrix(0, 2) = "Tipo Origen"
        .TextMatrix(0, 3) = "Codi Origen"
        .TextMatrix(0, 4) = "Gracia"
        .TextMatrix(0, 5) = "Duracion"
        .TextMatrix(0, 6) = "Mi Cuotas"
        .TextMatrix(0, 7) = "Max Cuotas"
        .TextMatrix(0, 8) = "Valor"
        .TextMatrix(0, 9) = "Codigo Cobro"
        .ColWidth(0) = 4000
        .ColWidth(9) = 0
        
        .FixedRows = 1
        .FixedCols = 0
    End With
    
    With grdPlanes
        .Cols = 3
        .Rows = 2
        
        .TextMatrix(0, 0) = "Plan"
        .TextMatrix(0, 1) = "Cant Perm."
        .TextMatrix(0, 2) = "CodiPlan"
                
        .ColWidth(0) = 3000
        .ColWidth(2) = 0
        
        .FixedCols = 0
        .FixedRows = 1
    End With
    
    With grdPromEquipos
        .Rows = 2
        .Cols = 6
        
        .TextMatrix(0, 0) = "Equipo"
        .TextMatrix(0, 1) = "Relacion"
        .TextMatrix(0, 2) = "Perm."
        .TextMatrix(0, 3) = "CodiEquipo"
        .TextMatrix(0, 4) = "Fecha Inicio"
        .TextMatrix(0, 5) = "Fecha T�rmino"
        
        .ColWidth(0) = 3000
        .ColWidth(3) = 0
        
        .FixedCols = 0
        .FixedRows = 1
    End With
    
    With grdPromLocalidades
        .Rows = 2
        .Cols = 3
        
        .TextMatrix(0, 0) = "Localidad"
        .TextMatrix(0, 1) = "CodiLocalida"
        .TextMatrix(0, 2) = "Marca"
        
        .ColWidth(0) = 2000
        .ColWidth(1) = 0
        .ColWidth(2) = 0
        
        .FixedRows = 1
        .FixedCols = 0
    End With
    
    
    
    Call suLlenaLocalidades
    Call suLlenaProductos
    Call suLlenaEquipos
    Call suLlenaCobros
    Call suLlenaTipoRelacion
    Call suLlenaTipoOrigen
    Call suLlenaTipoCobro
    Call suLlenaCodigoOrigen
    Call suLlenaPlanes
    
End Sub

Private Sub suLlenaPlanes()
Dim i As Long

    cbxPlanes.Clear
    cbxPlanes.AddItem "SELECCIONE"
    For i = 0 To UBound(arrPlanes)
        cbxPlanes.AddItem arrPlanes(i)
    Next i
    cbxPlanes.ListIndex = 0
End Sub

Private Sub suLlenaCodigoOrigen()
    Dim i As Long
    
    cbxCobrCodigoOrigen.Clear
    cbxCobrCodigoOrigen.AddItem "NADA"
    If cbxCobrTipoOrigen.Text <> "SELECCIONE" Then
        Select Case cbxCobrTipoOrigen.Text
            Case "EQUIPO"
                For i = 1 To cbxEquipo.ListCount - 1
                    cbxCobrCodigoOrigen.AddItem cbxEquipo.List(i)
                Next i
            Case "PLAN"
                For i = 1 To grdPlanes.Rows - 1
                    cbxCobrCodigoOrigen.AddItem grdPlanes.TextMatrix(i, 2) & " -- " & grdPlanes.TextMatrix(i, 0)
                Next i
            Case "CARGA INICIAL"
            Case "PRODUCTO"
                cbxCobrCodigoOrigen.AddItem cbxProductos.Text
        End Select
    End If
    cbxCobrCodigoOrigen.ListIndex = 0
End Sub

Private Sub suLlenaTipoCobro()
    cbxCobrTipoCobro.Clear
    cbxCobrTipoCobro.AddItem "SELECCIONE"
    cbxCobrTipoCobro.AddItem "VENTA"
    cbxCobrTipoCobro.AddItem "RENTA"
    cbxCobrTipoCobro.ListIndex = 0

End Sub

Private Sub suLlenaTipoRelacion()
    Dim sDesc As String
    
    cbxProdTipoRelacion.Clear
    cbxProdTipoRelacion.AddItem "SELECCIONE"
    If GetParam("7778") > 0 Then
        Do While GetParamRead(vbNull, sDesc)
            cbxProdTipoRelacion.AddItem sDesc
        Loop
    End If
    cbxProdTipoRelacion.ListIndex = 0
End Sub

Private Sub suLlenaTipoOrigen()
    Dim sDesc As String
    
    cbxCobrTipoOrigen.Clear
    cbxCobrTipoOrigen.AddItem "SELECCIONE"
    If GetParam("7777") > 0 Then
        Do While GetParamRead(vbNull, sDesc)
            cbxCobrTipoOrigen.AddItem sDesc
        Loop
    End If
    cbxCobrTipoOrigen.ListIndex = 0
End Sub

Private Sub suLlenaCobros()
    Dim i As Long
    
    cbxCobros.Clear
    cbxCobros.AddItem "SELECCIONE"
    For i = 0 To UBound(arrRentas)
        cbxCobros.AddItem arrRentas(i)
    Next i
    cbxCobros.ListIndex = 0

End Sub

Private Sub suLlenaEquipos()
    Dim i As Integer
    
    cbxEquipo.Clear
    cbxEquipo.AddItem "SELECCIONE"
    cbxEquipo.ListIndex = 0
End Sub

Private Sub suLlenaProductos()
    Dim i As Long
    
    cbxProductos.Clear
    cbxProductos.AddItem "SELECCIONE"
    For i = 0 To UBound(arrProductos)
        cbxProductos.AddItem arrProductos(i)
    Next i
    cbxProductos.ListIndex = 0
End Sub

Private Sub suLlenaLocalidades()
    Dim i As Integer

    cbxLocalidad.Clear
    cbxLocalidad.AddItem "SELECCIONE"
    For i = 0 To UBound(arrLocalidades)
        cbxLocalidad.AddItem arrLocalidades(i)
    Next i
    
    cbxLocalidad.ListIndex = 0
End Sub


Private Sub suInicializaPromociones()
    txtCodiPromocion.Text = "0"
    txtDescPromocion.Text = ""
    chkSinFechaFin.Value = 1
    chkPromEquiSinFechaFin.Value = 1
    Call chkSinFechaFin_Click
    dtFechaInicio.Enabled = True
    cmdModificarPromocion.Enabled = False
    cmdEliminarPromocion.Enabled = False
    cmdCopiaPromocion.Enabled = False
    
    Call suEnable_Ctl1(Me.Tag, cmdNuevaPromocion)
    
    dtFechaFin.Value = Date
    dtFechaInicio.Value = Date
    dtPromEquiFechaIni.Value = Date
    dtPromEquiFechaFin.Value = Date
    
    fraLocalidades.Enabled = False
    fraPlanes.Enabled = False
    fraProductos.Enabled = False
    fraCobros.Enabled = False
    
    cbxProductos.ListIndex = 0
    cbxEquipo.ListIndex = 0
    cbxProdTipoRelacion.ListIndex = 0
    txtProdCantPermanencia.Text = ""
    cbxCobros.ListIndex = 0
    cbxCobrTipoCobro.ListIndex = 0
    cbxCobrTipoOrigen.ListIndex = 0
    cbxCobrCodigoOrigen.ListIndex = 0
    txtCobrCantduracion.Text = ""
    txtCobrCantGracia.Text = ""
    txtCobrMaxCuotas.Text = ""
    txtCobrMinCuotas.Text = ""
    txtCobrValorCobro.Text = ""
    
    txtPlanCantPerm.Text = ""
    cbxPlanes.ListIndex = 0
    
    cmdPlanAgregar.Caption = "Agregar"
    
    grdPromociones.Row = 0
End Sub

Private Sub suLlenaPromociones()
    Dim lCodiPromocion As Long, sDescPromocion As String
    Dim sFechaCreacion As String, sFechaInicio As String, sFechaFin As String
    Dim ifiltro As Integer
    
    If Me.chkPromVigentes.Value = 1 And Me.chkPromNoVigentes.Value = 1 Then
        ifiltro = 0
    End If
    If Me.chkPromVigentes.Value = 1 And Me.chkPromNoVigentes.Value = 0 Then
        ifiltro = 1
    End If
    If Me.chkPromVigentes.Value = 0 And Me.chkPromNoVigentes.Value = 1 Then
        ifiltro = -1
    End If
    If Me.chkPromVigentes.Value = 0 And Me.chkPromNoVigentes.Value = 0 Then
        ifiltro = -2
    End If
    
    With grdPromociones
        .Rows = 1
        If GetPromociones(ifiltro) > 0 Then
            Do While GetPromocionesRead(lCodiPromocion, sDescPromocion, sFechaCreacion, sFechaInicio, sFechaFin)
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, 0) = lCodiPromocion
                .TextMatrix(.Rows - 1, 1) = sDescPromocion
                .TextMatrix(.Rows - 1, 2) = sFechaCreacion
                .TextMatrix(.Rows - 1, 3) = sFechaInicio
                .TextMatrix(.Rows - 1, 4) = sFechaFin
            Loop
        End If
    End With
End Sub

Private Sub Form_Load()
    Call suInicializaForm
    Call suInicializaPromociones

    Me.chkPromNoVigentes.Value = 1
    Me.chkPromVigentes.Value = 1
    
    Call suLlenaPromociones
    
    Dim controlesForm As Control
    For Each controlesForm In Controls
      If (TypeOf controlesForm Is CommandButton) Then
          If controlesForm.Tag <> "" Then
              Call suEnable_Ctl1(Me.Tag, controlesForm)
          End If
      End If
    Next
    
End Sub

Private Sub grdPlanes_Click()
    Dim i As Long

    If grdPlanes.Row >= 1 Then
        For i = 0 To UBound(arrPlanes) - 1
            If grdPlanes.TextMatrix(grdPlanes.Row, 2) = arrCodiPlanes(i) Then
                cbxPlanes.ListIndex = i + 1
                Exit For
            End If
        Next i
    
        cbxPlanes.Enabled = False
    
        Me.txtPlanCantPerm.Text = grdPlanes.TextMatrix(grdPlanes.Row, 1)
        
        cmdPlanAgregar.Caption = "Modificar"
    End If
End Sub

Private Sub grdPromCobros_Click()
    Dim i As Long
    Dim tmp As Variant
    Dim Texto As String
    
    With grdPromCobros
        If .Row >= 1 Then
        
            For i = 0 To UBound(arrCodiRentas)
                If arrCodiRentas(i) = .TextMatrix(.Row, 9) Then
                    cbxCobros.ListIndex = i + 1
                    Exit For
                End If
            Next i
        
            For i = 1 To cbxCobrTipoCobro.ListCount - 1
                If Left(cbxCobrTipoCobro.List(i), 1) = .TextMatrix(.Row, 1) Then
                    cbxCobrTipoCobro.ListIndex = i
                    Exit For
                End If
            Next i
            
            For i = 0 To cbxCobrTipoOrigen.ListCount - 1
                If Left(cbxCobrTipoOrigen.List(i), 4) = .TextMatrix(.Row, 2) Then
                    cbxCobrTipoOrigen.ListIndex = i
                    Exit For
                End If
            Next i
            
            Select Case cbxCobrTipoOrigen.Text
                Case "EQUIPO"
                    If .TextMatrix(.Row, 3) <> "" Then
                        For i = 1 To cbxEquipo.ListCount - 1
                            Texto = cbxEquipo.List(i)
                            tmp = Split(Texto, "(")
                            If Replace(tmp(1), ")", "") = .TextMatrix(.Row, 3) Then
                                cbxCobrCodigoOrigen.ListIndex = i
                                Exit For
                            End If
                        Next i
                    End If
                Case "PLAN"
                    For i = 1 To cbxCobrCodigoOrigen.ListCount - 1
                        If .TextMatrix(.Row, 3) <> "" Then
                            Texto = cbxCobrCodigoOrigen.List(i)
                            tmp = Split(Texto, " -- ")
                            If Trim(tmp(0)) = .TextMatrix(.Row, 3) Then
                                cbxCobrCodigoOrigen.ListIndex = i
                                Exit For
                            End If
                        End If
                    Next i
                Case "CARGA INICIAL"
                Case "PRODUCTO"
                    For i = 1 To cbxCobrCodigoOrigen.ListCount - 1
                        If .TextMatrix(.Row, 3) <> "" Then
                            Texto = cbxCobrCodigoOrigen.List(i)
                            tmp = Split(Texto, " -- ")
                            If Trim(tmp(0)) = .TextMatrix(.Row, 3) Then
                                cbxCobrCodigoOrigen.ListIndex = i
                                Exit For
                            End If
                        End If
                    Next i
            End Select
        
        End If
        txtCobrCantduracion.Text = .TextMatrix(.Row, 5)
        txtCobrCantGracia.Text = .TextMatrix(.Row, 4)
        txtCobrMaxCuotas.Text = .TextMatrix(.Row, 7)
        txtCobrMinCuotas.Text = .TextMatrix(.Row, 6)
        txtCobrValorCobro.Text = .TextMatrix(.Row, 8)
        
        cbxCobros.Enabled = False
        cmdCobrosAgregar.Caption = "Modificar"
        Call suEnable_Ctl1(Me.Tag, cmdCobrosEliminar)
    End With
End Sub

Private Sub grdPromEquipos_Click()
    Dim i As Long
    Dim tmp As Variant
    Dim Texto As String

    With grdPromEquipos
        If .Row >= 1 Then
            For i = 1 To cbxEquipo.ListCount - 1
                Texto = cbxEquipo.List(i)
                tmp = Split(Texto, "(")
                If Replace(tmp(1), ")", "") = .TextMatrix(.Row, 3) Then
                    cbxEquipo.ListIndex = i
                    Exit For
                End If
            Next i
            txtProdCantPermanencia.Text = .TextMatrix(.Row, 2)
            
            For i = 1 To cbxProdTipoRelacion.ListCount - 1
                If Left(cbxProdTipoRelacion.List(i), 4) = .TextMatrix(.Row, 1) Then
                    cbxProdTipoRelacion.ListIndex = i
                    Exit For
                End If
            Next i
            
            If .TextMatrix(.Row, 4) <> "" Then
                dtPromEquiFechaIni.Value = .TextMatrix(.Row, 4)
            Else
                dtPromEquiFechaIni.Value = Format(Now, "dd-mm-yyyy")
            End If
            If .TextMatrix(.Row, 5) = "" Then
                dtPromEquiFechaFin.Enabled = False
                chkPromEquiSinFechaFin.Value = 1
            End If
            
        End If
        cmdProdAgregar.Caption = "Modificar"
        cbxEquipo.Enabled = False
    End With
    
End Sub

Private Sub grdPromLocalidades_Click()
    If grdPromLocalidades.Row >= 1 Then
        Call gsuMarcaFilaGrilla(grdPromLocalidades, 2)
    End If
End Sub

Private Sub grdPromociones_Click()
    
    With grdPromociones
        If .Row > 0 Then
            txtCodiPromocion.Text = .TextMatrix(.Row, 0)
            txtDescPromocion.Text = .TextMatrix(.Row, 1)
            dtFechaInicio.Value = .TextMatrix(.Row, 3)
            dtFechaFin.Value = IIf(.TextMatrix(.Row, 4) = "", Now, .TextMatrix(.Row, 4))
            If .TextMatrix(.Row, 4) <> "" Then
                chkSinFechaFin.Value = 0
            Else
                chkSinFechaFin.Value = 1
            End If
            
            Call suEnable_Ctl1(Me.Tag, cmdModificarPromocion)
            Call suEnable_Ctl1(Me.Tag, cmdEliminarPromocion)
            Call suEnable_Ctl1(Me.Tag, cmdCopiaPromocion)
            
            cmdNuevaPromocion.Enabled = False
            
            
            fraLocalidades.Enabled = True
            fraProductos.Enabled = True
            fraCobros.Enabled = True
            fraPlanes.Enabled = True
            
            'Me.dtFechaInicio.Enabled = False
            Call cmdCobrosLimpiar_Click
            Call cmdProdLimpiar_Click
            Call cmdPlanLimpiar_Click
            Call suTraeDatosPromocion
        End If
    End With
    
    
End Sub

Private Sub txtCobrCantduracion_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtCobrCantGracia_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtCobrMaxCuotas_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtCobrMinCuotas_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtCobrValorCobro_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtDescPromocion_KeyPress(KEYASCII As Integer)
    KEYASCII = Asc(UCase(Chr(KEYASCII)))
End Sub

Private Sub txtPlanCantPerm_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub

Private Sub txtProdCantPermanencia_KeyPress(KEYASCII As Integer)
    If KEYASCII <> 8 Then
        If KEYASCII < 48 Or KEYASCII > 57 Then
            KEYASCII = 0
        End If
    End If
End Sub
