VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmFrecuentes 
   BorderStyle     =   0  'None
   Caption         =   "N�meros Frecuentes Funcionarios"
   ClientHeight    =   7170
   ClientLeft      =   4215
   ClientTop       =   945
   ClientWidth     =   6105
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7170
   ScaleWidth      =   6105
   ShowInTaskbar   =   0   'False
   Tag             =   "FRMFRECUENTES"
   Begin VB.CommandButton Command1 
      Caption         =   "Salir"
      Height          =   855
      Left            =   4680
      TabIndex        =   7
      Top             =   6120
      Width           =   1095
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Aprovisionar Cambios"
      Height          =   495
      Left            =   3120
      TabIndex        =   6
      Tag             =   "FR_APROVISIONA"
      Top             =   6600
      Width           =   1215
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Agregar"
      Height          =   375
      Left            =   3120
      TabIndex        =   5
      Tag             =   "FR_AGREGAR"
      Top             =   6000
      Width           =   1215
   End
   Begin VB.TextBox txFono 
      Height          =   285
      Left            =   960
      TabIndex        =   3
      Top             =   6000
      Width           =   1695
   End
   Begin VB.CommandButton cmdEliminaFila 
      Caption         =   "Elimina Fila"
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Tag             =   "FR_ELIMINAR"
      Top             =   6600
      Width           =   1215
   End
   Begin VB.CommandButton cmdBajaFrecuente 
      Caption         =   "Dar de baja"
      Height          =   495
      Left            =   1680
      TabIndex        =   1
      Tag             =   "FR_BAJA"
      Top             =   6600
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid grdFrecuentes 
      Height          =   5055
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   8916
      _Version        =   393216
   End
   Begin VB.Label Label2 
      Caption         =   "NUMEROS FRECUENTES FUNCIONARIOS"
      Height          =   255
      Left            =   840
      TabIndex        =   8
      Top             =   360
      Width           =   3735
   End
   Begin VB.Label Label1 
      Caption         =   "Fono"
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   6000
      Width           =   495
   End
End
Attribute VB_Name = "frmFrecuentes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBajaFrecuente_Click()
Dim sResult As String

    If MsgBox("�Est� seguro de dar de Baja el n�mero frecuente : " & txFono.Text & "?" & vbCrLf & " Luego deber� aprovisionar este cambio.", vbYesNo) = vbYes Then
        
        If BajaFrecuentes(txFono.Text, Format(Date, "dd-mm-yyyy"), Gp_Username_Aplicacion, sResult) Then
            If Mid(sResult, 1, 2) <> "00" Then
                MsgBox Mid(sResult, 4), vbCritical, "REVISE"
                Exit Sub
            Else
                Call suInicializaForm
            End If
        End If
    
    End If
End Sub

Private Sub cmdEliminaFila_Click()
Dim sResult As String

    If MsgBox("�Est� seguro de eliminar el registro en base de datos asociado al fono : " & txFono.Text & "?" & vbCrLf & "Esto no podr� ser deshecho.", vbYesNo) = vbYes Then
        
        If DelFrecuentes(txFono.Text, sResult) Then
            If Mid(sResult, 1, 2) <> "00" Then
                MsgBox Mid(sResult, 4), vbCritical, "REVISE"
                Exit Sub
            Else
                Call suInicializaForm
            End If
        End If
    
    End If
End Sub



Private Sub Command1_Click()
Unload Me
End Sub

Private Sub Command3_Click()
Dim sResult As String

If txFono.Text <> "" Then
        If InsFrecuentes(txFono.Text, Format(Date, "dd-mm-yyyy"), Gp_Username_Aplicacion, sResult) Then
            If Mid(sResult, 1, 2) <> "00" Then
                MsgBox Mid(sResult, 4), vbCritical, "REVISE"
                Exit Sub
            Else
                Call suInicializaForm
            End If
        End If
End If
          
End Sub

Private Sub Form_Load()
    Call suInicializaForm
    
    Dim controlesForm As Control
    For Each controlesForm In Controls
      If (TypeOf controlesForm Is CommandButton) Then
          If controlesForm.Tag <> "" Then
              Call suEnable_Ctl1(Me.Tag, controlesForm)
          End If
      End If
    Next
    
End Sub

Private Sub suInicializaForm()
    
    With grdFrecuentes
        .Cols = 4
        .Rows = 2
        
        .TextMatrix(0, 0) = "FONO"
        .ColWidth(0) = 900
        .TextMatrix(0, 1) = "FECHA ALTA"
        .ColWidth(1) = 1200
        .TextMatrix(0, 2) = "FECHA BAJA"
        .ColWidth(2) = 1200
        .TextMatrix(0, 3) = "USUARIO"
        .ColWidth(3) = 1200
        .FixedCols = 0
        .FixedRows = 1
    End With
    txFono.Text = ""
    Call suLlenaFrecuentes
    
End Sub

Private Sub suLlenaFrecuentes()
    Dim sFonoServicio As String, sFechaAlta As String, sFechaBaja As String, sUsuario As String
    
    Dim i As Long
    
    grdFrecuentes.Rows = 1
    
    If GetFrecuentes() > 0 Then
        With grdFrecuentes
            i = 1
            Do While GetFrecuentesRead(sFonoServicio, sFechaAlta, sFechaBaja, sUsuario)
                
                .Rows = .Rows + 1
                .TextMatrix(i, 0) = sFonoServicio
                .TextMatrix(i, 1) = sFechaAlta
                .TextMatrix(i, 2) = sFechaBaja
                .TextMatrix(i, 3) = sUsuario
                .Row = i
                i = i + 1
            Loop
        End With
        
        Call GetFrecuentesClose
    End If
    
End Sub

Private Sub grdFrecuentes_Click()
 Dim i As Long

   
    Call suEnable_Ctl1(Me.Tag, cmdEliminaFila)
       
    With grdFrecuentes
        txFono.Text = .TextMatrix(.Row, 0)
    End With
End Sub
